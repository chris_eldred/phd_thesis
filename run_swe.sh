#!/bin/bash

export OMP_NUM_THREADS=$2
export OMP_STACKSIZE=8g
ulimit -s unlimited

python -m cProfile -o swe.dat src/swe.py $1
#
gprof2dot -f pstats -n 1 swe.dat | dot -Tpdf -o swe.pdf
