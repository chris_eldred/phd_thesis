#!/bin/bash

actualtempname=${12}.$2.$3.$1.$5.$7

if [ ${11} = 'False' ]
then 
FINALDIR='none'
fi
if [ ${11} = 'True' ]
then 
if [ ${14} = 'hyperviscosity' ]
then
FINALDIR='hyper'
fi
if [ ${14} = 'APVM' ]
then
FINALDIR='apvm'
fi
fi

mkdir -p /pool/eldred/output/${7}/$2/$3/$1-$5/${FINALDIR}
mv /Users/celdred/output/${7}/$2/$3/$1-$5/${FINALDIR}/* /pool/eldred/output/${7}/$2/$3/$1-$5/${FINALDIR}
chmod -R 777 /pool/eldred/output/${7}
