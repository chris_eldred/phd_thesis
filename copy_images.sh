#!/bin/bash


#Meshes
cp meshplots/cs.thuburn.v=4.primal.png img_link/meshes/cs4-primal.png
cp meshplots/cs.thuburn.v=4.dual.png img_link/meshes/cs4-dual.png
cp meshplots/geo.tweaked.v=4.primal.png img_link/meshes/geo4-primal.png
cp meshplots/geo.tweaked.v=4.dual.png img_link/meshes/geo4-dual.png

#Order of Accuracy

cp orderofaccuracy/Ld0.l2.png img_link/accuracy/Lp-l2.png
cp orderofaccuracy/Ld0.linf.png img_link/accuracy/Lp-linf.png
cp orderofaccuracy/Lp0.l2.png img_link/accuracy/Ld-l2.png
cp orderofaccuracy/Lp0.linf.png img_link/accuracy/Ld-linf.png
cp orderofaccuracy/Le.l2.png img_link/accuracy/Le-l2.png
cp orderofaccuracy/Le.linf.png img_link/accuracy/Le-linf.png

cp orderofaccuracy/R.l2.png img_link/accuracy/R-l2.png
cp orderofaccuracy/R.linf.png img_link/accuracy/R-linf.png
cp orderofaccuracy/Phi.l2.png img_link/accuracy/Phi-l2.png
cp orderofaccuracy/Phi.linf.png img_link/accuracy/Phi-linf.png

cp orderofaccuracy/jacobianZ.l2.png img_link/accuracy/jacobianZ-l2.png
cp orderofaccuracy/jacobianZ.linf.png img_link/accuracy/jacobianZ-linf.png
cp orderofaccuracy/fluxdivZ.l2.png img_link/accuracy/fluxdivZ-l2.png
cp orderofaccuracy/fluxdivZ.linf.png img_link/accuracy/fluxdivZ-linf.png
cp orderofaccuracy/laplacZ.l2.png img_link/accuracy/laplacZ-l2.png
cp orderofaccuracy/laplacZ.linf.png img_link/accuracy/laplacZ-linf.png

cp orderofaccuracy/Wdiv.l2.png img_link/accuracy/Wdiv-l2.png
cp orderofaccuracy/Wdiv.linf.png img_link/accuracy/Wdiv-linf.png
cp orderofaccuracy/Wrot.l2.png img_link/accuracy/Wrot-l2.png
cp orderofaccuracy/Wrot.linf.png img_link/accuracy/Wrot-linf.png


cp orderofaccuracy/schemeTC2-Q.h.l2.png img_link/accuracy/TC2-Q-h-l2.png
cp orderofaccuracy/schemeTC2-Q.h.linf.png img_link/accuracy/TC2-Q-h-linf.png
cp orderofaccuracy/schemeTC2-Q.u.l2.png img_link/accuracy/TC2-Q-u-l2.png
cp orderofaccuracy/schemeTC2-Q.u.linf.png img_link/accuracy/TC2-Q-u-linf.png

cp orderofaccuracy/schemeTC2-TE.h.l2.png img_link/accuracy/TC2-TE-h-l2.png
cp orderofaccuracy/schemeTC2-TE.h.linf.png img_link/accuracy/TC2-TE-h-linf.png
cp orderofaccuracy/schemeTC2-TE.u.l2.png img_link/accuracy/TC2-TE-u-l2.png
cp orderofaccuracy/schemeTC2-TE.u.linf.png img_link/accuracy/TC2-TE-u-linf.png

cp orderofaccuracy/schemeTC2-TEQ.h.l2.png img_link/accuracy/TC2-TEQ-h-l2.png
cp orderofaccuracy/schemeTC2-TEQ.h.linf.png img_link/accuracy/TC2-TEQ-h-linf.png
cp orderofaccuracy/schemeTC2-TEQ.u.l2.png img_link/accuracy/TC2-TEQ-u-l2.png
cp orderofaccuracy/schemeTC2-TEQ.u.linf.png img_link/accuracy/TC2-TEQ-u-linf.png

cp orderofaccuracy/schemeTC2Z-TEQ.h.l2.png img_link/accuracy/TC2Z-h-l2.png
cp orderofaccuracy/schemeTC2Z-TEQ.h.linf.png img_link/accuracy/TC2Z-h-linf.png
cp orderofaccuracy/schemeTC2Z-TEQ.zeta.l2.png img_link/accuracy/TC2Z-zeta-l2.png
cp orderofaccuracy/schemeTC2Z-TEQ.zeta.linf.png img_link/accuracy/TC2Z-zeta-linf.png
cp orderofaccuracy/schemeTC2Z-TEQ.delta.l2.png img_link/accuracy/TC2Z-delta-l2.png
cp orderofaccuracy/schemeTC2Z-TEQ.delta.linf.png img_link/accuracy/TC2Z-delta-linf.png

#TC2
cp output/TC2/plots/TE.TC2.geodesic.tweaked.8.none.png img_link/testcases/TC2-TE-geo.png
cp output/TC2/plots/Q.TC2.geodesic.tweaked.8.none.png img_link/testcases/TC2-Q-geo.png

cp output/TC2/plots/TE.TC2.cubedsphere.thuburn.8.none.png img_link/testcases/TC2-TE-cs.png
cp output/TC2/plots/Q.TC2.cubedsphere.thuburn.8.none.png img_link/testcases/TC2-Q-cs.png

cp output/TC2Z/plots/TE.TC2Z.geodesic.tweaked.8.none.png img_link/testcases/TC2Z-TE-geo.png
cp output/TC2Z/plots/Q.TC2Z.geodesic.tweaked.8.none.png img_link/testcases/TC2Z-Q-geo.png

#TC5
cp output/TC5/plots/qv.TC5.TEQ.geodesic.tweaked.6.none.120.spherical.contour.png img_link/testcases/TC5-TEQ-geo-30.png
cp output/TC5/plots/qv.TC5.TE.geodesic.tweaked.6.none.120.spherical.contour.png img_link/testcases/TC5-TE-geo-30.png
cp output/TC5/plots/qv.TC5.Q.geodesic.tweaked.6.none.120.spherical.contour.png img_link/testcases/TC5-Q-geo-30.png

cp output/TC5/plots/qv.TC5.TEQ.cubedsphere.thuburn.6.none.120.spherical.contour.png img_link/testcases/TC5-TEQ-cs-30.png
cp output/TC5/plots/qv.TC5.TE.cubedsphere.thuburn.6.none.120.spherical.contour.png img_link/testcases/TC5-TE-cs-30.png
cp output/TC5/plots/qv.TC5.Q.cubedsphere.thuburn.6.none.120.spherical.contour.png img_link/testcases/TC5-Q-cs-30.png

cp output/TC5Z/plots/q.TC5Z.TEQ.geodesic.tweaked.6.none.120.spherical.contour.png img_link/testcases/TC5Z-TEQ-geo-30.png

#Galewsky
cp output/Galewsky/plots/etav.Galewsky.TEQ.geodesic.tweaked.8.none.24.latlon.contour.png img_link/testcases/Galewsky-TEQ-geo-24.png
cp output/Galewsky/plots/etav.Galewsky.TE.geodesic.tweaked.8.none.24.latlon.contour.png img_link/testcases/Galewsky-TE-geo-24.png
cp output/Galewsky/plots/etav.Galewsky.Q.geodesic.tweaked.8.none.24.latlon.contour.png img_link/testcases/Galewsky-Q-geo-24.png

cp output/Galewsky/plots/etav.Galewsky.TEQ.cubedsphere.thuburn.8.none.24.latlon.contour.png img_link/testcases/Galewsky-TEQ-cs-24.png
cp output/Galewsky/plots/etav.Galewsky.TE.cubedsphere.thuburn.8.none.24.latlon.contour.png img_link/testcases/Galewsky-TE-cs-24.png
cp output/Galewsky/plots/etav.Galewsky.Q.cubedsphere.thuburn.8.none.24.latlon.contour.png img_link/testcases/Galewsky-Q-cs-24.png

cp output/GalewskyZ/plots/eta.GalewskyZ.TEQ.geodesic.tweaked.8.none.24.latlon.contour.png img_link/testcases/GalewskyZ-TEQ-geo-24.png

cp output/Galewsky/plots/TE.Galewsky.geodesic.tweaked.8.none.png img_link/testcases/Galewsky-TE-geo.png
cp output/Galewsky/plots/Q.Galewsky.geodesic.tweaked.8.none.png img_link/testcases/Galewsky-Q-geo.png

cp output/Galewsky/plots/TE.Galewsky.cubedsphere.thuburn.8.none.png img_link/testcases/Galewsky-TE-cs.png
cp output/Galewsky/plots/Q.Galewsky.cubedsphere.thuburn.8.none.png img_link/testcases/Galewsky-Q-cs.png

cp output/GalewskyZ/plots/TE.GalewskyZ.geodesic.tweaked.8.none.png img_link/testcases/GalewskyZ-TE-geo.png
cp output/GalewskyZ/plots/Q.GalewskyZ.geodesic.tweaked.8.none.png img_link/testcases/GalewskyZ-Q-geo.png

#RHWave

cp output/RHwave/plots/etav.RHwave.TEQ.geodesic.tweaked.7.none.80.latlon.contour.png img_link/testcases/RHwave-TEQ-geo-20.png
cp output/RHwave/plots/etav.RHwave.TEQ.geodesic.tweaked.7.none.120.latlon.contour.png img_link/testcases/RHwave-TEQ-geo-30.png
cp output/RHwave/plots/etav.RHwave.TEQ.geodesic.tweaked.7.none.200.latlon.contour.png img_link/testcases/RHwave-TEQ-geo-50.png
cp output/RHwave/plots/etav.RHwave.TE.geodesic.tweaked.7.none.80.latlon.contour.png img_link/testcases/RHwave-TE-geo-20.png
cp output/RHwave/plots/etav.RHwave.TE.geodesic.tweaked.7.none.120.latlon.contour.png img_link/testcases/RHwave-TE-geo-30.png
cp output/RHwave/plots/etav.RHwave.TE.geodesic.tweaked.7.none.200.latlon.contour.png img_link/testcases/RHwave-TE-geo-50.png
cp output/RHwave/plots/etav.RHwave.Q.geodesic.tweaked.7.none.80.latlon.contour.png img_link/testcases/RHwave-Q-geo-20.png
cp output/RHwave/plots/etav.RHwave.Q.geodesic.tweaked.7.none.120.latlon.contour.png img_link/testcases/RHwave-Q-geo-30.png
cp output/RHwave/plots/etav.RHwave.Q.geodesic.tweaked.7.none.200.latlon.contour.png img_link/testcases/RHwave-Q-geo-50.png

cp output/RHwave/plots/etav.RHwave.TEQ.cubedsphere.thuburn.7.none.80.latlon.contour.png img_link/testcases/RHwave-TEQ-cs-20.png
cp output/RHwave/plots/etav.RHwave.TEQ.cubedsphere.thuburn.7.none.120.latlon.contour.png img_link/testcases/RHwave-TEQ-cs-30.png
cp output/RHwave/plots/etav.RHwave.TEQ.cubedsphere.thuburn.7.none.200.latlon.contour.png img_link/testcases/RHwave-TEQ-cs-50.png
cp output/RHwave/plots/etav.RHwave.TE.cubedsphere.thuburn.7.none.80.latlon.contour.png img_link/testcases/RHwave-TE-cs-20.png
cp output/RHwave/plots/etav.RHwave.TE.cubedsphere.thuburn.7.none.120.latlon.contour.png img_link/testcases/RHwave-TE-cs-30.png
cp output/RHwave/plots/etav.RHwave.TE.cubedsphere.thuburn.7.none.200.latlon.contour.png img_link/testcases/RHwave-TE-cs-50.png
cp output/RHwave/plots/etav.RHwave.Q.cubedsphere.thuburn.7.none.80.latlon.contour.png img_link/testcases/RHwave-Q-cs-20.png
cp output/RHwave/plots/etav.RHwave.Q.cubedsphere.thuburn.7.none.120.latlon.contour.png img_link/testcases/RHwave-Q-cs-30.png
cp output/RHwave/plots/etav.RHwave.Q.cubedsphere.thuburn.7.none.200.latlon.contour.png img_link/testcases/RHwave-Q-cs-50.png

cp output/RHwaveZ/plots/eta.RHwaveZ.TEQ.geodesic.tweaked.7.none.80.latlon.contour.png img_link/testcases/RHwaveZ-TEQ-geo-20.png
cp output/RHwaveZ/plots/eta.RHwaveZ.TEQ.geodesic.tweaked.7.none.120.latlon.contour.png img_link/testcases/RHwaveZ-TEQ-geo-30.png
cp output/RHwaveZ/plots/eta.RHwaveZ.TEQ.geodesic.tweaked.7.none.200.latlon.contour.png img_link/testcases/RHwaveZ-TEQ-geo-50.png

#Galewsky Init
cp output/GalewskyInit/plots/di.GalewskyInit.TEQ.geodesic.tweaked.8.none.3.latlon.contour.png img_link/testcases/GalewskyInit-TEQ-geo.png
cp output/GalewskyInit/plots/di.GalewskyInit.TE.geodesic.tweaked.8.none.3.latlon.contour.png img_link/testcases/GalewskyInit-TE-geo.png
cp output/GalewskyInit/plots/di.GalewskyInit.Q.geodesic.tweaked.8.none.3.latlon.contour.png img_link/testcases/GalewskyInit-Q-geo.png

cp output/GalewskyInit/plots/di.GalewskyInit.TEQ.cubedsphere.thuburn.8.none.3.latlon.contour.png img_link/testcases/GalewskyInit-TEQ-cs.png
cp output/GalewskyInit/plots/di.GalewskyInit.TE.cubedsphere.thuburn.8.none.3.latlon.contour.png img_link/testcases/GalewskyInit-TE-cs.png
cp output/GalewskyInit/plots/di.GalewskyInit.Q.cubedsphere.thuburn.8.none.3.latlon.contour.png img_link/testcases/GalewskyInit-Q-cs.png

cp output/GalewskyInitZ/plots/delta.GalewskyInitZ.TEQ.geodesic.tweaked.8.none.3.latlon.contour.png img_link/testcases/GalewskyInitZ-TEQ-geo.png

#Galewsky No Perturb
cp output/GalewskyNoPerturb/plots/etav.GalewskyNoPerturb.TEQ.geodesic.tweaked.8.none.24.latlon.contour.png img_link/testcases/GalewskyNoPerturb-TEQ-geo-24.png
cp output/GalewskyNoPerturb/plots/etav.GalewskyNoPerturb.TE.geodesic.tweaked.8.none.24.latlon.contour.png img_link/testcases/GalewskyNoPerturb-TE-geo-24.png
cp output/GalewskyNoPerturb/plots/etav.GalewskyNoPerturb.Q.geodesic.tweaked.8.none.24.latlon.contour.png img_link/testcases/GalewskyNoPerturb-Q-geo-24.png

cp output/GalewskyNoPerturb/plots/etav.GalewskyNoPerturb.TEQ.cubedsphere.thuburn.8.none.24.latlon.contour.png img_link/testcases/GalewskyNoPerturb-TEQ-cs-24.png
cp output/GalewskyNoPerturb/plots/etav.GalewskyNoPerturb.TE.cubedsphere.thuburn.8.none.24.latlon.contour.png img_link/testcases/GalewskyNoPerturb-TE-cs-24.png
cp output/GalewskyNoPerturb/plots/etav.GalewskyNoPerturb.Q.cubedsphere.thuburn.8.none.24.latlon.contour.png img_link/testcases/GalewskyNoPerturb-Q-cs-24.png

cp output/GalewskyNoPerturbZ/plots/eta.GalewskyNoPerturbZ.TEQ.geodesic.tweaked.8.none.24.latlon.contour.png img_link/testcases/GalewskyNoPerturbZ-TEQ-geo-24.png

#Held Suarez
cp output/heldsuarez/plots/hi.zonalstd.geodesic.tweaked.6.none.png img_link/testcases/hi-zonalstd-geo.png
cp output/heldsuarez/plots/hi.zonalmean.geodesic.tweaked.6.none.png img_link/testcases/hi-zonalmean-geo.png
cp output/heldsuarez/plots/zetav.zonalstd.geodesic.tweaked.6.none.png img_link/testcases/zetav-zonalstd-geo.png
cp output/heldsuarez/plots/zetav.zonalmean.geodesic.tweaked.6.none.png img_link/testcases/zetav-zonalmean-geo.png
cp output/heldsuarez/plots/di.zonalstd.geodesic.tweaked.6.none.png img_link/testcases/di-zonalstd-geo.png
cp output/heldsuarez/plots/di.zonalmean.geodesic.tweaked.6.none.png img_link/testcases/di-zonalmean-geo.png

cp output/heldsuarez/plots/hi.zonalstd.cubedsphere.thuburn.6.none.png img_link/testcases/hi-zonalstd-cs.png
cp output/heldsuarez/plots/hi.zonalmean.cubedsphere.thuburn.6.none.png img_link/testcases/hi-zonalmean-cs.png
cp output/heldsuarez/plots/zetav.zonalstd.cubedsphere.thuburn.6.none.png img_link/testcases/zetav-zonalstd-cs.png
cp output/heldsuarez/plots/zetav.zonalmean.cubedsphere.thuburn.6.none.png img_link/testcases/zetav-zonalmean-cs.png
cp output/heldsuarez/plots/di.zonalstd.cubedsphere.thuburn.6.none.png img_link/testcases/di-zonalstd-cs.png
cp output/heldsuarez/plots/di.zonalmean.cubedsphere.thuburn.6.none.png img_link/testcases/di-zonalmean-cs.png

cp output/heldsuarez/plots/hi.std.TEQ.cubedsphere.thuburn.6.none.latlon.contour.png img_link/testcases/hi-std-teq-cs.png
cp output/heldsuarez/plots/hi.mean.TEQ.cubedsphere.thuburn.6.none.latlon.contour.png img_link/testcases/hi-mean-teq-cs.png
cp output/heldsuarez/plots/hi.std.TE.cubedsphere.thuburn.6.none.latlon.contour.png img_link/testcases/hi-std-te-cs.png
cp output/heldsuarez/plots/hi.mean.TE.cubedsphere.thuburn.6.none.latlon.contour.png img_link/testcases/hi-mean-te-cs.png
cp output/heldsuarez/plots/hi.std.Q.cubedsphere.thuburn.6.none.latlon.contour.png img_link/testcases/hi-std-q-cs.png
cp output/heldsuarez/plots/hi.mean.Q.cubedsphere.thuburn.6.none.latlon.contour.png img_link/testcases/hi-mean-q-cs.png

cp output/heldsuarez/plots/hi.std.TEQ.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/hi-std-teq-geo.png
cp output/heldsuarez/plots/hi.mean.TEQ.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/hi-mean-teq-geo.png
cp output/heldsuarez/plots/hi.std.TE.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/hi-std-te-geo.png
cp output/heldsuarez/plots/hi.mean.TE.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/hi-mean-te-geo.png
cp output/heldsuarez/plots/hi.std.Q.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/hi-std-q-geo.png
cp output/heldsuarez/plots/hi.mean.Q.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/hi-mean-q-geo.png

cp output/heldsuarez/plots/zetav.std.TEQ.cubedsphere.thuburn.6.none.latlon.contour.png img_link/testcases/zetav-std-teq-cs.png
cp output/heldsuarez/plots/zetav.mean.TEQ.cubedsphere.thuburn.6.none.latlon.contour.png img_link/testcases/zetav-mean-teq-cs.png
cp output/heldsuarez/plots/zetav.std.TE.cubedsphere.thuburn.6.none.latlon.contour.png img_link/testcases/zetav-std-te-cs.png
cp output/heldsuarez/plots/zetav.mean.TE.cubedsphere.thuburn.6.none.latlon.contour.png img_link/testcases/zetav-mean-te-cs.png
cp output/heldsuarez/plots/zetav.std.Q.cubedsphere.thuburn.6.none.latlon.contour.png img_link/testcases/zetav-std-q-cs.png
cp output/heldsuarez/plots/zetav.mean.Q.cubedsphere.thuburn.6.none.latlon.contour.png img_link/testcases/zetav-mean-q-cs.png

cp output/heldsuarez/plots/zetav.std.TEQ.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/zetav-std-teq-geo.png
cp output/heldsuarez/plots/zetav.mean.TEQ.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/zetav-mean-teq-geo.png
cp output/heldsuarez/plots/zetav.std.TE.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/zetav-std-te-geo.png
cp output/heldsuarez/plots/zetav.mean.TE.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/zetav-mean-te-geo.png
cp output/heldsuarez/plots/zetav.std.Q.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/zetav-std-q-geo.png
cp output/heldsuarez/plots/zetav.mean.Q.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/zetav-mean-q-geo.png

cp output/heldsuarez/plots/di.std.TEQ.cubedsphere.thuburn.6.none.latlon.contour.png img_link/testcases/di-std-teq-cs.png
cp output/heldsuarez/plots/di.mean.TEQ.cubedsphere.thuburn.6.none.latlon.contour.png img_link/testcases/di-mean-teq-cs.png
cp output/heldsuarez/plots/di.std.TE.cubedsphere.thuburn.6.none.latlon.contour.png img_link/testcases/di-std-te-cs.png
cp output/heldsuarez/plots/di.mean.TE.cubedsphere.thuburn.6.none.latlon.contour.png img_link/testcases/di-mean-te-cs.png
cp output/heldsuarez/plots/di.std.Q.cubedsphere.thuburn.6.none.latlon.contour.png img_link/testcases/di-std-q-cs.png
cp output/heldsuarez/plots/di.mean.Q.cubedsphere.thuburn.6.none.latlon.contour.png img_link/testcases/di-mean-q-cs.png

cp output/heldsuarez/plots/di.std.TEQ.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/di-std-teq-geo.png
cp output/heldsuarez/plots/di.mean.TEQ.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/di-mean-teq-geo.png
cp output/heldsuarez/plots/di.std.TE.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/di-std-te-geo.png
cp output/heldsuarez/plots/di.mean.TE.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/di-mean-te-geo.png
cp output/heldsuarez/plots/di.std.Q.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/di-std-q-geo.png
cp output/heldsuarez/plots/di.mean.Q.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/di-mean-q-geo.png

cp output/heldsuarezZ/plots/delta.std.TEQ.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/delta-std-teq-geo.png
cp output/heldsuarezZ/plots/delta.mean.TEQ.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/delta-mean-teq-geo.png
cp output/heldsuarezZ/plots/zeta.std.TEQ.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/zeta-std-teq-geo.png
cp output/heldsuarezZ/plots/zeta.mean.TEQ.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/zeta-mean-teq-geo.png
cp output/heldsuarezZ/plots/hi.std.TEQ.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/height-std-teq-geo.png
cp output/heldsuarezZ/plots/hi.mean.TEQ.geodesic.tweaked.6.none.latlon.contour.png img_link/testcases/height-mean-teq-geo.png

cp output/heldsuarezZ/plots/hi.zonalstd.geodesic.tweaked.6.none.png img_link/testcases/height-zonalstd-geo.png
cp output/heldsuarezZ/plots/hi.zonalmean.geodesic.tweaked.6.none.png img_link/testcases/height-zonalmean-geo.png
cp output/heldsuarezZ/plots/zeta.zonalstd.geodesic.tweaked.6.none.png img_link/testcases/zeta-zonalstd-geo.png
cp output/heldsuarezZ/plots/zeta.zonalmean.geodesic.tweaked.6.none.png img_link/testcases/zeta-zonalmean-geo.png
cp output/heldsuarezZ/plots/delta.zonalstd.geodesic.tweaked.6.none.png img_link/testcases/delta-zonalstd-geo.png
cp output/heldsuarezZ/plots/delta.zonalmean.geodesic.tweaked.6.none.png img_link/testcases/delta-zonalmean-geo.png

cp hi.png img_link/testcases/hi.png
cp psi.png img_link/testcases/psi.png
cp ue.png img_link/testcases/ue.png
cp zeta.png img_link/testcases/zeta.png

#Planar Dispersion Relationships
cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/Scont-none/resolved/freq0.cont.S.tricontour.resolved.real.png img_link/linear/Scont-none-igw-real.png
cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/Scont-hyper/resolved/freq0.cont.S.tricontour.resolved.real.png img_link/linear/Scont-hyper-igw-real.png
cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/Scont-hyper/resolved/freq0.cont.S.tricontour.resolved.imag.png img_link/linear/Scont-hyper-igw-imag.png
#cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/Scont-hyper/resolved/freq2.cont.S.tricontour.resolved.imag.png img_link/linear/Scont-hyper-geo-imag.png DONT NEED THIS!

cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/SZ-none/resolved/freq0.Z.S.tricontour.resolved.real.png img_link/linear/SZ-none-igw-real.png
cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/SZ-hyper/resolved/freq0.Z.S.tricontour.resolved.real.png img_link/linear/SZ-hyper-igw-real.png
cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/SZ-hyper/resolved/freq0.Z.S.tricontour.resolved.imag.png img_link/linear/SZ-hyper-igw-imag.png

cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/SC-none/resolved/freq0.C.S.tricontour.resolved.real.png img_link/linear/SC-none-igw-real.png
cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/SC-hyper/resolved/freq0.C.S.tricontour.resolved.real.png img_link/linear/SC-hyper-igw-real.png
cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/SC-hyper/resolved/freq0.C.S.tricontour.resolved.imag.png img_link/linear/SC-hyper-igw-imag.png

cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/Hcont-none/resolved/freq0.cont.H.tricontour.resolved.real.png img_link/linear/Hcont-none-igw-real.png
cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/Hcont-hyper/resolved/freq0.cont.H.tricontour.resolved.real.png img_link/linear/Hcont-hyper-igw-real.png
cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/Hcont-hyper/resolved/freq0.cont.H.tricontour.resolved.imag.png img_link/linear/Hcont-hyper-igw-imag.png
#cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/Hcont-hyper/resolved/freq2.cont.H.tricontour.resolved.imag.png img_link/linear/Hcont-hyper-geo-imag.png

cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/HZ-none/resolved/freq0.Z.H.tricontour.resolved.real.png img_link/linear/HZ-none-igw-real.png
cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/HZ-hyper/resolved/freq0.Z.H.tricontour.resolved.real.png img_link/linear/HZ-hyper-igw-real.png
cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/HZ-hyper/resolved/freq0.Z.H.tricontour.resolved.imag.png img_link/linear/HZ-hyper-igw-imag.png

cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/HC-none/resolved/freq2.C.H.tricontour.resolved.real.png img_link/linear/HC-none-igw-real.png
cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/HC-hyper/resolved/freq0.C.H.tricontour.resolved.real.png img_link/linear/HC-hyper-igw-real.png
cp /home/user/Desktop/NTM/SCIPY/analyticdispersion/HC-hyper/resolved/freq0.C.H.tricontour.resolved.imag.png img_link/linear/HC-hyper-igw-imag.png

#Quasi-Uniform Dispersion Relations
cp output/fsphere/plots/none/geodesic.tweaked.2.TEQ.none.omega.real.pos.png img_link/linear/geo-fsphere-none-real.png
cp output/fsphere/plots/none/cubedsphere.thuburn.2.TEQ.none.omega.real.pos.png img_link/linear/cs-fsphere-none-real.png
cp output/fsphereZ/plots/none/geodesic.tweaked.2.TEQ.none.omega.real.pos.png img_link/linear/geoZ-fsphere-none-real.png

cp output/fsphere/plots/hyper/geodesic.tweaked.2.TEQ.hyper.omega.real.pos.png img_link/linear/geo-fsphere-hyper-real.png
cp output/fsphere/plots/hyper/cubedsphere.thuburn.2.TEQ.hyper.omega.real.pos.png img_link/linear/cs-fsphere-hyper-real.png
cp output/fsphereZ/plots/hyper/geodesic.tweaked.2.TEQ.hyper.omega.real.pos.png img_link/linear/geoZ-fsphere-hyper-real.png
cp output/fsphere/plots/hyper/geodesic.tweaked.2.TEQ.hyper.omega.imag.pos.png img_link/linear/geo-fsphere-hyper-imag.png
cp output/fsphere/plots/hyper/cubedsphere.thuburn.2.TEQ.hyper.omega.imag.pos.png img_link/linear/cs-fsphere-hyper-imag.png
cp output/fsphereZ/plots/hyper/geodesic.tweaked.2.TEQ.hyper.omega.imag.pos.png img_link/linear/geoZ-fsphere-hyper-imag.png

cp output/fullsphere/plots/none/geodesic.tweaked.2.TEQ.none.omega.real.pos.png img_link/linear/geo-fullsphere-none-real.png
cp output/fullsphere/plots/none/cubedsphere.thuburn.2.TEQ.none.omega.real.pos.png img_link/linear/cs-fullsphere-none-real.png
cp output/fullsphere/plots/none/geodesic.tweaked.2.TEQ.none.omega.real.pos.png img_link/linear/geoZ-fullsphere-none-real.png

cp output/fullsphere/plots/hyper/geodesic.tweaked.2.TEQ.hyper.omega.real.pos.png img_link/linear/geo-fullsphere-hyper-real.png
cp output/fullsphere/plots/hyper/cubedsphere.thuburn.2.TEQ.hyper.omega.real.pos.png img_link/linear/cs-fullsphere-hyper-real.png
cp output/fullsphereZ/plots/hyper/geodesic.tweaked.2.TEQ.hyper.omega.real.pos.png img_link/linear/geoZ-fullsphere-hyper-real.png
cp output/fullsphere/plots/hyper/geodesic.tweaked.2.TEQ.hyper.omega.imag.pos.png img_link/linear/geo-fullsphere-hyper-imag.png
cp output/fullsphere/plots/hyper/cubedsphere.thuburn.2.TEQ.hyper.omega.imag.pos.png img_link/linear/cs-fullsphere-hyper-imag.png
cp output/fullsphereZ/plots/hyper/geodesic.tweaked.2.TEQ.hyper.omega.imag.pos.png img_link/linear/geoZ-fullsphere-hyper-imag.png

#Quasi-Uniform Mode Structures- Stationary
cp output/fullsphere/geodesic/tweaked/2-TEQ/none/rossby/real/hi/hi.0.grid.png img_link/linear/hi-none-stat-zero-geo.png
cp output/fullsphere/geodesic/tweaked/2-TEQ/none/rossby/real/zeta/zeta.0.grid.png img_link/linear/zeta-none-stat-zero-geo.png

cp output/fullsphere/geodesic/tweaked/2-TEQ/none/rossby/real/hi/hi.4.grid.png img_link/linear/hi-none-stat-almostzero-geo.png
cp output/fullsphere/geodesic/tweaked/2-TEQ/none/rossby/real/zeta/zeta.4.grid.png img_link/linear/zeta-none-stat-almostzero-geo.png

cp output/fullsphere/cubedsphere/thuburn/2-TEQ/none/rossby/real/hi/hi.0.grid.png img_link/linear/hi-none-stat-zero-cs.png
cp output/fullsphere/cubedsphere/thuburn/2-TEQ/none/rossby/real/zeta/zeta.0.grid.png img_link/linear/zeta-none-stat-zero-cs.png

cp output/fullsphere/cubedsphere/thuburn/2-TEQ/none/rossby/real/hi/hi.4.grid.png img_link/linear/hi-none-stat-almostzero-cs.png
cp output/fullsphere/cubedsphere/thuburn/2-TEQ/none/rossby/real/zeta/zeta.4.grid.png img_link/linear/zeta-none-stat-almostzero-cs.png

cp output/fullsphereZ/geodesic/tweaked/2-TEQ/none/rossby/real/hi/hi.0.grid.png img_link/linear/hi-none-stat-zero-geoZ.png
cp output/fullsphereZ/geodesic/tweaked/2-TEQ/none/rossby/real/zeta/zeta.0.grid.png img_link/linear/zeta-none-stat-zero-geoZ.png

cp output/fullsphereZ/geodesic/tweaked/2-TEQ/none/rossby/real/hi/hi.4.grid.png img_link/linear/hi-none-stat-almostzero-geoZ.png
cp output/fullsphereZ/geodesic/tweaked/2-TEQ/none/rossby/real/zeta/zeta.4.grid.png img_link/linear/zeta-none-stat-almostzero-geoZ.png

#Quasi-Uniform Mode Structures- Propagating
cp output/fullsphere/geodesic/tweaked/2-TEQ/none/rossby/real/hi/hi.159.grid.png img_link/linear/hi-none-rossby-geo.png
cp output/fullsphere/geodesic/tweaked/2-TEQ/none/rossby/real/zeta/zeta.159.grid.png img_link/linear/zeta-none-rossby-geo.png

cp output/fullsphere/cubedsphere/thuburn/2-TEQ/none/rossby/real/hi/hi.108.grid.png img_link/linear/hi-none-rossby-cs.png
cp output/fullsphere/cubedsphere/thuburn/2-TEQ/none/rossby/real/zeta/zeta.108.grid.png img_link/linear/zeta-none-rossby-cs.png

cp output/fullsphereZ/geodesic/tweaked/2-TEQ/none/rossby/real/hi/hi.80.grid.png img_link/linear/hi-none-rossby-geoZ.png
cp output/fullsphereZ/geodesic/tweaked/2-TEQ/none/rossby/real/zeta/zeta.80.grid.png img_link/linear/zeta-none-rossby-geoZ.png

cp output/fullsphere/geodesic/tweaked/2-TEQ/none/igw/real/hi/hi.160.grid.png img_link/linear/hi-none-igw-fullsphere-geo.png
cp output/fullsphere/geodesic/tweaked/2-TEQ/none/igw/real/zeta/zeta.160.grid.png img_link/linear/zeta-none-igw-fullsphere-geo.png

cp output/fullsphere/cubedsphere/thuburn/2-TEQ/none/igw/real/hi/hi.214.grid.png img_link/linear/hi-none-igw-fullsphere-cs.png
cp output/fullsphere/cubedsphere/thuburn/2-TEQ/none/igw/real/zeta/zeta.214.grid.png img_link/linear/zeta-none-igw-fullsphere-cs.png

cp output/fullsphereZ/geodesic/tweaked/2-TEQ/none/igw/real/hi/hi.161.grid.png img_link/linear/hi-none-igw-fullsphere-geoZ.png
cp output/fullsphereZ/geodesic/tweaked/2-TEQ/none/igw/real/zeta/zeta.161.grid.png img_link/linear/zeta-none-igw-fullsphere-geoZ.png

cp output/fsphere/geodesic/tweaked/2-TEQ/none/igw/real/hi/hi.160.grid.png img_link/linear/hi-none-igw-fsphere-geo.png
cp output/fsphere/geodesic/tweaked/2-TEQ/none/igw/real/zeta/zeta.160.grid.png img_link/linear/zeta-none-igw-fsphere-geo.png

cp output/fsphere/cubedsphere/thuburn/2-TEQ/none/igw/real/hi/hi.214.grid.png img_link/linear/hi-none-igw-fsphere-cs.png
cp output/fsphere/cubedsphere/thuburn/2-TEQ/none/igw/real/zeta/zeta.214.grid.png img_link/linear/zeta-none-igw-fsphere-cs.png

cp output/fsphereZ/geodesic/tweaked/2-TEQ/none/igw/real/hi/hi.161.grid.png img_link/linear/hi-none-igw-fsphere-geoZ.png
cp output/fsphereZ/geodesic/tweaked/2-TEQ/none/igw/real/zeta/zeta.161.grid.png img_link/linear/zeta-none-igw-fsphere-geoZ.png
