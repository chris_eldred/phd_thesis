#!/bin/bash

./run_init_planar.sh square 9 10
./run_init_planar.sh square 9 10
./run_init_planar.sh square 9 11
./run_init_planar.sh square 10 9
./run_init_planar.sh square 10 10
./run_init_planar.sh square 10 11
./run_init_planar.sh square 11 9
./run_init_planar.sh square 11 10
./run_init_planar.sh square 11 11

./run_init_planar.sh hex 9 10
./run_init_planar.sh hex 9 10
./run_init_planar.sh hex 9 11
./run_init_planar.sh hex 10 9
./run_init_planar.sh hex 10 10
./run_init_planar.sh hex 10 11
./run_init_planar.sh hex 11 9
./run_init_planar.sh hex 11 10
./run_init_planar.sh hex 11 11

./run_init_planar.sh square 20 20
./run_init_planar.sh square 30 30
./run_init_planar.sh square 40 40
./run_init_planar.sh square 50 50
./run_init_planar.sh square 60 60
./run_init_planar.sh square 70 70
./run_init_planar.sh square 80 80
./run_init_planar.sh square 90 90
./run_init_planar.sh square 100 100

./run_init_planar.sh hex 20 20
./run_init_planar.sh hex 30 30
./run_init_planar.sh hex 40 40
./run_init_planar.sh hex 50 50
./run_init_planar.sh hex 60 60
./run_init_planar.sh hex 70 70
./run_init_planar.sh hex 80 80
./run_init_planar.sh hex 90 90
./run_init_planar.sh hex 100 100

