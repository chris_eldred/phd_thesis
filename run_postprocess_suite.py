from __future__ import print_function
import subprocess
import sys
import os

gridtypes = ['geodesic','cubedsphere']
gridvariant_dict = {'geodesic' : ['tweaked','cvt','spring_beta=0.8','spring_beta=1.1'] , 'cubedsphere': ['thuburn',]}
trisk_variants = ['TEQ','TE','Q'] 

dissipation_type = sys.argv[4]
use_dissipation = sys.argv[3]
scheme = sys.argv[2]
simulation = sys.argv[1]

###################### SIMULATIONS ##########################
if simulation == 'Galewsky':
	output_name = 'Galewsky'
	vlevels = [6,7,8]
	
if simulation == 'GalewskyInit':
	output_name = 'GalewskyInit'
	vlevels = [6,7,8]

	
if simulation == 'GalewskyNoPerturb':
	output_name = 'GalewskyNoPerturb'
	vlevels = [2,3,4,5,6,7,8]


if simulation == 'TC2':
	output_name = 'TC2'
	vlevels = [2,3,4,5,6,7,8]
	
if simulation == 'RHwave':
	output_name = 'RHwave'
	vlevels = [6,7]


if simulation == 'TC5':
	output_name = 'TC5'
	vlevels = [6,7]

if simulation == 'heldsuarez':
	output_name = 'heldsuarez'
	vlevels = [6,]

if simulation == 'geostrophic':
	output_name = 'geostrophic'
	vlevels = [6,]


if simulation == 'TEST':
	output_name = 'Galewsky'
	vlevels = [6]
	gridtypes = ['cubedsphere',]
	trisk_variants = ['TEQ',]

if use_dissipation == 'False':
	dissipdir = 'none'
if use_dissipation == 'True':
	if dissipation_type == 'hyperviscosity':
		dissipdir = 'hyper'
	if dissipation_type == 'APVM':
		dissipdir = 'apvm'

if scheme == 'ZGRID':
	trisk_variants = ['TEQ',]
	gridtypes = ['geodesic',]
	output_name = output_name + 'Z'
###################### ACTUAL RUN CODE ##########################

for vlevel in vlevels:
	for gridtype in gridtypes:
		for gridvariant in gridvariant_dict[gridtype]:
			if gridvariant == 'spring_beta=0.8' or gridvariant == 'spring_beta=1.1':
				if vlevel > 5:
					continue
			for triskvar in trisk_variants:
				print('processing',vlevel,gridtype,gridvariant,triskvar)
				path = 'output/' + output_name + '/' + gridtype + '/' + gridvariant + '/' + str(vlevel) + '-' + triskvar + '/' + dissipdir + '/'
				directory_contents = os.listdir(path)
				for item in directory_contents:
					splititem = item.split('.')
					#print(splititem)
					if splititem[-1] == 'template':
						#print(path+item)
						subprocess.call(['./run_postprocess.sh','../' + path + item])
