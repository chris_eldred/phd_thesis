#!/bin/bash

#export OMP_NUM_THREADS=1
#export OMP_STACKSIZE=8g
#ulimit -s unlimited

if [ $2 = 'TRISK' ]
then
mkdir -p output/$1/plots
fi

if [ $2 = 'ZGRID' ]
then
mkdir -p output/${1}Z/plots
fi

python src/general_plot.py $1 $2 $3 $4

