#!/bin/bash

export OMP_NUM_THREADS=$8
export OMP_STACKSIZE=8g
ulimit -s unlimited

if [ ${5} = 'False' ]
then 
FINALDIR='none'
fi
if [ ${5} = 'True' ]
then 
if [ ${6} = 'hyperviscosity' ]
then
FINALDIR='hyper'
fi
if [ ${6} = 'APVM' ]
then
FINALDIR='apvm'
fi
fi

if [ $4 = 'ZGRID' ]
then
PROFILEDIR='PROFILEZ'
fi 
if [ $4 = 'TRISK' ]
then
PROFILEDIR='PROFILE'
fi

actualtempname=temp.$2.$3.$1.$4.${FINALDIR}.TEQ.PROFILE

sed "s/VLEVEL/$1/g" cfg/swe_profile.template > cfg/${actualtempname}.temp1
sed "s/GRIDTYPE/$2/g" cfg/${actualtempname}.temp1 > cfg/${actualtempname}.temp2
sed "s/GRIDVARIANT/$3/g" cfg/${actualtempname}.temp2 > cfg/${actualtempname}.temp3
sed "s/SCHEME/$4/g" cfg/${actualtempname}.temp3 > cfg/${actualtempname}.temp4
sed "s/DISSIPATION/$5/g" cfg/${actualtempname}.temp4 > cfg/${actualtempname}.temp5
sed "s/DISSIPTYPE/$6/g" cfg/${actualtempname}.temp5 > cfg/${actualtempname}.temp6
sed "s/NSTEPS/$7/g" cfg/${actualtempname}.temp6 > cfg/${actualtempname}.temp7

/usr/local/bin/python3 -m cProfile -o swe.${actualtempname}.dat src/swe.py ${actualtempname}.temp7 > ${actualtempname}.out
mkdir -p /Users/celdred/output/${PROFILEDIR}/$2/$3/$1-TEQ/${FINALDIR}/$8
mv output/temp/vars.${actualtempname}.hdf5 /Users/celdred/output/${PROFILEDIR}/$2/$3/$1-TEQ/${FINALDIR}/$8/vars.hdf5
mv ${actualtempname}.out /Users/celdred/output/${PROFILEDIR}/$2/$3/$1-TEQ/${FINALDIR}/$8/${actualtempname}.out
mv output/temp/*.${actualtempname}.png /Users/celdred/output/${PROFILEDIR}/$2/$3/$1-TEQ/${FINALDIR}/$8/
mv cfg/${actualtempname}.temp7 /Users/celdred/output/${PROFILEDIR}/$2/$3/$1-TEQ/${FINALDIR}/$8/${actualtempname}.template
mv swe.${actualtempname}.dat /Users/celdred/output/${PROFILEDIR}/$2/$3/$1-TEQ/${FINALDIR}/$8/swe.dat

rm cfg/${actualtempname}.temp1
rm cfg/${actualtempname}.temp2
rm cfg/${actualtempname}.temp3
rm cfg/${actualtempname}.temp4
rm cfg/${actualtempname}.temp5
rm cfg/${actualtempname}.temp6
