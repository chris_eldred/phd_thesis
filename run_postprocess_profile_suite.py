from __future__ import print_function
import subprocess
import sys
import os
import csv
import numpy as np
import matplotlib.pyplot as plt

gridtypes = ['geodesic','cubedsphere']
gridvariant_dict = {'geodesic' : ['tweaked',] , 'cubedsphere': ['thuburn',]} #'cvt','spring_beta=0.8','spring_beta=1.1'


dissipationtype = sys.argv[4]
dissipation = sys.argv[3]
vlevel = int(sys.argv[2])
scheme = sys.argv[1]

if scheme == 'TRISK':
	nsteps = 240
	profiledir = 'PROFILE'

if scheme == 'ZGRID':
	nsteps = 240
	profiledir = 'PROFILEZ'
	gridtypes = ['geodesic',]

#GEOSTROPHIC TURBULENCE
#HELD SUAREZ DRY
#EVENTUALLY ADD MORE DISSIPATION OPTIONS INTO THIS!
if dissipation == 'False':
	dissipdir = 'none'
if dissipation == 'True':
	if dissipationtype == 'hyperviscosity':
		dissipdir = 'hyper'
	if dissipationtype == 'APVM':
		dissipdir = 'apvm'

###################### ACTUAL RUN CODE ##########################

for gridtype in gridtypes:
	for gridvariant in gridvariant_dict[gridtype]:
		if gridvariant == 'spring_beta=0.8' or gridvariant == 'spring_beta=1.1':
			if vlevel > 5:
				continue
		run_timings = np.zeros(24,dtype=np.float64)
		init_timings = np.zeros(24,dtype=np.float64)
		for threadnum in range(1,25):
			#print('processing',vlevel,gridtype,gridvariant,scheme,dissipation,dissipationtype,threadnum)
			path = 'output/' + profiledir + '/' + gridtype + '/' + gridvariant + '/' + str(vlevel) + '-' + 'TEQ' + '/' + dissipdir + '/' + str(threadnum) + '/'
			directory_contents = os.listdir(path)
			for item in directory_contents:
				splititem = item.split('.')
				#print(splititem)
			#	if splititem[-1] == 'template':
			#		print(path+item)
					#subprocess.call(['./run_postprocess.sh','../' + path + item])
			#GPROF
			#print('python gprof2dot.py -f pstats -n 1 ' + path + 'swe.dat | dot -Tpdf -o ' + path + 'swe.pdf')
			#subprocess.call(['gprof2dot.py -f pstats -n 1 ' + path + 'swe.dat | dot -Tpdf -o ' + path + 'swe.pdf'])
			#GET TIMING FOR THIS RUN
				if splititem[-1] == 'out':
					#print(path+item)
					timingfile = open(path+item,'r')
					for row in csv.reader(timingfile):
						#print(row)
						splitrow = row[0].split(' ')
						if splitrow[0] == 'run':
							if splitrow[1] == 'time':
								runtime = splitrow[2]
						if splitrow[0] == 'init' and len(splitrow) > 1:
							if splitrow[1] == 'time':
								inittime = splitrow[2]
					timingfile.close()
			run_timings[threadnum-1] = runtime
			init_timings[threadnum-1] = inittime
			#print(threadnum,runtime,inittime)
			
		plt.figure()
		plt.plot(range(24),run_timings)
		plt.scatter(range(24),run_timings)
		plt.savefig('output/' + profiledir + '/' + gridtype + '/' + gridvariant + '/' + str(vlevel) + '-' + 'TEQ' + '/' + dissipdir + '/run.png')

		plt.figure()
		plt.plot(range(24),init_timings)
		plt.scatter(range(24),init_timings)
		plt.savefig('output/' + profiledir + '/' + gridtype + '/' + gridvariant + '/' + str(vlevel) + '-' + 'TEQ' + '/' + dissipdir + '/init.png')
		
		print(vlevel,gridtype,gridvariant,scheme,dissipation,dissipationtype)
		print(run_timings)



