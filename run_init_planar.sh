#!/bin/bash

sed "s/NX/$2/g" cfg/planar_init.template > cfg/planar.temp1
sed "s/NY/$3/g" cfg/planar.temp1 > cfg/planar.temp2
sed "s/GRIDTYPE/$1/g" cfg/planar.temp2 > cfg/planar.temp3

python src/initializer.py planar.temp3 > planar.out

mv planar.out meshes/$1/${2}x${3}/init.out

rm cfg/planar.temp1
rm cfg/planar.temp2
rm cfg/planar.temp3
