#!/bin/bash

export OMP_NUM_THREADS=3
export OMP_STACKSIZE=8g
ulimit -s unlimited

python src/post_process.py $1
