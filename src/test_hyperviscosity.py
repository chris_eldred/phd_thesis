
from constants import *
from mesh import *
from configuration import *
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as spstats
from sympy import *
from sympy.utilities.lambdify import implemented_function
from math import pow
import h5py
import time
import grid
import sys

start = time.clock()
get_configuration(sys.argv[1])
print 'configured'

def l2_error(diff,Iinv):
	return np.sqrt(np.sum(Iinv * np.square(diff),dtype='float128')/Iinv.sum())
	
def linf_error(diff):
	return np.amax(np.abs(diff))

def grad(A):
	return 1/(rad*cos(theta)) * diff(A,lambda1), 1/rad * diff(A,theta) 

def skewgrad(A):
	return  1/rad * diff(A,theta) , -1/(rad*cos(theta)) * diff(A,lambda1),

def div(A,B):
	return 1/(rad*cos(theta)) * (diff(A,lambda1) + diff(B * cos(theta),theta))
	
def jacob(A,B):
	return 1/(rad*rad*cos(theta)) * (diff(A,lambda1)*diff(B,theta) - diff(A,theta)*diff(B,lambda1))

def laplac(A):
	#t1 = 1/(a*a*cos(theta)*cos(theta))
	#t2 = 1/(a*a*cos(theta)) * diff(cos(theta) * diff(A,theta),theta)
	gradA = grad(A)
	return div(gradA[0],gradA[1])

grid_types_and_variants = [('geodesic','tweaked'),('geodesic','spring_beta=0.8'),('geodesic','spring_beta=1.1'),('geodesic','cvt'),('cubedsphere','thuburn')]
vlevels = [1,2,3,4,5] #,6,7,8]
nmodels = len(grid_types_and_variants)

#operator accuracy tests- laplacian, H, W, Q
Le_l2_error_matrix = np.zeros((len(vlevels),nmodels))
Le_linf_error_matrix = np.zeros((len(vlevels),nmodels))
num_dofs = np.zeros((len(vlevels),nmodels))
diff_out_file = h5py.File('hypertest/errors.h5py','w')
for i,vlevel in enumerate(vlevels):
	for k,(gtype_and_var) in enumerate(grid_types_and_variants):
		if (gtype_and_var[1] == 'spring_beta=0.8' or gtype_and_var[1] == 'spring_beta=1.1'):
			if (vlevel > 5):
				Le_l2_error_matrix[i,k] = None
				Le_linf_error_matrix[i,k] = None
				num_dofs[i,k] = None
				continue
		print gtype_and_var[0],gtype_and_var[1],vlevel
		settings.grid_type = gtype_and_var[0]
		settings.grid_variant = gtype_and_var[1]
		settings.vlevel  = vlevel
		if settings.grid_type == 'cubedsphere':
			set_grid_sizes_cubedsphere(grid.grid,settings.vlevel)
			allocate_connectivity_cubedsphere(grid.grid)
			allocate_geometry_cubedsphere(grid.grid)
			allocate_operators_cubedsphere(grid.grid)
		if settings.grid_type == 'geodesic':
			set_grid_sizes_geodesic(grid.grid,settings.vlevel)
			#print 'sizes'
			allocate_connectivity_geodesic(grid.grid)
			#print 'connect'
			allocate_operators_geodesic(grid.grid)
			#print 'ops'
			allocate_geometry_geodesic(grid.grid)
			#print 'geom'
		load_connectivity(grid.grid)
		#print 'connect'
		load_operators(grid.grid)
		#print 'ops'
		grid.grid.dimensionalize_operators(settings.a)
		#print 'dimen'
		load_geometry(grid.grid)
		#print 'geo'
		grid.grid.dimensionalize_geometry(settings.a)
		#print 'dimen'

		meshfile = h5py.File(settings.meshdir + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(grid.grid.nface[settings.vlevel-1]) + '.hdf5','r')
		clat = np.array(meshfile['coords']['clat'])
		clon = np.array(meshfile['coords']['clon'])
		vlat = np.array(meshfile['coords']['vlat'])
		vlon = np.array(meshfile['coords']['vlon'])
		elat = np.array(meshfile['coords']['elat'])
		elon = np.array(meshfile['coords']['elon'])
		meshfile.close()
		#print 'mesh'
		num_dofs[i,k] = grid.grid.nface[vlevel-1]
		


		theta = symbols('theta') #lat
		lambda1 = symbols('lambda1') #lon
		rad = symbols('rad')

		psi = 
		chi = 
		vort = 
		div = 
		graddiv = grad(div)
		curlcurl = skewgrad(vort)
		laplac_vort = laplac(vort)
		laplac_div = laplac(div)

		alpha = cos(theta)*cos(theta)*cos(theta) * sin(5*lambda1)
		beta = -rad*rad/2*cos(theta)*cos(theta)*cos(theta) * sin(3*lambda1)
		grad_beta = grad(beta)
			
			fluxdiv = div(alpha*grad_beta[0],alpha*grad_beta[1])
			jacobian = jacob(alpha,beta)
			laplacian = laplac(beta)
			
			fluxdiv_func = lambdify((theta,lambda1,rad),fluxdiv,modules='numpy')
			jacobian_func = lambdify((theta,lambda1,rad),jacobian,modules='numpy')
			laplacian_func = lambdify((theta,lambda1,rad),laplacian,modules='numpy')
			alpha_func = lambdify((theta,lambda1,rad),alpha,modules='numpy')
			beta_func = lambdify((theta,lambda1,rad),beta,modules='numpy')
			
			alpha_num = alpha_func(clat,clon,a)
			beta_num = beta_func(clat,clon,a)
			
			#zgrid laplacian
			laplac_exact = laplacian_func(clat,clon,a)
			laplac_num = grid.grid.laplac_primal(beta_num,vlevel)
			
			diff_laplac = laplac_num - laplac_exact
			laplacZ_l2_error_matrix[i,k] = l2_error(diff_laplac,Iinv)
			laplacZ_linf_error_matrix[i,k] = linf_error(diff_laplac)

		Lp0_l2_error_matrix[i,k] = l2_error(diff_p0,Jinv)
		Lp0_linf_error_matrix[i,k] = linf_error(diff_p0)
		Ld0_l2_error_matrix[i,k] = l2_error(diff_d0,Iinv)
		Ld0_linf_error_matrix[i,k] = linf_error(diff_d0)
		Le_l2_error_matrix[i,k] = l2_error(diff_e,Einv)
		Le_linf_error_matrix[i,k] = linf_error(diff_e)

		if vlevel == vlevels[-1]:
			diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/Lp0',data=diff_p0)
			diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/Ld0',data=diff_d0)
			diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/Le',data=diff_e)
		
#plot operator accuracy
def plot_operator_accuracy(name,ndofs,error_matrix,label_list):
	print 'plotting',name
	plt.figure()
	x = np.arange(0.9*np.nanmin(ndofs),1.1*np.nanmax(ndofs),dtype=np.int32)
	maxerr = error_matrix[0,0]
	b1 = maxerr/pow(np.ravel(ndofs)[0],-1.)
	b2 = maxerr/pow(np.ravel(ndofs)[0],-2.)
	y1 = b1*np.power(x,-1.)
	y2 = b2*np.power(x,-2.)
	for i in range(error_matrix.shape[1]):
		plt.plot(ndofs[:,i],error_matrix[:,i],label=label_list[i])
		plt.scatter(ndofs[:,i],error_matrix[:,i]) 
	plt.plot(x,y1,linestyle='--',color='k')
	#plt.plot(x,y2,linestyle='--',color='k')
	plt.yscale('log')
	plt.xscale('log')
	plt.legend(loc=3)
	plt.title(title)
	plt.savefig('orderofaccuracy/' + name + '.png')
	plt.close()

plot_operator_accuracy('Le.l2',num_dofs,Le_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Le.linf',num_dofs,Le_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])



#print 'operator test time',mid - start
#print 'scheme test time',end - mid
#print 'total time',end - start
