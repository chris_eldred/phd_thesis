import matplotlib.pyplot as plt
import numpy as np
import h5py
import sys
from configuration import *
from math import ceil,sqrt,pow
from basemap_plotting_helpers import *
from mesh import *
from constants import *
import grid


#get configuration
get_configuration(sys.argv[1])
get_eigenconfig(sys.argv[1])
if settings.use_dissipation == False:
	disspdir = 'none'
if settings.use_dissipation == True:
	disspdir = 'hyper'
basedir = settings.outdir + settings.simname + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(settings.vlevel) + '-' + settings.trisk_variant + '/' + disspdir + '/'
outdir = settings.outdir + settings.simname + '/plots/' + disspdir + '/' + settings.grid_type + '.' + settings.grid_variant + '.' + str(settings.vlevel) +  '.' + settings.trisk_variant + '.' + disspdir + '.'

if settings.grid_type == 'geodesic':
	nfaces = 10*(2**settings.vlevel)**2 + 2
	nverts = 20*(2**settings.vlevel)**2
	nedges = 30*(2**settings.vlevel)**2
	
if settings.grid_type == 'cubedsphere':
	n0 = 3
	nfaces = int(pow(n0*pow(2,settings.vlevel-1),2) * 6)
	nedges = 2*nfaces
	nverts = nfaces + 2

if settings.scheme == 'TRISK':
	nstationary = nverts
	ninertia = 2*nfaces-2
	operators = ['Div','Grad','Perp','A']

if settings.scheme == 'ZGRID':
	nstationary = nfaces
	ninertia = 2*nfaces
	operators = ['Laplac','A']

if settings.grid_type == 'cubedsphere':
	set_grid_sizes_cubedsphere(grid.grid,settings.vlevel)
	allocate_connectivity_cubedsphere(grid.grid)
	allocate_geometry_cubedsphere(grid.grid)
	allocate_operators_cubedsphere(grid.grid)
if settings.grid_type == 'geodesic':
	set_grid_sizes_geodesic(grid.grid,settings.vlevel)
	allocate_connectivity_geodesic(grid.grid)
	allocate_operators_geodesic(grid.grid)
	allocate_geometry_geodesic(grid.grid)
if settings.grid_type == 'square':
	set_grid_sizes_square(grid.grid,settings.nx,settings.ny)
	allocate_connectivity_square(grid.grid)
	allocate_operators_square(grid.grid)
	allocate_geometry_square(grid.grid)
if settings.grid_type == 'hex':
	set_grid_sizes_hex(grid.grid,settings.nx,settings.ny)
	allocate_connectivity_hex(grid.grid)
	allocate_operators_hex(grid.grid)
	allocate_geometry_hex(grid.grid)
		
if settings.grid_type == 'square' or settings.grid_type == 'hex':
	settings.a = settings.dx
	
load_connectivity(grid.grid)
load_operators(grid.grid)
grid.grid.dimensionalize_operators(settings.a)
load_geometry(grid.grid)
grid.grid.dimensionalize_geometry(settings.a)

#load eigenvalues from file
eigenfile = h5py.File(basedir + 'eigen.hdf5', 'r')
eigenpostfile = h5py.File(basedir + 'eigen.post.hdf5', 'r')
evals = np.array(eigenfile['w'])
evals_real = -1.0*np.real(evals) #adjust based on definition of omega
evals_imag = np.imag(evals)

#do basic analyis on eigenmodes
omega = evals_imag / settings.f
omega_zero = np.less(np.abs(omega),0.1)
eval_indices = np.arange(omega.shape[0])

stationary_modes = omega[omega_zero]
stationary_indices = eval_indices[omega_zero]

stationary_imag = evals_real[omega_zero]
stationary_mode_index = np.arange(stationary_modes.shape[0])
print 'nstationary',nstationary,stationary_modes.shape[0]

inertia_gravity_modes = omega[np.logical_not(omega_zero)]
inertia_gravity_imag = evals_real[np.logical_not(omega_zero)]
inertia_gravity_indices = eval_indices[np.logical_not(omega_zero)]
print 'ninertia',ninertia,inertia_gravity_modes.shape[0]
#omega_imag = evals_real / settings.f
#print 'imaginary parts',np.amax(omega_imag),np.amin(omega_imag),np.average(omega_imag)
#print 'stationary parts',np.amax(stationary_modes),np.amin(stationary_modes),np.average(stationary_modes)

inertia_gravity_sorted_indices = np.argsort(inertia_gravity_modes)

inertia_gravity_modes = inertia_gravity_modes[inertia_gravity_sorted_indices]
inertia_gravity_imag = inertia_gravity_imag[inertia_gravity_sorted_indices]
inertia_gravity_indices = inertia_gravity_indices[inertia_gravity_sorted_indices]

neg_inertia_gravity_modes,pos_inertia_gravity_modes = np.split(inertia_gravity_modes,2)
neg_inertia_gravity_imag,pos_inertia_gravity_imag = np.split(inertia_gravity_imag,2)

neg_inertia_gravity_sorted_indices = np.argsort(-1.0*neg_inertia_gravity_modes)
neg_inertia_gravity_modes = neg_inertia_gravity_modes[neg_inertia_gravity_sorted_indices]
neg_inertia_gravity_imag = neg_inertia_gravity_imag[neg_inertia_gravity_sorted_indices]

ig_mode_index = np.arange(neg_inertia_gravity_modes.shape[0])


#determine theoretical eigenvalues
gridfile = h5py.File(settings.meshdir + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(nfaces) + '.hdf5','r')
de = np.array(gridfile['geometry']['de'],dtype=np.float64,order='F')
gridfile.close()
dx = np.average(de) * settings.a
Hbar = settings.f*settings.f * settings.C*settings.C * dx * dx / settings.g
lambda2 = settings.g * Hbar/settings.f/settings.f
lambda2scaled = lambda2/settings.a/settings.a
N = int(ceil(sqrt(ninertia/2 - 1)))
N_array = np.zeros(N*N+1,dtype=np.int32)
m_array = np.zeros(N*N+1,dtype=np.int32)
j = 0
for k in range(0,N):
	for l in range(-k,k+1):
		N_array[j] = k
		m_array[j] = l
		j = j+1
theoretical_inertia_gravity_modes = np.sqrt(1. + N_array * (N_array+1.) * lambda2scaled)

#plot eigenvalues
plt.figure(figsize=(12, 6), dpi=100)
plt.scatter(ig_mode_index,pos_inertia_gravity_modes,c='k',marker='+')
plt.scatter(ig_mode_index,theoretical_inertia_gravity_modes[:ninertia/2],facecolors='none', edgecolors='g',marker='o')
plt.scatter(stationary_mode_index,stationary_modes,c='r',marker='+')
plt.xlabel('Mode Index')
plt.ylabel('Normalized Real Frequency (omega/f)')
plt.savefig(outdir + 'omega.real.pos.png')


plt.figure(figsize=(12, 6), dpi=100)
plt.scatter(ig_mode_index,-1*neg_inertia_gravity_modes,c='k',marker='+')
plt.scatter(ig_mode_index,theoretical_inertia_gravity_modes[:ninertia/2],facecolors='none', edgecolors='g',marker='o')
plt.scatter(stationary_mode_index,stationary_modes,c='r',marker='+')
plt.xlabel('Mode Index')
plt.ylabel('Normalized Real Frequency (omega/f)')
plt.savefig(outdir + 'omega.real.neg.png')

plt.figure(figsize=(12, 6), dpi=100)
plt.scatter(ig_mode_index,pos_inertia_gravity_imag,c='k')
plt.scatter(stationary_mode_index,stationary_imag,c='r',marker='+')
ig_max = np.amax(pos_inertia_gravity_imag)
ig_min = np.amin(pos_inertia_gravity_imag)
stationary_max = np.amax(stationary_imag)
stationary_min = np.amin(stationary_imag)
plt.ylim(0.9*min(ig_min,stationary_min),1.1*max(ig_max,stationary_max))
plt.xlabel('Mode Index')
plt.ylabel('Imaginary Frequency')
plt.savefig(outdir + 'omega.imag.pos.png')

plt.figure(figsize=(12, 6), dpi=100)
plt.scatter(ig_mode_index,neg_inertia_gravity_imag,c='k')
plt.scatter(stationary_mode_index,stationary_imag,c='r',marker='+')
ig_max = np.amax(neg_inertia_gravity_imag)
ig_min = np.amin(neg_inertia_gravity_imag)
stationary_max = np.amax(stationary_imag)
stationary_min = np.amin(stationary_imag)
plt.ylim(0.9*min(ig_min,stationary_min),1.1*max(ig_max,stationary_max))
plt.xlabel('Mode Index')
plt.ylabel('Imaginary Frequency')
plt.savefig(outdir + 'omega.imag.neg.png')

#eigenvector plots
meshdata = get_geometry(settings.grid_type,settings.grid_variant,settings.vlevel)

#ig waves
print 'ig waves'
for i in range(inertia_gravity_modes.shape[0]):
	j = inertia_gravity_indices[i]
	real_val = inertia_gravity_modes[i]
	imag_val = inertia_gravity_imag[i]
	data = np.array(eigenfile['vr'+str(j)])
	if settings.scheme == 'TRISK':
		hi = data[:nfaces]
		ue = data[nfaces:]
		zeta_real = grid.grid.j(grid.grid.d2bar(np.real(ue),settings.vlevel,nverts),settings.vlevel)
		di_real = grid.grid.d2(grid.grid.h(np.real(ue),settings.vlevel),settings.vlevel,nfaces)
		zeta_imag = grid.grid.d2bar(np.imag(ue),settings.vlevel,nverts)
		di_imag = grid.grid.d2(grid.grid.h(np.imag(ue),settings.vlevel),settings.vlevel,nfaces)

		plot_scalar(meshdata,np.real(hi),'primal','IGW Eigenvector (Real Part) for hi', basedir + 'igw/real/hi/hi.' + str(j),'spherical','grid',None)
		plot_scalar(meshdata,np.real(di_real),'primal','IGW Eigenvector (Real Part) for delta', basedir + 'igw/real/delta/delta.' + str(j),'spherical','grid',None)
		plot_scalar(meshdata,np.real(zeta_real),'dual','IGW Eigenvector (Real Part) for zeta', basedir + 'igw/real/zeta/zeta.' + str(j),'spherical','grid',None)

		plot_scalar(meshdata,np.imag(hi),'primal','IGW Eigenvector (Imag Part) for hi', basedir + 'igw/imag/hi/hi.' + str(j),'spherical','grid',None)
		plot_scalar(meshdata,np.imag(di_imag),'primal','IGW Eigenvector (Imag Part) for delta', basedir + 'igw/imag/delta/delta.' + str(j),'spherical','grid',None)
		plot_scalar(meshdata,np.imag(zeta_imag),'dual','IGW Eigenvector (Imag Part) for zeta', basedir + 'igw/imag/zeta/zeta.' + str(j),'spherical','grid',None)

	if settings.scheme == 'ZGRID':
		hi = data[:nfaces]
		delta = data[2*nfaces:3*nfaces]
		zeta = data[nfaces:2*nfaces]
		plot_scalar(meshdata,np.real(hi),'primal','IGW Eigenvector (Real Part) for hi', basedir + 'igw/real/hi/hi.' + str(j),'spherical','grid',None)
		plot_scalar(meshdata,np.real(delta),'primal','IGW Eigenvector (Real Part) for delta', basedir + 'igw/real/delta/delta.' + str(j),'spherical','grid',None)
		plot_scalar(meshdata,np.real(zeta),'primal','IGW Eigenvector (Real Part) for zeta', basedir + 'igw/real/zeta/zeta.' + str(j),'spherical','grid',None)
		plot_scalar(meshdata,np.imag(hi),'primal','IGW Eigenvector (Imag Part) for hi', basedir + 'igw/imag/hi/hi.' + str(j),'spherical','grid',None)
		plot_scalar(meshdata,np.imag(delta),'primal','IGW Eigenvector (Imag Part) for delta', basedir + 'igw/imag/delta/delta.' + str(j),'spherical','grid',None)
		plot_scalar(meshdata,np.imag(zeta),'primal','IGW Eigenvector (Imag Part) for zeta', basedir + 'igw/imag/zeta/zeta.' + str(j),'spherical','grid',None)

#	plot_scalar(meshdata,np.real(data),varloc,'IGW Eigenvector (Real Part)', basedir + 'igw/real/igw.real.' + str(j),'spherical','grid',None)
#	plot_scalar(meshdata,np.imag(data),varloc,'IGW Eigenvector (Imag Part)', basedir + 'igw/imag/igw.imag.' + str(j),'spherical','grid',None)
	print j,real_val,imag_val

#stationary modes
print 'stationary modes'
for i in range(stationary_modes.shape[0]):
	j = stationary_indices[i]
	real_val = stationary_modes[i]
	imag_val = stationary_modes[i]
	data = np.array(eigenfile['vr'+str(j)])
	if settings.scheme == 'TRISK':
		hi = data[:nfaces]
		ue = data[nfaces:]
		zeta_real = grid.grid.d2bar(np.real(ue),settings.vlevel,nverts)
		di_real = grid.grid.d2(grid.grid.h(np.real(ue),settings.vlevel),settings.vlevel,nfaces)
		zeta_imag = grid.grid.d2bar(np.imag(ue),settings.vlevel,nverts)
		di_imag = grid.grid.d2(grid.grid.h(np.imag(ue),settings.vlevel),settings.vlevel,nfaces)

		plot_scalar(meshdata,np.real(hi),'primal','Stationary Eigenvector (Real Part) for hi', basedir + 'stat/real/hi/hi.' + str(j),'spherical','grid',None)
		plot_scalar(meshdata,np.real(di_real),'primal','Stationary Eigenvector (Real Part) for delta', basedir + 'stat/real/delta/delta.' + str(j),'spherical','grid',None)
		plot_scalar(meshdata,np.real(zeta_real),'dual','Stationary Eigenvector (Real Part) for zeta', basedir + 'stat/real/zeta/zeta.' + str(j),'spherical','grid',None)

		plot_scalar(meshdata,np.imag(hi),'primal','Stationary Eigenvector (Imag Part) for hi', basedir + 'stat/imag/hi/hi.' + str(j),'spherical','grid',None)
		plot_scalar(meshdata,np.imag(di_imag),'primal','Stationary Eigenvector (Imag Part) for delta', basedir + 'stat/imag/delta/delta.' + str(j),'spherical','grid',None)
		plot_scalar(meshdata,np.imag(zeta_imag),'dual','Stationary Eigenvector (Imag Part) for zeta', basedir + 'stat/imag/zeta/zeta.' + str(j),'spherical','grid',None)

	if settings.scheme == 'ZGRID':
		hi = data[:nfaces]
		delta = data[2*nfaces:3*nfaces]
		zeta = data[nfaces:2*nfaces]
		plot_scalar(meshdata,np.real(hi),'primal','Stationary Eigenvector (Real Part) for hi', basedir + 'stat/real/hi/hi.' + str(j),'spherical','grid',None)
		plot_scalar(meshdata,np.real(delta),'primal','Stationary Eigenvector (Real Part) for delta', basedir + 'stat/real/delta/delta.' + str(j),'spherical','grid',None)
		plot_scalar(meshdata,np.real(zeta),'primal','Stationary Eigenvector (Real Part) for zeta', basedir + 'stat/real/zeta/zeta.' + str(j),'spherical','grid',None)
		plot_scalar(meshdata,np.imag(hi),'primal','Stationary Eigenvector (Imag Part) for hi', basedir + 'stat/imag/hi/hi.' + str(j),'spherical','grid',None)
		plot_scalar(meshdata,np.imag(delta),'primal','Stationary Eigenvector (Imag Part) for delta', basedir + 'stat/imag/delta/delta.' + str(j),'spherical','grid',None)
		plot_scalar(meshdata,np.imag(zeta),'primal','Stationary Eigenvector (Imag Part) for zeta', basedir + 'stat/imag/zeta/zeta.' + str(j),'spherical','grid',None)

#	plot_scalar(meshdata,np.real(data),varloc,'Stationary Eigenvector (Real Part)', basedir + 'stat/real/stat.real.' + str(j),'spherical','grid',None)
#	plot_scalar(meshdata,np.imag(data),varloc,'Stationary Eigenvector (Imag Part)', basedir + 'stat/imag/stat.imag.' + str(j),'spherical','grid',None)
	print j,real_val,imag_val

varloc_dict = {'Laplac' : 'primal', 'Div' : 'edge', 'Grad': 'primal', 'Perp': 'edge', 'FD': 'primal', 'J' : 'primal'}

#analyze singular values
svdfile = h5py.File(basedir + 'svd.hdf5', 'r')
for operator in operators:
	try:
		svds = np.array(svdfile[operator]['s'])
	except:
		continue	
	svds = svds/np.amax(svds)
	svd_zero = np.less(svds,10**(-10))
	svd_indices = np.arange(svds.shape[0])
	svd_indices = svd_indices[svd_zero]
	singular_vals = svds[np.less(svds,10**(-10))]
	print operator,singular_vals.shape[0]
	VH = np.array(svdfile[operator]['Vh'])
	if operator == 'A':
		for i in range(singular_vals.shape[0]):
			j = svd_indices[i]
			print j,singular_vals[i]
			data = VH[j,:]
			if settings.scheme == 'TRISK':
				hi = data[:nfaces]
				ue = data[nfaces:]
				zetav = grid.grid.d2bar(ue,settings.vlevel,nverts)
				di = grid.grid.d2(grid.grid.h(ue,settings.vlevel),settings.vlevel,nfaces)
				plot_scalar(meshdata,hi,'primal','Singular Vector Basis for hi', basedir + 'svd/A/hi.' + str(j),'spherical','grid',None)
				plot_scalar(meshdata,ue,'edge','Singular Vector Basis for ue', basedir + 'svd/A/ue.' + str(j),'spherical','grid',None)
				plot_scalar(meshdata,zetav,'dual','Singular Vector Basis for zetav', basedir + 'svd/A/zetav.' + str(j),'spherical','grid',None)
				plot_scalar(meshdata,di,'primal','Singular Vector Basis for di', basedir + 'svd/A/delta.' + str(j),'spherical','grid',None)
			if settings.scheme == 'ZGRID':
				hi = data[:nfaces]
				delta = data[2*nfaces:3*nfaces]
				zeta = data[nfaces:2*nfaces]
				plot_scalar(meshdata,hi,'primal','Singular Vector Basis for hi', basedir + 'svd/A/hi.' + str(j),'spherical','grid',None)
				plot_scalar(meshdata,delta,'primal','Singular Vector Basis for delta', basedir + 'svd/A/delta.' + str(j),'spherical','grid',None)
				plot_scalar(meshdata,zeta,'primal','Singular Vector Basis for zeta', basedir + 'svd/A/zeta.' + str(j),'spherical','grid',None)
	else:
		varloc = varloc_dict[operator]
		for i in range(singular_vals.shape[0]):
			j = svd_indices[i]
			print j,singular_vals[i]
			data = VH[j,:]
			if settings.scheme == 'TRISK' and varloc == 'edge':
				zetav = grid.grid.d2bar(data,settings.vlevel,nverts)
				di = grid.grid.d2(grid.grid.h(data,settings.vlevel),settings.vlevel,nfaces)
				plot_scalar(meshdata,data,'edge','Singular Vector Basis for ' + operator, basedir + 'svd/' + operator + '.ue.' + str(j),'spherical','grid',None)
				plot_scalar(meshdata,zetav,'dual','Singular Vector Basis for ' + operator, basedir + 'svd/' + operator + '.zetav.' + str(j),'spherical','grid',None)
				plot_scalar(meshdata,di,'primal','Singular Vector Basis for ' + operator, basedir + 'svd/' + operator + '.di.' + str(j),'spherical','grid',None)
			else:
				plot_scalar(meshdata,data,varloc,'Singular Vector Basis for ' + operator, basedir + 'svd/' + operator + '.' + str(j),'spherical','grid',None)

svdfile.close()
eigenfile.close()
