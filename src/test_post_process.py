from constants import *
from mesh import *
from configuration import *
import sys
import time
import grid
import matplotlib.pyplot as plt

start = time.clock()
#get configuration
get_configuration(sys.argv[1])
if settings.use_dissipation == False:
	dissipdir = 'none'
if settings.use_dissipation == True:
	if settings.dissipation_type == 'hyperviscosity':
		dissipdir = 'hyper'
	if settings.dissipation_type == 'APVM':
		dissipdir = 'apvm'

#load operators and connectivity
if settings.grid_type == 'cubedsphere':
	set_grid_sizes_cubedsphere(grid.grid,settings.vlevel)
	allocate_connectivity_cubedsphere(grid.grid)
	allocate_operators_cubedsphere(grid.grid)
if settings.grid_type == 'geodesic':
	set_grid_sizes_geodesic(grid.grid,settings.vlevel)
	allocate_connectivity_geodesic(grid.grid)
	allocate_operators_geodesic(grid.grid)
	
load_connectivity(grid.grid)
load_operators(grid.grid)
grid.grid.dimensionalize_operators(settings.a)
#deallocate_unused(grid.grid)

#load variables from hdf5 file
settings.tempname = settings.tempname +'.'+ settings.grid_type +'.'+ settings.grid_variant +'.'+ str(settings.vlevel) + '.' + settings.trisk_variant +'.' + settings.simname
varsfile = h5py.File( 'output/temp/' + dissipdir + '/vars.' + settings.tempname + '.hdf5', 'a')
variables = {}

for name,data in varsfile['predicted'].items():
	variables[name] = np.array(data)
for name,data in varsfile['constants'].items():
	variables[name] = np.array(data)
timesteps = np.array(varsfile['timesteps'])
nsteps = timesteps.shape[0]
nface = grid.grid.nface[-1]
nvert = grid.grid.nvert[-1]
nedge = grid.grid.nedge[-1]

#create new variables
diagnosed_group = varsfile.require_group('diagnosed')

########### C GRID #######################
if settings.scheme == 'TRISK':
	diagnosed_group.require_dataset('hi' , (nsteps,nface), dtype='f')
	diagnosed_group.require_dataset('zetav' , (nsteps,nvert), dtype='f')
	diagnosed_group.require_dataset('etav' , (nsteps,nvert), dtype='f')
	diagnosed_group.require_dataset('hv' , (nsteps,nvert), dtype='f')
	diagnosed_group.require_dataset('qv' , (nsteps,nvert), dtype='f')
	diagnosed_group.require_dataset('di' , (nsteps,nface), dtype='f')
	diagnosed_group.require_dataset('dhi' , (nsteps,nface), dtype='f')

	#fill new variables
	fv = variables['fv']
	m0 = variables['mi'][0,:]
	for step in xrange(nsteps):
		mi = variables['mi'][step,:]
		ue = variables['ue'][step,:]
		zetav = grid.grid.d2bar(ue,settings.vlevel,nvert)
		di = grid.grid.d2(grid.grid.h(ue,settings.vlevel),settings.vlevel,nface)
		mv = grid.grid.r(mi,settings.vlevel,nvert)
		diagnosed_group['hi'][step,:] = grid.grid.i(mi,settings.vlevel)
		diagnosed_group['dhi'][step,:] = grid.grid.i(mi - m0,settings.vlevel)
		diagnosed_group['zetav'][step,:] = grid.grid.j(zetav,settings.vlevel)
		diagnosed_group['etav'][step,:] = grid.grid.j(zetav + fv,settings.vlevel)
		diagnosed_group['hv'][step,:] = grid.grid.j(mv,settings.vlevel)
		diagnosed_group['qv'][step,:] = (zetav + fv)/mv
		diagnosed_group['di'][step,:] = grid.grid.i(di,settings.vlevel)

############ Z GRID ##################
if settings.scheme == 'ZGRID':
	#create new variables
	diagnosed_group.require_dataset('eta' , (nsteps,nface), dtype='f')
	diagnosed_group.require_dataset('q' , (nsteps,nface), dtype='f')
	diagnosed_group.require_dataset('dhi' , (nsteps,nface), dtype='f')

	#fill new variables
	fi = variables['fi']
	h0 = variables['hi'][0,:]
	for step in xrange(nsteps):
		zetai = variables['zeta'][step,:]
		hi = variables['hi'][step,:]
		diagnosed_group['q'][step,:] = (zetai + fi)/hi
		diagnosed_group['eta'][step,:] = zetai + fi
		diagnosed_group['dhi'][step,:] = hi - h0

#for name in ['ldelta_max','ldelta_min','ldelta_average','lzeta_max','lzeta_min','lzeta_average']:
#	var = varsfile['statistics'][name]
#	plt.figure()
#	x = range(len(var))
#	plt.plot(x,var)
#	plt.savefig(settings.outdir + 'temp/' + dissipdir + '/' + name + '.' + settings.tempname + '.png')
#close varsfile
varsfile.close()

