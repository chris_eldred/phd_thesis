from basemap_plotting_helpers import *
import sys
from configuration import *

get_configuration(sys.argv[1])
settings.tempname = settings.tempname +'.'+ settings.grid_type +'.'+ settings.grid_variant +'.'+ str(settings.vlevel) + '.' + settings.trisk_variant +'.' + settings.simname

gridtype = settings.grid_type
gridvariant = settings.grid_variant
vlevel = settings.vlevel
variant = settings.trisk_variant
output_name = settings.simname
dissipation = settings.use_dissipation
dissipationtype = settings.dissipation_type

if dissipation == False:
	dissipdir = 'none'
if dissipation == True:
	if dissipationtype == 'hyperviscosity':
		dissipdir = 'hyper'
	if dissipationtype == 'APVM':
		dissipdir = 'apvm'

#variables_list = [('mi','predicted','primal','Mass at Cell Centers')]
if settings.scheme == 'TRISK':
	variables_list = [('hi','diagnosed','primal','Height at Cell Centers'),('zetav','diagnosed','dual','Relative Vorticity at Cell Corners'),
('etav','diagnosed','dual','Absolute Vorticity at Cell Corners'),
('dhi','diagnosed','primal','Height- H(0) at Cell Centers'),
#('qv','diagnosed','dual','Potential Vorticity at Cell Corners'),
('di','diagnosed','primal','Divergence at Cell Centers')]
if settings.scheme == 'ZGRID':
	variables_list = [('hi','predicted','primal','Height at Cell Centers'),('zeta','predicted','primal','Relative Vorticity at Cell Centers'),
('eta','diagnosed','primal','Absolute Vorticity at Cell Centers'),
('dhi','diagnosed','primal','Height- H(0) at Cell Centers'),
('delta','predicted','primal','Divergence at Cell Centers')]

steps = range(0,1201,100)
stepnames = [] #['0','2','6','10']	
for i in steps:
	stepnames.append(str(i))
print steps
print stepnames
#####PLOTS########

meshdata = get_geometry(gridtype,gridvariant,vlevel)
variables = {}
print 'output/temp/' + dissipdir + '/vars.' + settings.tempname + '.hdf5'
varsfile = h5py.File( 'output/temp/' + dissipdir + '/vars.' + settings.tempname + '.hdf5', 'r')

#data = varsfile['constants']['bi']
#step = 0
#stepname = '0'
#varname = 'bi'
#varloc = 'primal'
#longname = 'bi'
#plot_scalar(meshdata,data,varloc,longname + ' at Day ' + stepname, \
#'output/temp/' + varname + '.' + output_name + '.' + variant + '.' + gridtype + '.' + gridvariant + '.' + str(vlevel) + '.' + dissipdir + '.' + str(step),'latlon','contour')


for varname,vartype,varloc,longname in variables_list:
	variables[varname] = np.array(varsfile[vartype][varname])
varsfile.close()
for varname,vartype,varloc,longname in variables_list:
	print 'plotting',gridtype,gridvariant,vlevel,variant
	for step,stepname in zip(steps,stepnames):
		data = variables[varname][step,:]
		try:
			plot_scalar(meshdata,data,varloc,longname + ' at Day ' + stepname, \
		'output/temp/'+ dissipdir + '/' + varname + '.' + output_name + '.' + variant + '.' + gridtype + '.' + gridvariant + '.' + str(vlevel) + '.' + dissipdir + '.' + str(step),'spherical','contour',None)
		except:
			pass
