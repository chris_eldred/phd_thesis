from basemap_plotting_helpers import *
import sys
import subprocess

gridtypes = ['cubedsphere','geodesic']
gridvariant_dict = {'geodesic' : ['tweaked'] , 'cubedsphere': ['thuburn',]} #'cvt','spring_beta=1.1','spring_beta=0.8',
trisk_variants = ['TEQ','TE','Q'] 
title_list = ['Doubly Conservative','Energy Conserving','Enstrophy Conserving']
outdir = 'output'

dissipationtype = sys.argv[4]
dissipation = sys.argv[3]
scheme = sys.argv[2]
output_name = sys.argv[1]

gridtypes = ['geodesic']
trisk_variants = ['TEQ']

vlevels = [6,]
steps = range(0,5,1)
stepnames = [] #['0','2','6','10']	
for i in steps:
	stepnames.append(str(i))
print steps

variables_list = [('zetav','diagnosed','dual','Relative Vorticity','latlon'),('hi','diagnosed','primal','Fluid Height','latlon'),('di','diagnosed','primal','Divergence','latlon'),('qv','diagnosed','dual','Potential Vorticity','latlon'),('etav','diagnosed','dual','Absolute Vorticity','latlon')]

#adjust some things for ZGRID
if scheme == 'ZGRID':
	trisk_variants = ['TEQ',]
	gridtypes = ['geodesic',]
	output_name = output_name + 'Z'
	title_list = ['Doubly Conservative',]
	variables_list = [('zeta','predicted','primal','Relative Vorticity','latlon'),('hi','predicted','primal','Fluid Height','latlon'),('delta','predicted','primal','Divergence','latlon'),('q','diagnosed','primal','Potential Vorticity','latlon'),('eta','diagnosed','primal','Absolute Vorticity','latlon')]

if dissipation == 'False':
	dissipdir = 'none'
if dissipation == 'True':
	if dissipationtype == 'hyperviscosity':
		dissipdir = 'hyper'
	if dissipationtype == 'APVM':
		dissipdir = 'apvm'

#####PLOTS########
# Make plotting directory
subprocess.call(['mkdir','-p',outdir + '/' + output_name + '/plots/' + dissipdir])

for gridtype in gridtypes:
	for gridvariant in gridvariant_dict[gridtype]:
		for vlevel in vlevels:
			meshdata = get_geometry(gridtype,gridvariant,vlevel)
			for variant,title in zip(trisk_variants,title_list):
				print 'plotting',gridtype,gridvariant,vlevel,variant
				variables = {}
				try:
					varsfile = h5py.File(outdir + '/' + output_name + '/' + gridtype + '/' + gridvariant + '/' + str(vlevel) + '-' + variant + '/' + dissipdir + '/vars.hdf5', 'r')
					for varname,vartype,varloc,longname,plottype in variables_list:
						variables[varname] = np.array(varsfile[vartype][varname])
					varsfile.close()
				except:
					print 'does not exist',gridtype,gridvariant,vlevel,variant,dissipdir,scheme,output_name
					for varname,vartype,varloc,longname,plottype in variables_list:
						variables[varname] = None
				
				for varname,vartype,varloc,longname,plottype in variables_list:
					for step,stepname in zip(steps,stepnames):
						if variables[varname] != None:
							data = variables[varname][step,:]
						else:
							continue
						if output_name == 'GalewskyInit':
							timename = ' at Hour '
						else:
							timename = ' at Day '
						if np.sum(data) == 0:
							continue
						try:
							plot_scalar(meshdata,data,varloc,longname + timename + stepname + ', ' +title, \
							outdir +'/' + output_name + '/plots/' + dissipdir + '/' + varname + '.' + output_name + '.' + variant + '.' + gridtype + '.' + gridvariant + '.' + str(vlevel) + '.' + dissipdir + '.' + str(step),plottype,'contour',None)
						except:
							pass
