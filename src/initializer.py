from mesh import *
from constants import *
from configuration import *
import sys
import grid

import time

start = time.clock()

#get configuration
get_configuration(sys.argv[1])
print 'configured'

if settings.grid_type == 'cubedsphere':
	set_grid_sizes_cubedsphere(grid.grid,settings.vlevel)
	allocate_connectivity_cubedsphere(grid.grid)
	allocate_geometry_cubedsphere(grid.grid)
	allocate_operators_cubedsphere(grid.grid)
if settings.grid_type == 'geodesic':
	set_grid_sizes_geodesic(grid.grid,settings.vlevel)
	allocate_connectivity_geodesic(grid.grid)
	allocate_geometry_geodesic(grid.grid)
	allocate_operators_geodesic(grid.grid)
if settings.grid_type == 'square':
	set_grid_sizes_square(grid.grid,settings.nx,settings.ny)
	allocate_connectivity_square(grid.grid)
	allocate_geometry_square(grid.grid)
	allocate_operators_square(grid.grid)
if settings.grid_type == 'hex':
	set_grid_sizes_hex(grid.grid,settings.nx,settings.ny)
	allocate_connectivity_hex(grid.grid)
	allocate_geometry_hex(grid.grid)
	allocate_operators_hex(grid.grid)
	
load_connectivity(grid.grid)
load_geometry(grid.grid)

print 'mesh loaded!'
mesh_time = time.clock()

create_operators(grid.grid,settings.grid_type)
check_connectivity(grid.grid)
save_operator_h5py(grid.grid)
check_operators(grid.grid)
print 'operators checked and saved'

end = time.clock()


print 'mesh load time',mesh_time - start
print 'operator creation and save time', end - mesh_time
print 'total time',end-start
