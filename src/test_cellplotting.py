from basemap_plotting_helpers import *
import sys
import subprocess
from math import pi

gridtype = 'cubedsphere'
gridvariant = 'thuburn'
vlevel = 3

meshdata = get_geometry(gridtype,gridvariant,vlevel)

clats = (pi/180.)*meshdata.clats
vlats = (pi/180.)*meshdata.vlats
elats = (pi/180.)*meshdata.elats

varloc = 'dual' #primal dual edge
longname = 'test'
data = np.cos(vlats)

plot_scalar(meshdata,data,varloc,longname, 'test','spherical','contour',None)
plot_scalar(meshdata,data,varloc,longname, 'test','spherical','grid',None)
