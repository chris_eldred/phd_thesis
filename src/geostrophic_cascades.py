from basemap_plotting_helpers import *
import sys
import subprocess
from scipy.interpolate import griddata
import numpy as np
from math import pi

gridtypes = ['geodesic','cubedsphere']
gridvariant_dict = {'geodesic' : ['tweaked'] , 'cubedsphere': ['thuburn',]} #'cvt','spring_beta=1.1','spring_beta=0.8',
trisk_variants = ['TEQ','TE','Q'] 
title_list = ['Doubly Conservative','Energy Conserving','Enstrophy Conserving']
outdir = 'output'

dissipationtype = sys.argv[3]
dissipation = sys.argv[2]
scheme = sys.argv[1]

output_name = 'geostrophic'
vlevels = [6,]

variables_list = [('etav','diagnosed','dual','Absolute Vorticity','latlon'),('zetav','diagnosed','dual','Relative Vorticity','latlon'),('hi','diagnosed','primal','Fluid Height','latlon'),('di','diagnosed','primal','Divergence','latlon'),('qv','diagnosed','dual','Potential Vorticity','latlon')]

#adjust some things for ZGRID
if scheme == 'ZGRID':
	trisk_variants = ['TEQ',]
	gridtypes = ['geodesic',]
	output_name = output_name + 'Z'
	title_list = ['Doubly Conservative',]
	variables_list = [('eta','diagnosed','primal','Absolute Vorticity','latlon'),('zeta','predicted','primal','Relative Vorticity','latlon'),('hi','predicted','primal','Fluid Height','latlon'),('delta','predicted','primal','Divergence','latlon'),('q','diagnosed','primal','Potential Vorticity','latlon')]

if dissipation == 'False':
	dissipdir = 'none'
if dissipation == 'True':
	if dissipationtype == 'hyperviscosity':
		dissipdir = 'hyper'
	if dissipationtype == 'APVM':
		dissipdir = 'apvm'
steps = [1200,]
stepnames = ['1200',]

def interpolate_data(data,meshdata,varloc):
	lats = np.linspace(-90.,90,360)
	lons = np.linspace(-180.,180.,720)
	gridx,gridy = np.meshgrid(lons,lats)
	points = np.zeros((data.shape[0],2),dtype=np.float64)
	if varloc == 'primal':
		points[:,0] = meshdata.clons
		points[:,1] = meshdata.clats
	if varloc == 'dual':
		points[:,0] = meshdata.vlons
		points[:,1] = meshdata.vlats	
	return griddata(points,data, (gridx,gridy), method='cubic'),lats,lons

def plot_latlon(lons,lats,name,fname,data):
	plt.figure()
	map = Basemap(projection='cyl',llcrnrlat=-90,urcrnrlat=90,\
            llcrnrlon=-180,urcrnrlon=180,resolution='l')
	lons, lats = np.meshgrid(lons, lats)	
	x,y = map(lons, lats)
	map.contour(x,y,data)
	map.contourf(x,y,data)
	map.drawcoastlines(linewidth=0.3)
	map.drawparallels(np.arange(-90.,91.,30.),linewidth=0.2,labels=[1,0,0,1]) 
	map.drawmeridians(np.arange(-180.,181.,45.),linewidth=0.2,labels=[1,0,0,1])
	map.drawmapboundary()
	map.colorbar()
	plt.title(name)
	plt.tight_layout()
	plt.subplots_adjust(left=0.1, right=0.85, top=0.9, bottom=0.1)
	plt.savefig(fname + '.png')
	plt.close()
	
#####PLOTS########
# Make plotting directory
subprocess.call(['mkdir','-p',outdir + '/' + output_name + '/plots/' + dissipdir + '/cascades/'])
plotdir = outdir + '/' + output_name + '/plots/' + dissipdir + '/cascades/'

for gridtype in gridtypes:
	for gridvariant in gridvariant_dict[gridtype]:
		for vlevel in vlevels:
			meshdata = get_geometry(gridtype,gridvariant,vlevel)
			for variant,title in zip(trisk_variants,title_list):
				print 'plotting',gridtype,gridvariant,vlevel,variant
				variables = {}
				try:
					varsfile = h5py.File(outdir + '/' + output_name + '/' + gridtype + '/' + gridvariant + '/' + str(vlevel) + '-' + variant + '/' + dissipdir + '/vars.hdf5', 'r')
					for varname,vartype,varloc,longname,plottype in variables_list:
						variables[varname] = np.array(varsfile[vartype][varname])
					varsfile.close()
				except:
					print 'does not exist',gridtype,gridvariant,vlevel,variant,dissipdir,scheme,simulation
					for varname,vartype,varloc,longname,plottype in variables_list:
						variables[varname] = None
				
				for varname,vartype,varloc,longname,plottype in variables_list:
					for step,stepname in zip(steps,stepnames):
						if variables[varname] != None:
							data = variables[varname][step,:]
						else:
							continue
						if output_name == 'GalewskyInit':
							timename = ' at Hour '
						else:
							timename = ' at Day '
						plot_scalar(meshdata,data,varloc,longname + timename + stepname + ', ' +title, \
						plotdir + varname + '.' + output_name + '.' + variant + '.' + gridtype + '.' + gridvariant + '.' + str(vlevel) + '.' + dissipdir + '.' + str(step),plottype,'contour')
						
						interpolated_data,lats,lons = interpolate_data(data,meshdata,varloc)
						#print lats.shape
						#print interpolated_data.shape
						plot_latlon(lons,lats,varname,plotdir + varname,interpolated_data)
						
