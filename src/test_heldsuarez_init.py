import numpy as np
import matplotlib.pyplot as plt
from math import pi

g = 9.80616
a = 6371220.0

m = 12
u0 = 120.
dH = 8000./g
h0 = 10.**3 - 2.*dH/3.

def plot_zonal(x,y,name,xlab):
	plt.figure()
	plt.plot(y,x)
	plt.ylabel('Latitude')
	plt.xlabel(xlab)
	plt.savefig(name + '.png')

#Plot hi, ue, psi, zeta
lat = np.linspace(-pi/2.,pi/2.,500)
hi_eqm = h0 + dH * (1. - np.square(np.sin(lat)))
ue_eqm = u0 * np.square(np.sin(2.*lat)) * (np.square(np.sin(m*lat)) - 1/2.)
A = np.sin(2.*(m-2)*lat) / (8. * (m-2))
B = np.sin(2.*(m+2)*lat) / (8. * (m+2))
C = np.sin(2. * m * lat) / (4.*m)
psi_eqm = a * u0 * 0.5 * (A + B - C)
A = m * np.square(np.sin(2*lat)) * np.sin(m*lat) * np.cos(m*lat)
B = 2. * np.sin(2*lat) * np.cos(2*lat) * (np.square(np.sin(m*lat)) - 1/2.)
C = 0.5 * np.square(np.sin(2*lat)) * np.tan(lat) * (np.square(np.sin(m*lat)) - 1/2.)
zeta_eqm = 2.*u0 / a * (A + B - C)
plot_zonal(lat,hi_eqm,'hi','Fluid Height (m)')
plot_zonal(lat,ue_eqm,'ue','Wind Speed (m/s)')
plot_zonal(lat,psi_eqm,'psi','Streamfunction')
plot_zonal(lat,zeta_eqm,'zeta','Relative Vorticity')

############# CHECKS ##########
lat = np.linspace(-pi/2.,pi/2.,10000)
ue_eqm = u0 * np.square(np.sin(2.*lat)) * (np.square(np.sin(m*lat)) - 1/2.)
A = np.sin(2.*(m-2)*lat) / (8. * (m-2))
B = np.sin(2.*(m+2)*lat) / (8. * (m+2))
C = np.sin(2. * m * lat) / (4.*m)
psi_eqm = a * u0 * 0.5 * (A + B - C)
A = m * np.square(np.sin(2*lat)) * np.sin(m*lat) * np.cos(m*lat)
B = 2. * np.sin(2*lat) * np.cos(2*lat) * (np.square(np.sin(m*lat)) - 1/2.)
C = 0.5 * np.square(np.sin(2*lat)) * np.tan(lat) * (np.square(np.sin(m*lat)) - 1/2.)
zeta_eqm = 2.*u0 / a * (A + B - C)

deltalat = np.diff(lat)[0]
ue_from_psi = 1./a * np.gradient(psi_eqm) /deltalat
zeta_from_psi = 1./(a*a*np.cos(lat)) * np.gradient(np.cos(lat) * np.gradient(psi_eqm)) /deltalat / deltalat
zeta_from_ue = 1./(a*np.cos(lat)) * np.gradient(np.cos(lat) * ue_eqm) / deltalat

truncnum = 1
plot_zonal(lat,ue_from_psi ,'ue.psi','Wind Speed (m/s)')
plot_zonal(lat,ue_from_psi - ue_eqm,'ue.diff','Wind Speed (m/s)')
plot_zonal(lat[truncnum:-truncnum],zeta_from_psi[truncnum:-truncnum],'zeta.psi','Wind Speed (m/s)')
plot_zonal(lat[truncnum:-truncnum],zeta_from_ue[truncnum:-truncnum],'zeta.ue','Wind Speed (m/s)')
plot_zonal(lat[truncnum:-truncnum],zeta_eqm[truncnum:-truncnum],'zeta.trunc','Wind Speed (m/s)')
plot_zonal(lat[truncnum:-truncnum],zeta_from_psi[truncnum:-truncnum] - zeta_eqm[truncnum:-truncnum],'zeta.psi.diff','Wind Speed (m/s)')
plot_zonal(lat[truncnum:-truncnum],zeta_from_ue[truncnum:-truncnum] - zeta_eqm[truncnum:-truncnum],'zeta.ue.diff','Wind Speed (m/s)')
