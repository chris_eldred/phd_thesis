from __future__ import print_function
from constants import *
from mesh import *
from configuration import *
import sys
import time
import grid
import matplotlib.pyplot as plt
from scheme import *
from output import *

start = time.time()
#get configuration
get_configuration(sys.argv[1])
settings.tempname = settings.tempname +'.'+ settings.grid_type +'.'+ settings.grid_variant +'.'+ str(settings.vlevel) + '.' + settings.trisk_variant +'.' + settings.simname
print('configured')

#load operators and connectivity
if settings.grid_type == 'cubedsphere':
	set_grid_sizes_cubedsphere(grid.grid,settings.vlevel)
	allocate_connectivity_cubedsphere(grid.grid)
	if settings.scheme == 'ZGRID':
		allocate_geometry_cubedsphere(grid.grid)
	allocate_operators_cubedsphere(grid.grid)
if settings.grid_type == 'geodesic':
	set_grid_sizes_geodesic(grid.grid,settings.vlevel)
	allocate_connectivity_geodesic(grid.grid)
	allocate_operators_geodesic(grid.grid)
	if settings.scheme == 'ZGRID':
		allocate_geometry_geodesic(grid.grid)
if settings.grid_type == 'square':
	set_grid_sizes_square(grid.grid,settings.nx,settings.ny)
	allocate_connectivity_square(grid.grid)
	allocate_operators_square(grid.grid)
	if settings.scheme == 'ZGRID':
		allocate_geometry_square(grid.grid)
if settings.grid_type == 'hex':
	set_grid_sizes_hex(grid.grid,settings.nx,settings.ny)
	allocate_connectivity_hex(grid.grid)
	allocate_operators_hex(grid.grid)
	if settings.scheme == 'ZGRID':
		allocate_geometry_hex(grid.grid)
		
load_connectivity(grid.grid)
load_operators(grid.grid)
if settings.grid_type == 'square' or settings.grid_type == 'hex':
	settings.a = settings.dx
grid.grid.dimensionalize_operators(settings.a)
if settings.scheme == 'ZGRID':
	load_geometry(grid.grid)
	grid.grid.dimensionalize_geometry(settings.a)
#deallocate_unused(grid.grid)
print('operators and connectivity loaded!')

#create vars
create_scheme_vars()
print('vars created')

#initialize vars
initialize_scheme_vars()
print('init')
diagnose()
print('diag')
pre_compute_statistics()
print('pre')
compute_statistics(0)
print('stat')
create_scheme_var_dicts()
create_output_files_ntm()
output_constants()
output_vars(0,0)
print('vars initialized')

mid = time.time()

#take steps
print('timestepping:')
eulerstep(1)
diagnose()
compute_statistics(1)

eulerstep(2)
diagnose()
compute_statistics(2)

for i in range(3,settings.nsteps+1):
	ab3step(i)
	diagnose()
	compute_statistics(i)
	if (i % settings.output_frequency) == 0:
		create_scheme_var_dicts()
		output_vars(i/settings.output_frequency,i)
		print('vars output',i,i/settings.output_frequency)
output_statistics()
close_output_files_ntm()

end = time.time()
print('init time',mid - start)
print('run time', end - mid)
print('total time',end - start)

#do basic statistics on run
def plot_statistic(name,var):
	plt.figure()
	x = range(len(var))
	plt.plot(x,(var-var[0])/var[0])
	plt.savefig(settings.outdir + 'temp/' + name + '.' + settings.tempname + '.png')

for name,data in ntm.statistics.items():
	plot_statistic(name,data)
