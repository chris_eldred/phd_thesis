from basemap_plotting_helpers import *
import sys
import subprocess
from scipy.stats import binned_statistic

gridtypes = ['geodesic','cubedsphere']
gridvariant_dict = {'geodesic' : ['tweaked'] , 'cubedsphere': ['thuburn',]} #'cvt','spring_beta=1.1','spring_beta=0.8',
trisk_variants = ['TEQ','TE','Q'] 
title_list = ['Doubly Conservative','Energy Conserving','Enstrophy Conserving']
outdir = 'output'

dissipationtype = sys.argv[3]
dissipation = sys.argv[2]
scheme = sys.argv[1]
output_name = 'heldsuarez'

vlevels = [6,]
#steps = range(0,1201,50)
#stepnames = [] #['0','2','6','10']	
#for i in steps:
#	stepnames.append(str(i))
#print steps

hi_mean_levels = np.linspace(400,1200,9)
hi_std_levels = np.linspace(0,48,9)
zeta_mean_levels = np.linspace(-0.000024,0.000024,9)
zeta_std_levels = np.linspace(0,0.00008,9)
delta_mean_levels = np.linspace(-4e-8,4e-8,9)
delta_std_levels = np.linspace(0,8e-7,9)

variables_list = [('zetav','diagnosed','dual','Relative Vorticity','latlon','Relative Vorticity',zeta_mean_levels,zeta_std_levels),
('hi','diagnosed','primal','Fluid Height','latlon','Height (m)',hi_mean_levels,hi_std_levels),
('di','diagnosed','primal','Divergence','latlon','Divergence',delta_mean_levels,delta_std_levels)]
#('qv','diagnosed','dual','Potential Vorticity','latlon'),('etav','diagnosed','dual','Absolute Vorticity','latlon')]

#adjust some things for ZGRID
if scheme == 'ZGRID':
	trisk_variants = ['TEQ',]
	gridtypes = ['geodesic',]
	output_name = output_name + 'Z'
	title_list = ['Doubly Conservative Z',]
	variables_list = [('zeta','predicted','primal','Relative Vorticity','latlon','Relative Vorticity',zeta_mean_levels,zeta_std_levels),
('hi','predicted','primal','Fluid Height','latlon','Height (m)',hi_mean_levels,hi_std_levels),
('delta','predicted','primal','Divergence','latlon','Divergence',delta_mean_levels,delta_std_levels)]
	#('q','diagnosed','primal','Potential Vorticity','latlon'),('eta','diagnosed','primal','Absolute Vorticity','latlon')]

if dissipation == 'False':
	dissipdir = 'none'
if dissipation == 'True':
	if dissipationtype == 'hyperviscosity':
		dissipdir = 'hyper'
	if dissipationtype == 'APVM':
		dissipdir = 'apvm'

def plot_zonal_mean(datalist,datanamelist,lats,fname,name,xlab):
	plt.figure()
	plt.xlabel(xlab)
	plt.ylabel('Latitude')
	for data,dataname in zip(datalist,datanamelist):
		#print name,data.shape,np.amax(data),np.amin(data)
		plt.plot(data,lats,label=dataname)
	plt.title(name)
	plt.legend()
	plt.savefig(fname + '.png')
	
#####PLOTS########
# Make plotting directory
#subprocess.call(['mkdir','-p',outdir + '/' + output_name + '/plots/' + dissipdir])

bins = np.linspace(-80,80,320)
bins_adj = np.cumsum(np.diff(bins))
lats = bins_adj - 80 - bins_adj[0]/2.

for gridtype in gridtypes:
	for gridvariant in gridvariant_dict[gridtype]:
		for vlevel in vlevels:
			meshdata = get_geometry(gridtype,gridvariant,vlevel)
			statsfile = h5py.File(outdir +'/' + output_name + '/plots/' + gridtype + '.' + gridvariant + '.' + str(vlevel) + '.' + dissipdir + '.stats.hdf5', 'r')			
			for varname,vartype,varloc,longname,plottype,xlabel,levels_mean,levels_std in variables_list:
				for variant,title in zip(trisk_variants,title_list):
					#plot temporal averages
					print 'plotting mean + std',varname,variant

					mean = statsfile[variant][varname+'_mean']
					std = statsfile[variant][varname+'_stdev']

					plot_scalar(meshdata,mean,varloc,longname + ', ' +title, \
						outdir +'/' + output_name + '/plots/' + varname + '.mean.' + variant + '.' + gridtype + '.' + gridvariant + '.' + str(vlevel) + '.' + dissipdir,plottype,'contour',levels_mean)
					plot_scalar(meshdata,std,varloc,longname + ', ' +title, \
						outdir +'/' + output_name + '/plots/' + varname + '.std.' + variant + '.' + gridtype + '.' + gridvariant + '.' + str(vlevel) + '.' + dissipdir,plottype,'contour',levels_std)

				#plot zonal averages
				print 'plotting zonal mean + std',varname
				zonal_mean_list = []
				zonal_stddev_list = []
				for variant,title in zip(trisk_variants,title_list):
					zonal_mean_list.append(np.array(statsfile[variant][varname+'_zonalmean']))
					zonal_stddev_list.append(np.array(statsfile[variant][varname+'_zonalstdev']))

				if scheme == 'ZGRID':
					trisk_variants.append('TEQ-C')
					title_list.append('Doubly Conservative C')

					output_name_adj = output_name[:-1]
					#for cvariant in ['TEQ','TE']:
					cvariant = 'TEQ'
					if varname == 'zeta':
						adj_varname = 'zetav'
					if varname == 'delta':
						adj_varname = 'di'
					if varname == 'hi':
						adj_varname = 'hi'
					cstatsfile = h5py.File(outdir +'/' + output_name_adj + '/plots/' + gridtype + '.' + gridvariant + '.' + str(vlevel) + '.' + dissipdir + '.stats.hdf5', 'r')	
					zonal_mean_list.append(np.array(cstatsfile[cvariant][adj_varname+'_zonalmean']))
					zonal_stddev_list.append(np.array(cstatsfile[cvariant][adj_varname+'_zonalstdev']))
					cstatsfile.close()

				plot_zonal_mean(zonal_mean_list,trisk_variants,lats,outdir +'/' + output_name + '/plots/' + varname + '.zonalmean.' + 
					gridtype + '.' + gridvariant + '.' + str(vlevel) + '.' + dissipdir,varname,xlabel)
				plot_zonal_mean(zonal_stddev_list,trisk_variants,lats,outdir +'/' + output_name + '/plots/' + varname + '.zonalstd.' + 
					gridtype + '.' + gridvariant + '.' + str(vlevel) + '.' + dissipdir,varname,xlabel)

				if scheme == 'ZGRID':
					trisk_variants = ['TEQ',]
					title_list = ['Doubly Conservative Z',]
			statsfile.close()
