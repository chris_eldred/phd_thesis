from constants import *
import grid
import numpy as np
from mesh import *
from initialize import *

ab3_time_weights = [5./12. , -4./3. , 23./12.]
	
def diagnose():
	if settings.scheme == 'TRISK':
		if settings.grid_type == 'geodesic':
			psi,info = spsparselin.cgs(grid.A,grid.zeta,x0=grid.psi,tol=1e-16)
			if (info != 0):
				print 'solver not converging',info
			grid.psi = psi
		if settings.grid_type == 'cubedsphere':
			print grid.negD2bar.shape,grid.zeta.shape
			X,info = spsparselin.lgmres(grid.negD2bar,grid.zeta,tol=1e-16)
			psi,info = spsparselin.lgmres(grid.D1,grid.H * X,x0=grid.psi,tol=1e-16)
			if (info !=0):
				print 'solver not converging',info
			grid.psi = psi
	
	if settings.scheme == 'ZGRID':
		psi,info = spsparselin.lgmres(grid.A,grid.zeta,x0=grid.psi,tol=1e-16)
		if (info != 0):
			print 'solver not converging',info
		grid.psi = psi
		
def eulerstep(i):
	if settings.scheme == 'TRISK':
		rhs(i-1)
		grid.zeta = grid.zeta + settings.dt * grid.zeta_rhs_evals[i-1,:]
			
	if settings.scheme == 'ZGRID':
		rhs(i-1)
		grid.zeta = grid.zeta + settings.dt * grid.zeta_rhs_evals[i-1,:]

def ab3step(i):
	if settings.scheme == 'TRISK':
		rhs(2)
		for j in range(settings.ntlevels):
			grid.zeta = grid.zeta + ab3_time_weights[j] * settings.dt * grid.zeta_rhs_evals[j,:]
		grid.zeta_rhs_evals[0,:] = grid.zeta_rhs_evals[1,:]
		grid.zeta_rhs_evals[1,:] = grid.zeta_rhs_evals[2,:]
			
	if settings.scheme == 'ZGRID':
		rhs(2)
		for j in range(settings.ntlevels):
			grid.zeta = grid.zeta + ab3_time_weights[j] * settings.dt * grid.zeta_rhs_evals[j,:]
		grid.zeta_rhs_evals[0,:] = grid.zeta_rhs_evals[1,:]
		grid.zeta_rhs_evals[1,:] = grid.zeta_rhs_evals[2,:]
				
def rhs(i):
	if settings.scheme == 'TRISK':
		if settings.trisk_variant == 'TEQ':
			grid.zeta_rhs_evals[i,:] = grid.grid.zeta_trisk_bve_rhs_q(grid.zeta,grid.fv,grid.psi,settings.vlevel,grid.grid.nedge[-1])
		if settings.trisk_variant == 'TE':
			grid.zeta_rhs_evals[i,:] = grid.grid.zeta_trisk_bve_rhs_qte(grid.zeta,grid.fv,grid.psi,settings.vlevel,grid.grid.nedge[-1])
		if settings.trisk_variant == 'Q':
			grid.zeta_rhs_evals[i,:] = grid.grid.zeta_trisk_bve_rhs_qq(grid.zeta,grid.fv,grid.psi,settings.vlevel,grid.grid.nedge[-1])

	if settings.scheme == 'ZGRID':
		grid.zeta_rhs_evals[i,:] = grid.grid.zeta_zgrid_bve_rhs(grid.zeta,grid.fi,grid.psi,settings.vlevel)
		
def compute_statistics(n):
	if settings.scheme == 'TRISK':
		grid.grid.compute_trisk_bve_statistics(n+1,grid.zeta,grid.fv,grid.psi,grid.grid.nedge[-1]) #+1 is due to 0-based python vs. 1-based fortran indexing

	if settings.scheme == 'ZGRID':
		grid.grid.compute_zgrid_bve_statistics(n+1,grid.zeta,grid.fi,grid.psi,grid.grid.nedge[-1]) #+1 is due to 0-based python vs. 1-based fortran indexing

def initialize_scheme_vars():
	if settings.scheme == 'TRISK':
		grid.zeta,grid.fv = initialize_bve_vars()
		i = settings.vlevel - 1
		nedge = grid.grid.nedge[i]
		nvert = grid.grid.nvert[i]
		nface = grid.grid.nface[i]
		D2bar = create_scipy_sparse_matrix(grid.grid.ev[:nvert,:,i],grid.grid.evsize[:nvert,i],grid.grid.tev_vertex[:nvert,:,i],nvert,nedge)
		D1 =  create_scipy_sparse_matrix(grid.grid.ve[:nedge,:,i],np.ones(nedge,dtype=np.int32)*2,grid.grid.tev_edge[:nedge,:,i],nedge,nvert)
		J = create_scipy_sparse_matrix(grid.grid.j_stencil[:nvert,:,i],grid.grid.j_stencil_size[:nvert,i],grid.grid.j_coeffs[:nvert,:,i],nvert,nvert)
		H = create_scipy_sparse_matrix(grid.grid.h_stencil[:nedge,:,i],grid.grid.h_stencil_size[:nedge,i],grid.grid.h_coeffs[:nedge,:,i],nedge,nedge)
		if settings.grid_type == 'geodesic':
			Hinv = H.copy()
			Hinv.data = 1.0/Hinv.data
			grid.A = -1.0 * D2bar * Hinv * D1
		if settings.grid_type == 'cubedsphere':
			grid.negD2bar = -1.0 * D2bar
			grid.D1 = D1
			grid.H = H

	if settings.scheme == 'ZGRID':
		grid.zeta,grid.fi = initialize_bve_vars()
		i = settings.vlevel - 1
		nedge = grid.grid.nedge[i]
		nvert = grid.grid.nvert[i]
		nface = grid.grid.nface[i]
		D2 = create_scipy_sparse_matrix(grid.grid.ec[:nface,:,i],grid.grid.ecsize[:nface,i],grid.grid.nei_cell[:nface,:,i],nface,nedge)
		D1bar =  create_scipy_sparse_matrix(grid.grid.ce[:nedge,:,i],np.ones(nedge,dtype=np.int32)*2,-1*grid.grid.nei_edge[:nedge,:,i],nedge,nface)
		I = create_scipy_sparse_matrix(grid.grid.i_stencil[:nface,:,i],grid.grid.i_stencil_size[:nface,i],grid.grid.i_coeffs[:nface,:,i],nface,nface)
		H = create_scipy_sparse_matrix(grid.grid.h_stencil[:nedge,:,i],grid.grid.h_stencil_size[:nedge,i],grid.grid.h_coeffs[:nedge,:,i],nedge,nedge)
		grid.A = I * D2 * H * D1bar
		
def create_scheme_var_dicts():
	if settings.scheme == 'TRISK':
		ntm.predicted_vars['zeta'] = grid.zeta
		ntm.constants['fv'] = grid.fv
		ntm.statistics['EV'] = grid.grid.eta
		ntm.statistics['ZV'] = grid.grid.zeta
		ntm.statistics['KE'] = grid.grid.kinetic
		ntm.statistics['Q'] = grid.grid.enstrophy

	if settings.scheme == 'ZGRID':
		ntm.predicted_vars['zeta'] = grid.zeta
		ntm.constants['fi'] = grid.fi
		ntm.statistics['EV'] = grid.grid.eta
		ntm.statistics['ZV'] = grid.grid.zeta
		ntm.statistics['KE'] = grid.grid.kinetic
		ntm.statistics['Q'] = grid.grid.enstrophy

def create_scheme_vars():
	if settings.scheme == 'TRISK':
		grid.zeta_rhs_evals = np.zeros((settings.ntlevels,grid.grid.nvert[-1]),dtype=np.float64)
		grid.grid.eta = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.grid.zeta = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.grid.kinetic = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.grid.enstrophy = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.psi = np.zeros(grid.grid.nvert[-1],dtype=np.float64)

	if settings.scheme == 'ZGRID':
		grid.zeta_rhs_evals = np.zeros((settings.ntlevels,grid.grid.nface[-1]),dtype=np.float64)
		grid.grid.eta = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.grid.zeta = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.grid.kinetic = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.grid.enstrophy = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.psi = np.zeros(grid.grid.nface[-1],dtype=np.float64)
