import matplotlib.pyplot as plt
import numpy as np
import h5py
import sys
from configuration import *
from math import ceil,sqrt,pow
from basemap_plotting_helpers import *
from constants import *


#get configuration
get_configuration(sys.argv[1])
get_eigenconfig(sys.argv[1])
if settings.use_dissipation == False:
	disspdir = 'none'
if settings.use_dissipation == True:
	disspdir = 'hyper'
basedir = settings.outdir + settings.simname + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(settings.vlevel) + '-' + settings.trisk_variant + '/' + disspdir + '/'
outdir = settings.outdir + settings.simname + '/plots/' + disspdir + '/' + settings.grid_type + '.' + settings.grid_variant + '.' + str(settings.vlevel) +  '.' + settings.trisk_variant + '.' + disspdir + '.'

if settings.grid_type == 'geodesic':
	nfaces = 10*(2**settings.vlevel)**2 + 2
	nverts = 20*(2**settings.vlevel)**2
	nedges = 30*(2**settings.vlevel)**2
	
if settings.grid_type == 'cubedsphere':
	n0 = 3
	nfaces = int(pow(n0*pow(2,settings.vlevel-1),2) * 6)
	nedges = 2*nfaces
	nverts = nfaces + 2

if settings.scheme == 'TRISK':
	nstationary = nverts
	nrossby = nverts
	ninertia = 2*nfaces-2
	ndofs = nedges + nfaces
	operators = ['Div','Grad','Perp','A']

if settings.scheme == 'ZGRID':
	nstationary = nfaces
	nrossby = nfaces
	ninertia = 2*nfaces
	ndofs = 3*nfaces
	operators = ['Laplac','A','J','FD']


svdpostfile = h5py.File(basedir + 'svd.post.hdf5', 'r')
meshdata = get_geometry(settings.grid_type,settings.grid_variant,settings.vlevel)

varloc_dict = {'Laplac' : 'primal', 'Div' : 'edge', 'Grad': 'primal', 'Perp': 'edge', 'FD': 'primal', 'J' : 'primal'}

for operator in operators:
	try:
		svds = np.array(svdpostfile[operator])
	except:
		continue	
	if operator == 'A':
		for i in range(svds.shape[0]):
			if settings.scheme == 'TRISK':
				hi = svdpostfile[operator + '_vecs_decomp']['hi'+str(i)] 
				ue = svdpostfile[operator + '_vecs_decomp']['ue'+str(i)] 
				zeta = svdpostfile[operator + '_vecs_decomp']['zeta'+str(i)] 
				delta = svdpostfile[operator + '_vecs_decomp']['delta'+str(i)] 
				plot_scalar(meshdata,hi,'primal','Singular Vector Basis for hi', basedir + 'svd/A/hi.' + str(i),'spherical','grid',None)
				plot_scalar(meshdata,ue,'edge','Singular Vector Basis for ue', basedir + 'svd/A/ue.' + str(i),'spherical','grid',None)
				plot_scalar(meshdata,zeta,'dual','Singular Vector Basis for zetav', basedir + 'svd/A/zetav.' + str(i),'spherical','grid',None)
				plot_scalar(meshdata,delta,'primal','Singular Vector Basis for di', basedir + 'svd/A/delta.' + str(i),'spherical','grid',None)
			if settings.scheme == 'ZGRID':
				hi = svdpostfile[operator + '_vecs_decomp']['hi'+str(i)] 
				delta = svdpostfile[operator + '_vecs_decomp']['delta'+str(i)] 
				zeta = svdpostfile[operator + '_vecs_decomp']['zeta'+str(i)] 
				plot_scalar(meshdata,hi,'primal','Singular Vector Basis for hi', basedir + 'svd/A/hi.' + str(i),'spherical','grid',None)
				plot_scalar(meshdata,delta,'primal','Singular Vector Basis for delta', basedir + 'svd/A/delta.' + str(i),'spherical','grid',None)
				plot_scalar(meshdata,zeta,'primal','Singular Vector Basis for zeta', basedir + 'svd/A/zeta.' + str(i),'spherical','grid',None)
	else:
		varloc = varloc_dict[operator]
		for i in range(svds.shape[0]):
			if settings.scheme == 'TRISK' and varloc == 'edge':
				zeta = svdpostfile[operator + '_vecs_decomp']['zeta'+str(i)] 
				delta = svdpostfile[operator + '_vecs_decomp']['delta'+str(i)] 
				plot_scalar(meshdata,zeta,'dual','Singular Vector Basis for ' + operator, basedir + 'svd/' + operator + '.zetav.' + str(i),'spherical','grid',None)
				plot_scalar(meshdata,delta,'primal','Singular Vector Basis for ' + operator, basedir + 'svd/' + operator + '.di.' + str(i),'spherical','grid',None)
			else:
				data = svdpostfile[operator + '_vecs_decomp']['data'+str(i)] 
				plot_scalar(meshdata,data,varloc,'Singular Vector Basis for ' + operator, basedir + 'svd/' + operator + '.' + str(i),'spherical','grid',None)


svdpostfile.close()
