
from constants import *
from mesh import *
from configuration import *
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as spstats
from sympy import *
from sympy.utilities.lambdify import implemented_function
from math import pow
import h5py
import time
import grid
import sys

start = time.clock()
get_configuration(sys.argv[1])
print 'configured'

def l2_error(diff,Iinv):
	return np.sqrt(np.sum(Iinv * np.square(diff),dtype='float128')/Iinv.sum())
	
def linf_error(diff):
	return np.amax(np.abs(diff))

def grad(A):
	return 1/(rad*cos(theta)) * diff(A,lambda1), 1/rad * diff(A,theta) 

def div(A,B):
	return 1/(rad*cos(theta)) * (diff(A,lambda1) + diff(B * cos(theta),theta))
	
def jacob(A,B):
	return 1/(rad*rad*cos(theta)) * (diff(A,lambda1)*diff(B,theta) - diff(A,theta)*diff(B,lambda1))

def laplac(A):
	#t1 = 1/(a*a*cos(theta)*cos(theta))
	#t2 = 1/(a*a*cos(theta)) * diff(cos(theta) * diff(A,theta),theta)
	gradA = grad(A)
	return div(gradA[0],gradA[1])

grid_types_and_variants = [('geodesic','tweaked'),('geodesic','spring_beta=0.8'),('geodesic','spring_beta=1.1'),('geodesic','cvt'),('cubedsphere','thuburn')]
vlevels = [1,2,3,4,5,6,7]#5,6,7,8] #,6,7,8]
nmodels = len(grid_types_and_variants)

#operator accuracy tests- laplacian, H, W, Q
Lp0_l2_error_matrix = np.zeros((len(vlevels),nmodels))
Lp0_linf_error_matrix = np.zeros((len(vlevels),nmodels))
Ld0_l2_error_matrix = np.zeros((len(vlevels),nmodels))
Ld0_linf_error_matrix = np.zeros((len(vlevels),nmodels))
Le_l2_error_matrix = np.zeros((len(vlevels),nmodels))
Le_linf_error_matrix = np.zeros((len(vlevels),nmodels))
Wrot_l2_error_matrix = np.zeros((len(vlevels),nmodels))
Wrot_linf_error_matrix = np.zeros((len(vlevels),nmodels))
Wdiv_l2_error_matrix = np.zeros((len(vlevels),nmodels))
Wdiv_linf_error_matrix = np.zeros((len(vlevels),nmodels))
R_l2_error_matrix = np.zeros((len(vlevels),nmodels))
R_linf_error_matrix = np.zeros((len(vlevels),nmodels))
Phi_l2_error_matrix = np.zeros((len(vlevels),nmodels))
Phi_linf_error_matrix = np.zeros((len(vlevels),nmodels))
Q_Qrot_l2_error_matrix = np.zeros((len(vlevels),nmodels))
Q_Qrot_linf_error_matrix = np.zeros((len(vlevels),nmodels))
Q_TErot_l2_error_matrix = np.zeros((len(vlevels),nmodels))
Q_TErot_linf_error_matrix = np.zeros((len(vlevels),nmodels))
Q_TEQrot_l2_error_matrix = np.zeros((len(vlevels),nmodels))
Q_TEQrot_linf_error_matrix = np.zeros((len(vlevels),nmodels))
Q_Qdiv_l2_error_matrix = np.zeros((len(vlevels),nmodels))
Q_Qdiv_linf_error_matrix = np.zeros((len(vlevels),nmodels))
Q_TEdiv_l2_error_matrix = np.zeros((len(vlevels),nmodels))
Q_TEdiv_linf_error_matrix = np.zeros((len(vlevels),nmodels))
Q_TEQdiv_l2_error_matrix = np.zeros((len(vlevels),nmodels))
Q_TEQdiv_linf_error_matrix = np.zeros((len(vlevels),nmodels))
ki_div_l2_error_matrix = np.zeros((len(vlevels),nmodels))
ki_div_linf_error_matrix = np.zeros((len(vlevels),nmodels))
ki_rot_l2_error_matrix = np.zeros((len(vlevels),nmodels))
ki_rot_linf_error_matrix = np.zeros((len(vlevels),nmodels))
laplacZ_l2_error_matrix = np.zeros((len(vlevels),nmodels-1))
laplacZ_linf_error_matrix = np.zeros((len(vlevels),nmodels-1))
fluxdivZ_l2_error_matrix = np.zeros((len(vlevels),nmodels-1))
fluxdivZ_linf_error_matrix = np.zeros((len(vlevels),nmodels-1))
jacobianZ_l2_error_matrix = np.zeros((len(vlevels),nmodels-1))
jacobianZ_linf_error_matrix = np.zeros((len(vlevels),nmodels-1))
num_dofs = np.zeros((len(vlevels),nmodels))
diff_out_file = h5py.File('orderofaccuracy/errors.h5py','w')
for i,vlevel in enumerate(vlevels):
	for k,(gtype_and_var) in enumerate(grid_types_and_variants):
		if (gtype_and_var[1] == 'spring_beta=0.8' or gtype_and_var[1] == 'spring_beta=1.1'):
			if (vlevel > 5):
				Lp0_l2_error_matrix[i,k] = None
				Lp0_linf_error_matrix[i,k] = None
				Ld0_l2_error_matrix[i,k] = None
				Ld0_linf_error_matrix[i,k] = None
				Le_l2_error_matrix[i,k] = None
				Le_linf_error_matrix[i,k] = None
				Wrot_l2_error_matrix[i,k] = None
				Wrot_linf_error_matrix[i,k] = None
				Wdiv_l2_error_matrix[i,k] = None
				Wdiv_linf_error_matrix[i,k] = None
				R_l2_error_matrix[i,k] = None
				R_linf_error_matrix[i,k] = None
				Phi_l2_error_matrix[i,k] = None
				Phi_linf_error_matrix[i,k] = None
				Q_Qrot_l2_error_matrix[i,k] = None
				Q_Qrot_linf_error_matrix[i,k] = None
				Q_TErot_l2_error_matrix[i,k] = None
				Q_TErot_linf_error_matrix[i,k] = None
				Q_TEQrot_l2_error_matrix[i,k] = None
				Q_TEQrot_linf_error_matrix[i,k] = None
				Q_Qdiv_l2_error_matrix[i,k] = None
				Q_Qdiv_linf_error_matrix[i,k] = None
				Q_TEdiv_l2_error_matrix[i,k] = None
				Q_TEdiv_linf_error_matrix[i,k] = None
				Q_TEQdiv_l2_error_matrix[i,k] = None
				Q_TEQdiv_linf_error_matrix[i,k] = None
				num_dofs[i,k] = None
				continue
		print gtype_and_var[0],gtype_and_var[1],vlevel
		settings.grid_type = gtype_and_var[0]
		settings.grid_variant = gtype_and_var[1]
		settings.vlevel  = vlevel
		if settings.grid_type == 'cubedsphere':
			set_grid_sizes_cubedsphere(grid.grid,settings.vlevel)
			allocate_connectivity_cubedsphere(grid.grid)
			allocate_geometry_cubedsphere(grid.grid)
			allocate_operators_cubedsphere(grid.grid)
		if settings.grid_type == 'geodesic':
			set_grid_sizes_geodesic(grid.grid,settings.vlevel)
			#print 'sizes'
			allocate_connectivity_geodesic(grid.grid)
			#print 'connect'
			allocate_operators_geodesic(grid.grid)
			#print 'ops'
			allocate_geometry_geodesic(grid.grid)
			#print 'geom'
		load_connectivity(grid.grid)
		#print 'connect'
		load_operators(grid.grid)
		#print 'ops'
		grid.grid.dimensionalize_operators(settings.a)
		#print 'dimen'
		load_geometry(grid.grid)
		#print 'geo'
		grid.grid.dimensionalize_geometry(settings.a)
		#print 'dimen'

		meshfile = h5py.File(settings.meshdir + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(grid.grid.nface[settings.vlevel-1]) + '.hdf5','r')
		clat = np.array(meshfile['coords']['clat'])
		clon = np.array(meshfile['coords']['clon'])
		vlat = np.array(meshfile['coords']['vlat'])
		vlon = np.array(meshfile['coords']['vlon'])
		elat = np.array(meshfile['coords']['elat'])
		elon = np.array(meshfile['coords']['elon'])
		meshfile.close()
		#print 'mesh'
		num_dofs[i,k] = grid.grid.nface[vlevel-1]
		
		nface = grid.grid.nface[vlevel-1]
		nedge = grid.grid.nedge[vlevel-1]
		nvert =  grid.grid.nvert[vlevel-1]
		
		D1 =  create_scipy_sparse_matrix(grid.grid.ve[:nedge,:,i],np.ones(nedge,dtype=np.int32)*2,grid.grid.tev_edge[:nedge,:,i],nedge,nvert)
		D2 = create_scipy_sparse_matrix(grid.grid.ec[:nface,:,i],grid.grid.ecsize[:nface,i],grid.grid.nei_cell[:nface,:,i],nface,nedge)
		D1bar =  create_scipy_sparse_matrix(grid.grid.ce[:nedge,:,i],np.ones(nedge,dtype=np.int32)*2,-1*grid.grid.nei_edge[:nedge,:,i],nedge,nface)
		D2bar = create_scipy_sparse_matrix(grid.grid.ev[:nvert,:,i],grid.grid.evsize[:nvert,i],grid.grid.tev_vertex[:nvert,:,i],nvert,nedge)
		W = create_scipy_sparse_matrix(grid.grid.ecp[:nedge,:,i],grid.grid.ecpsize[:nedge,i],grid.grid.w_coeffs[:nedge,:,i],nedge,nedge)
		RT = create_scipy_sparse_matrix(grid.grid.rt_stencil[:nface,:,i],grid.grid.rt_stencil_size[:nface,i],grid.grid.rt_coeffs[:nface,:,i],nface,nvert)
		I = create_scipy_sparse_matrix(grid.grid.i_stencil[:nface,:,i],grid.grid.i_stencil_size[:nface,i],grid.grid.i_coeffs[:nface,:,i],nface,nface)
		J = create_scipy_sparse_matrix(grid.grid.j_stencil[:nvert,:,i],grid.grid.j_stencil_size[:nvert,i],grid.grid.j_coeffs[:nvert,:,i],nvert,nvert)
		Iinv = create_scipy_sparse_matrix(grid.grid.i_stencil[:nface,:,i],grid.grid.i_stencil_size[:nface,i],1.0/grid.grid.i_coeffs[:nface,:,i],nface,nface)
		Jinv = create_scipy_sparse_matrix(grid.grid.j_stencil[:nvert,:,i],grid.grid.j_stencil_size[:nvert,i],1.0/grid.grid.j_coeffs[:nvert,:,i],nvert,nvert)
		E = create_scipy_sparse_matrix(grid.grid.e_stencil[:nedge,:,i],grid.grid.e_stencil_size[:nedge,i],grid.grid.e_coeffs[:nedge,:,i],nedge,nedge)
		L = create_scipy_sparse_matrix(grid.grid.l_stencil[:nedge,:,i],grid.grid.l_stencil_size[:nedge,i],grid.grid.l_coeffs[:nedge,:,i],nedge,nedge)
		Einv = create_scipy_sparse_matrix(grid.grid.e_stencil[:nedge,:,i],grid.grid.e_stencil_size[:nedge,i],1.0/grid.grid.e_coeffs[:nedge,:,i],nedge,nedge)
		Linv = create_scipy_sparse_matrix(grid.grid.l_stencil[:nedge,:,i],grid.grid.l_stencil_size[:nedge,i],1.0/grid.grid.l_coeffs[:nedge,:,i],nedge,nedge)
		H = create_scipy_sparse_matrix(grid.grid.h_stencil[:nedge,:,i],grid.grid.h_stencil_size[:nedge,i],grid.grid.h_coeffs[:nedge,:,i],nedge,nedge)		
		Phi = create_scipy_sparse_matrix(grid.grid.ce[:nedge,:,i],np.ones(nedge,dtype=np.int32)*2,grid.grid.phi_coeffs[:nedge,:,i],nedge,nface)
		PhiT = create_scipy_sparse_matrix(grid.grid.ec[:nface,:,i],grid.grid.ecsize[:nface,i],grid.grid.phit_coeffs[:nface,:,i],nface,nedge)
		#print 'create'
		if settings.grid_type == 'geodesic':
			Hinv = create_scipy_sparse_matrix(grid.grid.h_stencil[:nedge,:,i],grid.grid.h_stencil_size[:nedge,i],1.0/grid.grid.h_coeffs[:nedge,:,i],nedge,nedge)		
			Lp0 = -1.0 * J * D2bar * Hinv * D1
			curlcurl = -1.0 * Hinv * D1 * J * D2bar
		#print 'hinv'
		Ld0 = I * D2 * H * D1bar
		graddiv = D1bar * I * D2 * H
		a = settings.a
		fd0 = np.cos(clat) * np.sin(clon)
		fp0 = np.cos(vlat) * np.sin(vlon)
		fe = np.cos(elat) * np.sin(elon)
		
		#laplacian tests
		if (settings.grid_type == 'geodesic'):
			fe1 = D1bar * fd0 - Hinv * D1 * fp0
		if (settings.grid_type == 'cubedsphere'):
			fe1 = D1bar * fd0 - apply_Hinv(D1 * fp0,H)
		l_p0_exact = -2.0 * fp0 
		l_d0_exact = -2.0 * fd0
		l_e_exact = -2.0 * fe1
	
		l_d0_numerical = Ld0 * fd0 * a * a
		
		if (settings.grid_type == 'geodesic'):
			l_p0_numerical = Lp0 * fp0 * a * a
			l_e_numerical = graddiv * fe1 * a * a + curlcurl * fe1 * a * a
		if (settings.grid_type == 'cubedsphere'):
			curlcurl_part = -1.0 * D1 * J * D2bar
			curlcurl = apply_Hinv(curlcurl_part * fe1 * a * a,H)
			l_e_numerical = graddiv * fe1 * a * a + curlcurl
			psi = apply_Hinv(D1 * fp0 * a * a,H)
			l_p0_numerical = -1.0 * J * D2bar * psi 
			
		diff_p0 = l_p0_numerical - l_p0_exact
		diff_d0 = l_d0_numerical -l_d0_exact
		diff_e = l_e_numerical - l_e_exact
		
		Lp0_l2_error_matrix[i,k] = l2_error(diff_p0,Jinv)
		Lp0_linf_error_matrix[i,k] = linf_error(diff_p0)
		Ld0_l2_error_matrix[i,k] = l2_error(diff_d0,Iinv)
		Ld0_linf_error_matrix[i,k] = linf_error(diff_d0)
		Le_l2_error_matrix[i,k] = l2_error(diff_e,Einv)
		Le_linf_error_matrix[i,k] = linf_error(diff_e)

		if vlevel == vlevels[-1]:
			diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/Lp0',data=diff_p0)
			diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/Ld0',data=diff_d0)
			diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/Le',data=diff_e)
		#print 'laplac'
		#W tests
		diff_divergent = D1 * a * fp0 + H * grid.grid.w(H * D1bar * a * fd0,vlevel)
		diff_rotational = D1bar * a * fd0 - grid.grid.w(D1 * a * fp0,vlevel)
		
		Wdiv_l2_error_matrix[i,k] = l2_error(L*diff_divergent,Linv)
		Wrot_l2_error_matrix[i,k] = l2_error(E*diff_rotational,Einv)
		Wdiv_linf_error_matrix[i,k] = linf_error(L*diff_divergent)
		Wrot_linf_error_matrix[i,k] = linf_error(E*diff_rotational)

		if vlevel == vlevels[-1]:
			diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/Wdiv',data=diff_divergent)
			diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/Wrot',data=diff_rotational)
		#print 'W'
			
		#R tests
		diff_R = RT * fp0 - fd0
		
		R_l2_error_matrix[i,k] = l2_error(diff_R,Iinv)
		R_linf_error_matrix[i,k] = linf_error(diff_R)
		
		if vlevel == vlevels[-1]:
			diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/R',data=diff_R)
		#print 'R'

		#Phi tests
		if settings.grid_type == 'geodesic':
			diff_Phi = grid.grid.phi(fd0,vlevel,nedge) - fe
		if settings.grid_type == 'cubedsphere':
			diff_Phi = grid.grid.phi(fd0,vlevel,nedge) + fe
		Phi_l2_error_matrix[i,k] = l2_error(diff_Phi,Einv)
		Phi_linf_error_matrix[i,k] = linf_error(diff_Phi)
		
		if vlevel == vlevels[-1]:
			diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/Phi',data=diff_Phi)
		#print 'Phi'
				
		#Q-TEQ tests		
		rot_exact = np.multiply(fe,D1bar * a * fd0)
		if settings.grid_type == 'geodesic':
			rot_num = -1.0 * grid.grid.q(D1 * a * fp0,fp0,vlevel)
		if settings.grid_type == 'cubedsphere':
			rot_num = grid.grid.q(D1 * a * fp0,fp0,vlevel)
		diff_rotational = rot_exact - rot_num
		#print 'rot q-teq'
		
		div_exact = np.multiply(fe,D1 * a * fp0)
		if settings.grid_type == 'geodesic':
			div_num = H * grid.grid.q(H*D1bar * a * fd0,fp0,vlevel)
		if settings.grid_type == 'cubedsphere':
			div_num = -1.0*H * grid.grid.q(H*D1bar * a * fd0,fp0,vlevel)
		diff_divergent = div_exact - div_num
		#print 'div q-teq'
		
		Q_TEQdiv_l2_error_matrix[i,k] = l2_error(L*diff_divergent,Linv)
		Q_TEQrot_l2_error_matrix[i,k] = l2_error(E*diff_rotational,Einv)
		Q_TEQdiv_linf_error_matrix[i,k] = linf_error(L*diff_divergent)
		Q_TEQrot_linf_error_matrix[i,k] = linf_error(E*diff_rotational)

		if vlevel == vlevels[-1]:
			diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/Q_TEQdiv',data=diff_divergent)
			diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/Q_TEQrot',data=diff_rotational)
		#print 'Q-TEQ'
			
		#Q-TE tests		
		rot_exact = np.multiply(fe,D1bar * a * fd0)
		if settings.grid_type == 'geodesic':
			rot_num = -1.0 * grid.grid.qte(D1 * a * fp0,fp0,vlevel)
		if settings.grid_type == 'cubedsphere':
			rot_num = grid.grid.qte(D1 * a * fp0,fp0,vlevel)
		diff_rotational = rot_exact - rot_num
	
		div_exact = np.multiply(fe,D1 * a * fp0)
		if settings.grid_type == 'geodesic':
			div_num = H * grid.grid.qte(H*D1bar * a * fd0,fp0,vlevel)
		if settings.grid_type == 'cubedsphere':
			div_num = -1.0*H * grid.grid.qte(H*D1bar * a * fd0,fp0,vlevel)
		diff_divergent = div_exact - div_num

		Q_TEdiv_l2_error_matrix[i,k] = l2_error(L*diff_divergent,Linv)
		Q_TErot_l2_error_matrix[i,k] = l2_error(E*diff_rotational,Einv)
		Q_TEdiv_linf_error_matrix[i,k] = linf_error(L*diff_divergent)
		Q_TErot_linf_error_matrix[i,k] = linf_error(E*diff_rotational)

		if vlevel == vlevels[-1]:
			diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/Q_TEdiv',data=diff_divergent)
			diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/Q_TErot',data=diff_rotational)
		#print 'Q-TE'
			
		#Q-Q tests		
		rot_exact = np.multiply(fe,D1bar * a * fd0)
		if settings.grid_type == 'geodesic':
			rot_num = -1.0 * grid.grid.qq(D1 * a * fp0,fp0,vlevel)
		if settings.grid_type == 'cubedsphere':
			rot_num = grid.grid.qq(D1 * a * fp0,fp0,vlevel)
		diff_rotational = rot_exact - rot_num
	
		div_exact = np.multiply(fe,D1 * a * fp0)
		if settings.grid_type == 'geodesic':
			div_num = H * grid.grid.qq(H*D1bar * a * fd0,fp0,vlevel)
		if settings.grid_type == 'cubedsphere':
			div_num = -1.0*H * grid.grid.qq(H*D1bar * a * fd0,fp0,vlevel)
		diff_divergent = div_exact - div_num
		
		Q_Qdiv_l2_error_matrix[i,k] = l2_error(L*diff_divergent,Linv)
		Q_Qrot_l2_error_matrix[i,k] = l2_error(E*diff_rotational,Einv)
		Q_Qdiv_linf_error_matrix[i,k] = linf_error(L*diff_divergent)
		Q_Qrot_linf_error_matrix[i,k] = linf_error(E*diff_rotational)

		if vlevel == vlevels[-1]:
			diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/Q_Qdiv',data=diff_divergent)
			diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/Q_Qrot',data=diff_rotational)
		#print 'Q-Q'
		
		#ki
		psi = a * np.cos(vlat) * np.sin(vlon)
		if settings.grid_type == 'geodesic':
			ue = -1.0 * Hinv * D1 * psi
		if settings.grid_type == 'cubedsphere':
			ue = apply_Hinv(-1.0 * D1 * psi,H)
		rot_exact = np.square(np.cos(clon)) + np.multiply(np.square(np.sin(clon)),np.square(np.sin(clat))) * 0.5
		rot_num = I * PhiT * np.multiply(ue,H * ue) * 0.5
		diff_rotational = rot_exact - rot_num
		
		chi = a * np.cos(clat) * np.sin(clon)
		ue = D1bar * chi
		div_exact = np.square(np.cos(clon)) + np.multiply(np.square(np.sin(clon)),np.square(np.sin(clat))) * 0.5
		div_num = I * PhiT * np.multiply(ue,H * ue) * 0.5
		diff_divergent = div_exact - div_num
		
		ki_div_l2_error_matrix[i,k] = l2_error(diff_divergent,Iinv)
		ki_rot_l2_error_matrix[i,k] = l2_error(diff_rotational,Iinv)
		ki_div_linf_error_matrix[i,k] = linf_error(diff_divergent)
		ki_rot_linf_error_matrix[i,k] = linf_error(diff_rotational)

		if vlevel == vlevels[-1]:
			diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/ki_div',data=diff_divergent)
			diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/ki_rot',data=diff_rotational)
			
		if gtype_and_var[0] == 'geodesic':
			theta = symbols('theta') #lat
			lambda1 = symbols('lambda1') #lon
			rad = symbols('rad')
			
			alpha = cos(theta)*cos(theta)*cos(theta) * sin(5*lambda1)
			beta = -rad*rad/2*cos(theta)*cos(theta)*cos(theta) * sin(3*lambda1)
			grad_beta = grad(beta)
			
			fluxdiv = div(alpha*grad_beta[0],alpha*grad_beta[1])
			jacobian = jacob(alpha,beta)
			laplacian = laplac(beta)
			
			fluxdiv_func = lambdify((theta,lambda1,rad),fluxdiv,modules='numpy')
			jacobian_func = lambdify((theta,lambda1,rad),jacobian,modules='numpy')
			laplacian_func = lambdify((theta,lambda1,rad),laplacian,modules='numpy')
			alpha_func = lambdify((theta,lambda1,rad),alpha,modules='numpy')
			beta_func = lambdify((theta,lambda1,rad),beta,modules='numpy')
			
			alpha_num = alpha_func(clat,clon,a)
			beta_num = beta_func(clat,clon,a)
			
			#zgrid laplacian
			laplac_exact = laplacian_func(clat,clon,a)
			laplac_num = grid.grid.laplac_primal(beta_num,vlevel)
			
			diff_laplac = laplac_num - laplac_exact
			laplacZ_l2_error_matrix[i,k] = l2_error(diff_laplac,Iinv)
			laplacZ_linf_error_matrix[i,k] = linf_error(diff_laplac)
			
			#zgrid flux divergence
			fluxdiv_exact = fluxdiv_func(clat,clon,a)
			fluxdiv_num = grid.grid.fluxdiv(alpha_num,beta_num,vlevel)
			
			diff_fluxdiv = fluxdiv_num - fluxdiv_exact
			fluxdivZ_l2_error_matrix[i,k] = l2_error(diff_fluxdiv,Iinv)
			fluxdivZ_linf_error_matrix[i,k] = linf_error(diff_fluxdiv)
			
			#zgrid jacobian
			jacobian_exact = jacobian_func(clat,clon,a)
			jacobian_num = grid.grid.jacobian(alpha_num,beta_num,vlevel)
			
			diff_jacobian = jacobian_num - jacobian_exact
			jacobianZ_l2_error_matrix[i,k] = l2_error(diff_jacobian,Iinv)
			jacobianZ_linf_error_matrix[i,k] = linf_error(diff_jacobian)
			
			if vlevel == vlevels[-1]:
				diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/laplacZ',data=diff_laplac)
				diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/fluxdivZ',data=diff_fluxdiv)
				diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/jacobianZ',data=diff_jacobian)
				
#plot operator accuracy
def plot_operator_accuracy(name,ndofs,error_matrix,label_list):
	print 'plotting',name
	plt.figure()
	x = np.arange(0.9*np.nanmin(ndofs),1.1*np.nanmax(ndofs),dtype=np.int32)
	maxerr = error_matrix[0,0]
	b1 = maxerr/pow(np.ravel(ndofs)[0],-1.)
	b2 = maxerr/pow(np.ravel(ndofs)[0],-2.)
	y1 = b1*np.power(x,-1.)
	y2 = b2*np.power(x,-2.)
	for i in range(error_matrix.shape[1]):
		plt.plot(ndofs[:,i],error_matrix[:,i],label=label_list[i])
		plt.scatter(ndofs[:,i],error_matrix[:,i]) 
	plt.plot(x,y1,linestyle='--',color='k')
	#plt.plot(x,y2,linestyle='--',color='k')
	plt.yscale('log')
	plt.xscale('log')
	plt.legend(loc=3)
	plt.ylabel('Error')
	plt.xlabel('Number of Degrees of Freedom')
	plt.savefig('orderofaccuracy/' + name + '.png')
	plt.close()

plot_operator_accuracy('Lp0.l2',num_dofs,Lp0_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Lp0.linf',num_dofs,Lp0_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Ld0.l2',num_dofs,Ld0_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Ld0.linf',num_dofs,Ld0_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Le.l2',num_dofs,Le_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Le.linf',num_dofs,Le_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Wrot.l2',num_dofs,Wrot_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Wrot.linf',num_dofs,Wrot_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Wdiv.l2',num_dofs,Wdiv_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Wdiv.linf',num_dofs,Wdiv_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('R.l2',num_dofs,R_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('R.linf',num_dofs,R_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Phi.l2',num_dofs,Phi_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Phi.linf',num_dofs,Phi_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Q_Qrot.l2',num_dofs,Q_Qrot_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Q_Qrot.linf',num_dofs,Q_Qrot_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Q_TErot.l2',num_dofs,Q_TErot_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Q_TErot.linf',num_dofs,Q_TErot_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Q_TEQrot.l2',num_dofs,Q_TEQrot_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Q_TEQrot.linf',num_dofs,Q_TEQrot_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Q_Qdiv.l2',num_dofs,Q_Qdiv_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Q_Qdiv.linf',num_dofs,Q_Qdiv_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Q_TEdiv.l2',num_dofs,Q_TEdiv_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Q_TEdiv.linf',num_dofs,Q_TEdiv_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Q_TEQdiv.l2',num_dofs,Q_TEQdiv_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Q_TEQdiv.linf',num_dofs,Q_TEQdiv_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('Q_TEQdiv.linf',num_dofs,Q_TEQdiv_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('ki_div.l2',num_dofs,ki_div_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('ki_div.linf',num_dofs,ki_div_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('ki_rot.l2',num_dofs,ki_rot_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('ki_rot.linf',num_dofs,ki_rot_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('laplacZ.l2',num_dofs,laplacZ_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt'])
plot_operator_accuracy('laplacZ.linf',num_dofs,laplacZ_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt'])
plot_operator_accuracy('fluxdivZ.l2',num_dofs,fluxdivZ_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt'])
plot_operator_accuracy('fluxdivZ.linf',num_dofs,fluxdivZ_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt'])
plot_operator_accuracy('jacobianZ.l2',num_dofs,jacobianZ_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt'])
plot_operator_accuracy('jacobianZ.linf',num_dofs,jacobianZ_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt'])
#mid = time.clock()




#print 'operator test time',mid - start
#print 'scheme test time',end - mid
#print 'total time',end - start
