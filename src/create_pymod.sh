#!/bin/bash

gfortran -c grid.f90 -shared -fopenmp -lgomp -fcheck=all
gcc -c mycpu.c -fPIC
f2py -c -m grid mycpu.o grid.f90 --f90flags="-shared -fopenmp -lgomp -fcheck=all" -lgomp 
#
#
