from constants import *
import numpy as np
import h5py

def create_output_files_ntm():
	settings.vars_file = h5py.File(settings.outdir + 'temp/vars.' + settings.tempname + '.hdf5', 'w')
	predicted_group = settings.vars_file.create_group('predicted')
	settings.vars_file.create_group('statistics')
	settings.vars_file.create_group('constants')
	
	nout = settings.nsteps / settings.output_frequency + 1
	settings.vars_file.create_dataset('timesteps',(nout,),dtype='i')
	
	for name,data in ntm.predicted_vars.items():
		predicted_group.create_dataset(name, (nout,data.shape[0]), dtype='f')


def close_output_files_ntm():
	settings.vars_file.close()

def output_constants():
	constants_group = settings.vars_file['constants']

	for name,data in ntm.constants.items():
		constants_group.create_dataset(name, data=data)

def output_vars(i,ts):
	predicted_group = settings.vars_file['predicted']
	dset = settings.vars_file['timesteps']
	dset[i] = ts
	
	for name,data in ntm.predicted_vars.items():
		dset = predicted_group[name]
		dset[i,:] = data
	
def output_statistics():
	statistics_group = settings.vars_file['statistics']
	for name,data in ntm.statistics.items():
		statistics_group.create_dataset(name, data=data)
