from constants import *
import scipy.linalg as linalg
import grid
import numpy as np
from mesh import *

def build_eigensystem_constant_f():
	nface = grid.grid.nface[-1]
	nedge = grid.grid.nedge[-1]
	nvert =  grid.grid.nvert[-1]
	i = grid.grid.ngrids - 1
	D2 = create_scipy_sparse_matrix(grid.grid.ec[:nface,:,i],grid.grid.ecsize[:nface,i],grid.grid.nei_cell[:nface,:,i],nface,nedge)
	D1bar =  create_scipy_sparse_matrix(grid.grid.ce[:nedge,:,i],np.ones(nedge,dtype=np.int32)*2,-1*grid.grid.nei_edge[:nedge,:,i],nedge,nface)
	D2bar = create_scipy_sparse_matrix(grid.grid.ev[:nvert,:,i],grid.grid.evsize[:nvert,i],grid.grid.tev_vertex[:nvert,:,i],nvert,nedge)
	D1 =  create_scipy_sparse_matrix(grid.grid.ve[:nedge,:,i],np.ones(nedge,dtype=np.int32)*2,grid.grid.tev_edge[:nedge,:,i],nedge,nvert)
	J = create_scipy_sparse_matrix(grid.grid.j_stencil[:nvert,:,i],grid.grid.j_stencil_size[:nvert,i],grid.grid.j_coeffs[:nvert,:,i],nvert,nvert)
	W = create_scipy_sparse_matrix(grid.grid.ecp[:nedge,:,i],grid.grid.ecpsize[:nedge,i],grid.grid.w_coeffs[:nedge,:,i],nedge,nedge)
	I = create_scipy_sparse_matrix(grid.grid.i_stencil[:nface,:,i],grid.grid.i_stencil_size[:nface,i],grid.grid.i_coeffs[:nface,:,i],nface,nface)
	E = create_scipy_sparse_matrix(grid.grid.e_stencil[:nedge,:,i],grid.grid.e_stencil_size[:nedge,i],grid.grid.e_coeffs[:nedge,:,i],nedge,nedge)
	Einv = create_scipy_sparse_matrix(grid.grid.e_stencil[:nedge,:,i],grid.grid.e_stencil_size[:nedge,i],1.0/grid.grid.e_coeffs[:nedge,:,i],nedge,nedge)
	H = create_scipy_sparse_matrix(grid.grid.h_stencil[:nedge,:,i],grid.grid.h_stencil_size[:nedge,i],grid.grid.h_coeffs[:nedge,:,i],nedge,nedge)	
	if settings.grid_type == 'geodesic':
		Hinv = create_scipy_sparse_matrix(grid.grid.h_stencil[:nedge,:,i],grid.grid.h_stencil_size[:nedge,i],1.0/grid.grid.h_coeffs[:nedge,:,i],nedge,nedge)	
	if settings.grid_type == 'cubedsphere':
		Hinv,rank = linalg.pinv2(H.todense(),return_rank=True)
	
	if settings.scheme == 'TRISK':
		Div = -1. * settings.Hbar * I * D2 * H * Einv
		Grad = -1. * settings.g * E * D1bar
		Perp =  settings.f * E * W * H * Einv
		if settings.use_dissipation == True:
			graddiv = settings.nu_gd * E * D1bar * I * D2 * H * Einv
			curlcurl = -1.0*settings.nu_cc * E * Hinv * D1 * J * D2bar * Einv
			ntm.eigensystem = (spsparse.bmat([[None,Div],[Grad,Perp + graddiv + curlcurl]],format='csr')).todense()
		else:
			ntm.eigensystem = (spsparse.bmat([[None,Div],[Grad,Perp]],format='csr')).todense()
		ntm.matrices['Div'] = Div.todense()
		ntm.matrices['Grad'] = Grad.todense()
		ntm.matrices['Perp'] = Perp.todense()
		ntm.matrices['A'] = np.copy(ntm.eigensystem)
		ntm.operator_list = ['Div','Grad','Perp','A']
		
	if settings.scheme == 'ZGRID':
		Laplac = I * D2 * H * D1bar
		ident = spsparse.eye(nface,format='csr')
		if settings.use_dissipation == True:
			ntm.eigensystem = (spsparse.bmat([[None,None,-1.0*settings.Hbar*ident],[None,settings.nu_cc*Laplac,-1.0*settings.f*ident],[-1.0*settings.g*Laplac,settings.f*ident,settings.nu_gd*Laplac]],format='csr')).todense()
		else:
			ntm.eigensystem = (spsparse.bmat([[None,None,-1.0*settings.Hbar*ident],[None,None,-1.0*settings.f*ident],[-1.0*settings.g*Laplac,settings.f*ident,None]],format='csr')).todense()
		ntm.matrices['Laplac'] = Laplac.todense()
		ntm.matrices['A'] = np.copy(ntm.eigensystem)
		ntm.operator_list = ['Laplac','A']

def build_eigensystem_variable_f():
	nface = grid.grid.nface[-1]
	nedge = grid.grid.nedge[-1]
	nvert =  grid.grid.nvert[-1]
	i = grid.grid.ngrids - 1
	D2 = create_scipy_sparse_matrix(grid.grid.ec[:nface,:,i],grid.grid.ecsize[:nface,i],grid.grid.nei_cell[:nface,:,i],nface,nedge)
	D1bar = create_scipy_sparse_matrix(grid.grid.ce[:nedge,:,i],np.ones(nedge,dtype=np.int32)*2,-1*grid.grid.nei_edge[:nedge,:,i],nedge,nface)
	W = create_scipy_sparse_matrix(grid.grid.ecp[:nedge,:,i],grid.grid.ecpsize[:nedge,i],grid.grid.w_coeffs[:nedge,:,i],nedge,nedge)
	I = create_scipy_sparse_matrix(grid.grid.i_stencil[:nface,:,i],grid.grid.i_stencil_size[:nface,i],grid.grid.i_coeffs[:nface,:,i],nface,nface)
	E = create_scipy_sparse_matrix(grid.grid.e_stencil[:nedge,:,i],grid.grid.e_stencil_size[:nedge,i],grid.grid.e_coeffs[:nedge,:,i],nedge,nedge)
	Einv = create_scipy_sparse_matrix(grid.grid.e_stencil[:nedge,:,i],grid.grid.e_stencil_size[:nedge,i],1.0/grid.grid.e_coeffs[:nedge,:,i],nedge,nedge)
	H = create_scipy_sparse_matrix(grid.grid.h_stencil[:nedge,:,i],grid.grid.h_stencil_size[:nedge,i],grid.grid.h_coeffs[:nedge,:,i],nedge,nedge)	
	D2bar = create_scipy_sparse_matrix(grid.grid.ev[:nvert,:,i],grid.grid.evsize[:nvert,i],grid.grid.tev_vertex[:nvert,:,i],nvert,nedge)
	D1 =  create_scipy_sparse_matrix(grid.grid.ve[:nedge,:,i],np.ones(nedge,dtype=np.int32)*2,grid.grid.tev_edge[:nedge,:,i],nedge,nvert)
	J = create_scipy_sparse_matrix(grid.grid.j_stencil[:nvert,:,i],grid.grid.j_stencil_size[:nvert,i],grid.grid.j_coeffs[:nvert,:,i],nvert,nvert)	
	meshfile = h5py.File(settings.meshdir + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(nface) + '.hdf5','r')
	vlat = np.array(meshfile['coords']['vlat'])
	clat = np.array(meshfile['coords']['clat'])
	meshfile.close()
	Phiv = create_scipy_sparse_matrix(grid.grid.ve[:nedge,:,i],np.ones(nedge,dtype=np.int32)*2,0.5*np.ones((nedge,2),dtype=np.float64),nedge,nvert)
	Phie = create_scipy_sparse_matrix(grid.grid.ce[:nedge,:,i],np.ones(nedge,dtype=np.int32)*2,0.5*np.ones((nedge,2),dtype=np.float64),nedge,nface)
	if settings.grid_type == 'geodesic':
		Hinv = create_scipy_sparse_matrix(grid.grid.h_stencil[:nedge,:,i],grid.grid.h_stencil_size[:nedge,i],1.0/grid.grid.h_coeffs[:nedge,:,i],nedge,nedge)	
	if settings.grid_type == 'cubedsphere':
		Hinv,rank = linalg.pinv2(H.todense(),return_rank=True)
	if settings.scheme == 'TRISK':
		Div = -1. * settings.Hbar * I * D2 * H * Einv
		Grad = -1. * settings.g * E * D1bar
		fv = 2.0 * settings.Omega * np.sin(vlat)
		fe = np.expand_dims(Phiv * fv,axis=0)
		femat = spsparse.dia_matrix((fe,[0,]),(nedge,nedge))
		if settings.trisk_variant == 'Q':
			Perp = E * femat * W * H * Einv
		if settings.trisk_variant == 'TE':
			Perp = 0.5*E * femat * W * H * Einv + 0.5 * E * W * femat * H * Einv
		if settings.trisk_variant == 'TEQ':
			QLcoeffs = grid.grid.compute_ql_coeffs(fv,i+1,nedge,np.amax(grid.grid.ecpsize))
			QL = create_scipy_sparse_matrix(grid.grid.ecp[:nedge,:,i],grid.grid.ecpsize[:nedge,i],QLcoeffs,nedge,nedge)
			Perp = E * QL * H * Einv
			#TEST THAT QL IS CORRECT
			#testvare = np.random.rand(nedge)
			#test = QL * testvare - grid.grid.q(testvare,fv,i+1)
			#print 'QL = Q',np.amax(test),np.amin(test),np.average(test)
		#Test Perp Operators
		#if settings.trisk_variant == 'Q':
			#rawPerp = femat * W
		#if settings.trisk_variant == 'TE':
			#rawPerp = 0.5*femat * W + 0.5 * W * femat
		#if settings.trisk_variant == 'TEQ':
			#rawPerp = QL
		#test = rawPerp + rawPerp.T
		#test = test.data
		#print 'QL^T = -QL',np.amax(test),np.amin(test),np.average(test)
		if settings.use_dissipation == True:
			graddiv = settings.nu_gd * E * D1bar * I * D2 * H * Einv
			curlcurl = -1.0*settings.nu_cc * E * Hinv * D1 * J * D2bar * Einv
			ntm.eigensystem = (spsparse.bmat([[None,Div],[Grad,Perp + graddiv + curlcurl]],format='csr')).todense()
		else:
			ntm.eigensystem = (spsparse.bmat([[None,Div],[Grad,Perp]],format='csr')).todense()
		ntm.matrices['A'] = np.copy(ntm.eigensystem)
		ntm.matrices['Perp'] = Perp.todense()
		ntm.matrices['Div'] = Div.todense()
		ntm.matrices['Grad'] = Grad.todense()
		ntm.operator_list = ['A','Perp','Div','Grad']
		
	if settings.scheme == 'ZGRID':
		D1 = create_scipy_sparse_matrix(grid.grid.ve[:nedge,:,i],np.ones(nedge,dtype=np.int32)*2,grid.grid.tev_edge[:nedge,:,i],nedge,nvert)
		RZ = create_scipy_sparse_matrix(grid.grid.cv[:nvert,:,i],grid.grid.evsize[:nvert,i],np.ones((nvert,3),dtype=np.float64)*1./3.,nvert,nface)
		fi = 2.0 * settings.Omega * np.sin(clat)
		fe = np.expand_dims(Phie * fi,axis=0)
		femat = spsparse.dia_matrix((fe,[0,]),(nedge,nedge))
		Laplac = I * D2 * H * D1bar
		Linv,rank = linalg.pinv2(Laplac.todense(),return_rank=True)
		#Linv = linalg.inv(Laplac.todense())
		d1rz = np.expand_dims(D1 * RZ * fi,axis=0)
		d1rzmat = spsparse.dia_matrix((d1rz,[0,]),(nedge,nedge))
		J = -1./2.*I * D2 * d1rzmat * Phie * Linv + 1./2.*I * D2 * femat * D1 * RZ * Linv
		FD = I * D2 * femat * H * D1bar * Linv
		#check that base FD, J and L are anti-symmetric
		#Lbase = D2 * H * D1bar
		#FDbase = D2 * femat * H * D1bar
		#Jbase = -1./2.*D2 * d1rzmat * Phie + 1./2.*D2 * femat * D1 * RZ
		#test = Lbase - Lbase.T
		#test = test.data
		#print 'L^T = L',np.amax(test),np.amin(test),np.average(test)
		#test = FDbase - FDbase.T
		#test = test.data
		#print 'FD^T = FD',np.amax(test),np.amin(test),np.average(test)
		#test = Jbase + Jbase.T
		#test = test.data
		#print 'J^T = -J^T',np.amax(test),np.amin(test),np.average(test)
		#test = Linv - Linv.T
		#test = test.sum()
		#print 'Linv^T = Linv',np.amax(test),np.amin(test),np.average(test)
		#test = Linv * Laplac
		#print test.diagonal()
		#test = test.diagonal().sum()
		#print 'Linv*L = I',np.amax(test),np.amin(test),np.average(test)
		#test = Laplac* Linv 
		#print test.diagonal()
		#test = test.diagonal().sum()
		#print 'L*Linv = I',np.amax(test),np.amin(test),np.average(test)
		ident = spsparse.eye(nface,format='csr')
		if settings.use_dissipation == True:
			ntm.eigensystem = (spsparse.bmat([[None,None,-1.0*settings.Hbar*ident],[None,J + settings.nu_cc*Laplac,-1.0*FD],[-1.0*settings.g*Laplac,FD,J + settings.nu_gd*Laplac]],format='csr')).todense()
		else:
			ntm.eigensystem = (spsparse.bmat([[None,None,-1.0*settings.Hbar*ident],[None,J,-1.0*FD],[-1.0*settings.g*Laplac,FD,J]],format='csr')).todense()
		ntm.matrices['A'] = np.copy(ntm.eigensystem)
		ntm.matrices['J'] = J
		ntm.matrices['FD'] = FD
		ntm.matrices['Laplac'] = Laplac.todense()
		ntm.operator_list = ['A','J','FD','Laplac']
	
def solve_eigensystem():
	eigenfile = h5py.File(settings.outdir + 'temp/eigen.' + settings.tempname + '.hdf5', 'w')
	eigenmatrix = ntm.eigensystem
	w,vr = linalg.eig(eigenmatrix)
	print len(w)
	eigenfile.create_dataset('w',data=w)
	#eigenfile.create_dataset('vr',data=vr)
	for i in range(len(w)):
		eigenfile.create_dataset('vr'+str(i),data=vr[:,i])
	eigenfile.close()
	
def solve_svd():
	svdfile = h5py.File(settings.outdir + '/temp/svd.' + settings.tempname + '.hdf5', 'w')
	for operator in ntm.operator_list:
		op = ntm.matrices[operator] #.todense()
		try:
			U,s,Vh = linalg.svd(op)
			svdop = svdfile.create_group(operator)
			svdop.create_dataset('s',data=s)
			svdop.create_dataset('U',data=U)
			svdop.create_dataset('Vh',data=Vh)
		except linalg.LinAlgError:
			print operator,' did not converge!'
	svdfile.close()
