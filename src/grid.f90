MODULE grid

IMPLICIT NONE
! ================================================================

! COUNTING

! Number of grids in multigrid hierarchy
INTEGER :: ngrids

! nface        Number of faces on each grid
! nedge        Number of edges on each gridcp
! nvert        Number of vertices on each edge 
INTEGER, ALLOCATABLE :: nface(:), nedge(:), nvert(:)

! ================================================================

! CONNECTIVITY AND ORIENTATION
INTEGER, ALLOCATABLE :: ecsize(:,:), evsize(:,:), ecpsize(:,:), ecpdsize(:,:)

INTEGER, ALLOCATABLE :: vc(:,:,:), ec(:,:,:), nc(:,:,:), cv(:,:,:), ev(:,:,:), ce(:,:,:), ve(:,:,:)
INTEGER, ALLOCATABLE :: ecp(:,:,:), ecpd(:,:,:)

INTEGER, ALLOCATABLE :: nei_cell(:,:,:), tev_vertex(:,:,:)
INTEGER, ALLOCATABLE :: nei_edge(:,:,:), tev_edge(:,:,:)

! ================================================================

! MULTIGRID
INTEGER, ALLOCATABLE :: inj_stencil_size(:,:), inj_stencil(:,:,:)
REAL*8, ALLOCATABLE :: inj_weights(:,:,:)
REAL*8, ALLOCATABLE :: diagcoeff(:,:), laplac_diag_coeffs(:,:), laplac_dual_diag_coeffs(:,:)
! ================================================================

! GEOMETRY
REAL*8, ALLOCATABLE :: ai(:,:), av(:,:), ae(:,:), le(:,:), de(:,:), aie(:,:,:), aiv(:,:,:)
REAL*8, ALLOCATABLE :: clon(:,:), clat(:,:)

! ================================================================
! OPERATORS
INTEGER, ALLOCATABLE :: i_stencil_size(:,:), j_stencil_size(:,:), h_stencil_size(:,:), r_stencil_size(:,:)
INTEGER, ALLOCATABLE :: q_stencil_size(:,:), e_stencil_size(:,:), l_stencil_size(:,:), rt_stencil_size(:,:)

INTEGER, ALLOCATABLE :: i_stencil(:,:,:), j_stencil(:,:,:), h_stencil(:,:,:), r_stencil(:,:,:)
INTEGER, ALLOCATABLE :: q_stencil(:,:,:,:), e_stencil(:,:,:), l_stencil(:,:,:), rt_stencil(:,:,:)

REAL*8, ALLOCATABLE :: i_coeffs(:,:,:), j_coeffs(:,:,:), h_coeffs(:,:,:), r_coeffs(:,:,:), q_coeffs(:,:,:)
REAL*8, ALLOCATABLE :: w_coeffs(:,:,:), phi_coeffs(:,:,:), phiT_coeffs(:,:,:),  e_coeffs(:,:,:), l_coeffs(:,:,:)
REAL*8, ALLOCATABLE :: wl_coeffs(:,:,:), wd_coeffs(:,:,:), rt_coeffs(:,:,:)
REAL*8, ALLOCATABLE :: laplac_primal_coeffs(:,:,:)
INTEGER :: h_stencil_max
REAL*8, ALLOCATABLE :: heinv(:,:) , hvinv(:,:),  heinv2(:,:),  hvinv2(:,:)
! ================================================================

! ================================================================
! STATISTICS
REAL*8, ALLOCATABLE :: mc(:), mv(:), eta(:), zeta(:), total(:), kinetic(:), potential(:), enstrophy(:)
REAL*8 :: flatpe
REAL*8, ALLOCATABLE :: bih(:)
REAL*8, PARAMETER :: pi = 3.14159265358979323d0
! ================================================================

CONTAINS

!================= GENERAL HELPER ROUTINES =======================!


! ================================================================
SUBROUTINE find_index(target_value,arr,narr,loc)
IMPLICIT NONE
INTEGER, INTENT(IN) :: target_value, arr(narr)
INTEGER, INTENT(IN) :: narr
INTEGER, INTENT(OUT) :: loc
INTEGER :: i

DO i = 1, narr
    IF (arr(i) == target_value) THEN
        loc = i
        EXIT
    ENDIF
ENDDO
END SUBROUTINE find_index
! ================================================================

! ================================================================
SUBROUTINE LL2XYZ(LONG,LAT,X,Y,Z)
!     To convert longitude and latitude to cartesian coordinates
!     on the unit sphere
IMPLICIT NONE
REAL*8, INTENT(IN) :: LONG,LAT
REAL*8, INTENT(OUT) :: X,Y,Z
REAL*8 :: CLN,SLN,CLT,SLT

SLN=SIN(LONG)
CLN=COS(LONG)
SLT=SIN(LAT)
CLT=COS(LAT)
X=CLN*CLT
Y=SLN*CLT
Z=SLT

END SUBROUTINE LL2XYZ
! ================================================================

! ================================================================
FUNCTION kahan_sum(arr,lenarr) RESULT(sumv)
IMPLICIT NONE
REAL*8, INTENT(IN) :: arr(lenarr)
INTEGER, INTENT(IN) :: lenarr
REAL*8 :: sumv
REAL*8 :: c,y,t
INTEGER :: i

sumv = 0.0d0
!$OMP PARALLEL DO PRIVATE(i) REDUCTION(+:sumv)
DO i=1,lenarr
sumv = sumv + arr(i)
ENDDO
!$OMP END PARALLEL DO

!sumv=0.0d0
!c=0.0d0
!DO i=1,lenarr
!y = arr(i) - c
!t = sumv + y
!c = (t - sumv) - y
!sumv = t
!ENDDO
!sumv = sumv - c

END FUNCTION kahan_sum
! ================================================================
!serial_kahan_sum

!! ================================================================
!RECURSIVE FUNCTION parallel_sum(arr,lenarr,nthreads) RESULT(sumv)
!IMPLICIT NONE
!REAL*8, INTENT(IN) :: arr(lenarr)
!INTEGER, INTENT(IN) :: lenarr, nthreads
!REAL*8 :: sumv
!INTEGER :: half,half_threads
!REAL*8 :: x,y

!IF (nthreads <= 1) THEN
!sumv = serial_kahan_sum(arr,lenarr)

!ELSE

!half = lenarr/2
!half_threads = nthreads/2
!!$OMP TASK SHARED(x)
!x = parallel_sum(arr(:half),half,half_threads)
!!$OMP END TASK
!!$OMP TASK SHARED(y)
!y = parallel_sum(arr(half:lenarr),lenarr - half,nthreads - half_threads)
!!$OMP END TASK
!!$OMP TASKWAIT
!sumv = x + y
!ENDIF

!END FUNCTION parallel_sum
!! ================================================================

!! ================================================================
!FUNCTION kahan_sum(arr,lenarr) RESULT(sumv)
!USE omp_lib
!IMPLICIT NONE
!REAL*8, INTENT(IN) :: arr(lenarr)
!INTEGER, INTENT(IN) :: lenarr
!REAL*8 :: sumv
!INTEGER :: nthreads

!nthreads = omp_get_num_threads()
!!$OMP PARALLEL
!!$OMP SINGLE 
!sumv = parallel_sum(arr,lenarr,nthreads)
!!$OMP END SINGLE NOWAIT
!!$OMP END PARALLEL

!END FUNCTION kahan_sum
!! ================================================================




!================= TRISK SPECIFIC STUFF =======================!
! ================================================================
SUBROUTINE precompute_TRISK_statistics(mi,bi,a,g,nf)
USE omp_lib
IMPLICIT NONE
INTEGER, INTENT(IN) :: nf
REAL*8, INTENT(IN) :: mi(nf), bi(nf)
REAL*8, INTENT(IN) :: a,g
REAL*8 :: mibar(nf), hibar(nf), bibar(nf), bihbar(nf)
integer :: id,cpuid,nthreads
!integer :: findmycpu
!external findmycpu

mibar = kahan_sum(mi,nf) / (4.d0 * pi * a * a)
bibar = kahan_sum(bi,nf) / (4.d0 * pi * a * a)
CALL Iinv(mibar,hibar,ngrids,nf)
CALL Iinv(bibar,bihbar,ngrids,nf)

flatpe = kahan_sum(( g * (hibar + bihbar) * (mibar + bibar) / 2.0d0),nf)

CALL I(bi,bih,ngrids,nf)

   !$omp parallel private(id)
   id = omp_get_thread_num()
   !cpuid = findmycpu()
   !write (*,*) 'Hello World from thread', id, 'on cpu',cpuid
   !$omp barrier
   if ( id == 0 ) then
     nthreads = omp_get_num_threads()
     write (*,*) 'There are', nthreads, 'threads'
   end if
   !$omp end parallel
   
END SUBROUTINE precompute_TRISK_statistics
! ================================================================

! ================================================================
SUBROUTINE compute_TRISK_statistics(n,mi,bi,ue,fv,g,nf,nv,ne)
IMPLICIT NONE
INTEGER, INTENT(IN) :: n, nf,ne,nv
REAL*8, INTENT(IN) :: mi(nf),bi(nf),fv(nv),ue(ne)
REAL*8, INTENT(IN) :: g
REAL*8 :: hv(nv), zetav(nv), me(ne), Hue(ne), hi(nf), etav(nv)

CALL I(mi,hi,ngrids,nf)
call R(mi,hv,ngrids,nf,nv)
call D2bar(ue,zetav,ngrids,ne,nv)
call phi(hi,me,ngrids,nf,ne)
call H(ue,Hue,ngrids,ne)

!$OMP PARALLEL WORKSHARE
etav = zetav + fv
!$OMP END PARALLEL WORKSHARE

mc(n) = kahan_sum(mi,nf)
mv(n) = kahan_sum(hv,nv)
eta(n) = kahan_sum(etav,nv)
zeta(n) = kahan_sum(zetav,nv)
kinetic(n) = kahan_sum(0.5d0 * (me * ue) * Hue,ne)
potential(n) = kahan_sum(0.5d0 * g * (mi+bi) * (hi+bih),nf) - flatPE
total(n) = kinetic(n) + potential(n)
enstrophy(n) = kahan_sum(0.5d0 * etav * etav / hv,nv)

!write(*,*) 'f2py',n,kinetic(n),potential(n)

END SUBROUTINE compute_TRISK_statistics
! ================================================================

! ================================================================
SUBROUTINE W(f1,f2,igrid,ne)
IMPLICIT NONE

! Apply the perp W operator that converts dual edge
! integrals (circulations) f1 to dual edge integrals (fluxes) f2
! on grid igrid.

INTEGER, INTENT(IN) :: igrid, ne
REAL*8, INTENT(IN) :: f1(ne)
REAL*8, INTENT(OUT) :: f2(ne)
INTEGER :: ie1, ie2, ix
REAL*8 :: temp

!$OMP PARALLEL DO SHARED(f1,f2,igrid,ne) PRIVATE(ie1,ix,temp,ie2)
DO ie1 = 1, ne
  temp = 0.0d0
  DO ix = 1, ecpsize(ie1,igrid)
    ie2 = ecp(ie1,ix,igrid)
    temp = temp + f1(ie2)*w_coeffs(ie1,ix,igrid)
  ENDDO
  f2(ie1) = temp
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE W
! ================================================================

! ================================================================
SUBROUTINE Wd(f1,f2,igrid,ne)
IMPLICIT NONE

! Apply the perp Wd operator that converts dual edge
! integrals (circulations) f1 to dual edge integrals (fluxes) f2
! on grid igrid.
! This operates on the dual grid (so meanings above should be flipped)

INTEGER, INTENT(IN) :: igrid, ne
REAL*8, INTENT(IN) :: f1(ne)
REAL*8, INTENT(OUT) :: f2(ne)
INTEGER :: ie1, ie2, ix
REAL*8 :: temp

!$OMP PARALLEL DO SHARED(f1,f2,igrid,ne) PRIVATE(ie1,ix,temp,ie2)
DO ie1 = 1, ne
  temp = 0.0d0
  DO ix = 1, ecpdsize(ie1,igrid)
    ie2 = ecpd(ie1,ix,igrid)
    temp = temp + f1(ie2)*wd_coeffs(ie1,ix,igrid)
  ENDDO
  f2(ie1) = temp
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE Wd
! ================================================================

! ================================================================
SUBROUTINE graddiv(e1,e2,igrid,ne,nf)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid, ne, nf
REAL*8, INTENT(IN) :: e1(ne)
REAL*8, INTENT(OUT) :: e2(ne)
REAL*8 :: temp1(ne), temp2(nf), temp3(nf)

! Compute graddiv e1 = D1bar I D2 H e1, store result in e2
call H(e1,temp1,igrid,ne)
call D2(temp1,temp2,igrid,ne,nf)
call I(temp2,temp3,igrid,nf)
call D1bar(temp3,e2,igrid,nf,ne)

END SUBROUTINE graddiv
! ================================================================

! ================================================================
SUBROUTINE curlcurl(e1,e2,igrid,ne,nv)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid, ne, nv
REAL*8, INTENT(IN) :: e1(ne)
REAL*8, INTENT(OUT) :: e2(ne)
REAL*8 :: temp1(nv), temp2(nv), temp3(ne)

! Compute curlcurl e1 = -1 * Hinv * D1 * J * D2bar e1, store result in e2
call D2bar(e1,temp1,igrid,ne,nv)
call J(temp1,temp2,igrid,nv)
call D1(temp2,temp3,igrid,nv,ne)
call Hinv(temp3,e2,igrid,ne)
!$OMP PARALLEL WORKSHARE
e2 = -1.0d0 * e2 
!$OMP END PARALLEL WORKSHARE

END SUBROUTINE curlcurl
! ================================================================

! ================================================================
SUBROUTINE curlcurl_part(e1,e2,igrid,ne,nv)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid, ne, nv
REAL*8, INTENT(IN) :: e1(ne)
REAL*8, INTENT(OUT) :: e2(ne)
REAL*8 :: temp1(nv), temp2(nv)

! Compute curlcurl_part e1 = -1 * D1 * J * D2bar e1, store result in e2
call D2bar(e1,temp1,igrid,ne,nv)
call J(temp1,temp2,igrid,nv)
call D1(temp2,e2,igrid,nv,ne)
!$OMP PARALLEL WORKSHARE
e2 = -1.0d0 * e2
!$OMP END PARALLEL WORKSHARE

END SUBROUTINE curlcurl_part
! ================================================================

! ================================================================
SUBROUTINE hypervisocity(e1,e2,igrid,ne,nf,nv)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid, ne, nv, nf
REAL*8, INTENT(IN) :: e1(ne)
REAL*8, INTENT(OUT) :: e2(ne)
REAL*8 :: temp(ne)

! Compute curlcurl e1 + graddiv e1 = -1 * Hinv * D1 * J * D2bar e1 + D1bar I D2 H e1, stores result in e2
CALL curlcurl(e1,e2,igrid,ne,nv)
CALL graddiv(e1,temp,igrid,ne,nf)
!$OMP PARALLEL WORKSHARE
e2 = e2 + temp
!$OMP END PARALLEL WORKSHARE

END SUBROUTINE hypervisocity
! ================================================================

! ================================================================
SUBROUTINE apvm(ue,qv,e1,c,igrid,ne,nv)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid, ne, nv
REAL*8, INTENT(IN) :: ue(ne), qv(nv), c
REAL*8, INTENT(OUT) :: e1(ne)
REAL*8 :: WHue(ne), grad_qv_s(ne), grad_qv_m(ne), temp_e(ne), apv_e(ne)
REAL*8:: temp, fve,fveprime
INTEGER :: ie1,ix,ie2

! Compute apvm of ue, stores result in e1

CALL D1(qv,temp_e,igrid,nv,ne)
CALL Hinv(temp_e,grad_qv_s,igrid,ne)

!temp_e = 0.0d0 
CALL Wd(grad_qv_s,temp_e,igrid,ne)
CALL Hinv(temp_e,grad_qv_m,igrid,ne)

!temp_e = 0.0d0
CALL H(ue,temp_e,igrid,ne)
CALL W(temp_e,WHue,igrid,ne)

!$OMP PARALLEL WORKSHARE
temp_e = WHue * grad_qv_s + ue * grad_qv_m
temp_e = -1.d0 * temp_e * c
!$OMP END PARALLEL WORKSHARE

CALL E(temp_e,apv_e,igrid,ne) !takes us from edge integrated to point values, as required (qe is a 0-form/point value)

!actually compute avpm effect as 0.5 W de + 0.5 de W
!$OMP PARALLEL DO SHARED(ue,qv,e1,c,igrid,ne,nv) PRIVATE(ie1,temp,fve,fveprime,ie2)
DO ie1 = 1, ne
  temp = 0.0d0
  fve = apv_e(ie1)
  DO ix = 1, ecpsize(ie1,igrid)
    ie2 = ecp(ie1,ix,igrid)
    fveprime = apv_e(ie2)
    temp = temp + 0.5d0*w_coeffs(ie1,ix,igrid)*(fve+fveprime)
  ENDDO
  e1(ie1) = temp
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE apvm
! ================================================================

! ================================================================
SUBROUTINE qv_diagnose(f1,e1,v1,v2,igrid,nf,ne,nv)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid, ne, nf, nv
REAL*8, INTENT(IN) :: f1(nf),e1(ne),v1(nv)
REAL*8, INTENT(OUT) :: v2(nv)
REAL*8 :: temp1(nv), temp2(nv)

! Diagnose qv= (D2bar*e1 + v1)/(R*f1) , store result in v1

call D2bar(e1,temp1,igrid,ne,nv) 
call R(f1,temp2,igrid,nf,nv)
!$OMP PARALLEL WORKSHARE
v2 = (temp1 + v1)/temp2
!$OMP END PARALLEL WORKSHARE

END SUBROUTINE qv_diagnose
! ================================================================

! ================================================================
SUBROUTINE hce_diagnose(f1,e1,e2,igrid,nf,ne)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid, ne, nf
REAL*8, INTENT(IN) :: f1(nf),e1(ne)
REAL*8, INTENT(OUT) :: e2(ne)
REAL*8 :: temp1(nf), temp2(ne)

! Diagnose H*ce where ce = (Phi*I*f1) * e1, store result in e2

call I(f1,temp1,igrid,nf)
call Phi(temp1,temp2,igrid,nf,ne)
!$OMP PARALLEL WORKSHARE
temp2 = temp2 * e1
!$OMP END PARALLEL WORKSHARE

call H(temp2,e2,igrid,ne)

END SUBROUTINE hce_diagnose
! ================================================================

! ================================================================
SUBROUTINE iphii_diagnose(f1,e1,f2,f3,igrid,nf,ne,g)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid, ne, nf
REAL*8, INTENT(IN) :: f1(nf),f2(nf),e1(ne)
REAL*8, INTENT(OUT) :: f3(nf)
REAL*8 :: temp1(nf),temp2(ne),temp3(nf),temp4(nf)
REAL*8 :: g
! Diagnose I*phii where phii = Ki + g*(f1+f2) and Ki = (e1 * He1)/2 , store result in f3

!I g(mi+bi)
!$OMP PARALLEL WORKSHARE
temp3 = g*(f1+f2)
!$OMP END PARALLEL WORKSHARE

call I(temp3,temp1,igrid,nf)
!I Ki
call H(e1,temp2,igrid,ne)
!$OMP PARALLEL WORKSHARE
temp2 = e1 * temp2
!$OMP END PARALLEL WORKSHARE
call PhiT(temp2,temp3,igrid,ne,nf)
call I(temp3,temp4,igrid,nf)

!$OMP PARALLEL WORKSHARE
f3 = temp1 + 0.5d0*temp4
!$OMP END PARALLEL WORKSHARE

END SUBROUTINE iphii_diagnose
! ================================================================

! ================================================================
SUBROUTINE QTE(f1,v1,f2,igrid,ne,nv)
IMPLICIT NONE

! Apply the bilinear Q operator that converts dual edge
! integrals (circulations) f1 and vertex values v1 to dual edge integrals (fluxes) f2
! on grid igrid.
! This version conserves only total energy

INTEGER, INTENT(IN) :: igrid, ne, nv
REAL*8, INTENT(IN) :: f1(ne), v1(nv)
REAL*8, INTENT(OUT) :: f2(ne)
INTEGER :: ie1, ie2, ix, iv1, iv2, iv3, iv4
REAL*8 :: temp, fve, fveprime

!$OMP PARALLEL DO SHARED(f1,f2,v1,igrid,ne,nv) PRIVATE(ie1,ie2,ix,iv1,iv2,iv3,iv4,temp,fve,fveprime)
DO ie1 = 1, ne
  temp = 0.0d0
  iv1 = ve(ie1,1,igrid)
  iv2 = ve(ie1,2,igrid)
  fve = 0.5d0*(v1(iv1) + v1(iv2))
  DO ix = 1, ecpsize(ie1,igrid)
    ie2 = ecp(ie1,ix,igrid)
    iv3 = ve(ie2,1,igrid)
    iv4 = ve(ie2,2,igrid)
    fveprime = 0.5d0*(v1(iv3) + v1(iv4))
    temp = temp - 0.5d0*f1(ie2)*w_coeffs(ie1,ix,igrid)*(fve+fveprime)
  ENDDO
  f2(ie1) = temp
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE QTE
! ================================================================

! ================================================================
SUBROUTINE QQ(f1,v1,f2,igrid,ne,nv)
IMPLICIT NONE

! Apply the bilinear Q operator that converts dual edge
! integrals (circulations) f1 and vertex values v1 to dual edge integrals (fluxes) f2
! on grid igrid.
! This version conserves only total potential enstrophy

INTEGER, INTENT(IN) :: igrid, ne, nv
REAL*8, INTENT(IN) :: f1(ne), v1(nv)
REAL*8, INTENT(OUT) :: f2(ne)
INTEGER :: ie1, ie2, ix, iv1, iv2
REAL*8 :: temp, fve

!$OMP PARALLEL DO SHARED(f1,f2,v1,igrid,ne,nv) PRIVATE(ie1,ie2,ix,iv1,iv2,temp,fve)
DO ie1 = 1, ne
  temp = 0.0d0
  iv1 = ve(ie1,1,igrid)
  iv2 = ve(ie1,2,igrid)
  fve = 0.5d0*(v1(iv1) + v1(iv2))
  DO ix = 1, ecpsize(ie1,igrid)
    ie2 = ecp(ie1,ix,igrid)
    temp = temp - f1(ie2)*w_coeffs(ie1,ix,igrid)*fve
  ENDDO
  f2(ie1) = temp
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE QQ
! ================================================================

! ================================================================
SUBROUTINE Q(f1,v1,f2,igrid,ne,nv)
IMPLICIT NONE

! Apply the bilinear Q operator that converts dual edge
! integrals (circulations) f1 and vertex values v1 to dual edge integrals (fluxes) f2
! on grid igrid.
! This version conserves both total energy and total potential enstrophy

INTEGER, INTENT(IN) :: igrid, ne, nv
REAL*8, INTENT(IN) :: f1(ne), v1(nv)
REAL*8, INTENT(OUT) :: f2(ne)
INTEGER :: ie1, ie2, ix1, iv, if1
INTEGER :: ix2,ix3,il
REAL*8 :: temp

!$OMP PARALLEL WORKSHARE
f2 = 0.0d0
!$OMP END PARALLEL WORKSHARE

!$OMP PARALLEL DO PRIVATE(if1,il,ix1,ie1,ix2,ie2,ix3,iv,temp) REDUCTION(+:f2)
DO if1=1,nface(igrid)

	!This makes use of prior knowledge for how Q coeffs are generated and stored
	!Factor of 2 faster than scipy sparse
	il=1
	DO ix1=1,ecsize(if1,igrid)
		ie1 = ec(if1,ix1,igrid) !e
		DO ix2=ix1+1,ecsize(if1,igrid)
			ie2 = ec(if1,ix2,igrid) !eprime 
			DO ix3=1,ecsize(if1,igrid)
				iv = vc(if1,ix3,igrid) !v
				temp = v1(iv)*q_coeffs(if1,il,igrid)
				!write(*,*) if1,il,ie1-q_stencil(if1,il,1,igrid),ie2-q_stencil(if1,il,2,igrid),iv-q_stencil(if1,il,3,igrid)
				!#$OMP ATOMIC
				f2(ie1) = f2(ie1) + (-1.d0)*f1(ie2)*temp
				!#$OMP ATOMIC
				f2(ie2) = f2(ie2) + f1(ie1)*temp
				il=il+1
			ENDDO
		ENDDO
	ENDDO
	
	!THIS VERSION WORKS BUT IS QUITE SLOW, ESPECIALLY ON GEODESIC GRIDS
	!COMPARED TO VERSION ABOVE, IT IS DOING A LOT MORE MEMORY ACCESS SINCE ie1 and ie2 are re-computed when they don't need to be
!	DO ix1=1,q_stencil_size(if1,igrid)
!		ie1 = q_stencil(if1,ix1,1,igrid)
!		ie2 = q_stencil(if1,ix1,2,igrid)
!		iv = q_stencil(if1,ix1,3,igrid)
!		!write(*,*) if1,ix1,ie1,ie2,iv,f1(ie1),f1(ie2),v1(iv),q_coeffs(if1,ix1,igrid)
!		f2(ie1) = f2(ie1) + (-1.d0)*f1(ie2)*v1(iv)*q_coeffs(if1,ix1,igrid) 
!		f2(ie2) = f2(ie2) + f1(ie1)*v1(iv)*q_coeffs(if1,ix1,igrid)
!	ENDDO
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE Q
! ================================================================

! ================================================================
SUBROUTINE compute_ql_coeffs(qv,qlcoeffs,igrid,nvert,ne,necp)
IMPLICIT NONE

INTEGER, INTENT(IN) :: igrid,ne,necp,nvert
REAL*8, INTENT(IN) :: qv(nvert)
REAL*8, INTENT(OUT) :: qlcoeffs(ne,necp)
INTEGER :: if1,ix1,iv,ie1,ie2,ix2,ix3,ixx1,ixx2,il
REAL*8 :: temp

qlcoeffs = 0.0d0
DO if1=1,nface(igrid)
	!This makes use of prior knowledge for how Q coeffs are generated and stored
	il=1
	DO ix1=1,ecsize(if1,igrid)
		ie1 = ec(if1,ix1,igrid) !e
		DO ix2=ix1+1,ecsize(if1,igrid)
			ie2 = ec(if1,ix2,igrid) !eprime 
			CALL find_index(ie2,ecp(ie1,:,igrid),ecpsize(ie1,igrid),ixx1)
			CALL find_index(ie1,ecp(ie2,:,igrid),ecpsize(ie2,igrid),ixx2)
			DO ix3=1,ecsize(if1,igrid)
				iv = vc(if1,ix3,igrid) !v
				!write(*,*) if1,il,ie1-q_stencil(if1,il,1,igrid),ie2-q_stencil(if1,il,2,igrid),iv-q_stencil(if1,il,3,igrid)
				qlcoeffs(ie1,ixx1) = qlcoeffs(ie1,ixx1) + (-1.d0)*qv(iv)*q_coeffs(if1,il,igrid) 
				qlcoeffs(ie2,ixx2) = qlcoeffs(ie2,ixx2) + qv(iv)*q_coeffs(if1,il,igrid)
				il=il+1
			ENDDO
		ENDDO
	ENDDO
ENDDO
!DO if1=1,nface(igrid)
!	DO ix1=1,q_stencil_size(if1,igrid)
!		ie1 = q_stencil(if1,ix1,1,igrid)
!		ie2 = q_stencil(if1,ix1,2,igrid)
!		iv = q_stencil(if1,ix1,3,igrid)
!		CALL find_index(ie2,ecp(ie1,:,igrid),ecpsize(ie1,igrid),ix2)
!		CALL find_index(ie1,ecp(ie2,:,igrid),ecpsize(ie2,igrid),ix3)
!		qlcoeffs(ie1,ix2) = qlcoeffs(ie1,ix2) + (-1.d0)*qv(iv)*q_coeffs(if1,ix1,igrid)
!		qlcoeffs(ie2,ix3) = qlcoeffs(ie2,ix3) +         qv(iv)*q_coeffs(if1,ix1,igrid)
!	ENDDO
!ENDDO

END SUBROUTINE compute_ql_coeffs
! ================================================================

! ================================================================
SUBROUTINE set_wl_coeffs(igrid)
IMPLICIT NONE

INTEGER, INTENT(IN) :: igrid
INTEGER :: if1,ix1,nv,ie1,ie2,ix2,ix3
REAL*8 :: temp

DO if1=1,nface(igrid)
	nv = ecsize(if1,igrid)
	DO ix1=1,(nv*(nv-1))/2 !loop over edge pairs in face
		temp = SUM(q_coeffs(if1,(ix1-1)*nv+1:ix1*nv,igrid))
		ie1 = q_stencil(if1,(ix1-1)*nv+1,1,igrid)
		ie2 = q_stencil(if1,(ix1-1)*nv+1,2,igrid)
		!write(*,*) if1,ix1,ie1,ie2,temp
		CALL find_index(ie2,ecp(ie1,:,igrid),ecpsize(ie1,igrid),ix2)
		CALL find_index(ie1,ecp(ie2,:,igrid),ecpsize(ie2,igrid),ix3)
		!write(*,*) if1,ix1,ie1,ie2,ix2,ix3
		!write(*,*) ecp(ie1,:,igrid)
		!write(*,*) ecp(ie2,:,igrid)
		wl_coeffs(ie1,ix2,igrid) = temp
		wl_coeffs(ie2,ix3,igrid) = -1.0d0*temp
	ENDDO
ENDDO
END SUBROUTINE set_wl_coeffs
! ================================================================

!return the element in the overlap between array1 and array2 (assumes they overlap by exactly 2 elements- ie vertex/cell edge overlap) that excludes exclude_val
INTEGER FUNCTION intersection_subtraction(array1,array2,exclude_val,narr1,narr2)
INTEGER :: exclude_val,i,j,overlap_index,narr1,narr2
INTEGER :: array1(narr1), array2(narr2)
DO i=1,narr1
DO j=1,narr2
IF ( (array1(i) .eq. array2(j)) .and. (array1(i) .ne. exclude_val)) then
intersection_subtraction = array1(i)
EXIT
ENDIF
ENDDO
ENDDO
END FUNCTION

! ================================================================
SUBROUTINE set_w_coeffs(igrid)
IMPLICIT NONE

INTEGER, INTENT(IN) :: igrid
INTEGER :: ie1,ix1,if1,ix2,ie2, curredge, currvertex, tev2, nei, vindex, ecpindex
REAL*8 :: temp

!loop over edges
DO ie1=1,nedge(igrid) !ie1 is e
	DO ix1=1,2 !loop over CE
		if1 = ce(ie1,ix1,igrid)
		DO ix2=1,ecsize(if1,igrid) !loop over EC
			ie2 = ec(if1,ix2,igrid) !ie2 is eprime
			temp = 0.0d0
			!find path and sum over rivsum
			curredge = ie1
			currvertex = ve(ie1,1,igrid)
			tev2 = tev_edge(ie1,1,igrid)
			!write(*,*) ie1,ie2,curredge,currvertex
			DO WHILE (curredge /= ie2)
			CALL find_index(currvertex,vc(if1,:,igrid),ecsize(if1,igrid),vindex)
			temp = temp + aiv(if1,vindex,igrid)/ai(if1,igrid)
			curredge = intersection_subtraction(ev(currvertex,:,igrid),ec(if1,:,igrid),curredge, &
			evsize(currvertex,igrid),ecsize(if1,igrid))
			IF (currvertex == ve(curredge,1,igrid)) THEN
			currvertex = ve(curredge,2,igrid)
			ELSE IF (currvertex == ve(curredge,2,igrid)) THEN
			currvertex = ve(curredge,1,igrid)
			ENDIF
			!write(*,*) 'looping',curredge,currvertex
			ENDDO
			IF (ie2 /= ie1) THEN
				!write(*,*) ie1,ie2,temp
				IF (if1 == ce(curredge,1,igrid)) nei = nei_edge(curredge,1,igrid)
				IF (if1 == ce(curredge,2,igrid)) nei = nei_edge(curredge,2,igrid)
				CALL find_index(curredge,ecp(ie1,:,igrid),ecpsize(ie1,igrid),ecpindex)
				w_coeffs(ie1,ecpindex,igrid) = nei*(temp-0.5d0)/tev2
			END IF
		ENDDO
	ENDDO
ENDDO

END SUBROUTINE set_w_coeffs
! ================================================================

! ================================================================
SUBROUTINE set_wd_coeffs(igrid)
IMPLICIT NONE

INTEGER, INTENT(IN) :: igrid
INTEGER :: ie1,ix1,if1,ix2,ie2, curredge, currvertex, tev2, nei, vindex, ecpindex
REAL*8 :: temp

!loop over edges
DO ie1=1,nedge(igrid) !ie1 is e
	DO ix1=1,2 !loop over VE
		if1 = ve(ie1,ix1,igrid)
		DO ix2=1,evsize(if1,igrid) !loop over EV
			ie2 = ev(if1,ix2,igrid) !ie2 is eprime
			temp = 0.0d0
			!find path and sum over rivsum
			curredge = ie1
			currvertex = ce(ie1,1,igrid)
			tev2 = nei_edge(ie1,1,igrid)
			!write(*,*) ie1,ie2,curredge,currvertex
			DO WHILE (curredge /= ie2)
			CALL find_index(if1,vc(currvertex,:,igrid),ecsize(currvertex,igrid),vindex)
			temp = temp + aiv(currvertex,vindex,igrid)/av(if1,igrid)
			curredge = intersection_subtraction(ec(currvertex,:,igrid),ev(if1,:,igrid),curredge, &
			ecsize(currvertex,igrid),evsize(if1,igrid))
			IF (currvertex == ce(curredge,1,igrid)) THEN
			currvertex = ce(curredge,2,igrid)
			ELSE IF (currvertex == ce(curredge,2,igrid)) THEN
			currvertex = ce(curredge,1,igrid)
			ENDIF
			!write(*,*) 'looping',curredge,currvertex
			ENDDO
			IF (ie2 /= ie1) THEN
				!write(*,*) ie1,ie2,temp
				IF (if1 == ve(curredge,1,igrid)) nei = tev_edge(curredge,1,igrid)
				IF (if1 == ve(curredge,2,igrid)) nei = tev_edge(curredge,2,igrid)
				CALL find_index(curredge,ecpd(ie1,:,igrid),ecpdsize(ie1,igrid),ecpindex)
				wd_coeffs(ie1,ecpindex,igrid) = nei*(temp-0.5d0)/tev2
			END IF
		ENDDO
	ENDDO
ENDDO

END SUBROUTINE set_wd_coeffs
! ================================================================

! ================================================================
SUBROUTINE mi_rhs(e1,f1,igrid,ne,nf)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid, ne, nf
REAL*8, INTENT(IN) :: e1(ne)
REAL*8, INTENT(OUT) :: f1(nf)

! Computes D2 e1, which is dmi/dt and stores in f1

call D2(e1,f1,igrid,ne,nf)
!$OMP PARALLEL WORKSHARE
f1 = -1.0d0 * f1
!$OMP END PARALLEL WORKSHARE

END SUBROUTINE mi_rhs
! ================================================================

! ================================================================
SUBROUTINE ue_rhs_q(f1,e1,v1,e2,igrid,nf,ne,nv)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid, ne, nf, nv
REAL*8, INTENT(IN) :: f1(nf),e1(ne),v1(nv)
REAL*8, INTENT(OUT) :: e2(ne)
REAL*8 :: temp1(ne) !this is the gradient term

! Computes D1bar f1 + Q(v1,e1), which is due/dt and stores in e2
! This version conserves both total energy and potential enstrophy

call D1bar(f1,temp1,igrid,nf,ne)
call Q(e1,v1,e2,igrid,ne,nv)
!$OMP PARALLEL WORKSHARE
e2 = e2 - temp1
!$OMP END PARALLEL WORKSHARE

END SUBROUTINE ue_rhs_q
! ================================================================

! ================================================================
SUBROUTINE ue_rhs_qte(f1,e1,v1,e2,igrid,nf,ne,nv)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid, ne, nf, nv
REAL*8, INTENT(IN) :: f1(nf),e1(ne),v1(nv)
REAL*8, INTENT(OUT) :: e2(ne)
REAL*8 :: temp1(ne) !this is the gradient term

! Computes D1bar f1 + QTE(v1,e1), which is due/dt and stores in e2
! This version conserves only total energy

call D1bar(f1,temp1,igrid,nf,ne)
call QTE(e1,v1,e2,igrid,ne,nv)
!$OMP PARALLEL WORKSHARE
e2 = e2 - temp1
!$OMP END PARALLEL WORKSHARE

END SUBROUTINE ue_rhs_qte
! ================================================================

! ================================================================
SUBROUTINE ue_rhs_qq(f1,e1,v1,e2,igrid,nf,ne,nv)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid, ne, nf, nv
REAL*8, INTENT(IN) :: f1(nf),e1(ne),v1(nv)
REAL*8, INTENT(OUT) :: e2(ne)
REAL*8 :: temp1(ne) !this is the gradient term

! Computes D1bar f1 + QQ(v1,e1), which is due/dt and stores in e2
! This version conserves only potential enstrophy

call D1bar(f1,temp1,igrid,nf,ne)
call QQ(e1,v1,e2,igrid,ne,nv)
!$OMP PARALLEL WORKSHARE
e2 = e2 - temp1
!$OMP END PARALLEL WORKSHARE

END SUBROUTINE ue_rhs_qq
! ================================================================









!================= OPERATOR SPECIFIC STUFF =======================!
! ================================================================
SUBROUTINE D1(f,df,igrid,nv,ne)
IMPLICIT NONE

! To compute the exterior derivative df of the field f
! on primal grid number igrid. f comprises pointwise values
! at vertices; df comprises integrals of the derivative
! of f along primal cell edges.

INTEGER, INTENT(IN) :: igrid, nv, ne
REAL*8, INTENT(IN) :: f(nv)
REAL*8, INTENT(OUT) :: df(ne)
INTEGER :: ie1, iv1, iv2, tev1, tev2

!$OMP PARALLEL DO SHARED(f,df,ne,nv,igrid) PRIVATE(ie1,iv1,iv2,tev1,tev2)
DO ie1 = 1, ne
  iv1 = ve(ie1,1,igrid)
  iv2 = ve(ie1,2,igrid)
  tev1 = tev_edge(ie1,1,igrid)
  tev2 = tev_edge(ie1,2,igrid)
  df(ie1) = tev2*f(iv2) + tev1*f(iv1)
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE D1
! ================================================================

! ================================================================
SUBROUTINE D2(f,df,igrid,ne,nf)
IMPLICIT NONE
! To compute the exterior derivative df of the field f
! on primal grid number igrid. f comprises integrals along
! primal edges; df comprises integrals of the derivative
! over primal cells.


INTEGER, INTENT(IN) :: igrid, ne, nf
REAL*8, INTENT(IN) :: f(ne)
REAL*8, INTENT(OUT) :: df(nf)
INTEGER :: if1, ix, ie1
REAL*8 :: temp
INTEGER :: TID

!$OMP PARALLEL DO SHARED(f,df,ne,nf,igrid) PRIVATE(if1,ix,ie1,temp)
DO if1 = 1, nf
  temp = 0.0d0
  DO ix = 1, ecsize(if1,igrid)
    ie1 = ec(if1,ix,igrid)
    temp = temp + f(ie1)*nei_cell(if1,ix,igrid)
  ENDDO
  df(if1) = temp
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE D2
! ================================================================

! ================================================================
SUBROUTINE D1bar(f,df,igrid,nf,ne)
IMPLICIT NONE

! To compute the exterior derivative df of the field f
! on dual grid number igrid. f comprises pointwise values
! at face centres; df comprises integrals of the derivative
! of f along dual cell edges.

INTEGER, INTENT(IN) :: igrid, nf, ne
REAL*8, INTENT(IN) :: f(nf)
REAL*8, INTENT(OUT) :: df(ne)
INTEGER :: ie1, if1, if2, nei1, nei2

!$OMP PARALLEL DO SHARED(f,df,ne,nf,igrid) PRIVATE(ie1,if1,if2,nei1,nei2)
DO ie1 = 1, ne
  if1 = ce(ie1,1,igrid)
  if2 = ce(ie1,2,igrid)
  nei1 = nei_edge(ie1,1,igrid)
  nei2 = nei_edge(ie1,2,igrid)
  df(ie1) = (-1)*nei2*f(if2) + (-1)*nei1*f(if1)
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE D1bar
! ================================================================

! ================================================================
SUBROUTINE D2bar(f,df,igrid,ne,nv) 
IMPLICIT NONE

! To compute the exterior derivative df of the field f
! on dual grid number igrid. f comprises integrals along
! dual edges; df comprises integrals of the derivative
! over dual cells.

INTEGER, INTENT(IN) :: igrid, ne, nv
REAL*8, INTENT(IN) :: f(ne)
REAL*8, INTENT(OUT) :: df(nv)
INTEGER :: iv1, ix, ie1
REAL*8 :: temp

!$OMP PARALLEL DO SHARED(f,df,ne,nv,igrid) PRIVATE(iv1,ix,ie1,temp)
DO iv1 = 1, nv
  temp = 0.0d0
  DO ix = 1, evsize(iv1,igrid)
    ie1 = ev(iv1,ix,igrid)
    temp = temp + f(ie1)*tev_vertex(iv1,ix,igrid)
    !write(*,*) iv1,ix,tev(iv1,ix,igrid)
  ENDDO
  df(iv1) = temp
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE D2bar
! ================================================================

! ================================================================
SUBROUTINE I(f1,f2,igrid,nf)
IMPLICIT NONE
! Apply the Hodge star I operator that converts face
! integrals f1 to face centre values f2 on grid igrid.

INTEGER, INTENT(IN) :: igrid, nf
REAL*8, INTENT(IN) :: f1(nf)
REAL*8, INTENT(OUT) :: f2(nf)
INTEGER :: if1, if2, ix
REAL*8 :: temp

!$OMP PARALLEL DO SHARED(f1,f2,nf,igrid) PRIVATE(if1,if2,ix,temp)
DO if1 = 1, nf
  temp = 0.0d0
  DO ix = 1, i_stencil_size(if1,igrid)
    if2 = i_stencil(if1,ix,igrid)
    temp = temp + f1(if2)*i_coeffs(if1,ix,igrid)
  ENDDO
  f2(if1) = temp
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE I
! ================================================================

! ================================================================
SUBROUTINE Iinv(f1,f2,igrid,nf)
IMPLICIT NONE

INTEGER, INTENT(IN) :: igrid, nf
REAL*8, INTENT(IN) :: f1(nf)
REAL*8, INTENT(OUT) :: f2(nf)
INTEGER :: if1

!$OMP PARALLEL DO SHARED(f1,f2,nf,igrid) PRIVATE(if1)
DO if1 = 1, nf
  f2(if1) = f1(if1)/i_coeffs(if1,1,igrid)
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE Iinv
! ================================================================

! ================================================================
SUBROUTINE J(f1,f2,igrid,nv)
IMPLICIT NONE
! Apply the Hodge star J operator that converts dual face
! integrals f1 to dual centre values f2 on grid igrid.

INTEGER, INTENT(IN) :: igrid, nv
REAL*8, INTENT(IN) :: f1(nv)
REAL*8, INTENT(OUT) :: f2(nv)
INTEGER :: iv1, iv2, ix
REAL*8 :: temp

!$OMP PARALLEL DO SHARED(f1,f2,nv,igrid) PRIVATE(iv1,temp,iv2,ix)
DO iv1 = 1, nv
  temp = 0.0d0
  DO ix = 1, j_stencil_size(iv1,igrid)
    iv2 = j_stencil(iv1,ix,igrid)
    temp = temp + f1(iv2)*j_coeffs(iv1,ix,igrid)
  ENDDO
  f2(iv1) = temp
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE J
! ================================================================

! ================================================================
SUBROUTINE Jinv(f1,f2,igrid,nv)
IMPLICIT NONE

INTEGER, INTENT(IN) :: igrid, nv
REAL*8, INTENT(IN) :: f1(nv)
REAL*8, INTENT(OUT) :: f2(nv)
INTEGER :: if1

!$OMP PARALLEL DO SHARED(f1,f2,nv,igrid) PRIVATE(if1)
DO if1 = 1, nv
  f2(if1) = f1(if1)/j_coeffs(if1,1,igrid)
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE Jinv
! ================================================================

! ================================================================
SUBROUTINE H(f1,f2,igrid,ne)
IMPLICIT NONE

! Apply the Hodge star H operator that converts dual edge
! integrals (circulations) f1 to primal edge integrals (fluxes) f2
! on grid igrid.

INTEGER, INTENT(IN) :: igrid, ne
REAL*8, INTENT(IN) :: f1(ne)
REAL*8, INTENT(OUT) :: f2(ne)
INTEGER :: ie1, ie2, ix
REAL*8 :: temp

!$OMP PARALLEL DO SHARED(f1,f2,igrid,ne) PRIVATE(ie1,ix,temp,ie2)
DO ie1 = 1, ne
  temp = 0.0d0
  DO ix = 1, h_stencil_size(ie1,igrid)
    ie2 = h_stencil(ie1,ix,igrid)
    temp = temp + f1(ie2)*h_coeffs(ie1,ix,igrid)
  ENDDO
  f2(ie1) = temp
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE H
! ================================================================

! ================================================================
SUBROUTINE Hinv(f1,f2,igrid,ne)
IMPLICIT NONE

! Apply the inverse Hodge star Hinv operator that converts primal edge integrals 
! (fluxes) f1 to  dual edge integrals (circulations) f2
! on grid igrid.

INTEGER :: iter, niter = 20
INTEGER, INTENT(IN) :: igrid, ne
REAL*8, INTENT(IN) :: f1(ne)
REAL*8, INTENT(OUT) :: f2(ne)
INTEGER :: ie1
REAL*8 :: temp(ne)

! First guess based on diagonal H
!$OMP PARALLEL DO SHARED(f1,f2,igrid,ne) PRIVATE(ie1)
DO ie1 = 1, ne
  f2(ie1) = f1(ie1)/h_coeffs(ie1,1,igrid)
ENDDO
!$OMP END PARALLEL DO

IF (h_stencil_max > 1) THEN
  ! H is not diagonal, so use Jacobi iteration to invert
  !print *,'*** HodgeHinv:  think about number of iterations needed...'
  DO iter = 1, niter
    CALL H(f2,temp,igrid,ne)
    !$OMP PARALLEL WORKSHARE
    temp = f1 - temp
    !$OMP END PARALLEL WORKSHARE

    !$OMP PARALLEL DO PRIVATE(ie1) 
    DO ie1 = 1, ne
          f2(ie1) = f2(ie1) + temp(ie1)/h_coeffs(ie1,1,igrid)
    ENDDO
    !$OMP END PARALLEL DO
  ENDDO
ENDIF

END SUBROUTINE Hinv
! ================================================================

! ================================================================
SUBROUTINE E(f1,f2,igrid,ne)
IMPLICIT NONE

INTEGER, INTENT(IN) :: igrid, ne
REAL*8, INTENT(IN) :: f1(ne)
REAL*8, INTENT(OUT) :: f2(ne)
INTEGER :: if1, if2, ix
REAL*8 :: temp

!$OMP PARALLEL DO SHARED(f1,f2,igrid,ne) PRIVATE(if1,ix,temp,if2)
DO if1 = 1, ne
  temp = 0.0d0
  DO ix = 1, e_stencil_size(if1,igrid)
    if2 = e_stencil(if1,ix,igrid)
    temp = temp + f1(if2)*e_coeffs(if1,ix,igrid)
  ENDDO
  f2(if1) = temp
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE E
! ================================================================

! ================================================================
SUBROUTINE Einv(f1,f2,igrid,ne)
IMPLICIT NONE

INTEGER, INTENT(IN) :: igrid, ne
REAL*8, INTENT(IN) :: f1(ne)
REAL*8, INTENT(OUT) :: f2(ne)
INTEGER :: if1

!$OMP PARALLEL DO SHARED(f1,f2,igrid,ne) PRIVATE(if1)
DO if1 = 1, ne
  f2(if1) = f1(if1)/e_coeffs(if1,1,igrid)
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE Einv
! ================================================================


! ================================================================
SUBROUTINE L(f1,f2,igrid,ne)
IMPLICIT NONE

INTEGER, INTENT(IN) :: igrid, ne
REAL*8, INTENT(IN) :: f1(ne)
REAL*8, INTENT(OUT) :: f2(ne)
INTEGER :: if1, if2, ix
REAL*8 :: temp

!$OMP PARALLEL DO SHARED(f1,f2,igrid,ne) PRIVATE(if1,ix,if2,temp)
DO if1 = 1, ne
  temp = 0.0d0
  DO ix = 1, l_stencil_size(if1,igrid)
    if2 = l_stencil(if1,ix,igrid)
    temp = temp + f1(if2)*l_coeffs(if1,ix,igrid)
  ENDDO
  f2(if1) = temp
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE L
! ================================================================

! ================================================================
SUBROUTINE Linv(f1,f2,igrid,ne)
IMPLICIT NONE

INTEGER, INTENT(IN) :: igrid, ne
REAL*8, INTENT(IN) :: f1(ne)
REAL*8, INTENT(OUT) :: f2(ne)
INTEGER :: if1

!$OMP PARALLEL DO SHARED(f1,f2,igrid,ne) PRIVATE(if1)
DO if1 = 1, ne
  f2(if1) = f1(if1)/l_coeffs(if1,1,igrid)
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE Linv
! ================================================================

! ================================================================
SUBROUTINE R(f1,f2,igrid,nf,nv)
IMPLICIT NONE

! Apply the R operator:
! given face integrals f1 on primal faces, map to dual cell
! integrals f2, on grid igrid.

INTEGER, INTENT(IN) :: igrid, nf, nv
REAL*8, INTENT(IN) :: f1(nf)
REAL*8, INTENT(OUT) :: f2(nv)
INTEGER :: iv1,if1,ix1
REAL*8 :: temp

!$OMP PARALLEL DO SHARED(f1,f2,igrid,nf,nv) PRIVATE(if1,ix1,temp,iv1)
! Loop over vertices
DO iv1 = 1, nvert(igrid)
	temp = 0.0d0
  ! Get contributions from all surrounding faces
  DO ix1 = 1, evsize(iv1,igrid)
    if1 = cv(iv1,ix1,igrid)
    temp = temp + f1(if1)*r_coeffs(iv1,ix1,igrid)
  ENDDO
  f2(iv1) = temp
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE R
! ================================================================

! ================================================================
SUBROUTINE RT(f1,f2,igrid,nv,nf)
IMPLICIT NONE

! Apply the RT operator:
! given point values f1 on primal vertices, 
! map to dual point values f2 on dual vertices
! on grid igrid.

INTEGER, INTENT(IN) :: igrid, nf, nv
REAL*8, INTENT(IN) :: f1(nv)
REAL*8, INTENT(OUT) :: f2(nf)
INTEGER :: if1, iv1, ix1
REAL*8 :: temp

! Initialize to zero
!f2 = 0.0d0

!$OMP PARALLEL DO SHARED(f1,f2,igrid,nf,nv) PRIVATE(if1,ix1,temp,iv1)
! Loop over faces
DO if1 = 1, nface(igrid)
	temp = 0.0d0
  ! Get contributions from all surrounding vertices
  DO ix1 = 1, ecsize(if1,igrid)
    iv1 = vc(if1,ix1,igrid)
    temp = temp + f1(iv1)*rt_coeffs(if1,ix1,igrid)
  ENDDO
  f2(if1) = temp
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE RT
! ================================================================

! ================================================================
SUBROUTINE phi(f1,f2,igrid,nf,ne)
IMPLICIT NONE

! Apply the phi operator

INTEGER, INTENT(IN) :: igrid, ne, nf
REAL*8, INTENT(IN) :: f1(nf)
REAL*8, INTENT(OUT) :: f2(ne)
INTEGER :: ie1, if1, if2

!$OMP PARALLEL DO SHARED(f1,f2,igrid,ne,nf) PRIVATE(ie1,if1,if2)
DO ie1 = 1, ne
  if1 = ce(ie1,1,igrid)
  if2 = ce(ie1,2,igrid)
  f2(ie1) = f1(if1)*phi_coeffs(ie1,1,igrid) + f1(if2)*phi_coeffs(ie1,2,igrid)
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE phi
! ================================================================

! ================================================================
SUBROUTINE phiT(f1,f2,igrid,ne,nf)
IMPLICIT NONE

! Apply the phiT operator

INTEGER, INTENT(IN) :: igrid, ne,nf
REAL*8, INTENT(IN) :: f1(ne)
REAL*8, INTENT(OUT) :: f2(nf)
INTEGER :: ie1, if1, ix
REAL*8 :: temp

!$OMP PARALLEL DO SHARED(f1,f2,igrid,ne,nf) PRIVATE(if1,ix,temp,ie1)
DO if1 = 1, nf
  temp = 0.0d0
  DO ix = 1,ecsize(if1,igrid)
    ie1 = ec(if1,ix,igrid)
    temp = temp + f1(ie1)*phiT_coeffs(if1,ix,igrid)
  ENDDO
  f2(if1) = temp
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE phiT
! ================================================================

! ================================================================
SUBROUTINE phiv(f1,f2,igrid,nv,ne)
IMPLICIT NONE

! Apply the Phiv operator

INTEGER, INTENT(IN) :: igrid, ne, nv
REAL*8, INTENT(IN) :: f1(nv)
REAL*8, INTENT(OUT) :: f2(ne)
INTEGER :: ie1, iv1, ix
REAL*8 :: temp

!$OMP PARALLEL DO SHARED(f1,f2,igrid,nv,ne) PRIVATE(ie1,temp,ix,iv1)
DO ie1 = 1, ne
  temp = 0.0d0
  DO ix = 1,2
    iv1 = ve(ie1,ix,igrid)
    temp = temp + f1(iv1)*0.5d0
  ENDDO
  f2(ie1) = temp
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE phiv
! ================================================================









!================= OPERATOR CONSTRUCTION STUFF =======================!

! ================================================================
SUBROUTINE build_I(igrid)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid
INTEGER :: if0

i_stencil_size = 1
DO if0 = 1, nface(igrid)
   i_stencil(if0,1,igrid) = if0
   i_coeffs(if0,1,igrid) = 1.0d0/ai(if0,igrid)
ENDDO
					
END SUBROUTINE build_i
! ================================================================

! ================================================================
SUBROUTINE build_J(igrid)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid
INTEGER :: if0

j_stencil_size = 1
DO if0 = 1, nvert(igrid)
   j_stencil(if0,1,igrid) = if0
   j_coeffs(if0,1,igrid) = 1.0d0/av(if0,igrid)
ENDDO
					
END SUBROUTINE build_J
! ================================================================

! ================================================================
SUBROUTINE build_H_geo(igrid)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid
INTEGER :: ie0

h_stencil_size = 1
DO ie0 = 1, nedge(igrid)
    h_stencil(ie0,1,igrid) = ie0
    h_coeffs(ie0,1,igrid) = le(ie0,igrid)/de(ie0,igrid)
ENDDO
h_stencil_max = 1
					
END SUBROUTINE build_H_geo
! ================================================================

! ================================================================
SUBROUTINE build_H_cs(igrid)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid
INTEGER :: ie1, ie2, if1, if2, if3, iv1, iv2, ix1, ix2, &
           ixtab, ipm, iside
REAL*8 :: long, lat, x0, y0, z0, x1, y1, z1, x2, y2, z2, &
          dx1, dy1, dz1, dxe, dye, dze, fac1, fac2, alpha, dede, d1de
LOGICAL :: lfound

h_stencil_size = 5
DO ie1 = 1, nedge(igrid)

    ! Stencil includes edge ie1
    ! Note edge ie1 is the first edge in the stencil; this is exploited
    ! in iteratively computing the inverse of H
    ixtab = 1
    h_stencil(ie1,1,igrid) = ie1
    ! Find vector of dual edge ie1
    if1 = ce(ie1,1,igrid)
    long = clon(if1,igrid)
    lat = clat(if1,igrid)
    CALL ll2xyz(long,lat,x1,y1,z1)
    if1 = ce(ie1,2,igrid)
    long = clon(if1,igrid)
    lat = clat(if1,igrid)
    CALL ll2xyz(long,lat,x2,y2,z2)
    dx1 = nei_edge(ie1,2,igrid)*x2 + nei_edge(ie1,1,igrid)*x1 
    dy1 = nei_edge(ie1,2,igrid)*y2 + nei_edge(ie1,1,igrid)*y1 
    dz1 = nei_edge(ie1,2,igrid)*z2 + nei_edge(ie1,1,igrid)*z1 

    ! Vertices either end of edge ie1:
    ! Determine whether they're triangles or squares
    iv1 = ve(ie1,1,igrid)
    IF (evsize(iv1,igrid) == 3) THEN
      fac1 = 6.0d0
    ELSE
      fac1 = 4.0d0
    ENDIF
    iv2 = ve(ie1,2,igrid)
    IF (evsize(iv2,igrid) == 3) THEN
      fac2 = 6.0d0
    ELSE
      fac2 = 4.0d0
    ENDIF

    ! Faces either side of edge ie1
    DO iside = 1, 2
      if1 = ce(ie1,iside,igrid)
      long = clon(if1,igrid)
      lat = clat(if1,igrid)
      CALL ll2xyz(long,lat,x0,y0,z0)
      ! Find index of edge ie1 as an edge of face if1
      lfound = .false.
      ix1 = 0
      DO WHILE (.NOT. lfound)
        ix1 = ix1 + 1
        IF (ec(if1,ix1,igrid) == ie1) lfound = .true.
      ENDDO
      ! Edges anticlockwise and clockwise of ie1
      DO ipm = 3, 5, 2
        ixtab = ixtab + 1
        ix2 = MOD(ix1 + ipm - 1, 4) + 1
        ie2 = ec(if1,ix2,igrid)
        ! Find vector of dual edge ie2
        if2 = ce(ie2,1,igrid)
        long = clon(if2,igrid)
        lat = clat(if2,igrid)
        CALL ll2xyz(long,lat,x1,y1,z1)
        if3 = ce(ie2,2,igrid)
        long = clon(if3,igrid)
        lat = clat(if3,igrid)
        CALL ll2xyz(long,lat,x2,y2,z2)
        dxe = nei_edge(ie2,2,igrid)*x2 + nei_edge(ie2,1,igrid)*x1 
        dye = nei_edge(ie2,2,igrid)*y2 + nei_edge(ie2,1,igrid)*y1 
        dze = nei_edge(ie2,2,igrid)*z2 + nei_edge(ie2,1,igrid)*z1
        alpha = x0*(dy1*dze - dz1*dye) &
              + y0*(dz1*dxe - dx1*dze) &
              + z0*(dx1*dye - dy1*dxe)
        alpha = ABS(alpha) !THIS IS CROSS PRODUCT de x de'
        IF (ixtab == 2 .or. ixtab == 5) THEN
          alpha = alpha*fac1
        ELSE
          alpha = alpha*fac2
        ENDIF
        dede = dxe*dxe + dye*dye + dze*dze
        d1de = dx1*dxe + dy1*dye + dz1*dze
        h_stencil(ie1,ixtab,igrid) = ie2
        h_coeffs(ie1,1,igrid) = h_coeffs(ie1,1,igrid) + dede/alpha
        h_coeffs(ie1,ixtab,igrid) = - d1de/alpha

     ENDDO
   ENDDO

ENDDO
h_stencil_max = 5
	
END SUBROUTINE build_H_cs
! ================================================================

! ================================================================
SUBROUTINE build_R(igrid)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid
INTEGER :: iv1,ix,ixx,if1
REAL*8 :: temp

r_stencil_size = evsize
DO iv1=1,nvert(igrid)
	DO ix=1,evsize(iv1,igrid) !LOOP OVER CV
		if1 = cv(iv1,ix,igrid)
		r_stencil(iv1,ix,igrid) = if1
		CALL find_index(iv1,vc(if1,:,igrid),ecsize(if1,igrid),ixx)
		r_coeffs(iv1,ix,igrid) =  aiv(if1,ixx,igrid)/ai(if1,igrid)
	ENDDO
ENDDO
					
END SUBROUTINE build_R
! ================================================================

! ================================================================
SUBROUTINE build_RT(igrid)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid
INTEGER :: if1,ix

rt_stencil_size = ecsize
DO if1=1,nface(igrid)
	DO ix=1,ecsize(if1,igrid) !LOOP OVER VC
		rt_stencil(if1,ix,igrid) = vc(if1,ix,igrid)
		rt_coeffs(if1,ix,igrid) = aiv(if1,ix,igrid)/ai(if1,igrid)
	ENDDO
ENDDO
					
END SUBROUTINE build_RT
! ================================================================

! ================================================================
SUBROUTINE build_E(igrid)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid
INTEGER :: ie0

e_stencil_size = 1
DO ie0 = 1, nedge(igrid)
    e_stencil(ie0,1,igrid) = ie0
    e_coeffs(ie0,1,igrid) = 1.0d0/de(ie0,igrid)
ENDDO
					
END SUBROUTINE build_E
! ================================================================

! ================================================================
SUBROUTINE build_L(igrid)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid
INTEGER :: ie0

l_stencil_size = 1
DO ie0 = 1, nedge(igrid)
    l_stencil(ie0,1,igrid) = ie0
    l_coeffs(ie0,1,igrid) = 1.0d0/le(ie0,igrid)
ENDDO
					
END SUBROUTINE build_L
! ================================================================

! ================================================================
SUBROUTINE set_phi_coeffs(igrid)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid
INTEGER :: if1

DO if1=1,nedge(igrid)
	phi_coeffs(if1,1,igrid) = aie(if1,1,igrid)/ae(if1,igrid)
	phi_coeffs(if1,2,igrid) = aie(if1,2,igrid)/ae(if1,igrid)
ENDDO
					
END SUBROUTINE set_phi_coeffs
! ================================================================

! ================================================================
SUBROUTINE set_phiT_coeffs(igrid)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid
INTEGER :: if1,ix,ie,if2

DO if1=1,nface(igrid)
	DO ix=1,ecsize(if1,igrid) !LOOP OVER EC
		ie = ec(if1,ix,igrid)
		if2 = ce(ie,1,igrid)
		IF (if1 == if2) then
			phiT_coeffs(if1,ix,igrid) = aie(ie,1,igrid)/ae(ie,igrid) !phi_coeffs(ie,1,igrid)
		ELSE
			phiT_coeffs(if1,ix,igrid) = aie(ie,2,igrid)/ae(ie,igrid) !phi_coeffs(ie,2,igrid)
		ENDIF
	ENDDO
ENDDO
					
END SUBROUTINE set_phiT_coeffs
! ================================================================

! ================================================================
SUBROUTINE dimensionalize_operators(a)
IMPLICIT NONE
REAL*8, INTENT(IN) :: a

i_coeffs = i_coeffs/a/a
j_coeffs = j_coeffs/a/a
e_coeffs = e_coeffs/a
l_coeffs = l_coeffs/a

END SUBROUTINE dimensionalize_operators
! ================================================================


! ================================================================
SUBROUTINE dimensionalize_geometry(a)
IMPLICIT NONE
REAL*8, INTENT(IN) :: a

ai = ai * a * a !needed for ZGRID
le = le * a !needed for ZGRID, also used in eigensolver
de = de * a !needed for ZGRID
END SUBROUTINE dimensionalize_geometry
! ================================================================






!================= ZGRID SPECIFIC STUFF =======================!

!Return the element in the intersection between two arrays (assuming they only have one element in common)
INTEGER FUNCTION intersection(array1,array2,narr1,narr2)
INTEGER :: i,j,narr1,narr2
INTEGER :: array1(narr1), array2(narr2)
DO i=1,narr1
DO j=1,narr2
IF (array1(i) .eq. array2(j)) then
intersection = array1(i)
EXIT
ENDIF
ENDDO
ENDDO
END FUNCTION

!THIS WORKS FOR H DIAGONAL, COULD WRITE A VERSION FOR THUBURN H THAT HAS A 9-PT STENCIL
! ================================================================
SUBROUTINE set_laplac_primal_coeffs(igrid)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid
INTEGER :: if1,ix1,if2,ie1
REAL*8 :: le_val, de_val

DO if1=1,nface(igrid)
	DO ix1=1,ecsize(if1,igrid)
		if2 = nc(if1,ix1,igrid)
		ie1 = intersection(ec(if1,:,igrid),ec(if2,:,igrid),ecsize(if1,igrid),ecsize(if2,igrid))
		le_val = le(ie1,igrid)
		de_val = de(ie1,igrid)
		laplac_primal_coeffs(if1,ix1,igrid) = le_val / de_val
	ENDDO
ENDDO


END SUBROUTINE set_laplac_primal_coeffs
! ================================================================

! ================================================================
SUBROUTINE laplac_primal(f,hf,igrid,nf)
IMPLICIT NONE

!Computes Lf = 1/Ai * sum_n (fn - f0) le/de

INTEGER, INTENT(IN) :: igrid, nf
REAL*8, INTENT(IN) :: f(nf)
REAL*8, INTENT(OUT) :: hf(nf)
INTEGER :: if1,if2,ix1
REAL*8 :: temp

!hf = 0.0d0

! Loop over cells
!$OMP PARALLEL DO SHARED(f,hf,nf,igrid) PRIVATE(if1,temp,ix1,if2)
DO if1 = 1, nf
	temp = 0.0d0
	DO ix1=1,ecsize(if1,igrid)
		if2 = nc(if1,ix1,igrid)
		temp = temp + laplac_primal_coeffs(if1,ix1,igrid) * (f(if2) - f(if1))
	ENDDO
	hf(if1) = temp / ai(if1,igrid)
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE laplac_primal
! ================================================================

! ================================================================
SUBROUTINE jacobian(f1,f2,hf,igrid,nf)
IMPLICIT NONE

!Compute J(f1,f2) = 1/(6*Ai) * \sum_n f1_n (f2_{n+1} - f2_{n-1})

INTEGER, INTENT(IN) :: igrid, nf
REAL*8, INTENT(IN) :: f1(nf),f2(nf)
REAL*8, INTENT(OUT) :: hf(nf)
INTEGER :: if1,if2,ix1,if3,if4,numedge
REAL*8 :: temp

!hf = 0.0d0

! Loop over cells
!$OMP PARALLEL DO SHARED(f1,f2,hf,nf,igrid) PRIVATE(if1,temp,numedge,if2,if3,if4)
DO if1 = 1, nf
	temp = 0.0d0
	numedge = ecsize(if1,igrid)
	!write(*,*) if1
	DO ix1=1,numedge
		if2 = nc(if1,ix1,igrid)
		if3 = nc(if1,mod(ix1,numedge)+1,igrid) !ix+1, circular
		if4 = nc(if1,mod(ix1-2+numedge,numedge)+1,igrid) !ix-1, circular 
		temp = temp + f1(if2) * (f2(if3) - f2(if4))
		!write(*,*) ix1,mod(ix1,numedge)+1,mod(ix1-2+numedge,numedge)+1
	ENDDO
	hf(if1) = temp / ai(if1,igrid)
ENDDO
!$OMP END PARALLEL DO
!$OMP PARALLEL WORKSHARE
hf = hf / 6.d0
!$OMP END PARALLEL WORKSHARE

END SUBROUTINE jacobian
! ================================================================

! ================================================================
SUBROUTINE fluxdiv(f1,f2,hf,igrid,nf)
IMPLICIT NONE

!Compute FD(f1,f2) = 1/Ai * sum_n (f2_n - f2_0) (f1_0 + f1_n)/2 * le/de

INTEGER, INTENT(IN) :: igrid, nf
REAL*8, INTENT(IN) :: f1(nf),f2(nf)
REAL*8, INTENT(OUT) :: hf(nf)
INTEGER :: if1,if2,ix1
REAL*8 :: temp

!hf = 0.0d0

! Loop over cells
!$OMP PARALLEL DO SHARED(f1,f2,hf,nf,igrid) PRIVATE(if1,temp,ix1,if2)
DO if1 = 1, nf
	temp = 0.0d0
	DO ix1=1,ecsize(if1,igrid)
		if2 = nc(if1,ix1,igrid)
		temp = temp + laplac_primal_coeffs(if1,ix1,igrid) * (f2(if2) - f2(if1)) * (f1(if1) + f1(if2))/2.0d0
	ENDDO
	hf(if1) = temp / ai(if1,igrid)
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE fluxdiv
! ================================================================

! ================================================================
SUBROUTINE diagnose_h_quantities(hi,igrid,nf)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid,nf
REAL*8, INTENT(In) :: hi(nf)
INTEGER :: iv1,ie1,if1,if2,ix1
REAL*8 :: temp

!$OMP PARALLEL DO SHARED(hi,igrid,nf) PRIVATE(ie1,if1,if2)
DO ie1=1,nedge(igrid)
	if1 = ce(ie1,1,igrid)
	if2 = ce(ie1,2,igrid)
	heinv(ie1,igrid) = 2.d0 / (hi(if1) + hi(if2))
ENDDO
!$OMP END PARALLEL DO

!$OMP PARALLEL DO SHARED(hi,igrid,nf) PRIVATE(iv1,temp,if1,ix1)
DO iv1=1,nvert(igrid)
	temp = 0.0d0
	DO ix1=1,evsize(iv1,igrid)
		if1 = cv(iv1,ix1,igrid)
		temp = temp + hi(if1) !this assumes a geodesic grid with triangular dual
	ENDDO
	hvinv(iv1,igrid) =  3.d0 / temp  !this assumes a geodesic grid with triangular dua
ENDDO
!$OMP END PARALLEL DO

!$OMP PARALLEL WORKSHARE
heinv2(:,igrid) = heinv(:,igrid) * heinv(:,igrid) 
hvinv2(:,igrid) = hvinv(:,igrid) * hvinv(:,igrid)  
!$OMP END PARALLEL WORKSHARE


END SUBROUTINE diagnose_h_quantities
! ================================================================

! ================================================================
SUBROUTINE diagnose_phi(hi,bi,chi,psi,phi,g,igrid,nf)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid,nf
REAL*8, INTENT(IN) :: g, hi(nf), bi(nf), chi(nf), psi(nf)
REAL*8, INTENT(OUT) :: phi(nf)
REAL*8 :: pe(nf), ke_fd(nf), ke_j(nf)
REAL*8 :: temp,temp2,temp3
INTEGER :: if1,ix1,ie1,if2,if3,iv1,iv2

!$OMP PARALLEL WORKSHARE
pe = g * (hi + bi)
!$OMP END PARALLEL WORKSHARE

!$OMP PARALLEL DO SHARED(hi,bi,chi,psi,phi,g,igrid,nf) PRIVATE(if1,temp,ix1,ie1,if2,if3)
DO if1=1,nface(igrid)
	temp = 0.0d0
	DO ix1=1,ecsize(if1,igrid)
		ie1 = ec(if1,ix1,igrid)
		if2 = ce(ie1,1,igrid)
		if3 = ce(ie1,2,igrid)
		temp = temp + ((chi(if2) - chi(if3))*(chi(if2) - chi(if3)) + &
		(psi(if2) - psi(if3))*(psi(if2) - psi(if3)) )*h_coeffs(ie1,1,igrid)*heinv2(ie1,igrid)
	ENDDO
	ke_fd(if1) = 0.25d0 * temp / ai(if1,igrid)
ENDDO
!$OMP END PARALLEL DO

!$OMP PARALLEL DO SHARED(hi,bi,chi,psi,phi,g,igrid,nf) PRIVATE(if1,temp,ix1,ie1,iv1,iv2,if2,if3)
DO if1=1,nface(igrid)
	temp = 0.0d0
	DO ix1=1,ecsize(if1,igrid)
		ie1 = ec(if1,ix1,igrid)
		iv1 = ve(ie1,1,igrid)
		iv2 = ve(ie1,2,igrid)
		if2 = ce(ie1,1,igrid)
		if3 = ce(ie1,2,igrid)
		temp2 = tev_edge(ie1,1,igrid) * hvinv2(iv1,igrid) + tev_edge(ie1,2,igrid) * hvinv2(iv2,igrid)
		temp3 = nei_edge(ie1,1,igrid)*psi(if2)*chi(if3) + nei_edge(ie1,2,igrid)*psi(if3)*chi(if2)
		temp = temp + temp3 * temp2
	ENDDO
	ke_j(if1) = 1.d0/6.d0*temp / ai(if1,igrid)
ENDDO
!$OMP END PARALLEL DO

!$OMP PARALLEL WORKSHARE
phi = ke_j + ke_fd + pe
!$OMP END PARALLEL WORKSHARE

END SUBROUTINE diagnose_phi
! ================================================================

! ================================================================
SUBROUTINE chi_psi_apply_fd_operator_highest(v,v_out,nf)
IMPLICIT NONE
INTEGER, INTENT(IN) :: nf
REAL*8, INTENT(IN) :: v(nf)
REAL*8, INTENT(OUT) :: v_out(nf)
INTEGER :: if1, ix1, if2,if3,ie1
REAL*8 :: temp, temp2

!$OMP PARALLEL DO SHARED(v,v_out,nf) PRIVATE(if1,ix1,temp,temp2,ie1,if2,if3)
DO if1=1,nf
	temp = 0.0d0
	DO ix1=1,ecsize(if1,ngrids)
		ie1 = ec(if1,ix1,ngrids)
		if2 = ce(ie1,1,ngrids)
		if3 = ce(ie1,2,ngrids)
		!write(*,*) if1,ix1,ie1,if2,if3
		temp2 = (-1.d0)*nei_edge(ie1,1,ngrids) * v(if2) + (-1.d0)*nei_edge(ie1,2,ngrids) * v(if3)
		temp = temp + nei_cell(if1,ix1,ngrids) * h_coeffs(ie1,1,ngrids) * heinv(ie1,ngrids) * temp2
		!this could be simplified into flux-div format IFF ec(ix1) and nc(ix1) refered to the same edge
		!this would be faster!- would save a memory access
	ENDDO
	v_out(if1) = v_out(if1) + temp / ai(if1,ngrids)
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE chi_psi_apply_fd_operator_highest
! ================================================================

! ================================================================
SUBROUTINE chi_psi_apply_jacob_operator_highest(v,v_out,nf)
IMPLICIT NONE
INTEGER, INTENT(IN) :: nf
REAL*8, INTENT(IN) :: v(nf)
REAL*8, INTENT(OUT) :: v_out(nf)
INTEGER :: if1, ix1, if2,if3,ie1,iv1,iv2
REAL*8 :: temp, temp2

!Jchi block (part associated with zeta)
!$OMP PARALLEL DO SHARED(v,v_out,nf) PRIVATE(if1,ix1,temp,temp2,ie1,if2,if3,iv1,iv2)
DO if1=1,nf
	temp = 0.0d0
	DO ix1=1,ecsize(if1,ngrids)
		ie1 = ec(if1,ix1,ngrids)
		if2 = ce(ie1,1,ngrids)
		if3 = ce(ie1,2,ngrids)
		iv1 = ve(ie1,1,ngrids)
		iv2 = ve(ie1,2,ngrids)
		temp2 = tev_edge(ie1,1,ngrids) * hvinv(iv1,ngrids) + tev_edge(ie1,2,ngrids) * hvinv(iv2,ngrids)		 
		temp = temp + nei_cell(if1,ix1,ngrids) * (v(if2) + v(if3)) * temp2
	ENDDO
	v_out(if1) = v_out(if1) + 0.5d0*temp / ai(if1,ngrids)
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE chi_psi_apply_jacob_operator_highest
! ================================================================

! ================================================================
SUBROUTINE chi_psi_apply_operator_highest(v,v_out,nf2)
IMPLICIT NONE
INTEGER, INTENT(IN) :: nf2
REAL*8, INTENT(IN) :: v(nf2)
REAL*8, INTENT(OUT) :: v_out(nf2)
!INTEGER :: if1, ix1, if2,if3,ie1,iv1,iv2
!REAL*8 :: temp, temp2
!v(1,nf2/2) is chi 
!v(nf2/2,nf2) is psi

v_out = 0.d0

!Delta block
CALL chi_psi_apply_fd_operator_highest(v(1:nf2/2),v_out(1:nf2/2),nf2/2) !FD CHI
CALL chi_psi_apply_jacob_operator_highest(-1.d0*v(nf2/2+1:nf2),v_out(1:nf2/2),nf2/2) !J PSI 

!Zeta block
CALL chi_psi_apply_fd_operator_highest(v(nf2/2+1:nf2),v_out(nf2/2+1:nf2),nf2/2) !FD PSI
CALL chi_psi_apply_jacob_operator_highest(v(1:nf2/2),v_out(nf2/2+1:nf2),nf2/2) !J CHI


!!FD chi block (part associated with delta)
!DO if1=1,nf2/2
!	temp = 0.0d0
!	DO ix1=1,ecsize(if1,ngrids)
!		ie1 = ec(if1,ix1,ngrids)
!		if2 = ce(ie1,1,ngrids)
!		if3 = ce(ie1,2,ngrids)
!		temp2 = (-1)*nei_edge(ie1,1,ngrids) * v(if2) + (-1)*nei_edge(ie1,2,ngrids) * v(if3)
!		temp = temp + nei_cell(if1,ix1,ngrids) * heinv(ie1) * h_coeffs(ie1,1,ngrids) * temp2
!		!this could be simplified into flux-div format IFF ec(ix1) and nc(ix2) refered to the same edge
!		!this would be faster!
!	ENDDO
!	v_out(if1) = v_out(if1) + temp / ai(if1,ngrids)
!ENDDO

!!J psi block (part associated with delta)
!DO if1=1,nf2/2
!	temp = 0.0d0
!	DO ix1=1,ecsize(if1,ngrids)
!		ie1 = ec(if1,ix1,ngrids)
!		if2 = ce(ie1,1,ngrids)
!		if3 = ce(ie1,2,ngrids)
!		iv1 = ve(ie1,1,ngrids)
!		iv2 = ve(ie1,2,ngrids)
!		temp2 = tev_edge(ie1,1,ngrids) * hvinv(iv1) + tev_edge(ie1,2,ngrids) * hvinv(iv2)		 
!		temp = temp + nei_cell(if1,ix1,ngrids) * (v(if2+nf2/2) + v(if3+nf2/2)) * temp2
!	ENDDO
!	v_out(if1) = v_out(if1) - 0.5d0*temp / ai(if1,ngrids) !note the minus sign here
!ENDDO

!!FD psi block (part associated with zeta)
!DO if1=1,nf2/2
!	temp = 0.0d0
!	DO ix1=1,ecsize(if1,ngrids)
!		ie1 = ec(if1,ix1,ngrids)
!		if2 = ce(ie1,1,ngrids)
!		if3 = ce(ie1,2,ngrids)
!		temp2 = (-1)*nei_edge(ie1,1,ngrids) * v(if2+nf2/2) + (-1)*nei_edge(ie1,2,ngrids) * v(if3+nf2/2)
!		temp = temp + nei_cell(if1,ix1,ngrids) * heinv(ie1) * h_coeffs(ie1,1,ngrids) * temp2
!		!this could be simplified into flux-div format IFF ec(ix1) and nc(ix2) refered to the same edge
!		!this would be faster!
!	ENDDO
!	v_out(if1+nf2/2) = v_out(if1+nf2/2) + temp / ai(if1,ngrids)
!ENDDO

!!Jchi block (part associated with zeta)
!DO if1=1,nf2/2
!	temp = 0.0d0
!	DO ix1=1,ecsize(if1,ngrids)
!		ie1 = ec(if1,ix1,ngrids)
!		if2 = ce(ie1,1,ngrids)
!		if3 = ce(ie1,2,ngrids)
!		iv1 = ve(ie1,1,ngrids)
!		iv2 = ve(ie1,2,ngrids)
!		temp2 = tev_edge(ie1,1,ngrids) * hvinv(iv1) + tev_edge(ie1,2,ngrids) * hvinv(iv2)		 
!		temp = temp + nei_cell(if1,ix1,ngrids) * (v(if2) + v(if3)) * temp2
!	ENDDO
!	v_out(if1+nf2/2) = v_out(if1+nf2/2) + 0.5d0*temp / ai(if1,ngrids)
!ENDDO

	
END SUBROUTINE chi_psi_apply_operator_highest
! ================================================================

! ================================================================
SUBROUTINE diagnose_qi(zetai,fi,hi,qi,igrid,nf)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid, nf
REAL*8, INTENT(IN) :: zetai(nf), fi(nf), hi(nf)
REAL*8, INTENT(OUT) :: qi(nf)

!$OMP PARALLEL WORKSHARE
qi = (zetai + fi)/hi
!$OMP END PARALLEL WORKSHARE

END SUBROUTINE diagnose_qi
! ================================================================

! ================================================================
SUBROUTINE precompute_ZGRID_statistics(hi,bi,a,g,nf)
IMPLICIT NONE
INTEGER, INTENT(IN) :: nf
REAL*8, INTENT(IN) :: hi(nf), bi(nf)
REAL*8, INTENT(IN) :: a,g
REAL*8 :: hibar, bihbar

hibar = kahan_sum(hi,nf) / (4.d0 * pi * a * a)
bihbar = kahan_sum(bi,nf) / (4.d0 * pi * a * a)

flatpe = 4.d0*pi*a*a * g * (hibar + bihbar) * (hibar + bihbar)/2.d0

CALL Iinv(bi,bih,ngrids,nf) !assumes I is diagonal!

!write(*,*) hi
!write(*,*) flatpe, hibar, bihbar

END SUBROUTINE precompute_ZGRID_statistics
! ================================================================


! ================================================================
SUBROUTINE compute_ZGRID_statistics(n,hi,bi,zetai,fi,chi,psi,g,nf,ne)
IMPLICIT NONE
INTEGER, INTENT(IN) :: nf,n,ne
REAL*8, INTENT(IN) :: hi(nf), bi(nf), zetai(nf), fi(nf), chi(nf), psi(nf)
REAL*8, INTENT(IN) :: g
REAL*8 :: mi(nf), etai(nf), eta_adj(nf), zeta_adj(nf), ke(ne)
INTEGER :: if1,ix1,ie1,iv1,iv2,if2,if3
REAL*8 :: temp,temp2,temp3

!$OMP PARALLEL WORKSHARE
etai = zetai + fi
!$OMP END PARALLEL WORKSHARE

CALL Iinv(hi,mi,ngrids,nf) !assumes I is diagonal!
CALL Iinv(zetai,zeta_adj,ngrids,nf) !assumes I is diagonal!
CALL Iinv(etai,eta_adj,ngrids,nf) !assumes I is diagonal!

mc(n) = kahan_sum(mi,nf)
eta(n) = kahan_sum(eta_adj,nf)
zeta(n) = kahan_sum(zeta_adj,nf)

!$OMP PARALLEL DO SHARED(hi,bi,zeta,fi,chi,psi,g,nf,ne) PRIVATE(ie1,temp,iv1,iv2,if1,if2,temp2,temp3)
DO ie1=1,ne
	iv1 = ve(ie1,1,ngrids)
	iv2 = ve(ie1,2,ngrids)
	if1 = ce(ie1,1,ngrids)
	if2 = ce(ie1,2,ngrids)
	temp = ((chi(if1) - chi(if2))*(chi(if1) - chi(if2)) + &
		(psi(if1) - psi(if2))*(psi(if1) - psi(if2)) )*h_coeffs(ie1,1,ngrids)*heinv(ie1,ngrids) !KE_FD
	temp2 = tev_edge(ie1,1,ngrids) * hvinv(iv1,ngrids) + tev_edge(ie1,2,ngrids) * hvinv(iv2,ngrids) !KE_J
	temp3 = nei_edge(ie1,1,ngrids)*psi(if1)*chi(if2) + nei_edge(ie1,2,ngrids)*psi(if2)*chi(if1) !KE_J
	ke(ie1) = 0.5d0*(temp + temp3 * temp2)
ENDDO
!$OMP END PARALLEL DO

kinetic(n) = kahan_sum(ke,ne)
potential(n) = kahan_sum(0.5d0 * g * (mi+bih) * (hi+bi),nf) - flatPE !INTRINSIC
total(n) = kinetic(n) + potential(n)
enstrophy(n) = kahan_sum(0.5d0 * etai * eta_adj / hi,nf) !INTRINSIC

	
END SUBROUTINE compute_ZGRID_statistics
! ================================================================

! ================================================================
SUBROUTINE hi_rhs(chi,hirhs,igrid,nf)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid,nf
REAL*8, INTENT(IN) :: chi(nf)
REAL*8, INTENT(OUT) :: hirhs(nf)

CALL laplac_primal(chi,hirhs,igrid,nf)
!$OMP PARALLEL WORKSHARE
hirhs = -1.d0 * hirhs
!$OMP END PARALLEL WORKSHARE

END SUBROUTINE hi_rhs
! ================================================================

! ================================================================
SUBROUTINE zeta_rhs(qi,chi,psi,zetarhs,igrid,nf)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid,nf
REAL*8, INTENT(IN) :: qi(nf), chi(nf), psi(nf)
REAL*8, INTENT(OUT) :: zetarhs(nf)
REAL*8 :: temp(nf)

CALL jacobian(qi,psi,temp,igrid,nf)
CALL fluxdiv(qi,chi,zetarhs,igrid,nf)
!$OMP PARALLEL WORKSHARE
zetarhs = -1.d0 * zetarhs + temp
!$OMP END PARALLEL WORKSHARE

END SUBROUTINE zeta_rhs
! ================================================================

! ================================================================
SUBROUTINE delta_rhs(qi,chi,psi,phi,deltarhs,igrid,nf)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid,nf
REAL*8, INTENT(IN) :: qi(nf), chi(nf), psi(nf), phi(nf)
REAL*8, INTENT(OUT) :: deltarhs(nf)
REAL*8 :: temp1(nf), temp2(nf)

CALL jacobian(qi,chi,temp1,igrid,nf)
CALL fluxdiv(qi,psi,temp2,igrid,nf)
CALL laplac_primal(phi,deltarhs,igrid,nf)

!$OMP PARALLEL WORKSHARE
deltarhs = -1.d0*deltarhs + temp1 + temp2
!$OMP END PARALLEL WORKSHARE

END SUBROUTINE delta_rhs
! ================================================================

! ============================ BVE STUFF ====================================

! ================================================================
SUBROUTINE zeta_trisk_bve_rhs_q(zeta,fv,psi,zetarhs,igrid,nv,ne)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid,nv,ne
REAL*8, INTENT(IN) :: zeta(nv), fv(nv), psi(nv)
REAL*8, INTENT(OUT) :: zetarhs(nv)
REAL*8 :: temp1(nv), temp2(ne), temp3(ne)

CALL J(zeta + fv,temp1,igrid,nv)
CALL D1(psi,temp2,igrid,nv,ne)
CALL Q(temp2,temp1,temp3,igrid,ne,nv)
CALL D2bar(temp3,zetarhs,igrid,ne,nv)

END SUBROUTINE zeta_trisk_bve_rhs_q
! ================================================================

! ================================================================
SUBROUTINE zeta_trisk_bve_rhs_qte(zeta,fv,psi,zetarhs,igrid,nv,ne)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid,nv,ne
REAL*8, INTENT(IN) :: zeta(nv), fv(nv), psi(nv)
REAL*8, INTENT(OUT) :: zetarhs(nv)
REAL*8 :: temp1(nv), temp2(ne), temp3(ne)

CALL J(zeta + fv,temp1,igrid,nv)
CALL D1(psi,temp2,igrid,nv,ne)
CALL QTE(temp2,temp1,temp3,igrid,ne,nv)
CALL D2bar(temp3,zetarhs,igrid,ne,nv)

END SUBROUTINE zeta_trisk_bve_rhs_qte
! ================================================================

! ================================================================
SUBROUTINE zeta_trisk_bve_rhs_qq(zeta,fv,psi,zetarhs,igrid,nv,ne)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid,nv,ne
REAL*8, INTENT(IN) :: zeta(nv), fv(nv), psi(nv)
REAL*8, INTENT(OUT) :: zetarhs(nv)
REAL*8 :: temp1(nv), temp2(ne), temp3(ne)

CALL J(zeta + fv,temp1,igrid,nv)
CALL D1(psi,temp2,igrid,nv,ne)
CALL QQ(temp2,temp1,temp3,igrid,ne,nv)
CALL D2bar(temp3,zetarhs,igrid,ne,nv)

END SUBROUTINE zeta_trisk_bve_rhs_qq
! ================================================================

! ================================================================
SUBROUTINE zeta_zgrid_bve_rhs(zeta,fi,psi,zetarhs,igrid,nf)
IMPLICIT NONE
INTEGER, INTENT(IN) :: igrid,nf
REAL*8, INTENT(IN) :: zeta(nf), fi(nf), psi(nf)
REAL*8, INTENT(OUT) :: zetarhs(nf)
REAL*8 :: temp1(nf)

CALL jacobian(zeta + fi,psi,temp1,igrid,nf)
zetarhs = -1.0d0 * temp1

END SUBROUTINE zeta_zgrid_bve_rhs
! ================================================================

! ================================================================
SUBROUTINE compute_zgrid_bve_statistics(n,zetai,fi,psi,nf,ne)
IMPLICIT NONE
INTEGER, INTENT(IN) :: nf,n,ne
REAL*8, INTENT(IN) :: zetai(nf), fi(nf), psi(nf)
REAL*8 :: etai(nf), eta_adj(nf), zeta_adj(nf), ke(ne)
INTEGER :: if1,ix1,ie1,if2
REAL*8 :: temp

etai = zetai + fi
CALL Iinv(zetai,zeta_adj,ngrids,nf) !assumes I is diagonal!
CALL Iinv(etai,eta_adj,ngrids,nf) !assumes I is diagonal!

eta(n) = kahan_sum(eta_adj,nf)
zeta(n) = kahan_sum(zeta_adj,nf)

DO ie1=1,ne
	if1 = ce(ie1,1,ngrids)
	if2 = ce(ie1,2,ngrids)
	ke(ie1) = ((psi(if1) - psi(if2))*(psi(if1) - psi(if2)))*h_coeffs(ie1,1,ngrids) * 0.5d0
ENDDO

kinetic(n) = kahan_sum(ke,ne)
enstrophy(n) = kahan_sum(0.5d0 * etai * eta_adj,nf)

	
END SUBROUTINE compute_zgrid_bve_statistics
! ================================================================

! ================================================================
SUBROUTINE compute_trisk_bve_statistics(n,zetav,fv,psi,nv,ne)
IMPLICIT NONE
INTEGER, INTENT(IN) :: nv,n,ne
REAL*8, INTENT(IN) :: zetav(nv), fv(nv), psi(nv)
REAL*8 :: etav(nv), eta_adj(nv), ke(ne)
INTEGER :: iv1,ie1,iv2
REAL*8 :: temp

etav = zetav + fv
CALL J(etav,eta_adj,ngrids,nv)

eta(n) = kahan_sum(etav,nv)
zeta(n) = kahan_sum(zetav,nv)

DO ie1=1,ne
	iv1 = ve(ie1,1,ngrids)
	iv2 = ve(ie1,2,ngrids)
	ke(ie1) = ((psi(iv1) - psi(iv2))*(psi(iv1) - psi(iv2)))*h_coeffs(ie1,1,ngrids) * 0.5d0
ENDDO

kinetic(n) = kahan_sum(ke,ne)
enstrophy(n) = kahan_sum(0.5d0 * etav * eta_adj,nv)

	
END SUBROUTINE compute_trisk_bve_statistics
! ================================================================












!================= MULTGRID SPECIFIC STUFF =======================!
! ================================================================
SUBROUTINE restrict(f1,nf1,f2,nf2,igrid)
IMPLICIT NONE
! To perform the restriction operation needed for a multigrid solver.
! Restrict field f1 from grid igrid + 1 to grid igrid and put the
! result in field f2. f1 and f2 are assumed to be area integrals
! (discrete 2-forms).

INTEGER, INTENT(IN) :: nf1, nf2, igrid
REAL*8, INTENT(IN) :: f1(nf1)
REAL*8, INTENT(OUT) :: f2(nf2)
INTEGER :: if1, if2, ix
REAL*8 :: wgt, temp1(nf1),temp2(nf2)

! Safety check
!IF (nf2 .ne. nface(igrid)) THEN
!  PRINT *,'Wrong size array in subroutine restrict'
!  STOP
!ENDIF

!IF (nf2 .ne. 2*nface(igrid)) THEN
!  PRINT *,'Wrong size array in subroutine restrict'
!  STOP
!ENDIF

CALL restrict_h(f1(1:nf1/2),nf1/2,f2(1:nf2/2),nf2/2,igrid)
CALL restrict_h(f1(nf1/2+1:nf1),nf1/2,f2(nf2/2+1:nf2),nf2/2,igrid)

!CALL Iinv(f1(1:nf1/2),temp1(1:nf1/2),igrid+1,nf1/2)
!DO if2 = 1, nf2/2
!  temp2(if2) = 0.0d0
!  DO ix = 1, inj_stencil_size(if2,igrid)
!    if1 = inj_stencil(if2,ix,igrid)
!    wgt = inj_weights(if2,ix,igrid)
!    temp2(if2) = temp2(if2) + wgt*temp1(if1)
!  ENDDO
!ENDDO
!CALL I(temp2(1:nf2/2),f2(1:nf2/2),igrid,nf2/2)

!CALL Iinv(f1(nf1/2:nf1),temp1(nf1/2:nf1),igrid+1,nf1/2)
!DO if2 = 1, nf2/2
!  temp2(if2+nf2/2) = 0.0d0
!  DO ix = 1, inj_stencil_size(if2,igrid)
!    if1 = inj_stencil(if2,ix,igrid)
!    wgt = inj_weights(if2,ix,igrid)
!    temp2(if2+nf2/2) = temp2(if2+nf2/2) + wgt*temp1(if1+nf1/2)
!  ENDDO
!ENDDO
!CALL I(temp2(nf2/2:nf2),f2(nf2/2:nf2),igrid,nf2/2)

!CALL Iinv(f1,temp1,igrid+1,nf1)
!DO if2 = 1, nf2
!  temp2(if2) = 0.0d0
!  DO ix = 1, inj_stencil_size(if2,igrid)
!    if1 = inj_stencil(if2,ix,igrid)
!    wgt = inj_weights(if2,ix,igrid)
!    temp2(if2) = temp2(if2) + wgt*temp1(if1)
!  ENDDO
!ENDDO
!call I(temp2,f2,igrid,nf2)

!DO if2 = 1, nf2
!  f2(if2) = 0.0d0
!  DO ix = 1, inj_stencil_size(if2,igrid)
!    if1 = inj_stencil(if2,ix,igrid)
!    wgt = inj_weights(if2,ix,igrid)
!    f2(if2) = f2(if2) + wgt*f1(if1)
!  ENDDO
!ENDDO

END SUBROUTINE restrict
! ================================================================

! ================================================================
SUBROUTINE prolong(f2,nf2,f1,nf1,igrid)
IMPLICIT NONE

!The prolongation operator is the adjoint ofthe restriction operator, so uses the same stencil and weights.
INTEGER, INTENT(IN) :: nf1, nf2, igrid
REAL*8, INTENT(IN) :: f2(nf2)
REAL*8, INTENT(OUT) :: f1(nf1)
INTEGER :: if1, if2, ix
REAL*8 :: wgt, temp1(nf1),temp2(nf2), f2if2

! Safety check
!IF (nf2 .ne. nface(igrid)) THEN
!  PRINT *,'Wrong size array in subroutine prolong'
!  STOP
!ENDIF

!IF (nf2 .ne. 2*nface(igrid)) THEN
!  PRINT *,'Wrong size array in subroutine prolong'
!  STOP
!ENDIF

!f1 = 0.0d0

CALL prolong_h(f2(1:nf2/2),nf2/2,f1(1:nf1/2),nf1/2,igrid)
CALL prolong_h(f2(nf2/2+1:nf2),nf2/2,f1(nf1/2+1:nf1),nf1/2,igrid)

!DO if2 = 1, nf2/2
!  DO ix = 1, inj_stencil_size(if2,igrid)
!    if1 = inj_stencil(if2,ix,igrid)
!    wgt = inj_weights(if2,ix,igrid)
!    f1(if1) = f1(if1) + wgt*f2(if2)
!  ENDDO
!ENDDO
!DO if2 = 1, nf2/2
!  DO ix = 1, inj_stencil_size(if2,igrid)
!    if1 = inj_stencil(if2,ix,igrid)
!    wgt = inj_weights(if2,ix,igrid)
!    f1(if1+nf1/2) = f1(if1+nf1/2) + wgt*f2(if2+nf2/2)
!  ENDDO
!ENDDO
!f1 = 0.0d0
!DO if2 = 1, nf2
!  DO ix = 1, inj_stencil_size(if2,igrid)
!    if1 = inj_stencil(if2,ix,igrid)
!    wgt = inj_weights(if2,ix,igrid)
!    f1(if1) = f1(if1) + wgt*f2(if2)
!  ENDDO
!ENDDO

!CALL I(f2,temp2,igrid,nf2)
!temp1 = 0.0d0
!DO if2 = 1, nf2
!  f2if2 = temp2(if2)
!  DO ix = 1, inj_stencil_size(if2,igrid)
!    if1 = inj_stencil(if2,ix,igrid)
!    wgt = inj_weights(if2,ix,igrid)
!    temp1(if1) = temp1(if1) + wgt*f2if2
!  ENDDO
!ENDDO
!CALL Iinv(temp1,f1,igrid+1,nf1)

END SUBROUTINE prolong
! ================================================================

! ================================================================
SUBROUTINE smooth(f,rhs,igrid,nf,niter)
IMPLICIT NONE

INTEGER, INTENT(IN) :: igrid, nf, niter
REAL*8, INTENT(IN) :: rhs(nf)
REAL*8, INTENT(INOUT) :: f(nf)
REAL*8, ALLOCATABLE :: res(:), finc(:)
REAL*8 :: u
INTEGER :: iter

ALLOCATE(res(nf), finc(nf))

!WRITE(*,*) igrid,nf

u = 0.8d0 !COULD BE OPTIMIZED

DO iter = 1, niter
  CALL calculate_residual(f,rhs,res,igrid,nf)
!$OMP PARALLEL WORKSHARE
  finc = res/diagcoeff(1:nf,igrid)
  !finc = res*laplac_dual_diag_coeffs(1:nf,igrid)
  f = f + u*finc
!$OMP END PARALLEL WORKSHARE
!write(*,*) iter,res(1:5),finc(1:5)
ENDDO

DEALLOCATE(res, finc)
END SUBROUTINE
! ================================================================

! ================================================================
SUBROUTINE calculate_residual(f,rhs,res,igrid,nf)
IMPLICIT NONE

INTEGER, INTENT(IN) :: igrid, nf
REAL*8, INTENT(IN) :: f(nf), rhs(nf)
REAL*8, INTENT(OUT) :: res(nf)

CALL chi_psi_apply_operator(f,res,igrid,nf)
!CALL apply_laplacian_dual(f,res,igrid,nf)

!$OMP PARALLEL WORKSHARE
res = rhs - res
!$OMP END PARALLEL WORKSHARE

!WRITE(*,*) 'RMS RESIDUAL',SQRT(SUM(res*res)/nf)
END SUBROUTINE
! ================================================================

! ================================================================
SUBROUTINE smooth_single(f,rhs,igrid,nf,niter)
IMPLICIT NONE

INTEGER, INTENT(IN) :: igrid, nf, niter
REAL*8, INTENT(IN) :: rhs(nf)
REAL*8, INTENT(INOUT) :: f(nf)
REAL*8, ALLOCATABLE :: res(:), finc(:)
REAL*8 :: u
INTEGER :: iter

ALLOCATE(res(nf), finc(nf))

!WRITE(*,*) igrid,nf
u = 0.8d0 !COULD BE OPTIMIZED
DO iter = 1, niter
  CALL calculate_residual_single(f,rhs,res,igrid,nf)
  finc = res*laplac_diag_coeffs(1:nf,igrid) !
  f = f + u*finc
!write(*,*) iter,res(1:5),finc(1:5)
ENDDO

DEALLOCATE(res, finc)
END SUBROUTINE smooth_single
! ================================================================

! ================================================================
SUBROUTINE calculate_residual_single(f,rhs,res,igrid,nf)
IMPLICIT NONE

INTEGER, INTENT(IN) :: igrid, nf
REAL*8, INTENT(IN) :: f(nf), rhs(nf)
REAL*8, INTENT(OUT) :: res(nf)

!CALL chi_psi_apply_operator(f,res,igrid,nf)
CALL apply_laplacian(f,res,igrid,nf)
res = rhs - res
!WRITE(*,*) 'RMS RESIDUAL',SQRT(SUM(res*res)/nf)
END SUBROUTINE calculate_residual_single
! ================================================================

! ================================================================
SUBROUTINE Fcycle(phi,rr,ng,ncells,npass,niter,niterc)
IMPLICIT NONE
! Multigrid solver for elliptic equation
!
! L phi = rr
!
! using full multigrid algorithm.
!
! where L is the operator specific in apply_operator

INTEGER :: ncells
INTEGER, INTENT(IN) :: ng,npass,niterc,niter
REAL*8, INTENT(IN) :: rr(ncells)
REAL*8, INTENT(OUT) :: phi(ncells)

INTEGER :: ipass, nf1, nf2, igrid, igridp, jgrid, jgridp
REAL*8, ALLOCATABLE :: ff(:,:), rf(:,:)
REAL*8 :: temp1(ncells)

!write(*,*) ncells,npass,ng,niter,niterc
! ------------------------------------------------------------------------

! Allocate space on all grids
ALLOCATE(ff(ncells,ngrids),rf(ncells,ngrids))

! One pass should be enough. Warn user if npass is set to
! some other value for testing
!IF (npass > 1) PRINT *,'mgsolve: npass = ',npass

! ------------------------------------------------------------------------

! Initialize solution to zero
!!$OMP PARALLEL WORKSHARE
!phi = 0.0d0
!!$OMP END PARALLEL WORKSHARE
!!$OMP END PARALLEL WORKSHARE
!write(*,*) MAXVAL(phi),MINVAL(phi)
!write(*,*) MAXVAL(rr),MINVAL(rr)

! For diagnostics
!nf1 = nface(ngrids)
!ne1 = nedge(ngrids)
!CALL calculate_residual(phi,rr,temp1,ngrids,nf1,ne1)
!print *,'Pass ',0,'  RMS residual = ',SQRT(SUM(temp1*temp1)/nf1)

DO ipass = 1, npass

  ! Initialize rhs as residual using latest estimate
  IF (ipass == 1) THEN
    ! No need to do the calculation
!$OMP PARALLEL WORKSHARE
    rf(:,ngrids) = rr(:)
!$OMP END PARALLEL WORKSHARE
  ELSE
    nf1 = nface(ngrids)
    CALL calculate_residual(phi(1:2*nf1),rr(1:2*nf1),rf(1:2*nf1,ngrids),ngrids,2*nf1)
  ENDIF
!write(*,*) ipass, MAXVAL(phi),MINVAL(phi)

  ! Initialize correction to solution on all grids to zero
  ff = 0.0d0

  ! Restrict right hand side to each grid in the hierarchy
  DO igrid = ngrids-1, ngrids-ng+1, -1
    igridp = igrid + 1
    nf1 = nface(igridp)
    nf2 = nface(igrid)
    CALL restrict(rf(1:2*nf1,igridp),2*nf1,rf(1:2*nf2,igrid),2*nf2,igrid)
  ENDDO

  ! Iterate to convergence on coarsest grid
  igrid = ngrids-ng+1
  nf1 = nface(igrid)
!$OMP PARALLEL WORKSHARE
  ff(1:2*nf1,igrid) = 0.0d0
!$OMP END PARALLEL WORKSHARE
  CALL smooth(ff(1:2*nf1,igrid),rf(1:2*nf1,igrid),igrid,2*nf1,niterc)

  ! Sequence of growing V-cycles
  DO igridp = ngrids-ng+2, ngrids

    igrid = igridp - 1
    nf1 = nface(igridp)
    nf2 = nface(igrid)

    ! Prolong solution to grid igridp
    ! and execute one V-cycle starting from grid igridp

    ! Prolong
    CALL prolong(ff(1:2*nf2,igrid),2*nf2,ff(1:2*nf1,igridp),2*nf1,igrid)

    ! Descending part of V-cycle
    DO jgrid = igrid, ngrids-ng+1, -1
    
      jgridp = jgrid + 1
      nf1 = nface(jgridp)
      nf2 = nface(jgrid)

      ! Relax on grid jgridp
      CALL smooth(ff(1:2*nf1,jgridp),rf(1:2*nf1,jgridp),jgridp,2*nf1,niter)

      ! Calculate residual on jgridp
      CALL calculate_residual(ff(1:2*nf1,jgridp),rf(1:2*nf1,jgridp),temp1(1:2*nf1),jgridp,2*nf1)
  
      ! Restrict residual to jgrid
      CALL restrict(temp1(1:2*nf1),2*nf1,rf(1:2*nf2,jgrid),2*nf2,jgrid)

      ! Set correction first guess to zero on grid jgrid-1
!$OMP PARALLEL WORKSHARE
      ff(1:2*nf2,jgrid) = 0.0d0
!$OMP END PARALLEL WORKSHARE

    ENDDO

    ! Iterate to convergence on coarsest grid
    jgrid = ngrids-ng+1
    nf1 = nface(jgrid)
!$OMP PARALLEL WORKSHARE
    ff(1:2*nf1,jgrid) = 0.0d0
!$OMP END PARALLEL WORKSHARE
    CALL smooth(ff(1:2*nf1,jgrid),rf(1:2*nf1,jgrid),jgrid,2*nf1,niterc)

    ! Ascending part of V-cycle
    DO jgrid = ngrids-ng+1, igrid

      jgridp = jgrid + 1
      igrid = igrid - 1
      nf1 = nface(jgridp)
      nf2 = nface(jgrid)

      ! Prolong correction to jgridp
      CALL prolong(ff(1:2*nf2,jgrid),2*nf2,temp1(1:2*nf1),2*nf1,jgrid)

      ! Add correction to solution on jgridp
!$OMP PARALLEL WORKSHARE
      ff(1:2*nf1,jgridp) = ff(1:2*nf1,jgridp) + temp1(1:2*nf1)
!$OMP END PARALLEL WORKSHARE

      ! Relax on grid jgridp
      CALL smooth(ff(1:2*nf1,jgridp),rf(1:2*nf1,jgridp),jgridp,2*nf1,niter)

    ENDDO

  ENDDO

  ! Add correction to phi
  !$OMP PARALLEL WORKSHARE
  phi = phi + ff(:,ngrids)
!$OMP END PARALLEL WORKSHARE
  ! For diagnostics
  !nf1 = nface(ngrids)
  !ne1 = nedge(ngrids)
  !CALL calculate_residual(phi,rr,temp1,ngrids,nf1,ne1)
  !print *,'Pass ',ipass,'  RMS residual = ',SQRT(SUM(temp1*temp1)/nf1)
!write(*,*) ipass, MAXVAL(ff(:,ngrids)),MINVAL(ff(:,ngrids))
!write(*,*) ipass, MAXVAL(phi),MINVAL(phi)


ENDDO

!write(*,*) MAXVAL(phi),MINVAL(phi)
!write(*,*) MAXVAL(rr),MINVAL(rr)

DEALLOCATE(ff,rf)
END SUBROUTINE
! ================================================================

! ================================================================
SUBROUTINE Fcycle_single(phi,rr,ng,ncells,npass,niter,niterc)
IMPLICIT NONE
! Multigrid solver for elliptic equation
!
! L phi = rr
!
! using full multigrid algorithm.
!
! where L is the operator specific in apply_operator

INTEGER :: ncells
INTEGER, INTENT(IN) :: ng,npass,niterc,niter
REAL*8, INTENT(IN) :: rr(ncells)
REAL*8, INTENT(OUT) :: phi(ncells)

INTEGER :: ipass, nf1, nf2, igrid, igridp, jgrid, jgridp
REAL*8, ALLOCATABLE :: ff(:,:), rf(:,:)
REAL*8 :: temp1(ncells)

!write(*,*) ncells,npass,ng,niter,niterc
! ------------------------------------------------------------------------

! Allocate space on all grids
ALLOCATE(ff(ncells,ngrids),rf(ncells,ngrids))

! One pass should be enough. Warn user if npass is set to
! some other value for testing
!IF (npass > 1) PRINT *,'mgsolve: npass = ',npass

! ------------------------------------------------------------------------

! Initialize solution to zero
phi = 0.0d0
!write(*,*) MAXVAL(phi),MINVAL(phi)
!write(*,*) MAXVAL(rr),MINVAL(rr)

! For diagnostics
!nf1 = nface(ngrids)
!ne1 = nedge(ngrids)
!CALL calculate_residual(phi,rr,temp1,ngrids,nf1,ne1)
!print *,'Pass ',0,'  RMS residual = ',SQRT(SUM(temp1*temp1)/nf1)

DO ipass = 1, npass

  ! Initialize rhs as residual using latest estimate
  IF (ipass == 1) THEN
    ! No need to do the calculation
    rf(:,ngrids) = rr(:)
  ELSE
    nf1 = nface(ngrids)
    CALL calculate_residual_single(phi(1:nf1),rr(1:nf1),rf(1:nf1,ngrids),ngrids,nf1)
  ENDIF

  ! Initialize correction to solution on all grids to zero
  ff = 0.0d0

  ! Restrict right hand side to each grid in the hierarchy
  DO igrid = ngrids-1, ngrids-ng+1, -1
    igridp = igrid + 1
    nf1 = nface(igridp)
    nf2 = nface(igrid)
    CALL restrict_h(rf(1:nf1,igridp),nf1,rf(1:nf2,igrid),nf2,igrid)
  ENDDO

  ! Iterate to convergence on coarsest grid
  igrid = ngrids-ng+1
  nf1 = nface(igrid)
  ff(1:2*nf1,igrid) = 0.0d0
  CALL smooth_single(ff(1:nf1,igrid),rf(1:nf1,igrid),igrid,nf1,niterc)

  ! Sequence of growing V-cycles
  DO igridp = ngrids-ng+2, ngrids

    igrid = igridp - 1
    nf1 = nface(igridp)
    nf2 = nface(igrid)

    ! Prolong solution to grid igridp
    ! and execute one V-cycle starting from grid igridp

    ! Prolong
    CALL prolong_h(ff(1:nf2,igrid),nf2,ff(1:nf1,igridp),nf1,igrid)

    ! Descending part of V-cycle
    DO jgrid = igrid, ngrids-ng+1, -1
    
      jgridp = jgrid + 1
      nf1 = nface(jgridp)
      nf2 = nface(jgrid)

      ! Relax on grid jgridp
      CALL smooth_single(ff(1:nf1,jgridp),rf(1:nf1,jgridp),jgridp,nf1,niter)

      ! Calculate residual on jgridp
      CALL calculate_residual_single(ff(1:nf1,jgridp),rf(1:nf1,jgridp),temp1(1:nf1),jgridp,nf1)
  
      ! Restrict residual to jgrid
      CALL restrict_h(temp1(1:nf1),nf1,rf(1:nf2,jgrid),nf2,jgrid)

      ! Set correction first guess to zero on grid jgrid-1
      ff(1:nf2,jgrid) = 0.0d0

    ENDDO

    ! Iterate to convergence on coarsest grid
    jgrid = ngrids-ng+1
    nf1 = nface(jgrid)
    ff(1:nf1,jgrid) = 0.0d0
    CALL smooth_single(ff(1:nf1,jgrid),rf(1:nf1,jgrid),jgrid,nf1,niterc)

    ! Ascending part of V-cycle
    DO jgrid = ngrids-ng+1, igrid

      jgridp = jgrid + 1
      igrid = igrid - 1
      nf1 = nface(jgridp)
      nf2 = nface(jgrid)

      ! Prolong correction to jgridp
      CALL prolong_h(ff(1:nf2,jgrid),nf2,temp1(1:nf1),nf1,jgrid)

      ! Add correction to solution on jgridp
      ff(1:nf1,jgridp) = ff(1:nf1,jgridp) + temp1(1:nf1)

      ! Relax on grid jgridp
      CALL smooth_single(ff(1:nf1,jgridp),rf(1:nf1,jgridp),jgridp,nf1,niter)

    ENDDO

  ENDDO

  ! Add correction to phi
  phi = phi + ff(:,ngrids)

  ! For diagnostics
  !nf1 = nface(ngrids)
  !ne1 = nedge(ngrids)
  !CALL calculate_residual(phi,rr,temp1,ngrids,nf1,ne1)
  !print *,'Pass ',ipass,'  RMS residual = ',SQRT(SUM(temp1*temp1)/nf1)

ENDDO

!write(*,*) MAXVAL(phi),MINVAL(phi)
!write(*,*) MAXVAL(rr),MINVAL(rr)

DEALLOCATE(ff,rf)
END SUBROUTINE Fcycle_single
! ================================================================

! ================================================================
SUBROUTINE set_diagonal_coefficient()
IMPLICIT NONE

!INTEGER :: igrid,if1,ixd2,ie1,ixh,ie2,ixd1,if2
!REAL*8 :: cd2,ch,ci,cd1,temp

INTEGER :: if1,ix1,ie1,igrid
REAL*8 :: temp
!Lii = -1/Ai * sum_e le/de
DO igrid=1,ngrids
!$OMP PARALLEL DO SHARED(diagcoeff,igrid) PRIVATE(if1,temp,ix1,ie1)
DO if1=1,nface(igrid)
	temp = 0.0d0
	DO ix1=1,ecsize(if1,igrid)
		ie1 = ec(if1,ix1,igrid)
		temp = temp + h_coeffs(ie1,1,igrid) * heinv(ie1,igrid)
	ENDDO
    diagcoeff(if1,igrid) = -1.d0/ai(if1,igrid)*temp
    diagcoeff(if1+nface(igrid),igrid) = -1.d0/ai(if1,igrid)*temp
ENDDO
!$OMP END PARALLEL DO
ENDDO

! Extract diagonal coefficient of flux-div operator
!DO igrid = 1, ngrids
!  ! Loop over cells
!  DO if1 = 1, nface(igrid)

!    temp = 0.0d0
!    !ASSUMES THAT I is DIAGONAL
!    ci = i_coeffs(if1,1,igrid)
            
!    ! Loop over edges of if1 involved in Dprimal2 operator
!    DO ixd2 = 1, ecsize(if1,igrid)
!      ie1 = ec(if1,ixd2,igrid)
!      cd2 = nei_cell(if1,ixd2,igrid)
!      ! Loop over edges involved in H operator
!      DO ixh = 1, h_stencil_size(ie1,igrid)
!        ie2 = h_stencil(ie1,ixh,igrid)
!        ch = h_coeffs(ie1,ixh,igrid) * heinv(ie1,igrid)
!        ! Loop over cells involved in Ddual1 operator
!        DO ixd1 = 1, 2
!          if2 = ce(ie2,ixd1,igrid)
!          cd1 = (-1)*nei_edge(ie2,ixd1,igrid)
!          IF (if2 == if1) temp = temp + cd2*ch*cd1*ci
!        ENDDO
!      ENDDO
!    ENDDO
!    diagcoeff(if1,igrid) = temp
!    diagcoeff(if1+nface(igrid),igrid) = temp

!  ENDDO
!ENDDO

!THIS IS L = D2 H D1bar I
!    ! Loop over edges of if1 involved in Dprimal2 operator
!    DO ixd2 = 1, ecsize(if1,igrid)
!      ie1 = ec(if1,ixd2,igrid)
!      cd2 = nei(if1,ixd2,igrid)
!      ! Loop over edges involved in H operator
!      DO ixh = 1, h_stencil_size(ie1,igrid)
!        ie2 = h_stencil(ie1,ixh,igrid)
!        ch = h_coeffs(ie1,ixh,igrid)
!        ! Loop over cells involved in Ddual1 operator
!        DO ixd1 = 1, 2
!          if2 = ce(ie2,ixd1,igrid)
!          cd1 = (-1)*nei_edge(ie2,ixd1,igrid)
!          ! Loop over cells in I operator
!          DO ixi = 1, i_stencil_size(if2,igrid)
!            if3 = i_stencil(if2,ixi,igrid)
!            ci = i_coeffs(if2,ixi,igrid)
!            IF (if3 == if1) temp = temp + cd2*ch*cd1*ci
!          ENDDO
!        ENDDO
!      ENDDO
!    ENDDO
    

END SUBROUTINE
! ================================================================

! ================================================================
SUBROUTINE apply_laplacian_highest(f,hf,nf)
IMPLICIT NONE

!Computes Lf = 1/Ai * sum_n (fn - f0) le/de

INTEGER, INTENT(IN) :: nf
REAL*8, INTENT(IN) :: f(nf)
REAL*8, INTENT(OUT) :: hf(nf)
INTEGER :: if1,if2,ix1
REAL*8 :: temp

!hf = 0.0d0

! Loop over cells
!$OMP PARALLEL DO SHARED(f,hf,nf) PRIVATE(if1,temp,ix1,if2)
DO if1 = 1, nf
	temp = 0.0d0
	DO ix1=1,ecsize(if1,ngrids)
		if2 = nc(if1,ix1,ngrids)
		temp = temp + laplac_primal_coeffs(if1,ix1,ngrids) * (f(if2) - f(if1))
	ENDDO
	hf(if1) = temp / ai(if1,ngrids)
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE apply_laplacian_highest
! ================================================================

! ================================================================
SUBROUTINE apply_laplacian_dual(f,hf,igrid,nf)
IMPLICIT NONE

!Computes Lf = 1/Ai * sum_n (fn - f0) le/de

INTEGER, INTENT(IN) :: nf,igrid
REAL*8, INTENT(IN) :: f(nf)
REAL*8, INTENT(OUT) :: hf(nf)
INTEGER :: if1,if2,ix1
REAL*8 :: temp1,temp2

hf = 0.0d0
!write(*,*) igrid,nf,SIZE(f)
!Loop over cells
!$OMP PARALLEL DO SHARED(f,hf,nf,igrid) PRIVATE(if1,temp1,temp2,ix1,if2)
DO if1 = 1, nf/2
	temp1 = 0.0d0
	temp2 = 0.0d0
	DO ix1=1,ecsize(if1,igrid)
		if2 = nc(if1,ix1,igrid)
		temp1 = temp1 + laplac_primal_coeffs(if1,ix1,igrid) * (f(if2) - f(if1))
		temp2 = temp2 + laplac_primal_coeffs(if1,ix1,igrid) * (f(if2+nf/2) - f(if1+nf/2))
	ENDDO
	hf(if1) = temp1 / ai(if1,igrid)
	hf(if1+nf/2) = temp2 / ai(if1,igrid)
ENDDO
!$OMP END PARALLEL DO

!! Loop over cells
!!$OMP PARALLEL DO SHARED(f,hf,nf,igrid) PRIVATE(if1,temp,ix1,if2)
!DO if1 = 1, nf
!	temp = 0.0d0
!	DO ix1=1,ecsize(if1,igrid)
!		if2 = nc(if1,ix1,igrid)
!		temp = temp + laplac_primal_coeffs(if1,ix1,igrid) * (f(if2)/ai(if2,igrid) - f(if1)/ai(if1,igrid))
!	ENDDO
!	hf(if1) = temp
!ENDDO
!!$OMP END PARALLEL DO

END SUBROUTINE apply_laplacian_dual
! ================================================================

! ================================================================
SUBROUTINE apply_laplacian(f,hf,igrid,nf)
IMPLICIT NONE

!Computes Lf = 1/Ai * sum_n (fn - f0) le/de

INTEGER, INTENT(IN) :: nf,igrid
REAL*8, INTENT(IN) :: f(nf)
REAL*8, INTENT(OUT) :: hf(nf)
INTEGER :: if1,if2,ix1
REAL*8 :: temp

!hf = 0.0d0
!write(*,*) igrid,nf,SIZE(f)
!Loop over cells
!$OMP PARALLEL DO SHARED(f,hf,nf,igrid) PRIVATE(if1,temp,ix1,if2)
DO if1 = 1, nf
	temp = 0.0d0
	DO ix1=1,ecsize(if1,igrid)
		if2 = nc(if1,ix1,igrid)
		temp = temp + laplac_primal_coeffs(if1,ix1,igrid) * (f(if2) - f(if1))
	ENDDO
	hf(if1) = temp / ai(if1,igrid)
ENDDO
!$OMP END PARALLEL DO

!! Loop over cells
!!$OMP PARALLEL DO SHARED(f,hf,nf,igrid) PRIVATE(if1,temp,ix1,if2)
!DO if1 = 1, nf
!	temp = 0.0d0
!	DO ix1=1,ecsize(if1,igrid)
!		if2 = nc(if1,ix1,igrid)
!		temp = temp + laplac_primal_coeffs(if1,ix1,igrid) * (f(if2)/ai(if2,igrid) - f(if1)/ai(if1,igrid))
!	ENDDO
!	hf(if1) = temp
!ENDDO
!!$OMP END PARALLEL DO

END SUBROUTINE apply_laplacian
! ================================================================

! ================================================================
SUBROUTINE set_laplac_diag_coeffs()
IMPLICIT NONE
INTEGER :: if1,ix1,ie1,igrid
REAL*8 :: temp
!Lii = -1/Ai * sum_e le/de
DO igrid=1,ngrids
DO if1=1,nface(igrid)
	temp = 0.0d0
	DO ix1=1,ecsize(if1,igrid)
		ie1 = ec(if1,ix1,igrid)
		temp = temp + h_coeffs(ie1,1,igrid)
	ENDDO
	laplac_diag_coeffs(if1,igrid) = -1.d0*ai(if1,igrid)/temp
	laplac_dual_diag_coeffs(if1,igrid) = -1.d0*ai(if1,igrid)/temp
	laplac_dual_diag_coeffs(if1+nface(igrid),igrid) = -1.d0*ai(if1,igrid)/temp
ENDDO
ENDDO

END SUBROUTINE set_laplac_diag_coeffs
! ================================================================

! ================================================================
SUBROUTINE find_matching_face(if0,igrid,ifp0,igridp)
INTEGER, INTENT(IN) :: if0,igrid,igridp
INTEGER, INTENT(OUT) :: ifp0
INTEGER :: if1
!Finds the index of grid point if0 from igrid on igridp, stores in ifp0

ifp0 = 0
DO if1=1,nface(igridp)

IF ((clon(if1,igridp) .EQ. clon(if0,igrid)) .AND. (clat(if1,igridp) .EQ. clat(if0,igrid))) THEN
!WRITE(*,*) ABS(clon(if1,igridp)-clon(if0,igrid)),ABS((clon(if1,igridp)-clon(if0,igrid))/clon(if0,igrid))
ifp0 = if1
EXIT
END IF

ENDDO

END SUBROUTINE
! ================================================================

! ================================================================
SUBROUTINE buildinj()
IMPLICIT NONE
INTEGER :: if0, if1, igrid, igridp, nf, ix, ixp, ninjmx, ifp0

! Allocate array for size of operator stencil
!ALLOCATE(inj_stencil_size(MAXVAL(nface),ngrids-1))

!! Stencil is 6 or 7 cells on standard buckyball grid
!ninjmx = 7

!! Allocate arrays for operator stencils and coefficients
!ALLOCATE(inj_stencil(MAXVAL(nface),ninjmx,ngrids-1))
!ALLOCATE(inj_weights(MAXVAL(nface),ninjmx,ngrids-1))

!! Initialize to zero
!inj_stencil = 0
!inj_weights = 0.0d0
!inj_stencil_size = 0

! And define stencil and coefficients
DO igrid = 1, ngrids-1
  igridp = igrid + 1
  DO if0 = 1, nface(igrid)
    ! Find face if0 on igridp
    CALL find_matching_face(if0,igrid,ifp0,igridp)
    !WRITE(*,*) igrid,igridp,if0,ifp0
    inj_stencil(if0,1,igrid) = ifp0
    inj_weights(if0,1,igrid) = 1.0d0
    ! The neighbours of if0 on grid igrid+1 are the other members
    ! of the stencil
    nf = ecsize(ifp0,igridp)
    inj_stencil_size(if0,igrid) = 1 + nf
    DO ix = 1, nf
      ixp = ix + 1
      if1 = nc(ifp0,ix,igridp)
      inj_stencil(if0,ixp,igrid) = if1
      inj_weights(if0,ixp,igrid) = 0.5d0
    ENDDO
  ENDDO
ENDDO

END SUBROUTINE buildinj
! ================================================================

SUBROUTINE build_multigrid_operator(hi,numhi)
REAL*8, INTENT(IN) :: hi(numhi)
INTEGER, INTENT(IN) :: numhi
REAL*8, ALLOCATABLE :: temp(:,:)
INTEGER :: igrid

!restrict heinv and hvinv to other grids
ALLOCATE(temp(numhi,ngrids))
!$OMP PARALLEL WORKSHARE
temp = 0.0d0
temp(:,ngrids) = hi
!$OMP END PARALLEL WORKSHARE
DO igrid = ngrids-1,1,-1
CALL restrict_h(temp(1:nface(igrid+1),igrid+1),nface(igrid+1),temp(1:nface(igrid),igrid),nface(igrid),igrid)
CALL diagnose_h_quantities(temp(1:nface(igrid),igrid),igrid,nface(igrid))
ENDDO
DEALLOCATE(temp)

END SUBROUTINE build_multigrid_operator


! ================================================================
SUBROUTINE restrict_h(f1,nf1,f2,nf2,igrid)
IMPLICIT NONE
! To perform the restriction operation needed for a multigrid solver.
! Restrict field f1 from grid igrid + 1 to grid igrid and put the
! result in field f2. f1 and f2 are assumed to be area integrals
! (discrete 2-forms).

INTEGER, INTENT(IN) :: nf1, nf2, igrid
REAL*8, INTENT(IN) :: f1(nf1)
REAL*8, INTENT(OUT) :: f2(nf2)
INTEGER :: if1, if2, ix
REAL*8 :: wgt, temp1(nf1),temp2(nf2), temp

! Safety check
!IF (nf2 .ne. nface(igrid)) THEN
!  PRINT *,'Wrong size array in subroutine restrict'
!  STOP
!ENDIF

CALL Iinv(f1,temp1,igrid+1,nf1)
!$OMP PARALLEL DO SHARED(temp1,temp2,nf2,igrid) PRIVATE(if2,ix,temp)
DO if2 = 1, nf2
  temp = 0.0d0
  DO ix = 1, inj_stencil_size(if2,igrid)
    if1 = inj_stencil(if2,ix,igrid)
    wgt = inj_weights(if2,ix,igrid)
    temp = temp + wgt*temp1(if1)
  ENDDO
  temp2(if2) = temp
ENDDO
!$OMP END PARALLEL DO
call I(temp2,f2,igrid,nf2)

END SUBROUTINE restrict_h
! ================================================================

! ================================================================
SUBROUTINE chi_psi_apply_fd_operator(v,v_out,igrid,nf)
IMPLICIT NONE
INTEGER, INTENT(IN) :: nf, igrid
REAL*8, INTENT(IN) :: v(nf)
REAL*8, INTENT(OUT) :: v_out(nf)
INTEGER :: if1, ix1, if2,if3,ie1
REAL*8 :: temp, temp2

!$OMP PARALLEL DO SHARED(v,v_out,nf,igrid) PRIVATE(if1,ix1,temp,temp2,ie1,if2,if3)
DO if1=1,nf
	temp = 0.0d0
	DO ix1=1,ecsize(if1,igrid)
		ie1 = ec(if1,ix1,igrid)
		if2 = ce(ie1,1,igrid)
		if3 = ce(ie1,2,igrid)
		!write(*,*) if1,ix1,ie1,if2,if3
		temp2 = (-1.d0)*nei_edge(ie1,1,igrid) * v(if2) + (-1.d0)*nei_edge(ie1,2,igrid) * v(if3)
		temp = temp + nei_cell(if1,ix1,igrid) * h_coeffs(ie1,1,igrid) * heinv(ie1,igrid) * temp2
		!this could be simplified into flux-div format IFF ec(ix1) and nc(ix1) refered to the same edge
		!this would be faster!- would save a memory access
	ENDDO
	v_out(if1) = v_out(if1) + temp / ai(if1,igrid)
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE chi_psi_apply_fd_operator
! ================================================================

! ================================================================
SUBROUTINE chi_psi_apply_jacob_operator(v,v_out,igrid,nf)
IMPLICIT NONE
INTEGER, INTENT(IN) :: nf, igrid
REAL*8, INTENT(IN) :: v(nf)
REAL*8, INTENT(OUT) :: v_out(nf)
INTEGER :: if1, ix1, if2,if3,ie1,iv1,iv2
REAL*8 :: temp, temp2

!Jchi block (part associated with zeta)
!$OMP PARALLEL DO SHARED(v,v_out,nf,igrid) PRIVATE(if1,ix1,temp,temp2,ie1,if2,if3,iv1,iv2)
DO if1=1,nf
	temp = 0.0d0
	DO ix1=1,ecsize(if1,igrid)
		ie1 = ec(if1,ix1,igrid)
		if2 = ce(ie1,1,igrid)
		if3 = ce(ie1,2,igrid)
		iv1 = ve(ie1,1,igrid)
		iv2 = ve(ie1,2,igrid)
		temp2 = tev_edge(ie1,1,igrid) * hvinv(iv1,igrid) + tev_edge(ie1,2,igrid) * hvinv(iv2,igrid)		 
		temp = temp + nei_cell(if1,ix1,igrid) * (v(if2) + v(if3)) * temp2
	ENDDO
	v_out(if1) = v_out(if1) + 0.5d0*temp / ai(if1,igrid)
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE chi_psi_apply_jacob_operator
! ================================================================

! ================================================================
SUBROUTINE chi_psi_apply_operator(v,v_out,igrid,nf2)
IMPLICIT NONE
INTEGER, INTENT(IN) :: nf2,igrid
REAL*8, INTENT(IN) :: v(nf2)
REAL*8, INTENT(OUT) :: v_out(nf2)
!INTEGER :: if1, ix1, if2,if3,ie1,iv1,iv2
!REAL*8 :: temp, temp2
!v(1,nf2/2) is chi 
!v(nf2/2,nf2) is psi

!$OMP PARALLEL WORKSHARE
v_out = 0.d0
!$OMP END PARALLEL WORKSHARE

!Delta block
CALL chi_psi_apply_fd_operator(v(1:nf2/2),v_out(1:nf2/2),igrid,nf2/2) !FD CHI
CALL chi_psi_apply_jacob_operator(-1.d0*v(nf2/2+1:nf2),v_out(1:nf2/2),igrid,nf2/2) !J PSI 

!Zeta block
CALL chi_psi_apply_fd_operator(v(nf2/2+1:nf2),v_out(nf2/2+1:nf2),igrid,nf2/2) !FD PSI
CALL chi_psi_apply_jacob_operator(v(1:nf2/2),v_out(nf2/2+1:nf2),igrid,nf2/2) !J CHI

END SUBROUTINE chi_psi_apply_operator
! ================================================================

! ================================================================
SUBROUTINE prolong_h(f2,nf2,f1,nf1,igrid)
IMPLICIT NONE

!The prolongation operator is the adjoint ofthe restriction operator, so uses the same stencil and weights.
INTEGER, INTENT(IN) :: nf1, nf2, igrid
REAL*8, INTENT(IN) :: f2(nf2)
REAL*8, INTENT(OUT) :: f1(nf1)
INTEGER :: if1, if2, ix
REAL*8 :: wgt

!Safety check
!IF (nf2 .ne. nface(igrid)) THEN
!  PRINT *,'Wrong size array in subroutine prolong'
!  STOP
!ENDIF

!$OMP PARALLEL WORKSHARE
f1 = 0.0d0
!$OMP END PARALLEL WORKSHARE

!$OMP PARALLEL DO PRIVATE(if2,ix,if1,wgt) REDUCTION(+:f1)
DO if2 = 1, nf2
  DO ix = 1, inj_stencil_size(if2,igrid)
    if1 = inj_stencil(if2,ix,igrid)
    wgt = inj_weights(if2,ix,igrid)
    f1(if1) = f1(if1) + wgt*f2(if2)
  ENDDO
ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE prolong_h
! ================================================================


! ================================================================
SUBROUTINE forcing(f,feqm,fout,nf,tau)
IMPLICIT NONE
REAL*8, INTENT(IN) :: f(nf),feqm(nf), tau
REAL*8, INTENT(OUT) :: fout(nf)
INTEGER, INTENT(IN) :: nf

!$OMP PARALLEL WORKSHARE
fout = (feqm - f)/tau
!$OMP END PARALLEL WORKSHARE

END SUBROUTINE forcing
! ================================================================

END MODULE grid

