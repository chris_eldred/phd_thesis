from basemap_plotting_helpers import *
import sys
import subprocess

gridtypes = ['geodesic','cubedsphere']
gridvariant_dict = {'geodesic' : ['tweaked'] , 'cubedsphere': ['thuburn',]} #'cvt','spring_beta=1.1','spring_beta=0.8',
trisk_variants = ['TEQ','TE','Q'] 
title_list = ['Doubly Conservative','Energy Conserving','Enstrophy Conserving']
outdir = 'output'

dissipationtype = sys.argv[4]
dissipation = sys.argv[3]
scheme = sys.argv[2]
simulation = sys.argv[1]

#gridtypes = ['cubedsphere',]
#trisk_variants = ['TE',]

eta_levels_galew = np.linspace(-0.00018,0.00030,9)
eta_levels_galewnoperturb = np.linspace(-0.00015,0.00025,9)
eta_levels_tc2 = np.linspace(-0.00016,0.00016,9)
eta_levels_rhwave = np.linspace(-0.0002,0.0002,9)
h_levels_galew = np.linspace(8500,10500,9)
h_levels_galewnoperturb = np.linspace(8500,10500,9)
h_levels_tc2 = np.linspace(1000,3000,9)
h_levels_rhwave = np.linspace(8000,10800,9)
delta_levels =  np.linspace(-0.0000024,0.0000040,9)
q_levels = np.linspace(-3.2,3.2,9) * 10.**(-8)

###################### SIMULATIONS ##########################
if simulation == 'Galewsky':
	output_name = 'Galewsky'
	vlevels = [8,7,6]
	steps = [24,40]
	stepnames = ['6','10'] 
	variables_list = [('etav','diagnosed','dual','Absolute Vorticity','latlon',eta_levels_galew),('hi','diagnosed','primal','Fluid Height','latlon',h_levels_galew)]
	
if simulation == 'GalewskyInit':
	output_name = 'GalewskyInit'
	vlevels = [6,7,8]
	steps = [3,6,9,12,24]
	stepnames = ['3','6','9','12','24']
	variables_list = [('di','diagnosed','primal','Divergence','latlon',delta_levels)]

if simulation == 'GalewskyNoPerturb':
	output_name = 'GalewskyNoPerturb'
	vlevels = [6,7,8]
	steps = [0,24,40]
	stepnames = ['0','6','10']
	variables_list = [('etav','diagnosed','dual','Absolute Vorticity','latlon',eta_levels_galewnoperturb),('hi','diagnosed','primal','Fluid Height','latlon',h_levels_galewnoperturb)]

if simulation == 'TC2':
	output_name = 'TC2'
	vlevels = [6,7,8]
	steps = [0,24,40]
	stepnames = ['0','6','10']
	variables_list = [('etav','diagnosed','dual','Absolute Vorticity','latlon',eta_levels_tc2),('hi','diagnosed','primal','Fluid Height','latlon',h_levels_tc2)]

if simulation == 'RHwave':
	output_name = 'RHwave'
	vlevels = [6,7]
	steps = [40,60,80,120,200]
	stepnames = ['10','15','20','30','50']
	variables_list = [('etav','diagnosed','dual','Absolute Vorticity','latlon',eta_levels_rhwave),('hi','diagnosed','primal','Fluid Height','latlon',h_levels_rhwave)]

if simulation == 'TC5':
	output_name = 'TC5'
	vlevels = [6,7]
	steps = [120,160,200]
	stepnames = ['30','40','50']
	variables_list = [('qv','diagnosed','dual','Potential Vorticity','spherical',q_levels)]

#adjust some things for ZGRID
if scheme == 'ZGRID':
	trisk_variants = ['TEQ',]
	gridtypes = ['geodesic',]
	output_name = output_name + 'Z'
	title_list = ['Doubly Conservative',]
	if simulation == 'Galewsky':
		variables_list = [('eta','diagnosed','primal','Absolute Vorticity','latlon',eta_levels_galew),('hi','predicted','primal','Fluid Height','latlon',h_levels_galew)]
	if simulation == 'GalewskyNoPerturb':
		variables_list = [('eta','diagnosed','primal','Absolute Vorticity','latlon',eta_levels_galewnoperturb),('hi','predicted','primal','Fluid Height','latlon',h_levels_galewnoperturb)]
	if simulation == 'TC2':
		variables_list = [('eta','diagnosed','primal','Absolute Vorticity','latlon',eta_levels_tc2),('hi','predicted','primal','Fluid Height','latlon',h_levels_tc2)]
	if simulation == 'RHwave':
		variables_list = [('eta','diagnosed','primal','Absolute Vorticity','latlon',eta_levels_rhwave),('hi','predicted','primal','Fluid Height','latlon',h_levels_rhwave)]
	if simulation == 'TC5':
		variables_list = [('q','diagnosed','primal','Potential Vorticity','spherical',q_levels)]
	if simulation == 'GalewskyInit':
		variables_list = [('delta','predicted','primal','Divergence','latlon',delta_levels)]

if dissipation == 'False':

	dissipdir = 'none'
if dissipation == 'True':
	if dissipationtype == 'hyperviscosity':
		dissipdir = 'hyper'
	if dissipationtype == 'APVM':
		dissipdir = 'apvm'

#####PLOTS########
# Make plotting directory
subprocess.call(['mkdir','-p',outdir + '/' + output_name + '/plots'])

for gridtype in gridtypes:
	for gridvariant in gridvariant_dict[gridtype]:
		for vlevel in vlevels:
			meshdata = get_geometry(gridtype,gridvariant,vlevel)
			for variant,title in zip(trisk_variants,title_list):
				print 'plotting',gridtype,gridvariant,vlevel,variant
				variables = {}
				try:
					varsfile = h5py.File(outdir + '/' + output_name + '/' + gridtype + '/' + gridvariant + '/' + str(vlevel) + '-' + variant + '/' + dissipdir + '/vars.hdf5', 'r')
					for varname,vartype,varloc,longname,plottype,_ in variables_list:
						variables[varname] = np.array(varsfile[vartype][varname])
					varsfile.close()
				except:
					print 'does not exist',gridtype,gridvariant,vlevel,variant,dissipdir,scheme,simulation
					for varname,vartype,varloc,longname,plottype,_ in variables_list:
						variables[varname] = None
				
				for varname,vartype,varloc,longname,plottype,levels in variables_list:
					for step,stepname in zip(steps,stepnames):
						if variables[varname] != None:
							data = variables[varname][step,:]
						else:
							continue
						if output_name == 'GalewskyInit':
							timename = ' at Hour '
						else:
							timename = ' at Day '
						plot_scalar(meshdata,data,varloc,longname + timename + stepname + ', ' +title, \
						outdir +'/' + output_name + '/plots/' + varname + '.' + output_name + '.' + variant + '.' + gridtype + '.' + gridvariant + '.' + str(vlevel) + '.' + dissipdir + '.' + str(step),plottype,'contour',levels)
