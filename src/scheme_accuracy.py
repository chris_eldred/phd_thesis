
from constants import *
from mesh import *
from configuration import *
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as spstats
from sympy import *
from sympy.utilities.lambdify import implemented_function
from math import pow
import h5py
import time
import grid
import sys

start = time.clock()
get_configuration(sys.argv[1])
print 'configured'

def l2_error(diff,Iinv):
	return np.sqrt(np.sum(Iinv * np.square(diff),dtype='float128')/Iinv.sum())
	
def linf_error(diff):
	return np.amax(np.abs(diff))

grid_types_and_variants = [('geodesic','tweaked'),('geodesic','spring_beta=0.8'),('geodesic','spring_beta=1.1'),('geodesic','cvt'),('cubedsphere','thuburn')]
vlevels = [1,2,3,4,5,6,7,8] #,6,7,8]
nmodels = len(grid_types_and_variants)
num_dofs = np.zeros((len(vlevels),nmodels))
diff_out_file = h5py.File('orderofaccuracy/scheme_errors.h5py','w')

#plot operator accuracy
def plot_operator_accuracy(name,ndofs,error_matrix,label_list):
	print 'plotting',name
	plt.figure()
	x = np.arange(0.9*np.nanmin(ndofs),1.1*np.nanmax(ndofs),dtype=np.int32)
	maxerr = error_matrix[0,0]
	b1 = maxerr/pow(np.ravel(ndofs)[0],-1.)
	b2 = maxerr/pow(np.ravel(ndofs)[0],-2.)
	y1 = b1*np.power(x,-1.)
	y2 = b2*np.power(x,-2.)
	for i in range(error_matrix.shape[1]):
		plt.plot(ndofs[:,i],error_matrix[:,i],label=label_list[i])
		plt.scatter(ndofs[:,i],error_matrix[:,i]) 
	plt.plot(x,y1,linestyle='--',color='k')
	#plt.plot(x,y2,linestyle='--',color='k')
	plt.yscale('log')
	plt.xscale('log')
	plt.legend(loc=3)
	plt.ylabel('Error')
	plt.xlabel('Number of Degrees of Freedom')
	plt.savefig('orderofaccuracy/' + name + '.png')
	plt.close()

vlevels = [2,3,4,5,6,7,8] #,8]
num_dofs = np.zeros((len(vlevels),nmodels))
scheme_Q_h_l2_error_matrix = np.zeros((len(vlevels),nmodels))
scheme_Q_h_linf_error_matrix = np.zeros((len(vlevels),nmodels))
scheme_Q_u_l2_error_matrix = np.zeros((len(vlevels),nmodels))
scheme_Q_u_linf_error_matrix = np.zeros((len(vlevels),nmodels))
scheme_TE_h_l2_error_matrix = np.zeros((len(vlevels),nmodels))
scheme_TE_h_linf_error_matrix = np.zeros((len(vlevels),nmodels))
scheme_TE_u_l2_error_matrix = np.zeros((len(vlevels),nmodels))
scheme_TE_u_linf_error_matrix = np.zeros((len(vlevels),nmodels))
scheme_TEQ_h_l2_error_matrix = np.zeros((len(vlevels),nmodels))
scheme_TEQ_h_linf_error_matrix = np.zeros((len(vlevels),nmodels))
scheme_TEQ_u_l2_error_matrix = np.zeros((len(vlevels),nmodels))
scheme_TEQ_u_linf_error_matrix = np.zeros((len(vlevels),nmodels))
scheme_Z_h_l2_error_matrix = np.zeros((len(vlevels),nmodels-1))
scheme_Z_h_linf_error_matrix = np.zeros((len(vlevels),nmodels-1))
scheme_Z_zeta_l2_error_matrix = np.zeros((len(vlevels),nmodels-1))
scheme_Z_zeta_linf_error_matrix = np.zeros((len(vlevels),nmodels-1))
scheme_Z_delta_l2_error_matrix = np.zeros((len(vlevels),nmodels-1))
scheme_Z_delta_linf_error_matrix = np.zeros((len(vlevels),nmodels-1))
error_matrix_list = [[scheme_Q_h_l2_error_matrix,scheme_Q_h_linf_error_matrix,scheme_Q_u_l2_error_matrix,scheme_Q_u_linf_error_matrix],
[scheme_TE_h_l2_error_matrix,scheme_TE_h_linf_error_matrix,scheme_TE_u_l2_error_matrix,scheme_TE_u_linf_error_matrix],
[scheme_TEQ_h_l2_error_matrix,scheme_TEQ_h_linf_error_matrix,scheme_TEQ_u_l2_error_matrix,scheme_TEQ_u_linf_error_matrix]]
scheme_list = ['TC2Z','TC2']
for scheme in scheme_list:		
	for i,vlevel in enumerate(vlevels):
		for k,(gtype_and_var) in enumerate(grid_types_and_variants):
			if (gtype_and_var[1] == 'spring_beta=0.8' or gtype_and_var[1] == 'spring_beta=1.1'):
				if (vlevel > 5):
					scheme_Q_h_l2_error_matrix[i,k] = None
					scheme_Q_h_linf_error_matrix[i,k] = None
					scheme_Q_u_l2_error_matrix[i,k] = None
					scheme_Q_u_linf_error_matrix[i,k] = None
					scheme_TE_h_l2_error_matrix[i,k] = None
					scheme_TE_h_linf_error_matrix[i,k] = None
					scheme_TE_u_l2_error_matrix[i,k] = None
					scheme_TE_u_linf_error_matrix[i,k] = None
					scheme_TEQ_h_l2_error_matrix[i,k] = None
					scheme_TEQ_h_linf_error_matrix[i,k] = None
					scheme_TEQ_u_l2_error_matrix[i,k] = None
					scheme_TEQ_u_linf_error_matrix[i,k] = None
					num_dofs[i,k] = None
					continue
			if gtype_and_var[0] == 'cubedsphere' and scheme == 'TC2Z':
				continue
			print gtype_and_var[0],gtype_and_var[1],vlevel
			settings.grid_type = gtype_and_var[0]
			settings.grid_variant = gtype_and_var[1]
			settings.vlevel  = vlevel
			if settings.grid_type == 'cubedsphere':
				set_grid_sizes_cubedsphere(grid.grid,settings.vlevel)
				allocate_connectivity_cubedsphere(grid.grid)
				allocate_geometry_cubedsphere(grid.grid)
				allocate_operators_cubedsphere(grid.grid)
			if settings.grid_type == 'geodesic':
				set_grid_sizes_geodesic(grid.grid,settings.vlevel)
				allocate_connectivity_geodesic(grid.grid)
				allocate_operators_geodesic(grid.grid)
				allocate_geometry_geodesic(grid.grid)
			load_connectivity(grid.grid)
			load_operators(grid.grid)
			grid.grid.dimensionalize_operators(settings.a)
			load_geometry(grid.grid)
			grid.grid.dimensionalize_geometry(settings.a)
	
			meshfile = h5py.File(settings.meshdir + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(grid.grid.nface[settings.vlevel-1]) + '.hdf5','r')
			clat = np.array(meshfile['coords']['clat'])
			clon = np.array(meshfile['coords']['clon'])
			vlat = np.array(meshfile['coords']['vlat'])
			vlon = np.array(meshfile['coords']['vlon'])
			elat = np.array(meshfile['coords']['elat'])
			elon = np.array(meshfile['coords']['elon'])
			meshfile.close()
			num_dofs[i,k] = grid.grid.nface[vlevel-1]
			
			nface = grid.grid.nface[vlevel-1]
			nedge = grid.grid.nedge[vlevel-1]
			nvert =  grid.grid.nvert[vlevel-1]
			
			I = create_scipy_sparse_matrix(grid.grid.i_stencil[:nface,:,i],grid.grid.i_stencil_size[:nface,i],grid.grid.i_coeffs[:nface,:,i],nface,nface)
			J = create_scipy_sparse_matrix(grid.grid.j_stencil[:nvert,:,i],grid.grid.j_stencil_size[:nvert,i],grid.grid.j_coeffs[:nvert,:,i],nvert,nvert)
			Iinv = create_scipy_sparse_matrix(grid.grid.i_stencil[:nface,:,i],grid.grid.i_stencil_size[:nface,i],1.0/grid.grid.i_coeffs[:nface,:,i],nface,nface)
			Jinv = create_scipy_sparse_matrix(grid.grid.j_stencil[:nvert,:,i],grid.grid.j_stencil_size[:nvert,i],1.0/grid.grid.j_coeffs[:nvert,:,i],nvert,nvert)
			E = create_scipy_sparse_matrix(grid.grid.e_stencil[:nedge,:,i],grid.grid.e_stencil_size[:nedge,i],grid.grid.e_coeffs[:nedge,:,i],nedge,nedge)
			Einv = create_scipy_sparse_matrix(grid.grid.e_stencil[:nedge,:,i],grid.grid.e_stencil_size[:nedge,i],1.0/grid.grid.e_coeffs[:nedge,:,i],nedge,nedge)
			
			if scheme == 'TC2':
				#get vars.hdf5 from runs and load relevant fields
				for Qvariant,error_matrices in zip(['TEQ','TE','Q'],error_matrix_list):
					varsfile = h5py.File(settings.outdir + '/' + scheme + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(vlevel) + '-' + Qvariant + '/none/vars.hdf5', 'r')
					variables = {}
					for name,data in varsfile['predicted'].items():
						variables[name] = np.array(data)
					varsfile.close()
					mi5 = (variables['mi'])[20]
					ue5 = (variables['ue'])[20]
					mi0 = (variables['mi'])[0]
					ue0 = (variables['ue'])[0]
					
					#error calculations
					diff_h = mi5 - mi0
					diff_v = ue5 - ue0
					(error_matrices[0])[i,k] = l2_error(I*diff_h,Iinv)
					(error_matrices[1])[i,k] = linf_error(I*diff_h) 
					(error_matrices[2])[i,k] = l2_error(E*diff_v,Einv) 
					(error_matrices[3])[i,k] = linf_error(E*diff_v)
					if vlevel == vlevels[-1]:
						diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/' + Qvariant + '/' + scheme + '/h',data=diff_h) 
						diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/' + Qvariant + '/' + scheme + '/u',data=diff_v)
						
			if scheme == 'TC2Z':
				varsfile = h5py.File(settings.outdir + '/' + scheme + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(vlevel) + '-TEQ' + '/none/vars.hdf5', 'r')
				variables = {}
				for name,data in varsfile['predicted'].items():
					variables[name] = np.array(data)
				varsfile.close()
				hi5 = (variables['hi'])[20]
				zeta5 = (variables['zeta'])[20]
				delta5 = (variables['delta'])[20]
				hi0 = (variables['hi'])[0]
				zeta0 = (variables['zeta'])[0]
				delta0 = (variables['delta'])[0]
	
				#error calculations
				diff_h = hi5 - hi0
				diff_zeta = zeta5 - zeta0
				diff_delta = delta5 - delta0
				scheme_Z_h_l2_error_matrix[i,k] = l2_error(diff_h,Iinv)
				scheme_Z_h_linf_error_matrix[i,k] = linf_error(diff_h) 
				scheme_Z_zeta_l2_error_matrix[i,k] = l2_error(diff_zeta,Iinv)
				scheme_Z_zeta_linf_error_matrix[i,k] = linf_error(diff_zeta) 
				scheme_Z_delta_l2_error_matrix[i,k] = l2_error(diff_delta,Iinv)
				scheme_Z_delta_linf_error_matrix[i,k] = linf_error(diff_delta) 
				#if vlevel == vlevels[-1]:
				#	diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/' + Qvariant + '/' + scheme + '/h',data=diff_h) 
				#	diff_out_file.create_dataset(gtype_and_var[0] + '/' + gtype_and_var[1] + '/' + Qvariant + '/' + scheme + '/u',data=diff_v)
					
plot_operator_accuracy('schemeTC2-Q.h.l2',num_dofs,scheme_Q_h_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('schemeTC2-Q.h.linf',num_dofs,scheme_Q_h_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('schemeTC2-Q.u.l2',num_dofs,scheme_Q_u_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('schemeTC2-Q.u.linf',num_dofs,scheme_Q_u_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])

plot_operator_accuracy('schemeTC2-TE.h.l2',num_dofs,scheme_TE_h_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('schemeTC2-TE.h.linf',num_dofs,scheme_TE_h_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('schemeTC2-TE.u.l2',num_dofs,scheme_TE_u_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('schemeTC2-TE.u.linf',num_dofs,scheme_TE_u_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])

plot_operator_accuracy('schemeTC2-TEQ.h.l2',num_dofs,scheme_TEQ_h_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('schemeTC2-TEQ.h.linf',num_dofs,scheme_TEQ_h_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('schemeTC2-TEQ.u.l2',num_dofs,scheme_TEQ_u_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])
plot_operator_accuracy('schemeTC2-TEQ.u.linf',num_dofs,scheme_TEQ_u_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt','cs'])

plot_operator_accuracy('schemeTC2Z-TEQ.h.l2',num_dofs,scheme_Z_h_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt'])
plot_operator_accuracy('schemeTC2Z-TEQ.h.linf',num_dofs,scheme_Z_h_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt'])
plot_operator_accuracy('schemeTC2Z-TEQ.zeta.l2',num_dofs,scheme_Z_zeta_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt'])
plot_operator_accuracy('schemeTC2Z-TEQ.zeta.linf',num_dofs,scheme_Z_zeta_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt'])
plot_operator_accuracy('schemeTC2Z-TEQ.delta.l2',num_dofs,scheme_Z_delta_l2_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt'])
plot_operator_accuracy('schemeTC2Z-TEQ.delta.linf',num_dofs,scheme_Z_delta_linf_error_matrix,['geo-tweaked','geo-spring0.8','geo-spring1.1','geo-cvt'])
diff_out_file.close()
