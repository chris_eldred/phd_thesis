from basemap_plotting_helpers import *
import sys
import subprocess

gridtypes = ['geodesic','cubedsphere']
gridvariant_dict = {'geodesic' : ['tweaked'] , 'cubedsphere': ['thuburn',]} #'cvt','spring_beta=1.1','spring_beta=0.8',
trisk_variants = ['TEQ','TE','Q'] 
title_list = ['Doubly Conservative','Energy Conserving','Enstrophy Conserving']
outdir = 'output'

dissipationtype = sys.argv[4]
dissipation = sys.argv[3]
scheme = sys.argv[2]
simulation = sys.argv[1]

###################### SIMULATIONS ##########################
if simulation == 'Galewsky':
	output_name = 'Galewsky'
	vlevels = [6,7,8]
	
if simulation == 'GalewskyInit':
	output_name = 'GalewskyInit'
	vlevels = [6,7,8]

if simulation == 'GalewskyNoPerturb':
	output_name = 'GalewskyNoPerturb'
	vlevels = [2,3,4,5,6,7,8]

if simulation == 'TC2':
	output_name = 'TC2'
	vlevels = [2,3,4,5,6,7,8]

if simulation == 'RHwave':
	output_name = 'RHwave'
	vlevels = [6,7]

if simulation == 'TC5':
	output_name = 'TC5'
	vlevels = [6,7]

if simulation == 'geostrophic':
	output_name = 'geostrophic'
	vlevels = [6]
	
if simulation == 'heldsuarez':
	output_name = 'heldsuarez'
	vlevels = [6]
	
#adjust some things for ZGRID
if scheme == 'ZGRID':
	trisk_variants = ['TEQ',]
	gridtypes = ['geodesic',]
	output_name = output_name + 'Z'
	title_list = ['Doubly Conservative Z',]

if dissipation == 'False':

	dissipdir = 'none'
if dissipation == 'True':
	if dissipationtype == 'hyperviscosity':
		dissipdir = 'hyper'
	if dissipationtype == 'APVM':
		dissipdir = 'apvm'

#####PLOTS########
# Make plotting directory
subprocess.call(['mkdir','-p',outdir + '/' + output_name + '/plots'])

for gridtype in gridtypes:
	for gridvariant in gridvariant_dict[gridtype]:
		for vlevel in vlevels:
			Q = {}
			TE = {}
			for variant,title in zip(trisk_variants,title_list):
				try:
					varsfile = h5py.File(outdir + '/' + output_name + '/' + gridtype + '/' + gridvariant + '/' + str(vlevel) + '-' + variant + '/' + dissipdir + '/vars.hdf5', 'r')
					Q[variant] = np.array(varsfile['statistics']['Q'])
					TE[variant] = np.array(varsfile['statistics']['TE'])  
					varsfile.close()
				except:
					print 'does not exist',gridtype,gridvariant,vlevel,variant,dissipdir,scheme,simulation
					Q[variant] = None
					TE[variant] = None

			if scheme == 'ZGRID':
				trisk_variants.append('TEQ-C')
				title_list.append('Doubly Conservative C')
				#trisk_variants.append('TE-C')
				#title_list.append('Total Energy Only C')
				output_name_adj = output_name[:-1]
				#for cvariant in ['TEQ','TE']:
				cvariant = 'TEQ'
				try:
					varsfile = h5py.File(outdir + '/' + output_name_adj + '/' + gridtype + '/' + gridvariant + '/' + str(vlevel) + '-' + cvariant + '/' + dissipdir + '/vars.hdf5', 'r')
					Q[cvariant+'-C'] = np.array(varsfile['statistics']['Q'])
					TE[cvariant+'-C'] = np.array(varsfile['statistics']['TE'])  
					varsfile.close()
				except:
					print 'does not exist',gridtype,gridvariant,vlevel,variant,dissipdir,scheme,simulation,'TRISK',cvariant
					Q[cvariant+'-C'] = None
					TE[cvariant+'-C'] = None
			tempname = output_name + '.' + gridtype + '.' + gridvariant + '.' + str(vlevel) + '.' + dissipdir
	
			plt.figure()
			plt.xlabel('Time Step')
			plt.ylabel('% Change in Potential Enstrophy')
			for variant,title in zip(trisk_variants,title_list):
				var = Q[variant]
				if var != None:
					x = range(len(var))
					plt.plot(x,(var-var[0])/var[0],label=title)
			plt.legend(loc=2)
			plt.savefig(outdir + '/' + output_name + '/plots/' + 'Q' + '.' + tempname + '.png')
				
			plt.figure()
			plt.xlabel('Time Step')
			plt.ylabel('% Change in Total Energy')
			for variant,title in zip(trisk_variants,title_list):
				var = TE[variant]
				if var != None:
					x = range(len(var))
					plt.plot(x,(var-var[0])/var[0],label=title)
			plt.legend(loc=2)
			plt.savefig(outdir + '/' + output_name + '/plots/' + 'TE' + '.' + tempname + '.png')

			if scheme == 'ZGRID':
				trisk_variants = ['TEQ',]
				title_list = ['Doubly Conservative Z',]
