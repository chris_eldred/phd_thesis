from basemap_plotting_helpers import *
import sys
import subprocess
from scipy.stats import binned_statistic

gridtypes = ['geodesic','cubedsphere']
gridvariant_dict = {'geodesic' : ['tweaked'] , 'cubedsphere': ['thuburn',]} #'cvt','spring_beta=1.1','spring_beta=0.8',
trisk_variants = ['TEQ','TE','Q'] 
title_list = ['Doubly Conservative','Energy Conserving','Enstrophy Conserving']
outdir = 'output'

dissipationtype = sys.argv[3]
dissipation = sys.argv[2]
scheme = sys.argv[1]
output_name = 'heldsuarez'

vlevels = [6,]
#steps = range(0,1201,50)
#stepnames = [] #['0','2','6','10']	
#for i in steps:
#	stepnames.append(str(i))
#print steps

variables_list = [('zetav','diagnosed','dual','Relative Vorticity','latlon'),('hi','diagnosed','primal','Fluid Height','latlon'),('di','diagnosed','primal','Divergence','latlon'),]
#('qv','diagnosed','dual','Potential Vorticity','latlon'),('etav','diagnosed','dual','Absolute Vorticity','latlon')]

#adjust some things for ZGRID
if scheme == 'ZGRID':
	trisk_variants = ['TEQ',]
	gridtypes = ['geodesic',]
	output_name = output_name + 'Z'
	title_list = ['Doubly Conservative',]
	variables_list = [('zeta','predicted','primal','Relative Vorticity','latlon'),('hi','predicted','primal','Fluid Height','latlon'),('delta','predicted','primal','Divergence','latlon'),]
	#('q','diagnosed','primal','Potential Vorticity','latlon'),('eta','diagnosed','primal','Absolute Vorticity','latlon')]

if dissipation == 'False':
	dissipdir = 'none'
if dissipation == 'True':
	if dissipationtype == 'hyperviscosity':
		dissipdir = 'hyper'
	if dissipationtype == 'APVM':
		dissipdir = 'apvm'

def plot_zonal_mean(data,lats,fname,name):
	plt.figure()
	plt.plot(data,lats)
	plt.title(name)
	plt.savefig(fname + '.png')
	
#####PLOTS########
# Make plotting directory
#subprocess.call(['mkdir','-p',outdir + '/' + output_name + '/plots/' + dissipdir])

bins = np.linspace(-80,80,320)
bins_adj = np.cumsum(np.diff(bins))
lats = bins_adj - 80 - bins_adj[0]/2.
					
def get_zonal_means(y):
	sy, _, _ = binned_statistic(x,y,statistic='sum',bins=bins)
	return sy
	
for gridtype in gridtypes:
	for gridvariant in gridvariant_dict[gridtype]:
		for vlevel in vlevels:
			meshdata = get_geometry(gridtype,gridvariant,vlevel)
			statsfile = h5py.File(outdir +'/' + output_name + '/plots/' + gridtype + '.' + gridvariant + '.' + str(vlevel) + '.' + dissipdir + '.stats.hdf5', 'w')			
			for variant,title in zip(trisk_variants,title_list):
				print 'postprocessing',gridtype,gridvariant,vlevel,variant
				variables = {}
				varsfile = h5py.File(outdir + '/' + output_name + '/' + gridtype + '/' + gridvariant + '/' + str(vlevel) + '-' + variant + '/' + dissipdir + '/vars.hdf5', 'r')
				for varname,vartype,varloc,longname,plottype in variables_list:
					variables[varname] = np.array(varsfile[vartype][varname])
				varsfile.close()
				
				variantgroup = statsfile.create_group(variant)
				for varname,vartype,varloc,longname,plottype in variables_list:
					
					#compute temporal averages
					print 'computing mean + std',varname
					data = variables[varname][401:]
					time_averaged_data = np.mean(data,axis=0)
					std_deviation_time_data = np.std(data,axis=0)
					
					variantgroup.create_dataset(varname+'_mean',data=time_averaged_data)
					variantgroup.create_dataset(varname+'_stdev',data=std_deviation_time_data)

					#plot them
					#plot_scalar(meshdata,time_averaged_data,varloc,longname + ', ' +title, \
					#	outdir +'/' + output_name + '/plots/' + varname + '.mean.' + variant + '.' + gridtype + '.' + gridvariant + '.' + str(vlevel) + '.' + dissipdir,plottype,'contour')
					#plot_scalar(meshdata,std_deviation_time_data,varloc,longname + ', ' +title, \
					#	outdir +'/' + output_name + '/plots/' + varname + '.std.' + variant + '.' + gridtype + '.' + gridvariant + '.' + str(vlevel) + '.' + dissipdir,plottype,'contour')

					#compute zonal averages
					print 'computing zonal mean + std',varname
					if varloc == 'dual':
						areas = meshdata.Av
						areaweighted_data = data * areas
						x = meshdata.vlats
						zonal_means = np.apply_along_axis(get_zonal_means,1,areaweighted_data)
						area_means,_,_ = binned_statistic(x,areas,statistic='sum',bins=bins)
					if varloc == 'primal':
						areas = meshdata.Ai
						areaweighted_data = data * areas
						x = meshdata.clats
						zonal_means = np.apply_along_axis(get_zonal_means,1,areaweighted_data)
						area_means,_,_ = binned_statistic(x,areas,statistic='sum',bins=bins)

					zonal_means = zonal_means / area_means
						
					zonal_mean = np.mean(zonal_means,axis=0)
					zonal_stddev = np.std(zonal_means,axis=0)

					variantgroup.create_dataset(varname+'_areamean',data=area_means)

					variantgroup.create_dataset(varname+'_zonalmeans',data=zonal_means)
					variantgroup.create_dataset(varname+'_zonalmean',data=zonal_mean)
					variantgroup.create_dataset(varname+'_zonalstdev',data=zonal_stddev)

					#print 'plotting zonal mean + std',varname
					#plot_zonal_mean(zonal_mean,lats,outdir +'/' + output_name + '/plots/' + varname + '.zonalmean.' + variant + '.' + 
					#gridtype + '.' + gridvariant + '.' + str(vlevel) + '.' + dissipdir,varname)
					#plot_zonal_mean(zonal_stddev,lats,outdir +'/' + output_name + '/plots/' + varname + '.zonalstd.' + variant + '.' + 
					#gridtype + '.' + gridvariant + '.' + str(vlevel) + '.' + dissipdir,varname)
			statsfile.close()
