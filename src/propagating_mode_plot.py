import matplotlib.pyplot as plt
import numpy as np
import h5py
import sys
from configuration import *
from math import ceil,sqrt,pow
from basemap_plotting_helpers import *
from constants import *


#get configuration
get_configuration(sys.argv[1])
get_eigenconfig(sys.argv[1])
if settings.use_dissipation == False:
	disspdir = 'none'
if settings.use_dissipation == True:
	disspdir = 'hyper'
basedir = settings.outdir + settings.simname + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(settings.vlevel) + '-' + settings.trisk_variant + '/' + disspdir + '/'
outdir = settings.outdir + settings.simname + '/plots/' + disspdir + '/' + settings.grid_type + '.' + settings.grid_variant + '.' + str(settings.vlevel) +  '.' + settings.trisk_variant + '.' + disspdir + '.'

if settings.grid_type == 'geodesic':
	nfaces = 10*(2**settings.vlevel)**2 + 2
	nverts = 20*(2**settings.vlevel)**2
	nedges = 30*(2**settings.vlevel)**2
	
if settings.grid_type == 'cubedsphere':
	n0 = 3
	nfaces = int(pow(n0*pow(2,settings.vlevel-1),2) * 6)
	nedges = 2*nfaces
	nverts = nfaces + 2

if settings.scheme == 'TRISK':
	nstationary = nverts
	nrossby = nverts
	ninertia = 2*nfaces-2
	ndofs = nedges + nfaces
	operators = ['Div','Grad','Perp','A']

if settings.scheme == 'ZGRID':
	nstationary = nfaces
	nrossby = nfaces
	ninertia = 2*nfaces
	ndofs = 3*nfaces
	operators = ['Laplac','A','J','FD']


eigenpostfile = h5py.File(basedir + 'eigen.post.hdf5', 'r')
meshdata = get_geometry(settings.grid_type,settings.grid_variant,settings.vlevel)

def trisk_propagating_mode_plot(branch,modetype,i):
	hi_real = eigenpostfile[branch + '_' + modetype + '_vecs_decomp']['hi_real'+str(i)] 
	hi_imag = eigenpostfile[branch + '_' + modetype + '_vecs_decomp']['hi_imag'+str(i)] 
	zeta_real = eigenpostfile[branch + '_' + modetype + '_vecs_decomp']['zeta_real'+str(i)] 
	zeta_imag = eigenpostfile[branch + '_' + modetype + '_vecs_decomp']['zeta_imag'+str(i)] 
	delta_real = eigenpostfile[branch + '_' + modetype + '_vecs_decomp']['delta_real'+str(i)] 
	delta_imag = eigenpostfile[branch + '_' + modetype + '_vecs_decomp']['delta_imag'+str(i)] 

	plot_scalar(meshdata,hi_real,'primal', modetype + 'Eigenvector (Real Part) for hi', basedir + modetype + '/real/hi/hi.' + str(i),'spherical','grid',None)
	plot_scalar(meshdata,delta_real,'primal',modetype + 'Eigenvector (Real Part) for delta', basedir + modetype + '/real/delta/delta.' + str(i),'spherical','grid',None)
	plot_scalar(meshdata,zeta_real,'dual',modetype + 'Eigenvector (Real Part) for zeta', basedir + modetype + '/real/zeta/zeta.' + str(i),'spherical','grid',None)

	plot_scalar(meshdata,hi_imag,'primal',modetype + 'Eigenvector (Imaginary Part) for hi', basedir + modetype + '/imag/hi/hi.' + str(i),'spherical','grid',None)
	plot_scalar(meshdata,delta_imag,'primal',modetype + 'Eigenvector (Imaginary Part) for delta', basedir + modetype + '/imag/delta/delta.' + str(i),'spherical','grid',None)
	plot_scalar(meshdata,zeta_imag,'dual',modetype + 'Eigenvector (Imaginary Part) for zeta', basedir + modetype + '/imag/zeta/zeta.' + str(i),'spherical','grid',None)


def zgrid_propagating_mode_plot(branch,modetype,i):
	hi_real = eigenpostfile[branch + '_' + modetype + '_vecs_decomp']['hi_real'+str(i)] 
	hi_imag = eigenpostfile[branch + '_' + modetype + '_vecs_decomp']['hi_imag'+str(i)] 
	zeta_real = eigenpostfile[branch + '_' + modetype + '_vecs_decomp']['zeta_real'+str(i)] 
	zeta_imag = eigenpostfile[branch + '_' + modetype + '_vecs_decomp']['zeta_imag'+str(i)] 
	delta_real = eigenpostfile[branch + '_' + modetype + '_vecs_decomp']['delta_real'+str(i)] 
	delta_imag = eigenpostfile[branch + '_' + modetype + '_vecs_decomp']['delta_imag'+str(i)] 

	plot_scalar(meshdata,hi_real,'primal', modetype + 'Eigenvector (Real Part) for hi', basedir + modetype + '/real/hi/hi.' + str(i),'spherical','grid',None)
	plot_scalar(meshdata,delta_real,'primal',modetype + 'Eigenvector (Real Part) for delta', basedir + modetype + '/real/delta/delta.' + str(i),'spherical','grid',None)
	plot_scalar(meshdata,zeta_real,'primal',modetype + 'Eigenvector (Real Part) for zeta', basedir + modetype + '/real/zeta/zeta.' + str(i),'spherical','grid',None)

	plot_scalar(meshdata,hi_imag,'primal',modetype + 'Eigenvector (Imaginary Part) for hi', basedir + modetype + '/imag/hi/hi.' + str(i),'spherical','grid',None)
	plot_scalar(meshdata,delta_imag,'primal',modetype + 'Eigenvector (Imaginary Part) for delta', basedir + modetype + '/imag/delta/delta.' + str(i),'spherical','grid',None)
	plot_scalar(meshdata,zeta_imag,'primal',modetype + 'Eigenvector (Imaginary Part) for zeta', basedir + modetype + '/imag/zeta/zeta.' + str(i),'spherical','grid',None)


for i in range(ninertia/2): #just plot positive branches, since negative is identical
	if settings.scheme == 'TRISK':
		trisk_propagating_mode_plot('pos','igw',i)
	if settings.scheme == 'ZGRID':
		zgrid_propagating_mode_plot('pos','igw',i)

if settings.variable_f == True:
	for i in range(nrossby/2): #just plot positive branches, since negative is identical
		if settings.scheme == 'TRISK':
			trisk_propagating_mode_plot('pos','rossby',i)
		if settings.scheme == 'ZGRID':
			zgrid_propagating_mode_plot('pos','rossby',i)

if settings.variable_f == False:
	for i in range(nstationary/2): #just plot positive branches, since negative is identical
		if settings.scheme == 'TRISK':
			trisk_propagating_mode_plot('pos','stat',i)
		if settings.scheme == 'ZGRID':
			zgrid_propagating_mode_plot('pos','stat',i)

eigenpostfile.close()
