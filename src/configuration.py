from constants import *
try:
     import configparser
except ImportError:
	import ConfigParser as configparser

def get_eigenconfig(configfile):
	cfg = configparser.ConfigParser()
	cfg.read(settings.cfgdir  + configfile)
	settings.C =  cfg.getfloat('Scheme','C')
	settings.variable_f = cfg.getboolean('Scheme','variable_f')

def get_postconfig(configfile):
	pass
def get_initconfig(configfile):
	pass
def get_sweconfig(configfile):
	pass
def get_bveconfig(configfile):
	pass
def get_bveconfig(configfile):
	pass
	
def get_configuration(configfile):
	cfg = configparser.ConfigParser()
	cfg.read(settings.cfgdir + configfile)
	
	#GRID
	settings.nx = cfg.getint('Grid','nx')
	settings.ny = cfg.getint('Grid','ny')
	settings.dx = cfg.getfloat('Grid','dx')
	settings.vlevel = cfg.getint('Grid','vlevel')
	settings.grid_type = cfg.get('Grid','grid_type')
	settings.grid_variant = cfg.get('Grid','grid_variant')
	settings.a = cfg.getfloat('Grid','a')
	
	#SCHEME
	settings.scheme = cfg.get('Scheme','scheme')
	settings.f =  cfg.getfloat('Scheme','f')
	settings.g =  cfg.getfloat('Scheme','g')
	settings.Omega =  cfg.getfloat('Scheme','Omega')
	settings.trisk_variant = cfg.get('Scheme','trisk_variant')

	settings.nu_gd = cfg.getfloat('Scheme','nu_gd')
	settings.nu_cc = cfg.getfloat('Scheme','nu_cc')
	settings.use_dissipation = cfg.getboolean('Scheme','use_dissipation')
	settings.dissipation_type = cfg.get('Scheme','dissipation_type')
	settings.apvm_c = cfg.getfloat('Scheme','apvm_c')

	#INITIAL CONDITIONS
	settings.initial_condition = cfg.get('Init','initial_condition')
	if settings.initial_condition == 'geostrophic_turbulence':
		if settings.scheme == 'TRISK':
			settings.hi_variance = cfg.getfloat('Init','hi_variance')
			settings.hi_mean = cfg.getfloat('Init','hi_mean')
		settings.ue_variance = cfg.getfloat('Init','ue_variance')
		settings.ue_mean = cfg.getfloat('Init','ue_mean')

		if settings.scheme == 'ZGRID':
			settings.hi_variance = cfg.getfloat('Init','hi_variance')
			settings.hi_mean = cfg.getfloat('Init','hi_mean')
			settings.zeta_variance = cfg.getfloat('Init','zeta_variance')
			settings.zeta_mean = cfg.getfloat('Init','zeta_mean')
			settings.delta_variance = cfg.getfloat('Init','delta_variance')
			settings.delta_mean = cfg.getfloat('Init','delta_mean')
	
	if settings.initial_condition == 'Galewsky':
		settings.perturb_H = cfg.getboolean('Init','perturb_H')


	
	#TIME STEPPING
	settings.nsteps = cfg.getint('Time','nsteps')
	settings.dt = cfg.getfloat('Time','dt')
	settings.time_scheme = cfg.get('Time','time_scheme')
	
	if settings.time_scheme == 'ab3':
		settings.ntlevels = 3
			
	#OUTPUT
	settings.simname = cfg.get('Output','simname')
	settings.output_frequency = cfg.getint('Output','output_frequency')
	settings.tempname = cfg.get('Output','tempname')
	
