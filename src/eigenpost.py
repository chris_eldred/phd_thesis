import matplotlib.pyplot as plt
import numpy as np
import h5py
import sys
from configuration import *
from math import ceil,sqrt,pow
from basemap_plotting_helpers import *
from mesh import *
from constants import *
import grid


#get configuration
get_configuration(sys.argv[1])
get_eigenconfig(sys.argv[1])
if settings.use_dissipation == False:
	disspdir = 'none'
if settings.use_dissipation == True:
	disspdir = 'hyper'
basedir = settings.outdir + settings.simname + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(settings.vlevel) + '-' + settings.trisk_variant + '/' + disspdir + '/'
outdir = settings.outdir + settings.simname + '/plots/' + disspdir + '/' + settings.grid_type + '.' + settings.grid_variant + '.' + str(settings.vlevel) +  '.' + settings.trisk_variant + '.' + disspdir + '.'

if settings.grid_type == 'geodesic':
	nfaces = 10*(2**settings.vlevel)**2 + 2
	nverts = 20*(2**settings.vlevel)**2
	nedges = 30*(2**settings.vlevel)**2
	
if settings.grid_type == 'cubedsphere':
	n0 = 3
	nfaces = int(pow(n0*pow(2,settings.vlevel-1),2) * 6)
	nedges = 2*nfaces
	nverts = nfaces + 2

if settings.scheme == 'TRISK':
	nstationary = nverts
	nrossby = nverts
	ninertia = 2*nfaces-2
	ndofs = nedges + nfaces
	operators = ['Div','Grad','Perp','A']

if settings.scheme == 'ZGRID':
	nstationary = nfaces
	nrossby = nfaces
	ninertia = 2*nfaces
	ndofs = 3*nfaces
	operators = ['Laplac','A','J','FD']

if settings.grid_type == 'cubedsphere':
	set_grid_sizes_cubedsphere(grid.grid,settings.vlevel)
	allocate_connectivity_cubedsphere(grid.grid)
	allocate_geometry_cubedsphere(grid.grid)
	allocate_operators_cubedsphere(grid.grid)
if settings.grid_type == 'geodesic':
	set_grid_sizes_geodesic(grid.grid,settings.vlevel)
	allocate_connectivity_geodesic(grid.grid)
	allocate_operators_geodesic(grid.grid)
	allocate_geometry_geodesic(grid.grid)
if settings.grid_type == 'square':
	set_grid_sizes_square(grid.grid,settings.nx,settings.ny)
	allocate_connectivity_square(grid.grid)
	allocate_operators_square(grid.grid)
	allocate_geometry_square(grid.grid)
if settings.grid_type == 'hex':
	set_grid_sizes_hex(grid.grid,settings.nx,settings.ny)
	allocate_connectivity_hex(grid.grid)
	allocate_operators_hex(grid.grid)
	allocate_geometry_hex(grid.grid)
		
if settings.grid_type == 'square' or settings.grid_type == 'hex':
	settings.a = settings.dx
	
load_connectivity(grid.grid)
load_operators(grid.grid)
grid.grid.dimensionalize_operators(settings.a)
load_geometry(grid.grid)
grid.grid.dimensionalize_geometry(settings.a)

#load eigenvalues and eigenvectors from file
eigenfile = h5py.File(basedir + 'eigen.hdf5', 'r')
evals = np.array(eigenfile['w'])
evals_real = -1.0*np.real(evals) #adjust based on definition of omega
evals_imag = np.imag(evals)
eigenvectors = np.zeros((evals.shape[0],ndofs),dtype=np.complex128)
for i in range(evals.shape[0]):
	eigenvectors[i,:] = np.array(eigenfile['vr'+str(i)])
eigenfile.close()

def zgrid_eigenvector_output(vecs,efile,vecname):
	vecgroup = efile.create_group(vecname)
	for i in range(vecs.shape[0]):
		hi = vecs[i,:nfaces]
		zeta = vecs[i,nfaces:2*nfaces]
		delta = vecs[i,2*nfaces:3*nfaces]
		hi_real = np.real(hi)
		hi_imag = np.imag(hi)
		delta_real = np.real(delta)
		delta_imag = np.imag(delta)
		zeta_real = np.real(zeta)
		zeta_imag = np.imag(zeta)
		vecgroup.create_dataset('hi_real'+str(i),data=hi_real)
		vecgroup.create_dataset('zeta_real'+str(i),data=zeta_real)
		vecgroup.create_dataset('delta_real'+str(i),data=delta_real)
		vecgroup.create_dataset('hi_imag'+str(i),data=hi_imag)
		vecgroup.create_dataset('zeta_imag'+str(i),data=zeta_imag)
		vecgroup.create_dataset('delta_imag'+str(i),data=delta_imag)

def trisk_eigenvector_output(vecs,efile,vecname):
	vecgroup = efile.create_group(vecname)
	for i in range(vecs.shape[0]):
		hi = vecs[i,:nfaces]
		ue = vecs[i,nfaces:]
		hi_real = np.real(hi)
		hi_imag = np.imag(hi)
		ue_real = np.real(ue)
		ue_imag = np.imag(ue)
		zeta_real = grid.grid.j(grid.grid.d2bar(grid.grid.einv(np.real(ue),settings.vlevel),settings.vlevel,nverts),settings.vlevel)
		delta_real = grid.grid.i(grid.grid.d2(grid.grid.h(grid.grid.einv(np.real(ue),settings.vlevel),settings.vlevel),settings.vlevel,nfaces),settings.vlevel)
		zeta_imag = grid.grid.j(grid.grid.d2bar(grid.grid.einv(np.imag(ue),settings.vlevel),settings.vlevel,nverts),settings.vlevel)
		delta_imag = grid.grid.i(grid.grid.d2(grid.grid.h(grid.grid.einv(np.imag(ue),settings.vlevel),settings.vlevel),settings.vlevel,nfaces),settings.vlevel)
		vecgroup.create_dataset('hi_real'+str(i),data=hi_real)
		vecgroup.create_dataset('ue_real'+str(i),data=ue_real)
		vecgroup.create_dataset('zeta_real'+str(i),data=zeta_real)
		vecgroup.create_dataset('delta_real'+str(i),data=delta_real)
		vecgroup.create_dataset('hi_imag'+str(i),data=hi_imag)
		vecgroup.create_dataset('ue_imag'+str(i),data=ue_imag)
		vecgroup.create_dataset('zeta_imag'+str(i),data=zeta_imag)
		vecgroup.create_dataset('delta_imag'+str(i),data=delta_imag)

eigenpostfile = h5py.File(basedir + 'eigen.post.hdf5', 'w')
if settings.variable_f == False:
#sort out stationary and igw
	
	omega = evals_imag / settings.f
	omega_zero = np.less(np.abs(omega),0.1)
	omega_nonzero = np.logical_not(omega_zero)
	
	igw_vals = omega[omega_nonzero]
	igw_vecs = eigenvectors[omega_nonzero,:]
	igw_imag = evals_real[omega_nonzero]
	stat_vals = omega[omega_zero]
	stat_vecs = eigenvectors[omega_zero,:]
	stat_imag = evals_real[omega_zero]
	print 'nstationary',nstationary,stat_vals.shape[0]
	print 'ninertia',ninertia,igw_vals.shape[0]

	igw_sorted_indices = np.argsort(igw_vals)
	stat_sorted_indices = np.argsort(stat_vals)

	igw_vals = igw_vals[igw_sorted_indices]
	igw_vecs = igw_vecs[igw_sorted_indices,:]
	igw_imag = igw_imag[igw_sorted_indices]
	stat_vals = stat_vals[stat_sorted_indices]
	stat_vecs = stat_vecs[stat_sorted_indices,:]
	stat_imag = stat_imag[stat_sorted_indices]
	
	neg_igw_vals,pos_igw_vals = np.split(igw_vals,2)
	neg_igw_vecs,pos_igw_vecs = np.split(igw_vecs,2,axis=0)
	neg_igw_imag,pos_igw_imag = np.split(igw_imag,2)
	neg_stat_vals,pos_stat_vals = np.split(stat_vals,2)
	neg_stat_vecs,pos_stat_vecs = np.split(stat_vecs,2,axis=0)
	neg_stat_imag,pos_stat_imag = np.split(stat_imag,2)
	
	neg_igw_sorted_indices = np.argsort(-1*neg_igw_vals)
	neg_stat_sorted_indices = np.argsort(-1*neg_stat_vals)
	neg_igw_vals = neg_igw_vals[neg_igw_sorted_indices]
	neg_igw_vecs = neg_igw_vecs[neg_igw_sorted_indices,:]
	neg_igw_imag = neg_igw_imag[neg_igw_sorted_indices]
	neg_stat_vals = neg_stat_vals[neg_stat_sorted_indices]
	neg_stat_vecs = neg_stat_vecs[neg_stat_sorted_indices,:]
	neg_stat_imag = neg_stat_imag[neg_stat_sorted_indices]

	eigenpostfile.create_dataset('igw_real_pos',data=pos_igw_vals)
	eigenpostfile.create_dataset('igw_real_neg',data=neg_igw_vals)
	eigenpostfile.create_dataset('igw_imag_pos',data=pos_igw_imag)
	eigenpostfile.create_dataset('igw_imag_neg',data=neg_igw_imag)
	eigenpostfile.create_dataset('igw_vecs_pos',data=pos_igw_vecs)
	eigenpostfile.create_dataset('igw_vecs_neg',data=neg_igw_vecs)

	eigenpostfile.create_dataset('stat_real_pos',data=pos_stat_vals)
	eigenpostfile.create_dataset('stat_real_neg',data=neg_stat_vals)
	eigenpostfile.create_dataset('stat_imag_pos',data=pos_stat_imag)
	eigenpostfile.create_dataset('stat_imag_neg',data=neg_stat_imag)
	eigenpostfile.create_dataset('stat_vecs_pos',data=pos_stat_vecs)
	eigenpostfile.create_dataset('stat_vecs_neg',data=neg_stat_vecs)

	if settings.scheme == 'TRISK':
		trisk_eigenvector_output(pos_igw_vecs,eigenpostfile,'pos_igw_vecs_decomp')
		trisk_eigenvector_output(neg_igw_vecs,eigenpostfile,'neg_igw_vecs_decomp')
		trisk_eigenvector_output(pos_stat_vecs,eigenpostfile,'pos_stat_vecs_decomp')
		trisk_eigenvector_output(neg_stat_vecs,eigenpostfile,'neg_stat_vecs_decomp')
	if settings.scheme == 'ZGRID':
		zgrid_eigenvector_output(pos_igw_vecs,eigenpostfile,'pos_igw_vecs_decomp')
		zgrid_eigenvector_output(neg_igw_vecs,eigenpostfile,'neg_igw_vecs_decomp')
		zgrid_eigenvector_output(pos_stat_vecs,eigenpostfile,'pos_stat_vecs_decomp')
		zgrid_eigenvector_output(neg_stat_vecs,eigenpostfile,'neg_stat_vecs_decomp')

if settings.variable_f == True:

	omega = evals_imag / settings.f
	omega_indices = np.argsort(omega)
	omega = omega[omega_indices]
	omega_imag = evals_real[omega_indices]
	evecs = eigenvectors[omega_indices,:]

	neg_omega,pos_omega = np.split(omega,2)
	neg_omega_imag,pos_omega_imag = np.split(omega_imag,2)
	neg_omega_vecs,pos_omega_vecs = np.split(evecs,2,axis=0)

	neg_omega_sorted_indices = np.argsort(-1*neg_omega)
	neg_omega = neg_omega[neg_omega_sorted_indices]
	neg_omega_imag = neg_omega_imag[neg_omega_sorted_indices]
	neg_omega_vecs =  neg_omega_vecs[neg_omega_sorted_indices,:]

#sorting here assumes that the ninertia highest frequency modes are IGW, and the rest are Rossby
#this probably mischaracterizes some modes

	neg_rossby_vals = neg_omega[:nrossby/2]
	pos_rossby_vals = pos_omega[:nrossby/2]
	neg_igw_vals = neg_omega[nrossby/2:nrossby/2+ninertia/2]
	pos_igw_vals = pos_omega[nrossby/2:nrossby/2+ninertia/2]

	neg_rossby_imag = neg_omega_imag[:nrossby/2]
	pos_rossby_imag = pos_omega_imag[:nrossby/2]
	neg_igw_imag = neg_omega_imag[nrossby/2:nrossby/2+ninertia/2]
	pos_igw_imag = pos_omega_imag[nrossby/2:nrossby/2+ninertia/2]

	neg_rossby_vecs = neg_omega_vecs[:nrossby/2,:]
	pos_rossby_vecs = pos_omega_vecs[:nrossby/2,:]
	neg_igw_vecs = neg_omega_vecs[nrossby/2:nrossby/2+ninertia/2,:]
	pos_igw_vecs = pos_omega_vecs[nrossby/2:nrossby/2+ninertia/2,:]

	print 'nrossby',nrossby/2,pos_rossby_vals.shape[0]
	print 'ninertia',ninertia/2,pos_igw_vals.shape[0]

	eigenpostfile.create_dataset('igw_real_pos',data=pos_igw_vals)
	eigenpostfile.create_dataset('igw_real_neg',data=neg_igw_vals)
	eigenpostfile.create_dataset('igw_imag_pos',data=pos_igw_imag)
	eigenpostfile.create_dataset('igw_imag_neg',data=neg_igw_imag)
	eigenpostfile.create_dataset('igw_vecs_pos',data=pos_igw_vecs)
	eigenpostfile.create_dataset('igw_vecs_neg',data=neg_igw_vecs)

	eigenpostfile.create_dataset('rossby_real_pos',data=pos_rossby_vals)
	eigenpostfile.create_dataset('rossby_real_neg',data=neg_rossby_vals)
	eigenpostfile.create_dataset('rossby_imag_pos',data=pos_rossby_imag)
	eigenpostfile.create_dataset('rossby_imag_neg',data=neg_rossby_imag)
	eigenpostfile.create_dataset('rossby_vecs_pos',data=pos_rossby_vecs)
	eigenpostfile.create_dataset('rossby_vecs_neg',data=neg_rossby_vecs)

	if settings.scheme == 'TRISK':
		trisk_eigenvector_output(pos_igw_vecs,eigenpostfile,'pos_igw_vecs_decomp')
		trisk_eigenvector_output(neg_igw_vecs,eigenpostfile,'neg_igw_vecs_decomp')
		trisk_eigenvector_output(pos_rossby_vecs,eigenpostfile,'pos_rossby_vecs_decomp')
		trisk_eigenvector_output(neg_rossby_vecs,eigenpostfile,'neg_rossby_vecs_decomp')
	if settings.scheme == 'ZGRID':
		zgrid_eigenvector_output(pos_igw_vecs,eigenpostfile,'pos_igw_vecs_decomp')
		zgrid_eigenvector_output(neg_igw_vecs,eigenpostfile,'neg_igw_vecs_decomp')
		zgrid_eigenvector_output(pos_rossby_vecs,eigenpostfile,'pos_rossby_vecs_decomp')
		zgrid_eigenvector_output(neg_rossby_vecs,eigenpostfile,'neg_rossby_vecs_decomp')


#SVD STUFF
varloc_dict = {'Laplac' : 'primal', 'Div' : 'edge', 'Grad': 'primal', 'Perp': 'edge', 'FD': 'primal', 'J' : 'primal'}
svdfile = h5py.File(basedir + 'svd.hdf5', 'r')
svdpostfile = h5py.File(basedir + 'svd.post.hdf5', 'w')
for operator in operators:
	try:
		svds = np.array(svdfile[operator]['s'])
		VH = np.array(svdfile[operator]['Vh'])
	except:
		continue	
	svds = svds/np.amax(svds)
	svd_zero = np.less(svds,10**(-10))
	singular_vals = svds[np.less(svds,10**(-10))]
	singular_vecs = VH[svd_zero,:]
	print operator,singular_vals.shape[0]
	
	if singular_vals.shape[0] == 0:
		continue

	svdpostfile.create_dataset(operator,data=singular_vals)
	svdpostfile.create_dataset(operator+'_vecs',data=singular_vecs)
	vecsgroup = svdpostfile.create_group(operator + '_vecs_decomp')

	if operator == 'A':
		for i in range(singular_vals.shape[0]):
			data = singular_vecs[i,:]
			if settings.scheme == 'TRISK':
				hi = data[:nfaces]
				ue = data[nfaces:]
				zeta = grid.grid.j(grid.grid.d2bar(grid.grid.einv(ue,settings.vlevel),settings.vlevel,nverts),settings.vlevel)
				delta = grid.grid.i(grid.grid.d2(grid.grid.h(grid.grid.einv(ue,settings.vlevel),settings.vlevel),settings.vlevel,nfaces),settings.vlevel)
				vecsgroup.create_dataset('hi'+str(i),data=hi)
				vecsgroup.create_dataset('ue'+str(i),data=ue)
				vecsgroup.create_dataset('zeta'+str(i),data=zeta)
				vecsgroup.create_dataset('delta'+str(i),data=delta)
			if settings.scheme == 'ZGRID':
				hi = data[:nfaces]
				delta = data[2*nfaces:3*nfaces]
				zeta = data[nfaces:2*nfaces]
				vecsgroup.create_dataset('hi'+str(i),data=hi)
				vecsgroup.create_dataset('zeta'+str(i),data=zeta)
				vecsgroup.create_dataset('delta'+str(i),data=delta)
	else:
		varloc = varloc_dict[operator]
		for i in range(singular_vals.shape[0]):
			data = singular_vecs[i,:]
			if settings.scheme == 'TRISK' and varloc == 'edge':
				zeta = grid.grid.j(grid.grid.d2bar(grid.grid.einv(data,settings.vlevel),settings.vlevel,nverts),settings.vlevel)
				delta = grid.grid.i(grid.grid.d2(grid.grid.h(grid.grid.einv(data,settings.vlevel),settings.vlevel),settings.vlevel,nfaces),settings.vlevel)
				vecsgroup.create_dataset('zeta'+str(i),data=zeta)
				vecsgroup.create_dataset('delta'+str(i),data=delta)
			else:
				vecsgroup.create_dataset('data'+str(i),data=data)

svdpostfile.close()
svdfile.close()
eigenpostfile.close()
