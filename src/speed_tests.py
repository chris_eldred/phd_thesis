import grid
from mesh import *
import scipy.sparse as spsparse
import scipy.sparse.linalg as spsparselin
from configuration import *
import time

get_configuration('config.template')

ngrids = 5
rad = 6371220.0
niters = 100
set_grid_sizes_cubedsphere(grid.grid,ngrids)
allocate_connectivity_cubedsphere(grid.grid)
allocate_operators_cubedsphere(grid.grid)

load_connectivity(grid.grid)
load_operators(grid.grid)
grid.grid.dimensionalize_operators(rad)

nvert = grid.grid.nvert[ngrids-1]
nface = grid.grid.nface[ngrids-1]
nedge = grid.grid.nedge[ngrids-1]

I = create_scipy_sparse_matrix(grid.grid.i_stencil[:nface,:,ngrids-1],grid.grid.i_stencil_size[:nface,ngrids-1],
grid.grid.i_coeffs[:nface,:,ngrids-1],nface,nface)
print 'I'

J = create_scipy_sparse_matrix(grid.grid.j_stencil[:nvert,:,ngrids-1],grid.grid.j_stencil_size[:nvert,ngrids-1],
grid.grid.j_coeffs[:nvert,:,ngrids-1],nvert,nvert)
print 'J'

H = create_scipy_sparse_matrix(grid.grid.h_stencil[:nedge,:,ngrids-1],grid.grid.h_stencil_size[:nedge,ngrids-1],
grid.grid.h_coeffs[:nedge,:,ngrids-1],nedge,nedge)
print 'H'

D1 =  create_scipy_sparse_matrix(grid.grid.ve[:nedge,:,ngrids-1],np.ones(nedge,dtype=np.int32)*2,
grid.grid.tev_edge[:nedge,:,ngrids-1],nedge,nvert)
print 'D1'

D2 = create_scipy_sparse_matrix(grid.grid.ec[:nface,:,ngrids-1],grid.grid.ecsize[:nface,ngrids-1],
grid.grid.nei_cell[:nface,:,ngrids-1],nface,nedge)
print 'D2'

D1bar =  create_scipy_sparse_matrix(grid.grid.ce[:nedge,:,ngrids-1],np.ones(nedge,dtype=np.int32)*2,
-1*grid.grid.nei_edge[:nedge,:,ngrids-1],nedge,nface)
print 'D1bar'

D2bar = create_scipy_sparse_matrix(grid.grid.ev[:nvert,:,ngrids-1],grid.grid.evsize[:nvert,ngrids-1],
grid.grid.tev_vertex[:nvert,:,ngrids-1],nvert,nedge)
print 'D2bar'

curlcurl_part = -1.0 * D1 * J * D2bar
print 'curlcurlpart'
graddiv = D1bar * I * D2 * H
print 'graddiv'

testvare = np.random.rand(nedge)
testvarf = np.random.rand(nface)
testvarv = np.random.rand(nvert)

############  D1  #################
start = time.time()
for i in range(niters):
	test = D1 * testvarv
end = time.time()
print 'Sparse D1 time',(end-start)/niters

start = time.time()
for i in range(niters):
	testop = grid.grid.d1(testvarv,ngrids,grid.grid.nedge[-1])
end = time.time()
print 'f2py D1 time',(end-start)/niters

diff1 = test - testop
print np.amax(diff1/test),np.amin(diff1/test),np.average(diff1/test)
print 

############  D2  #################
start = time.time()
for i in range(niters):
	test = D2 * testvare
end = time.time()
print 'Sparse D2 time',(end-start)/niters

start = time.time()
for i in range(niters):
	testop = grid.grid.d2(testvare,ngrids,grid.grid.nface[-1])
end = time.time()
print 'f2py D2 time',(end-start)/niters

diff1 = test - testop
print np.amax(diff1/test),np.amin(diff1/test),np.average(diff1/test)
print 

############  D1bar  #################
start = time.time()
for i in range(niters):
	test = D1bar * testvarf
end = time.time()
print 'Sparse D1bar time',(end-start)/niters

start = time.time()
for i in range(niters):
	testop = grid.grid.d1bar(testvarf,ngrids,grid.grid.nedge[-1])
end = time.time()
print 'f2py D1bar time',(end-start)/niters

diff1 = test - testop
print np.amax(diff1/test),np.amin(diff1/test),np.average(diff1/test)
print 

############  D2bar  #################
start = time.time()
for i in range(niters):
	test = D2bar * testvare
end = time.time()
print 'Sparse D2bar time',(end-start)/niters

start = time.time()
for i in range(niters):
	testop = grid.grid.d2bar(testvare,ngrids,grid.grid.nvert[-1])
end = time.time()
print 'f2py D2bar time',(end-start)/niters

diff1 = test - testop
print np.amax(diff1/test),np.amin(diff1/test),np.average(diff1/test)
print 


##########  I  #############
start = time.time()
for i in range(niters):
	test = I * testvarf
end = time.time()
print 'Sparse I time',(end-start)/niters

start = time.time()
for i in range(niters):
	testop = grid.grid.i(testvarf,ngrids)
end = time.time()
print 'f2py I time',(end-start)/niters

diff1 = test - testop
print np.amax(diff1/test),np.amin(diff1/test),np.average(diff1/test)
print 


##########  J   #############
start = time.time()
for i in range(niters):
	test = J * testvarv
end = time.time()
print 'Sparse J time',(end-start)/niters

start = time.time()
for i in range(niters):
	testop = grid.grid.j(testvarv,ngrids)
end = time.time()
print 'f2py J time',(end-start)/niters

diff1 = test - testop
print np.amax(diff1/test),np.amin(diff1/test),np.average(diff1/test)
print 


##########  H  #############
start = time.time()
for i in range(niters):
	test = H * testvare
end = time.time()
print 'Sparse H time',(end-start)/niters

start = time.time()
for i in range(niters):
	testop = grid.grid.h(testvare,ngrids)
end = time.time()
print 'f2py H time',(end-start)/niters

diff1 = test - testop
print np.amax(diff1/test),np.amin(diff1/test),np.average(diff1/test)
print 


##############  graddiv  #################
start = time.time()
for i in range(niters):
	test = graddiv * testvare
end = time.time()
print 'Sparse graddiv time',(end-start)/niters

start = time.time()
for i in range(niters):
	testop = grid.grid.graddiv(testvare,ngrids,nface)
end = time.time()
print 'f2py graddiv time',(end-start)/niters

diff1 = test - testop
print np.amax(diff1/test),np.amin(diff1/test),np.average(diff1/test)
print 


##############  Hinv  #################
start = time.time()
for i in range(niters):
	test = apply_Hinv(testvare,H)
end = time.time()
print 'Sparse Hinv time',(end-start)/niters

start = time.time()
for i in range(niters):
	testop = grid.grid.hinv(testvare,ngrids)
end = time.time()
print 'f2py Hinv time',(end-start)/niters

diff1 = test - testop
print np.amax(diff1/test),np.amin(diff1/test),np.average(diff1/test)
print 

##############  curlcurlpart  #################
start = time.time()
for i in range(niters):
	test = curlcurl_part * testvare
end = time.time()
print 'Sparse curlcurlpart time',(end-start)/niters

start = time.time()
for i in range(niters):
	testop = grid.grid.curlcurl_part(testvare,ngrids,nvert)
end = time.time()
print 'f2py curlcurlpart time',(end-start)/niters

diff1 = test - testop
print np.amax(diff1/test),np.amin(diff1/test),np.average(diff1/test)
print 

##############  curlcurl  #################
start = time.time()
for i in range(niters):
	test = apply_Hinv(curlcurl_part * testvare,H)
end = time.time()
print 'Sparse curlcurl (solve Hinv) time',(end-start)/niters

start = time.time()
for i in range(niters):
	test2 = apply_Hinv(grid.grid.curlcurl_part(testvare,ngrids,nvert),H)
end = time.time()
print 'f2py curlcurl_part time',(end-start)/niters

start = time.time()
for i in range(niters):
	testop = grid.grid.curlcurl(testvare,ngrids,nvert)
end = time.time()
print 'f2py curlcurl time',(end-start)/niters

diff1 = test - testop
diff2 = test2 - testop
diff3 = test - test2
print np.amax(diff1/test),np.amin(diff1/test),np.average(diff1/test)
print np.amax(diff2/test2),np.amin(diff2/test2),np.average(diff2/test2)
print np.amax(diff3/test),np.amin(diff3/test),np.average(diff3/test)
print 
