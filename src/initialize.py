import numpy as np
from constants import *
from math import pi,sqrt,cos,sin,exp
import scipy.integrate as integ
import scipy.interpolate as intp
import grid
import h5py
from mesh import *



def initialize_bve_vars():
		
	if settings.initial_condition == 'RHwave':
		return bve_RHwave()
	if settings.initial_condition == 'Neamtan':
		return bve_Neamtan()
		
def initialize_vars():
	#planar only
	if settings.initial_condition == 'dualvortices':
		return dualvortices()
	
	#both 
	if settings.initial_condition == 'geostrophic_turbulence':
		return geostrophic_turbulence()
		
	#spherical only
	if settings.initial_condition == 'TC2':
		alpha = 0.0
		u0 = 2.*pi*settings.a/(12.*24.*60.*60.)
		gh0 = 29400.0
		return TC2(gh0,u0,alpha)
		
	if settings.initial_condition == 'TC5':
		return TC5()
	if settings.initial_condition == 'RHwave':
		return RHwave()
	if settings.initial_condition == 'Galewsky':
		return Galewsky()
	if settings.initial_condition == 'heldsuarezdry':
		return heldsuarezdry()

def heldsuarezdry():
	meshfile = h5py.File(settings.meshdir + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(grid.grid.nface[-1]) + '.hdf5','r')
	vlat = np.array(meshfile['coords']['vlat'])
	clat = np.array(meshfile['coords']['clat'])
	meshfile.close()
	
	m = 12
	u0 = 120.
	dH = 8000./settings.g
	h0 = 10.**3 - 2.*dH/3.
	hi = h0 + dH * (1. - np.square(np.sin(clat)))
	bi = np.zeros(grid.grid.nface[-1],dtype=np.float64)	

	if settings.scheme == 'TRISK':
		fv = grid.grid.jinv(2.*settings.Omega*np.sin(vlat),grid.grid.ngrids)
		mi = grid.grid.iinv(hi,grid.grid.ngrids)
		A = np.sin(2.*(m-2)*vlat) / (8. * (m-2))
		B = np.sin(2.*(m+2)*vlat) / (8. * (m+2))
		C = np.sin(2. * m * vlat) / (4.*m)
		psiv = settings.a * u0 * 0.5 * (A + B - C)
		if settings.grid_type == 'geodesic':
			ue = -1.0 * grid.grid.hinv(grid.grid.d1(psiv,settings.vlevel,grid.grid.nedge[-1]),settings.vlevel)
		if settings.grid_type == 'cubedsphere':
			ue = -1.0 * apply_Hinv(grid.grid.d1(psiv,settings.vlevel,grid.grid.nedge[-1]),grid.H)


		return mi,ue,fv,bi
	
	if settings.scheme == 'ZGRID':
		delta = np.zeros(grid.grid.nface[-1],dtype=np.float64)
		fi = 2.*settings.Omega*np.sin(clat)
		#A = np.tan(clat) * np.square(np.sin(2. * clat)) * (np.square(np.sin(m * clat)) - 0.5)
		#B = 2. * m * np.square(np.sin(2.*clat)) * np.sin(m*clat) * np.cos(m*clat)
		#C = 4. * np.sin(2.*clat) * np.cos(2.*clat) * (np.square(np.sin(m * clat)) - 0.5)
		#zeta = u0 / settings.a * (-A + B + C)
		A = m * np.square(np.sin(2*clat)) * np.sin(m*clat) * np.cos(m*clat)
		B = 2. * np.sin(2*clat) * np.cos(2*clat) * (np.square(np.sin(m*clat)) - 1/2.)
		C = 0.5 * np.square(np.sin(2*clat)) * np.tan(clat) * (np.square(np.sin(m*clat)) - 1/2.)
		zeta = 2.*u0 / settings.a * (A + B - C)
		#print np.amin(A),np.amax(A),np.average(A)
		#print np.amin(B),np.amax(B),np.average(B)
		#print np.amin(C),np.amax(C),np.average(C)
		#
		return hi,zeta,delta,fi,bi
			
def geostrophic_turbulence():
	if settings.grid_type == 'geodesic' or settings.grid_type == 'cubedsphere':
		meshfile = h5py.File(settings.meshdir + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(grid.grid.nface[-1]) + '.hdf5','r')
		vlat = np.array(meshfile['coords']['vlat'])
		clat = np.array(meshfile['coords']['clat'])
		meshfile.close()
	
	hi = 2. * np.random.random(grid.grid.nface[-1]) - 1.
	hi = hi * settings.hi_variance 
	hi_average = np.average(hi) 
	hi = hi + settings.hi_mean - hi_average

	bi = np.zeros(grid.grid.nface[-1],dtype=np.float64)

	if settings.scheme == 'TRISK':

		mi = grid.grid.iinv(hi,grid.grid.ngrids)
		
		ue = 2. * np.random.random(grid.grid.nedge[-1]) - 1.
		ue = ue * settings.ue_variance
		ue_average = np.average(ue)
		ue = ue + settings.ue_mean - ue_average
		ue = grid.grid.einv(ue,grid.grid.ngrids) 

		if settings.grid_type == 'geodesic' or settings.grid_type == 'cubedsphere':
			sinlat = np.sin(vlat)
			fv = grid.grid.jinv(2.*settings.Omega*sinlat,grid.grid.ngrids)
		else:
			fv = grid.grid.jinv(np.ones(grid.grid.nvert[-1])*settings.f,grid.grid.ngrids)
		return mi,ue,fv,bi

	if settings.scheme == 'ZGRID':

		zeta = 2. * np.random.random(grid.grid.nface[-1]) - 1.
		zeta = zeta * settings.zeta_variance 
		zeta_average = np.average(zeta) 
		zeta = zeta + settings.zeta_mean - zeta_average
		
		delta = 2. * np.random.random(grid.grid.nface[-1]) - 1.
		delta = delta * settings.delta_variance 
		delta_average = np.average(delta) 
		delta = delta + settings.delta_mean - delta_average
	
		if settings.grid_type == 'geodesic' or settings.grid_type == 'cubedsphere':
			sinlat = np.sin(clat)
			fi = 2.*settings.Omega*sinlat
		else:
			fi = np.ones(grid.grid.nface[-1])*settings.f
			
		return hi,zeta,delta,fi,bi


def dualvortices():
	meshfile = h5py.File(settings.meshdir + '/' + settings.grid_type + '/' + grid.grid.basename + '/' + grid.grid.basename + '.hdf5','r')
	xi = np.array(meshfile['coords']['ccoords'][:,0]) + settings.dx/2.
	yi = np.array(meshfile['coords']['ccoords'][:,0]) + settings.dx/2.
	xe = np.array(meshfile['coords']['ecoords'][:,0]) + settings.dx/2.
	ye = np.array(meshfile['coords']['ecoords'][:,0]) + settings.dx/2.
	xv = np.array(meshfile['coords']['vcoords'][:,0]) + settings.dx/2.
	yv = np.array(meshfile['coords']['vcoords'][:,0]) + settings.dx/2.
	meshfile.close()
	
	if settings.grid_type == 'square':
		yfactor = 1.0
		uoffset = 0.0
	if settings.grid_type == 'hex':
		yfactor = sqrt(3.)/2.
		uoffset = settings.ny/4. * settings.dx
	Lx = settings.nx * settings.dx
	Ly = settings.ny * settings.dx * yfactor
	stddev_x = 5.* settings.dx
	stddev_y = stddev_x * yfactor
	ux = Lx/2.
	uy = Ly/2.
	bi = np.zeros(grid.grid.nface[-1],dtype=np.float64)
	
	if settings.scheme == 'TRISK':
		streamfunc1 = 200.0 * np.exp(-1./2. * (np.square(yv - uy)/(stddev_y*stddev_y) + np.square(xv - ux - uoffset - 7.5*settings.dx)/(stddev_x*stddev_x)))
		streamfunc2 = 200.0 * np.exp(-1./2. * (np.square(yv - uy)/(stddev_y*stddev_y) + np.square(xv - ux - uoffset + 7.5*settings.dx)/(stddev_x*stddev_x)))
		streamfunc = streamfunc1 + streamfunc2 + 6500.0
		
		mi = grid.grid.iinv(grid.grid.rt(streamfunc,grid.grid.ngrids,grid.grid.nface[-1]),grid.grid.ngrids)
		ue = -1. * settings.g / settings.f * grid.grid.hinv(grid.grid.d1(streamfunc,grid.grid.ngrids,grid.grid.nedge[-1]),grid.grid.ngrids)
		fv = np.ones(grid.grid.nvert[-1],dtype=np.float64) * settings.f
		fv = grid.grid.jinv(fv,grid.grid.ngrids)
		return mi,ue,fv,bi


	if settings.scheme == 'ZGRID':
		streamfunc1 = 200.0 * np.exp(-1./2. * (np.square(yi - uy)/(stddev_y*stddev_y) + np.square(xi - ux - uoffset - 7.5*settings.dx)/(stddev_x*stddev_x)))
		streamfunc2 = 200.0 * np.exp(-1./2. * (np.square(yi - uy)/(stddev_y*stddev_y) + np.square(xi - ux - uoffset + 7.5*settings.dx)/(stddev_x*stddev_x)))
		streamfunc = streamfunc1 + streamfunc2 + 6500.0
		
		hi = streamfunc
		zeta = settings.g / settings.f * grid.grid.laplac_primal(streamfunc,grid.grid.ngrids)
		delta = np.zeros(grid.grid.nface[-1],dtype=np.float64)
		fi = np.ones(grid.grid.nface[-1],dtype=np.float64) * settings.f
		return hi,zeta,delta,fi,bi
		
		
def TC2(gh0,u0,alpha):
	meshfile = h5py.File(settings.meshdir + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(grid.grid.nface[-1]) + '.hdf5','r')
	vlat = np.array(meshfile['coords']['vlat'])
	vlon = np.array(meshfile['coords']['vlon'])
	clat = np.array(meshfile['coords']['clat'])
	clon = np.array(meshfile['coords']['clon'])
	meshfile.close()

	mod_sin_lat_c = np.sin(clat) * cos(alpha) - np.cos(clon) * np.cos(clat) * sin(alpha)
	C = -1.*(settings.a * settings.Omega * u0 + u0*u0/2.)/settings.g
	BG = np.ones(grid.grid.nface[-1]) * gh0/settings.g
	hi = BG + np.square(mod_sin_lat_c)*C 
	bi = np.zeros(grid.grid.nface[-1],dtype=np.float64)

	if settings.scheme == 'TRISK':
		mod_sin_lat_v = np.sin(vlat) * cos(alpha) - np.cos(vlon) * np.cos(vlat) * sin(alpha)
		psiv = -1.0 * settings.a * u0 * mod_sin_lat_v

		if settings.grid_type == 'geodesic':
			ue = -1.0 * grid.grid.hinv(grid.grid.d1(psiv,settings.vlevel,grid.grid.nedge[-1]),settings.vlevel)
		if settings.grid_type == 'cubedsphere':
			ue = -1.0 * apply_Hinv(grid.grid.d1(psiv,settings.vlevel,grid.grid.nedge[-1]),grid.H)
		mi = grid.grid.iinv(hi,settings.vlevel)
		fv = grid.grid.jinv(2.*settings.Omega*mod_sin_lat_v,settings.vlevel) 
		return mi,ue,fv,bi

	if settings.scheme == 'ZGRID':
		delta = np.zeros(grid.grid.nface[-1],dtype=np.float64)
		psic = -1.0 * settings.a * u0 * mod_sin_lat_c
		zeta = grid.grid.laplac_primal(psic,settings.vlevel)
		fi = 2.*settings.Omega*mod_sin_lat_c
		return hi,zeta,delta,fi,bi		
				
def TC5():
	meshfile = h5py.File(settings.meshdir + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(grid.grid.nface[-1]) + '.hdf5','r')
	clon = np.array(meshfile['coords']['clon'])
	clat = np.array(meshfile['coords']['clat'])
	meshfile.close()
	
	u0 = 20.0
	gh0 = settings.g * 5960.
	alpha = 0.
	
	R = pi/9.
	hs0 = 2000.0
	lonc = 3.*pi/2.
	latc = pi/6.
	d = np.arccos( np.multiply(np.cos(clat) * cos(latc),np.cos(clon - lonc)) + np.sin(clat) * sin(latc))
	r = np.minimum(R,d)
	bi = hs0 * (1. - r/R)
	
	if settings.scheme == 'TRISK':
		mi,ue,fv,junk = TC2(gh0,u0,alpha)
		bim = grid.grid.iinv(bi,settings.vlevel)
		return mi,ue,fv,bim
		
	if settings.scheme == 'ZGRID':
		hi,zeta,delta,fi,junk = TC2(gh0,u0,alpha)
		return hi,zeta,delta,fi,bi

def RHwave():
	meshfile = h5py.File(settings.meshdir + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(grid.grid.nface[-1]) + '.hdf5','r')
	clon = np.array(meshfile['coords']['clon'])
	clat = np.array(meshfile['coords']['clat'])
	vlon = np.array(meshfile['coords']['vlon'])
	vlat = np.array(meshfile['coords']['vlat'])
	meshfile.close()
	
	R = 4
	w = 0.000007848
	K = 0.000007848
	h0 = 8000.0

	t1 = K*K/4. * np.power(np.cos(clat),2*R)
	t2 = (R+1.) * np.square(np.cos(clat)) + (2.*R*R - R - 2.) - 2*R*R*np.power(np.cos(clat),-2.) 
	A = w / 2. * (2. * settings.Omega + w) * np.square(np.cos(clat)) + t1*t2
	t1 = R*R + 2.*R +2. - (R+1.)*(R+1.)*np.square(np.cos(clat))
	B = 2. * (settings.Omega + w) * K / ((R+1.)*(R+2.)) * np.power(np.cos(clat),R) * t1
	t1 = (R+1.)*np.square(np.cos(clat)) - (R+2.)
	C = K*K/4. * np.power(np.cos(clat),2.*R) * t1
	BG = np.ones(grid.grid.nface[-1]) * h0

	hi = BG + settings.a * settings.a / settings.g * (A + B * np.cos(R * clon) + C * np.cos(2.*R*clon))
	bi = np.zeros(grid.grid.nface[-1],dtype=np.float64)

	if settings.scheme == 'TRISK':
		psiv = - settings.a * settings.a * w * np.sin(vlat) + settings.a * settings.a * K * np.power(np.cos(vlat),R) * np.sin(vlat) * np.cos(R*vlon)
		if settings.grid_type == 'geodesic':
			ue = -1.0 * grid.grid.hinv(grid.grid.d1(psiv,settings.vlevel,grid.grid.nedge[-1]),settings.vlevel)
		if settings.grid_type == 'cubedsphere':
			ue = -1.0 * apply_Hinv(grid.grid.d1(psiv,settings.vlevel,grid.grid.nedge[-1]),grid.H)
		mi = grid.grid.iinv(hi,settings.vlevel)
		fv = grid.grid.jinv(2.*settings.Omega*np.sin(vlat),settings.vlevel) 
		return mi,ue,fv,bi
		
	if settings.scheme == 'ZGRID':
		delta = np.zeros(grid.grid.nface[-1],dtype=np.float64)
		psic = - settings.a * settings.a * w * np.sin(clat) + settings.a * settings.a * K * np.power(np.cos(clat),R) * np.sin(clat) * np.cos(R*clon)
		zeta = 2. * w * np.sin(clat) - K * np.sin(clat) * np.power(np.cos(clat),R) * (R*R + 3.*R + 2.) * np.cos(R*clon)
		fi = 2.*settings.Omega*np.sin(clat)
		return hi,zeta,delta,fi,bi	

def Galewsky():
	meshfile = h5py.File(settings.meshdir + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(grid.grid.nface[-1]) + '.hdf5','r')
	vlat = np.array(meshfile['coords']['vlat'])
	clat = np.array(meshfile['coords']['clat'])
	clon = np.array(meshfile['coords']['clon'])
	meshfile.close()
	
	#compute height and streamfunction as a function of latitude
	nlat = 2000
	#dphi = pi/2./nphi
	lat = np.linspace(-pi/2.,pi/2.,nlat)
	umax = 80.
	lat0 = pi/7.
	lat1 =  pi/2. - lat0
	en = exp(-4./((lat1-lat0)**2))
	f = 2.0 * settings.Omega * np.sin(lat)
	u = umax / en * np.exp(1./((lat-lat0)*(lat-lat1)))
	u[np.greater_equal(lat,lat1)] = 0.0
	u[np.less_equal(lat,lat0)] = 0.0
	
	psi = integ.cumtrapz(-1.0*settings.a*u,lat,initial=0.0)
	h = integ.cumtrapz( -1.0 * settings.a * u / settings.g * (f + np.tan(lat)/settings.a*u),lat,initial=0.0)
	psif = intp.interp1d(lat,psi)
	hf = intp.interp1d(lat,h)
	
	#compute height
	hi = hf(clat)

	#adjust mean layer height to be equal to 10km
	himean = np.mean(hi)
	hi = 10000.0 - himean + hi
	#add height perturbation
	if settings.perturb_H == True:
		alpha = 1/3.
		beta = 1/15.
		lat2 = pi/4.
		hprime = 120.
		hiperturb = hprime * np.cos(clat) * np.exp(-1.*np.square(clon/alpha)) * np.exp(-1.0*np.square((lat2 - clat)/beta))
		hi = hi + hiperturb	
		
	bi = np.zeros(grid.grid.nface[-1],dtype=np.float64)

	if settings.scheme == 'TRISK':
		psiv = psif(vlat)
		if settings.grid_type == 'geodesic':
			ue = -1.0 * grid.grid.hinv(grid.grid.d1(psiv,settings.vlevel,grid.grid.nedge[-1]),settings.vlevel)
		if settings.grid_type == 'cubedsphere':
			ue = -1.0 * apply_Hinv(grid.grid.d1(psiv,settings.vlevel,grid.grid.nedge[-1]),grid.H)
		mi = grid.grid.iinv(hi,settings.vlevel)
		fv = grid.grid.jinv(2.*settings.Omega*np.sin(vlat),settings.vlevel)
		return mi,ue,fv,bi

	if settings.scheme == 'ZGRID':
		delta = np.zeros(grid.grid.nface[-1],dtype=np.float64)
		psic = psif(clat)
		zeta = grid.grid.laplac_primal(psic,settings.vlevel)
		fi = 2.*settings.Omega*np.sin(clat)
		return hi,zeta,delta,fi,bi		

def P(A):
	return 135135. * A * np.power(np.sqrt(1. - A*A),6)
	
def bve_Neamtan():
	meshfile = h5py.File(settings.meshdir + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(grid.grid.nface[-1]) + '.hdf5','r')
	vlat = np.array(meshfile['coords']['vlat'])
	vlat = np.array(meshfile['coords']['vlon'])
	clat = np.array(meshfile['coords']['clat'])
	clon = np.array(meshfile['coords']['clon'])
	meshfile.close()
	
	A = 1000.
	m = 6
	n = 7
	B = 2. * settings.Omega / (n * (n+1) - 2)
	if settings.scheme == 'TRISK':
		sinlat = np.sin(vlat)
		psi = A * np.sin(m * vlon) * P(sinlat) - B * settings.a * settings.a * sinlat
		#zeta = grid.grid.laplac_dual(psi,settings.vlevel)
		fv = grid.grid.jinv(2.*settings.Omega*np.sin(vlat),settings.vlevel) 
		return zeta,fv

	if settings.scheme == 'ZGRID':
		sinlat = np.sin(clat)
		psi = A * np.sin(m * clon) * P(sinlat) - B * settings.a * settings.a * sinlat
		zeta = grid.grid.laplac_primal(psi,settings.vlevel)
		fi = 2.*settings.Omega*np.sin(clat)
		return zeta,fi
		
def bve_RHwave():
	meshfile = h5py.File(settings.meshdir + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(grid.grid.nface[-1]) + '.hdf5','r')
	vlon = np.array(meshfile['coords']['vlon'])
	vlat = np.array(meshfile['coords']['vlat'])
	clat = np.array(meshfile['coords']['clat'])
	clon = np.array(meshfile['coords']['clon'])
	meshfile.close()

	R = 4
	w = 0.000007848
	K = 0.000007848

	if settings.scheme == 'TRISK':
		zetav = 2. * w * np.sin(vlat) - K * np.sin(vlat) * np.power(np.cos(vlat),R) * (R*R + 3.*R + 2.) * np.cos(R*vlon)
		zeta = grid.grid.jinv(zetav,settings.vlevel)
		fv = grid.grid.jinv(2.*settings.Omega*np.sin(vlat),settings.vlevel) 
		return zeta,fv
		
	if settings.scheme == 'ZGRID':
		zeta = 2. * w * np.sin(clat) - K * np.sin(clat) * np.power(np.cos(clat),R) * (R*R + 3.*R + 2.) * np.cos(R*clon)
		fi = 2.*settings.Omega*np.sin(clat)
		return zeta,fi
