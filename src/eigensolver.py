from mesh import *
from constants import *
from eigen import *
from configuration import *
import sys
import time
import grid
from math import sqrt

start = time.clock()
#get configuration
get_configuration(sys.argv[1])
get_eigenconfig(sys.argv[1])

print 'configured'

#load operators and connectivity
if settings.grid_type == 'cubedsphere':
	set_grid_sizes_cubedsphere(grid.grid,settings.vlevel)
	allocate_connectivity_cubedsphere(grid.grid)
	allocate_geometry_cubedsphere(grid.grid)
	allocate_operators_cubedsphere(grid.grid)
if settings.grid_type == 'geodesic':
	set_grid_sizes_geodesic(grid.grid,settings.vlevel)
	allocate_connectivity_geodesic(grid.grid)
	allocate_operators_geodesic(grid.grid)
	allocate_geometry_geodesic(grid.grid)
if settings.grid_type == 'square':
	set_grid_sizes_square(grid.grid,settings.nx,settings.ny)
	allocate_connectivity_square(grid.grid)
	allocate_operators_square(grid.grid)
	allocate_geometry_square(grid.grid)
if settings.grid_type == 'hex':
	set_grid_sizes_hex(grid.grid,settings.nx,settings.ny)
	allocate_connectivity_hex(grid.grid)
	allocate_operators_hex(grid.grid)
	allocate_geometry_hex(grid.grid)
		
if settings.grid_type == 'square' or settings.grid_type == 'hex':
	settings.a = settings.dx
	
load_connectivity(grid.grid)
load_operators(grid.grid)
grid.grid.dimensionalize_operators(settings.a)
load_geometry(grid.grid)
grid.grid.dimensionalize_geometry(settings.a)
init = time.clock()
print 'operators and connectivity loaded!'

#build eigensystem
dx = np.average(grid.grid.de[:,-1])
if settings.variable_f == False:
	settings.Hbar = settings.f*settings.f * settings.C*settings.C * dx * dx / settings.g
	build_eigensystem_constant_f()
if settings.variable_f == True:
	settings.Hbar = 10.**5/settings.g # matches Weller stuff, should maybe be higher so Rossby radius is better resolved?
	build_eigensystem_variable_f()
eigenbuild = time.clock()
print 'dx',dx/1000.
print 'Hbar',settings.Hbar
if settings.variable_f == False:
	print 'lambda',sqrt(settings.g * settings.Hbar) / settings.f / 1000.
if settings.variable_f == True:
	print 'lambda',sqrt(settings.g * settings.Hbar) / (2. * settings.Omega) / 1000. #worst case (ie at poles)

print 'eigensystem and svd matrices built'

#solve eigensystem
solve_eigensystem()
eigensolve = time.clock()
print 'eigensystem solved'

#solve svd
solve_svd()
svdsolve = time.clock()
print 'svd system solved'

print 'init time',init - start
print 'build time', eigenbuild - init
print 'eigensolve time',eigensolve - eigenbuild
print 'svdsolve time',svdsolve - eigensolve
print 'total time',svdsolve - start
