import matplotlib.pyplot as plt
import h5py
import numpy as np
from basemap_plotting_helpers import *
from tabulate import tabulate
import sys

plotmeshes = sys.argv[1]

gridlist = ['geodesic','cubedsphere']
gridvariantdict = {'geodesic': ['tweaked','cvt','spring_beta=0.8','spring_beta=1.1'], 'cubedsphere': ['thuburn',]}
vlevels = [2,3,4,5,6,7,8]
a = 6371220.0
headers = ['Grid','Cells','Max de (km)','Max/Min de','Max/Min le','Max/Min Ai','Max/Min Av','$\Delta t (s)$']
short_gridtypes = {'geodesic': 'G', 'cubedsphere': 'C'}
short_variants = {'tweaked':'HR','cvt':'CVT','spring_beta=0.8':'SB0.8','spring_beta=1.1':'SB1.1','thuburn':'TH'}
geodesic_timestep_dict = {}
geodesic_timestep_dict[2] = 1440
geodesic_timestep_dict[3] = 720
geodesic_timestep_dict[4] = 360
geodesic_timestep_dict[5] = 180
geodesic_timestep_dict[6] = 90
geodesic_timestep_dict[7] = 45
geodesic_timestep_dict[8] = 22.5
cubedsphere_timestep_dict = {}
cubedsphere_timestep_dict[2] = 1200
cubedsphere_timestep_dict[3] = 480
cubedsphere_timestep_dict[4] = 240
cubedsphere_timestep_dict[5] = 120
cubedsphere_timestep_dict[6] = 60
cubedsphere_timestep_dict[7] = 30
cubedsphere_timestep_dict[8] = 15 #GALEWSKY CS-TE-8 IS UNSTABLE? POSSIBLY? WITH THIS- MAYBE DO 12.5 or 10 INSTEAD?
timestep_dict = { 'geodesic' : geodesic_timestep_dict , 'cubedsphere' : cubedsphere_timestep_dict}

for gridtype in gridlist:
	for gridvariant in gridvariantdict[gridtype]:
		gridstats_table = []
		for vlevel in vlevels:
			if (gridvariant == 'spring_beta=0.8' or gridvariant == 'spring_beta=1.1'):
				if (vlevel > 5):
					continue
			grid = get_geometry(gridtype,gridvariant,vlevel)

			print gridtype,gridvariant,vlevel,grid.ncells,grid.nverts,grid.nedges
		
			Ai = grid.Ai / (10.**3) * a * a
			Av = grid.Av / (10.**3) * a * a
			le = grid.le / (10.**3) * a
			de = grid.de / (10.**3) * a
			#print 'Ai',np.amin(Ai),np.amax(Ai),np.average(Ai),np.amax(Ai)/np.amin(Ai)
			#print 'Av',np.amin(Av),np.amax(Av),np.average(Av),np.amax(Av)/np.amin(Av)
			#print 'le',np.amin(le),np.amax(le),np.average(le),np.amax(le)/np.amin(le)
			print 'de',np.amin(de),np.amax(de),np.average(de),np.amax(de)/np.amin(de)
			#print 'le/de',np.amin(le/de),np.amax(le/de),np.average(le/de),np.amax(le/de)/np.amin(le/de)

			dt = timestep_dict[gridtype][vlevel]

			gridname = short_gridtypes[gridtype]
			opt = short_variants[gridvariant]
			gridstats_table.append([gridname+str(vlevel)+'-'+opt,grid.ncells,np.round(np.amax(de)),np.amax(de)/np.amin(de),np.amax(le)/np.amin(le),np.amax(Ai)/np.amin(Ai),np.amax(Av)/np.amin(Av),dt])

			if vlevel <= 5 and plotmeshes == True:
				plot_primal_mesh(grid)
				plot_dual_mesh(grid)
				plot_edge_mesh(grid)
	
		print tabulate(gridstats_table, headers, tablefmt="latex")
			
			#ADD Ae stuff
			#ADD SKEWNESS
			#ADD NON-ORTHOGONAL DEGREE
			#ADD CENTROIDALITY- BOTH PRIMAL AND DUAL
			#ADD PEDRO STUFF- ALIGNMENT			
