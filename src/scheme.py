from __future__ import print_function
from constants import *
import grid
import numpy as np
from mesh import *
from initialize import *

ab3_time_weights = [5./12. , -4./3. , 23./12.]
		
def create_scheme_var_dicts():
	if settings.scheme == 'TRISK':
		ntm.predicted_vars['mi'] = grid.mi
		ntm.predicted_vars['ue'] = grid.ue
		ntm.constants['bi'] = grid.bi
		ntm.constants['fv'] = grid.fv
		ntm.statistics['MI'] = grid.grid.mc
		ntm.statistics['MV'] = grid.grid.mv
		ntm.statistics['EV'] = grid.grid.eta
		ntm.statistics['ZV'] = grid.grid.zeta
		ntm.statistics['KE'] = grid.grid.kinetic
		ntm.statistics['PE'] = grid.grid.potential
		ntm.statistics['TE'] = grid.grid.total
		ntm.statistics['Q'] = grid.grid.enstrophy

	if settings.scheme == 'ZGRID':
		ntm.predicted_vars['hi'] = grid.hi
		ntm.predicted_vars['zeta'] = grid.zeta
		ntm.predicted_vars['delta'] = grid.delta
		ntm.constants['bi'] = grid.bi
		ntm.constants['fi'] = grid.fi
		ntm.statistics['MI'] = grid.grid.mc
		ntm.statistics['EV'] = grid.grid.eta
		ntm.statistics['ZV'] = grid.grid.zeta
		ntm.statistics['KE'] = grid.grid.kinetic
		ntm.statistics['PE'] = grid.grid.potential
		ntm.statistics['TE'] = grid.grid.total
		ntm.statistics['Q'] = grid.grid.enstrophy

	#ntm.statistics['ldelta_max'] = grid.grid.ldelta_max
	#ntm.statistics['ldelta_min'] = grid.grid.ldelta_min
	#ntm.statistics['ldelta_average'] = grid.grid.ldelta_average
	#ntm.statistics['lzeta_max'] = grid.grid.lzeta_max
	#ntm.statistics['lzeta_min'] = grid.grid.lzeta_min
	#ntm.statistics['lzeta_average'] = grid.grid.lzeta_average

def create_scheme_vars():

	#grid.grid.ldelta_max = np.zeros(settings.nsteps+1,dtype=np.float64)
	#grid.grid.ldelta_min = np.zeros(settings.nsteps+1,dtype=np.float64)
	#grid.grid.ldelta_average = np.zeros(settings.nsteps+1,dtype=np.float64)
	#grid.grid.lzeta_max = np.zeros(settings.nsteps+1,dtype=np.float64)
	#grid.grid.lzeta_min = np.zeros(settings.nsteps+1,dtype=np.float64)
	#grid.grid.lzeta_average = np.zeros(settings.nsteps+1,dtype=np.float64)

	if settings.scheme == 'TRISK':
		grid.mi_rhs_evals = np.zeros((settings.ntlevels,grid.grid.nface[-1]),dtype=np.float64)
		grid.ue_rhs_evals = np.zeros((settings.ntlevels,grid.grid.nedge[-1]),dtype=np.float64)
		grid.ue_tendency = np.zeros(grid.grid.nedge[-1],dtype=np.float64)
		grid.mi_tendency = np.zeros(grid.grid.nface[-1],dtype=np.float64)

		grid.grid.mc = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.grid.mv = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.grid.eta = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.grid.zeta = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.grid.total = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.grid.kinetic = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.grid.potential = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.grid.enstrophy = np.zeros(settings.nsteps+1,dtype=np.float64)
		
		
		grid.grid.bih = np.zeros(grid.grid.nface[-1],dtype=np.float64)
		
		grid.H = create_scipy_sparse_matrix(grid.grid.h_stencil[:grid.grid.nedge[settings.vlevel-1],:,settings.vlevel-1],grid.grid.h_stencil_size[:grid.grid.nedge[settings.vlevel-1],settings.vlevel-1],
grid.grid.h_coeffs[:grid.grid.nedge[settings.vlevel-1],:,settings.vlevel-1],grid.grid.nedge[settings.vlevel-1],grid.grid.nedge[settings.vlevel-1])

		grid.apvm_c = settings.apvm_c * settings.dt
		
	if settings.scheme == 'ZGRID':
		grid.hi_rhs_evals = np.zeros((settings.ntlevels,grid.grid.nface[-1]),dtype=np.float64)
		grid.zeta_rhs_evals = np.zeros((settings.ntlevels,grid.grid.nface[-1]),dtype=np.float64)
		grid.delta_rhs_evals = np.zeros((settings.ntlevels,grid.grid.nface[-1]),dtype=np.float64)
		grid.hi_tendency = np.zeros(grid.grid.nface[-1],dtype=np.float64)
		grid.zeta_tendency = np.zeros(grid.grid.nface[-1],dtype=np.float64)
		grid.delta_tendency = np.zeros(grid.grid.nface[-1],dtype=np.float64)

		grid.A = spsparselin.LinearOperator((2*grid.grid.nface[-1],2*grid.grid.nface[-1]),grid.grid.chi_psi_apply_operator_highest,dtype=np.float64) 
		
		grid.grid.inj_stencil_size = np.zeros((grid.grid.nface[-1],grid.grid.ngrids-1),dtype=np.int32)
		grid.grid.inj_weights = np.zeros((grid.grid.nface[-1],7,grid.grid.ngrids-1),dtype=np.float64)
		grid.grid.inj_stencil = np.zeros((grid.grid.nface[-1],7,grid.grid.ngrids-1),dtype=np.int32)
		grid.grid.buildinj()
		grid.grid.diagcoeff = np.zeros((2*grid.grid.nface[-1],grid.grid.ngrids),dtype=np.float64)
		grid.npass = 3
		grid.niter = 2
		grid.niterc = 10
		
		grid.chi = np.zeros(grid.grid.nface[-1],dtype=np.float64)
		grid.psi = np.zeros(grid.grid.nface[-1],dtype=np.float64)
		grid.chipsi = np.zeros(2*grid.grid.nface[-1],dtype=np.float64)
		
		grid.grid.mc = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.grid.eta = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.grid.zeta = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.grid.total = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.grid.kinetic = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.grid.potential = np.zeros(settings.nsteps+1,dtype=np.float64)
		grid.grid.enstrophy = np.zeros(settings.nsteps+1,dtype=np.float64)
		
		grid.grid.bih = np.zeros(grid.grid.nface[-1],dtype=np.float64)
		grid.grid.heinv = np.zeros((grid.grid.nedge[-1],grid.grid.ngrids),dtype=np.float64)
		grid.grid.hvinv = np.zeros((grid.grid.nvert[-1],grid.grid.ngrids),dtype=np.float64)
		grid.grid.heinv2 = np.zeros((grid.grid.nedge[-1],grid.grid.ngrids),dtype=np.float64)
		grid.grid.hvinv2 = np.zeros((grid.grid.nvert[-1],grid.grid.ngrids),dtype=np.float64)

def initialize_scheme_vars():
	if settings.scheme == 'TRISK':
		grid.mi,grid.ue,grid.fv,grid.bi = initialize_vars()
		if settings.initial_condition == 'heldsuarezdry':
			grid.mi_eqm = np.copy(grid.mi)
			grid.ue_eqm = np.copy(grid.ue)
			grid.tau_h = 100. * 86400.
			grid.tau_u = 100. * 86400.
			grid.mi = grid.grid.iinv(np.ones(grid.mi_eqm.shape,dtype=np.float64) * 10.**3,settings.vlevel)  #should be mi_eqm!
			grid.ue = np.zeros(grid.ue_eqm.shape,dtype=np.float64)

	if settings.scheme == 'ZGRID':
		grid.hi,grid.zeta,grid.delta,grid.fi,grid.bi = initialize_vars()
		
		#grid.grid.diagnose_h_quantities(grid.hi,settings.vlevel)
		#rhs = np.concatenate((grid.delta,grid.zeta))
		#grid.grid.build_multigrid_operator(grid.hi)
		#grid.grid.set_diagonal_coefficient()
		#grid.grid.fcycle(grid.chipsi,rhs,grid.grid.ngrids,5*grid.npass,grid.niter,grid.niterc)
			
		if settings.initial_condition == 'heldsuarezdry':
			grid.hi_eqm = np.copy(grid.hi)
			grid.zeta_eqm = np.copy(grid.zeta)
			grid.delta_eqm = np.copy(grid.delta)
			grid.tau_h = 100. * 86400.
			grid.tau_u = 100. * 86400.
			grid.hi = np.ones(grid.hi_eqm.shape,dtype=np.float64) * 10.**3 #should be hi_eqm!
			grid.zeta = np.zeros(grid.zeta_eqm.shape,dtype=np.float64)
			grid.delta = np.zeros(grid.delta_eqm.shape,dtype=np.float64)
def diagnose():
	if settings.scheme == 'TRISK':
		grid.qv = grid.grid.qv_diagnose(grid.mi,grid.ue,grid.fv,settings.vlevel)
		grid.Hce = grid.grid.hce_diagnose(grid.mi,grid.ue,settings.vlevel)
		grid.Iphii = grid.grid.iphii_diagnose(grid.mi,grid.ue,grid.bi,settings.vlevel,settings.g)
		
	if settings.scheme == 'ZGRID':
		grid.grid.diagnose_h_quantities(grid.hi,settings.vlevel)
		grid.qi = grid.grid.diagnose_qi(grid.zeta,grid.fi,grid.hi,settings.vlevel)
		rhs = np.concatenate((grid.delta,grid.zeta))

		grid.grid.build_multigrid_operator(grid.hi)
		grid.grid.set_diagonal_coefficient()
		grid.chipsi = grid.grid.fcycle(rhs,grid.grid.ngrids,grid.npass,grid.niter,grid.niterc)
					
		#grid.chipsi,info = spsparselin.lgmres(grid.A,rhs,x0=grid.chipsi,tol=1e-16)
		#if (info != 0):
		#	print('solver not converging',info)
			
		grid.chi,grid.psi = np.split(grid.chipsi,2)
		grid.phi = grid.grid.diagnose_phi(grid.hi,grid.bi,grid.chi,grid.psi,settings.g,settings.vlevel)
		
def eulerstep(i):
	if settings.scheme == 'TRISK':
		rhs(i-1)
		grid.ue = grid.ue + settings.dt * grid.ue_rhs_evals[i-1,:]
		grid.mi = grid.mi + settings.dt * grid.mi_rhs_evals[i-1,:]	
			
	if settings.scheme == 'ZGRID':
		rhs(i-1)
		grid.hi = grid.hi + settings.dt * grid.hi_rhs_evals[i-1,:]
		grid.zeta = grid.zeta + settings.dt * grid.zeta_rhs_evals[i-1,:]
		grid.delta = grid.delta + settings.dt * grid.delta_rhs_evals[i-1,:]

def ab3step(i):
	if settings.scheme == 'TRISK':
		rhs(2)
		if settings.use_dissipation == True:
			if settings.dissipation_type == 'hyperviscosity':
				grid.ue_tendency = settings.dt * ( settings.nu_gd * grid.grid.graddiv(grid.ue,settings.vlevel,grid.grid.nface[-1]) + settings.nu_cc * grid.grid.curlcurl(grid.ue,settings.vlevel,grid.grid.nvert[-1]))
		else:
			grid.ue_tendency[:] = 0.0

		for j in range(settings.ntlevels):
			grid.ue_tendency = grid.ue_tendency + ab3_time_weights[j] * settings.dt * grid.ue_rhs_evals[j,:]
			grid.mi = grid.mi + ab3_time_weights[j] * settings.dt * grid.mi_rhs_evals[j,:]
		grid.ue = grid.ue + grid.ue_tendency
		grid.mi_rhs_evals[0,:] = grid.mi_rhs_evals[1,:]
		grid.mi_rhs_evals[1,:] = grid.mi_rhs_evals[2,:]
		grid.ue_rhs_evals[0,:] = grid.ue_rhs_evals[1,:]
		grid.ue_rhs_evals[1,:] = grid.ue_rhs_evals[2,:]	
			
	if settings.scheme == 'ZGRID':
		rhs(2)
		if settings.use_dissipation == True:
			if settings.dissipation_type == 'hyperviscosity':
				grid.delta_tendency = settings.dt * settings.nu_gd * grid.grid.laplac_primal(grid.delta,settings.vlevel)
				grid.zeta_tendency = settings.dt * settings.nu_cc * grid.grid.laplac_primal(grid.zeta,settings.vlevel)
		else:
			grid.zeta_tendency[:] = 0.0
			grid.delta_tendency[:] = 0.0

		for j in range(settings.ntlevels):
			grid.hi = grid.hi + ab3_time_weights[j] * settings.dt * grid.hi_rhs_evals[j,:]
			grid.zeta_tendency = grid.zeta_tendency + ab3_time_weights[j] * settings.dt * grid.zeta_rhs_evals[j,:]
			grid.delta_tendency = grid.delta_tendency + ab3_time_weights[j] * settings.dt * grid.delta_rhs_evals[j,:]
		grid.zeta = grid.zeta + grid.zeta_tendency
		grid.delta = grid.delta + grid.delta_tendency

		grid.hi_rhs_evals[0,:] = grid.hi_rhs_evals[1,:]
		grid.hi_rhs_evals[1,:] = grid.hi_rhs_evals[2,:]
		grid.zeta_rhs_evals[0,:] = grid.zeta_rhs_evals[1,:]
		grid.zeta_rhs_evals[1,:] = grid.zeta_rhs_evals[2,:]	
		grid.delta_rhs_evals[0,:] = grid.delta_rhs_evals[1,:]
		grid.delta_rhs_evals[1,:] = grid.delta_rhs_evals[2,:]
				
def rhs(i):
	if settings.scheme == 'TRISK':
		grid.mi_rhs_evals[i,:] = grid.grid.mi_rhs(grid.Hce,settings.vlevel,grid.grid.nface[-1])
		
		if settings.trisk_variant == 'TEQ':
			grid.ue_rhs_evals[i,:] = grid.grid.ue_rhs_q(grid.Iphii,grid.Hce,grid.qv,settings.vlevel)
		elif settings.trisk_variant == 'TE':
			grid.ue_rhs_evals[i,:] = grid.grid.ue_rhs_qte(grid.Iphii,grid.Hce,grid.qv,settings.vlevel)
		elif settings.trisk_variant == 'Q':
			grid.ue_rhs_evals[i,:] = grid.grid.ue_rhs_qq(grid.Iphii,grid.Hce,grid.qv,settings.vlevel)		
		#if settings.use_dissipation == True:
		#	if settings.dissipation_type == 'hyperviscosity':
		#		if settings.grid_type == 'cubedsphere':
		#			#grid.ue_rhs_evals[i,:] = grid.ue_rhs_evals[i,:] + settings.nu * grid.grid.hypervisocity(grid.ue,settings.vlevel,grid.grid.nface[-1],grid.grid.nvert[-1])
		#			grid.ue_rhs_evals[i,:] = grid.ue_rhs_evals[i,:] + settings.nu_gd * grid.grid.graddiv(grid.ue,settings.vlevel,grid.grid.nface[-1]) 
		#			+ settings.nu_cc * grid.grid.curlcurl(grid.ue,settings.vlevel,grid.grid.nvert[-1])
		#			#+ settings.nu_cc * apply_Hinv(grid.grid.curlcurl_part(grid.ue,settings.vlevel,grid.grid.nvert[-1]),grid.H)
		#		elif settings.grid_type == 'geodesic':
		#			gd = grid.grid.graddiv(grid.ue,settings.vlevel,grid.grid.nface[-1])
		#			cc = grid.grid.curlcurl(grid.ue,settings.vlevel,grid.grid.nvert[-1])
		#			grid.ue_rhs_evals[i,:] = grid.ue_rhs_evals[i,:] + settings.nu_gd * gd + settings.nu_cc * cc
		#			#ldelta = grid.grid.i(grid.grid.d2(grid.grid.h(gd,settings.vlevel),settings.vlevel,grid.grid.nface[-1]),settings.vlevel)
		#			#lzeta = grid.grid.j(grid.grid.d2bar(cc,settings.vlevel,grid.grid.nvert[-1]),settings.vlevel)
		#			#print(i,'ldelta',np.amin(ldelta),np.amax(ldelta),np.average(ldelta))
		#			#print(i,'lzeta',np.amin(lzeta),np.amax(lzeta),np.average(lzeta)) #,np.amin(grid.ue_rhs_evals[i,:]),np.amax(grid.ue_rhs_evals[i,:]),np.average(grid.ue_rhs_evals[i,:])
		#	elif settings.dissipation_type == 'APVM':
		#		#apvm = grid.grid.apvm(grid.ue,grid.qv,grid.apvm_c,settings.vlevel)
		#		#print np.amin(apvm),np.amax(apvm),np.average(apvm),np.amin(grid.ue_rhs_evals[i,:]),np.amax(grid.ue_rhs_evals[i,:]),np.average(grid.ue_rhs_evals[i,:])
		#		grid.ue_rhs_evals[i,:] = grid.ue_rhs_evals[i,:] + grid.grid.apvm(grid.ue,grid.qv,grid.apvm_c,settings.vlevel)
		if settings.initial_condition == 'heldsuarezdry':
			grid.mi_rhs_evals[i,:] = grid.mi_rhs_evals[i,:] + grid.grid.forcing(grid.mi,grid.mi_eqm,grid.tau_h)
			grid.ue_rhs_evals[i,:] = grid.ue_rhs_evals[i,:] + grid.grid.forcing(grid.ue,grid.ue_eqm,grid.tau_u)
			
	if settings.scheme == 'ZGRID':
		grid.hi_rhs_evals[i,:] = grid.grid.hi_rhs(grid.chi,settings.vlevel)
		grid.zeta_rhs_evals[i,:] = grid.grid.zeta_rhs(grid.qi,grid.chi,grid.psi,settings.vlevel)
		grid.delta_rhs_evals[i,:] = grid.grid.delta_rhs(grid.qi,grid.chi,grid.psi,grid.phi,settings.vlevel)
		#if settings.use_dissipation == True:
		#	if settings.dissipation_type == 'hyperviscosity':
		#		ldelta = grid.grid.laplac_primal(grid.delta,settings.vlevel)
		#		lzeta = grid.grid.laplac_primal(grid.zeta,settings.vlevel)
		#		grid.zeta_rhs_evals[i,:]  = grid.zeta_rhs_evals[i,:]  + settings.nu_cc * lzeta
		#		grid.delta_rhs_evals[i,:]  = grid.delta_rhs_evals[i,:]  + settings.nu_gd * ldelta
				#print(i,'ldelta',np.amin(ldelta),np.amax(ldelta),np.average(ldelta))
				#print(i,'lzeta',np.amin(lzeta),np.amax(lzeta),np.average(lzeta)) #,np.amin(grid.ue_rhs_evals[i,:]),np.amax(grid.ue_rhs_evals[i,:]),np.average(grid.ue_rhs_evals[i,:])
		#print 'hi',np.amax(grid.hi_rhs_evals[i,:])*settings.dt,np.amin(grid.hi_rhs_evals[i,:])*settings.dt,np.average(grid.hi_rhs_evals[i,:])*settings.dt
		#print 'zeta',np.amax(grid.zeta_rhs_evals[i,:])*settings.dt,np.amin(grid.zeta_rhs_evals[i,:])*settings.dt,np.average(grid.zeta_rhs_evals[i,:])*settings.dt
		#print 'rhs',np.amax(grid.delta_rhs_evals[i,:])*settings.dt,np.amin(grid.delta_rhs_evals[i,:])*settings.dt,np.average(grid.delta_rhs_evals[i,:])*settings.dt
		#ADD APVM!
		if settings.initial_condition == 'heldsuarezdry':
			grid.zeta_rhs_evals[i,:]  = grid.zeta_rhs_evals[i,:] + grid.grid.forcing(grid.zeta,grid.zeta_eqm,grid.tau_u)
			grid.delta_rhs_evals[i,:]  = grid.delta_rhs_evals[i,:] + grid.grid.forcing(grid.delta,grid.delta_eqm,grid.tau_u)
			grid.hi_rhs_evals[i,:]  = grid.hi_rhs_evals[i,:] + grid.grid.forcing(grid.hi,grid.hi_eqm,grid.tau_h)

def pre_compute_statistics():
	if settings.scheme == 'TRISK':
		grid.grid.precompute_trisk_statistics(grid.mi,grid.bi,settings.a,settings.g)

	if settings.scheme == 'ZGRID':
		grid.grid.precompute_zgrid_statistics(grid.hi,grid.bi,settings.a,settings.g)
		
def compute_statistics(n):
	if settings.scheme == 'TRISK':
		grid.grid.compute_trisk_statistics(n+1,grid.mi,grid.bi,grid.ue,grid.fv,settings.g) #+1 is due to 0-based python vs. 1-based fortran indexing
		#gd = grid.grid.graddiv(grid.ue,settings.vlevel,grid.grid.nface[-1])
		#cc = grid.grid.curlcurl(grid.ue,settings.vlevel,grid.grid.nvert[-1])
		#ldelta = grid.grid.i(grid.grid.d2(grid.grid.h(gd,settings.vlevel),settings.vlevel,grid.grid.nface[-1]),settings.vlevel)
		#lzeta = grid.grid.j(grid.grid.d2bar(cc,settings.vlevel,grid.grid.nvert[-1]),settings.vlevel)
		#grid.grid.ldelta_max[n] = np.amax(ldelta)
		#grid.grid.ldelta_min[n] = np.amin(ldelta)
		#grid.grid.ldelta_average[n] = np.average(ldelta)
		#grid.grid.lzeta_max[n] = np.amax(lzeta)
		#grid.grid.lzeta_min[n] = np.amin(lzeta)
		#grid.grid.lzeta_average[n] = np.average(lzeta)

	if settings.scheme == 'ZGRID':
		grid.grid.compute_zgrid_statistics(n+1,grid.hi,grid.bi,grid.zeta,grid.fi,grid.chi,grid.psi,settings.g,grid.grid.nedge[-1]) #+1 is due to 0-based python vs. 1-based fortran indexin
		#ldelta = grid.grid.laplac_primal(grid.delta,settings.vlevel)
		#lzeta = grid.grid.laplac_primal(grid.zeta,settings.vlevel)
		#grid.grid.ldelta_max[n] = np.amax(ldelta)
		#grid.grid.ldelta_min[n] = np.amin(ldelta)
		#grid.grid.ldelta_average[n] = np.average(ldelta)
		#grid.grid.lzeta_max[n] = np.amax(lzeta)
		#grid.grid.lzeta_min[n] = np.amin(lzeta)
		#grid.grid.lzeta_average[n] = np.average(lzeta)
