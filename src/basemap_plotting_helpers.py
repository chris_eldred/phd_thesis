from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np
import h5py
from matplotlib.collections import PolyCollection
import scipy.sparse as spsparse
import matplotlib.cm as cm

class container:
	def __init__(self):
		pass
		
def get_geometry(gridtype,gridvariant,vlevel):
	grid = container()
	if gridtype == 'geodesic':
		ncells = 10*(2**vlevel)**2 + 2
		grid.name = 'G' + str(vlevel) + ' - ' + gridvariant
		grid.fname = 'geo.' + str(gridvariant) + '.v=' + str(vlevel)
	if gridtype == 'cubedsphere':
		n0 = 3
		ncells = 6*(n0*(2**(vlevel-1)))**2
		grid.name = 'C' + str(vlevel) + ' - ' + '6x' + str(n0*(2**(vlevel-1))) + 'x' + str(n0*(2**(vlevel-1))) + ' - ' + gridvariant
		grid.fname = 'cs.' + str(gridvariant) + '.v=' + str(vlevel)
	gridfile = h5py.File('meshes/' + gridtype + '/' + gridvariant + '/' + str(ncells) + '.hdf5','r')
	grid.clats = (180./np.pi)*np.array(gridfile['coords']['clat'])
	grid.clons = (180./np.pi)*np.array(gridfile['coords']['clon'])
	grid.vlats = (180./np.pi)*np.array(gridfile['coords']['vlat'])
	grid.vlons = (180./np.pi)*np.array(gridfile['coords']['vlon'])
	grid.elats = (180./np.pi)*np.array(gridfile['coords']['elat'])
	grid.elons = (180./np.pi)*np.array(gridfile['coords']['elon'])
	grid.CV = np.array(gridfile['connectivity']['CV']) - 1
	grid.VC = np.array(gridfile['connectivity']['VC']) - 1
	grid.VE = np.array(gridfile['connectivity']['VE']) - 1
	grid.CE = np.array(gridfile['connectivity']['CE']) - 1
	
	#adjust CV,VC so that empty spots are duplicated instead of 0 (fixes some plotting stuff)
	if gridtype == 'geodesic':
		nedgef = np.array(gridfile['connectivity']['nedgef'])
		grid.VC[np.equal(nedgef,5),-1] = grid.VC[np.equal(nedgef,5),-2]
	if gridtype == 'cubedsphere':
		nedgev = np.array(gridfile['connectivity']['nedgev'])
		grid.CV[np.equal(nedgev,3),-1] = grid.CV[np.equal(nedgev,3),-2]
	
	grid.vlevel = vlevel
	grid.ncells = ncells
	grid.nverts = int(gridfile['sizes']['nvert'][()])
	grid.nedges = int(gridfile['sizes']['nedge'][()])
	grid.Ai = np.array(gridfile['geometry']['Ai'])
	grid.Av = np.array(gridfile['geometry']['Av'])
	grid.le = np.array(gridfile['geometry']['le'])
	grid.de = np.array(gridfile['geometry']['de'])
	gridfile.close()
	return grid
	
def plot_primal_mesh(grid):
	lons = grid.vlons
	lats = grid.vlats
	VC = grid.VC
	plot_mesh_grid(lons,lats,VC,grid.name + ' Primal','meshplots/' + grid.fname + '.primal')

def plot_dual_mesh(grid):
	lons = grid.clons
	lats = grid.clats
	VC = grid.CV
	plot_mesh_grid(lons,lats,VC,grid.name + ' Dual','meshplots/' + grid.fname + '.dual')
	
def plot_edge_mesh(grid):
	vlons = grid.vlons
	vlats = grid.vlats
	clons = grid.clons
	clats = grid.clats
	CE = grid.CE
	VE = grid.VE
	CEVE = np.squeeze(np.dstack((VE[:,0],CE[:,0],VE[:,1],CE[:,1])))
	VE_offsets_lon = vlons[VE]
	VE_offsets_lat = vlats[VE]
	CE_offsets_lon = clons[CE]
	CE_offsets_lat = clats[CE]
	CEVE_lon  = np.squeeze(np.dstack((VE_offsets_lon[:,0],CE_offsets_lon[:,0],VE_offsets_lon[:,1],CE_offsets_lon[:,1])))
	CEVE_lat = np.squeeze(np.dstack((VE_offsets_lat[:,0],CE_offsets_lat[:,0],VE_offsets_lat[:,1],CE_offsets_lat[:,1])))
	CEVEpols = np.dstack((CEVE_lon,CEVE_lat))
	
	plt.figure()
	map = Basemap(projection='ortho',lon_0=-105,lat_0=40)
	x,y = map(CEVE_lon, CEVE_lat)
	map.drawcoastlines()
	map.drawmapboundary()
	pols = np.dstack((x,y))
	pols_in_domain = np.equal(np.sum(np.sum(np.equal(pols,np.amax(x)),axis=2),axis=1),0)
	pols = pols[pols_in_domain,:,:]
	coll = PolyCollection(pols,facecolor='None',edgecolor='k',zorder=2,closed=True)
	plt.gca().add_collection(coll)
	plt.title(grid.name + ' Edge')
	plt.savefig('meshplots/' + grid.fname + '.edge.png',bbox_inches='tight')
	plt.close()

def plot_mesh_grid(lons,lats,VC,name,fname):
	plt.figure()
	map = Basemap(projection='ortho',lon_0=-105,lat_0=40)
	x,y = map(lons, lats)
	map.drawcoastlines()
	map.drawmapboundary()
	pols = np.dstack((x[VC],y[VC]))
	pols_in_domain = np.equal(np.sum(np.sum(np.equal(pols,np.amax(x)),axis=2),axis=1),0)
	pols = pols[pols_in_domain,:,:]
	coll = PolyCollection(pols,facecolor='None',edgecolor='k',zorder=2,closed=True)
	plt.gca().add_collection(coll)
	plt.title(name)
	plt.savefig(fname + '.png',bbox_inches='tight')
	plt.close()

def plot_scalar(grid,data,loc,plotname,filename,plottype,filltype,levels):
	#print plottype,filltype
	if filltype == 'grid':
		if loc == 'primal':
			lons = grid.vlons
			lats = grid.vlats
			VC = grid.VC
		if loc == 'dual':
			lons = grid.clons
			lats = grid.clats
			VC = grid.CV
		if loc == 'edge':
			plot_edge_scalar_grid(grid,plotname,filename + '.grid',data)
		if plottype == 'spherical' and (not loc == 'edge'):
			plot_scalar_grid(lons,lats,VC,plotname,filename + '.grid',data)
			
	if filltype == 'contour':
		if loc == 'primal':
			lons = grid.clons
			lats = grid.clats
		if loc == 'dual':
			lons = grid.vlons
			lats = grid.vlats
		if loc == 'edge':
			lons = grid.elons
			lats = grid.elats
		if plottype == 'spherical':
			if levels == None:
				plot_scalar_spherical_contour_nolevels(lons,lats,plotname,filename + '.spherical.contour',data)
			else:
				plot_scalar_spherical_contour(lons,lats,plotname,filename + '.spherical.contour',data,levels)
		if plottype == 'latlon':
			if levels == None:
				plot_scalar_latlon_contour_nolevels(lons,lats,plotname,filename + '.latlon.contour',data)
			else:
				plot_scalar_latlon_contour(lons,lats,plotname,filename + '.latlon.contour',data,levels)

def plot_edge_scalar_grid(grid,name,fname,data):
	vlons = grid.vlons
	vlats = grid.vlats
	clons = grid.clons
	clats = grid.clats
	CE = grid.CE
	VE = grid.VE
	CEVE = np.squeeze(np.dstack((VE[:,0],CE[:,0],VE[:,1],CE[:,1])))
	VE_offsets_lon = vlons[VE]
	VE_offsets_lat = vlats[VE]
	CE_offsets_lon = clons[CE]
	CE_offsets_lat = clats[CE]
	CEVE_lon  = np.squeeze(np.dstack((VE_offsets_lon[:,0],CE_offsets_lon[:,0],VE_offsets_lon[:,1],CE_offsets_lon[:,1])))
	CEVE_lat = np.squeeze(np.dstack((VE_offsets_lat[:,0],CE_offsets_lat[:,0],VE_offsets_lat[:,1],CE_offsets_lat[:,1])))
	CEVEpols = np.dstack((CEVE_lon,CEVE_lat))
	
	plt.figure()
	map = Basemap(projection='ortho',lon_0=-105,lat_0=40,resolution='l')
	x,y = map(CEVE_lon, CEVE_lat)
	pols = np.dstack((x,y))
	pols_in_domain = np.equal(np.sum(np.sum(np.equal(pols,np.amax(x)),axis=2),axis=1),0)
	pols = pols[pols_in_domain,:,:]
	coll = PolyCollection(pols,edgecolor='k',linewidth=0.1,zorder=2,closed=True)
	data = data[pols_in_domain]
	coll.set_array(data)
	coll.set_cmap(None)
	coll.set_norm(None)
	coll.set_alpha(None)

	plt.gca().add_collection(coll)
	plt.colorbar(coll)
	map.drawcoastlines(linewidth=0.3)
	map.drawparallels(np.arange(-90.,91.,15.),linewidth=0.3) 
	map.drawmeridians(np.arange(-180.,181.,30.),linewidth=0.3) 
	map.drawmapboundary()
	plt.title(name)
	plt.savefig(fname + '.png',bbox_inches='tight')
	plt.close()
	
def plot_scalar_grid(lons,lats,VC,name,fname,data):
	plt.clf()
	plt.figure()
	map = Basemap(projection='ortho',lon_0=-105,lat_0=40,resolution='l')
	x,y = map(lons, lats)
	pols = np.dstack((x[VC],y[VC]))
	pols_in_domain = np.equal(np.sum(np.sum(np.equal(pols,np.amax(x)),axis=2),axis=1),0)
	pols = pols[pols_in_domain,:,:]
	coll = PolyCollection(pols,edgecolor='k',linewidth=0.1,zorder=2,closed=True) #facecolor='None', transOffset = plt.gca().transData
	data = data[pols_in_domain]
	coll.set_array(data)
	coll.set_cmap(None)
	coll.set_norm(None)
	coll.set_alpha(None)

	plt.gca().add_collection(coll)
	plt.colorbar(coll)
	map.drawcoastlines(linewidth=0.3)
	map.drawparallels(np.arange(-90.,91.,15.),linewidth=0.3) 
	map.drawmeridians(np.arange(-180.,181.,30.),linewidth=0.3) 
	map.drawmapboundary()

	plt.title(name)
	plt.savefig(fname + '.png',bbox_inches='tight')
	plt.close()


def plot_scalar_spherical_contour(lons,lats,name,fname,data,levels):
	plt.figure()
	map = Basemap(projection='ortho',lon_0=-105,lat_0=40,resolution='l')
	x,y = map(lons, lats)
	map.contour(x,y,data,levels,tri=True)
	map.contourf(x,y,data,levels,tri=True)
	map.drawcoastlines(linewidth=0.3)
	map.drawparallels(np.arange(-90.,91.,15.),linewidth=0.3) 
	map.drawmeridians(np.arange(-180.,181.,30.),linewidth=0.3)
	map.drawmapboundary()
	map.colorbar()
	plt.title(name)
	plt.savefig(fname + '.png')
	plt.close()

def plot_scalar_spherical_contour_nolevels(lons,lats,name,fname,data):
	plt.figure()
	map = Basemap(projection='ortho',lon_0=-105,lat_0=40,resolution='l')
	x,y = map(lons, lats)
	map.contour(x,y,data,tri=True)
	map.contourf(x,y,data,tri=True)
	map.drawcoastlines(linewidth=0.3)
	map.drawparallels(np.arange(-90.,91.,15.),linewidth=0.3) 
	map.drawmeridians(np.arange(-180.,181.,30.),linewidth=0.3)
	map.drawmapboundary()
	map.colorbar()
	plt.title(name)
	plt.savefig(fname + '.png',bbox_inches='tight')
	plt.close()

def plot_scalar_latlon_contour_nolevels(lons,lats,name,fname,data):
	#plt.figure(figsize=(8,2),dpi=100)
	plt.figure()
	map = Basemap(projection='cyl',llcrnrlat=-90,urcrnrlat=90,\
            llcrnrlon=-180,urcrnrlon=180,resolution='l')	
	x,y = map(lons, lats)
	map.contour(x,y,data,tri=True)
	map.contourf(x,y,data,tri=True)
	#map.contour(x,y,data,tri=True)
	map.drawcoastlines(linewidth=0.3)
	map.drawparallels(np.arange(-90.,91.,30.),linewidth=0.2,labels=[1,0,0,1]) 
	map.drawmeridians(np.arange(-180.,181.,45.),linewidth=0.2,labels=[1,0,0,1])
	map.drawmapboundary()
	map.colorbar()
	plt.title(name)
	plt.tight_layout()
	plt.subplots_adjust(left=0.1, right=0.85, top=0.9, bottom=0.1)
	plt.savefig(fname + '.png',bbox_inches='tight')
	plt.close()

def plot_scalar_latlon_contour(lons,lats,name,fname,data,levels):
	#plt.figure(figsize=(8,2),dpi=100)
	plt.figure()
	map = Basemap(projection='cyl',llcrnrlat=-90,urcrnrlat=90,\
            llcrnrlon=-180,urcrnrlon=180,resolution='l')	
	x,y = map(lons, lats)
	#map.contour(x,y,data,tri=True)
	map.contourf(x,y,data,levels,tri=True)
	map.colorbar()
	map.contour(x,y,data,levels,tri=True)
	map.drawcoastlines(linewidth=0.3)
	map.drawparallels(np.arange(-90.,91.,30.),linewidth=0.2,labels=[1,0,0,1]) 
	map.drawmeridians(np.arange(-180.,181.,45.),linewidth=0.2,labels=[1,0,0,1])
	map.drawmapboundary()
	plt.title(name)
	plt.tight_layout()
	plt.subplots_adjust(left=0.1, right=0.85, top=0.9, bottom=0.1)
	plt.savefig(fname + '.png',bbox_inches='tight')
	plt.close()

def load_operator_error(operator,gridtype,gridvariant):
	ofile = h5py.File('orderofaccuracy/errors.h5py','r')
	return np.array(ofile[gridtype + '/' + gridvariant + '/' + operator])
	
def load_scheme_error(test,variant,var,gridtype,gridvariant):
	ofile = h5py.File('orderofaccuracy/errors.h5py','r')
	return np.array(ofile[gridtype + '/' + gridvariant + '/' + variant + '/' + test + '/' + var])
	
def load_sparse_csr(filename):
    loader = np.load(filename)
    return spsparse.csr_matrix((  loader['data'], loader['indices'], loader['indptr']),
                         shape = loader['shape'])

def load_matrix(name,vlevel,gridtype,gridvariant):
	if gridtype == 'geodesic':
		ncells = 10*(2**vlevel)**2 + 2
	if gridtype == 'cubedsphere':
		n0 = 3
		ncells = 6*(n0*(2**(vlevel-1)))**2
	output = open('../meshes/' + gridtype + '/' + gridvariant + '/' + name + '.' + str(ncells) + '.npz', 'r')
	mat = load_sparse_csr(output)
	output.close()
	return mat

def load_constant(name,vlevel,variant,simname,gridtype,gridvariant):
	vfile = h5py.File(simname + '/' + gridtype + '/' + gridvariant + '/' + str(vlevel) + '-' + variant  + '/vars.hdf5', 'r')
	vartype_group = vfile['constants']
	data = np.array((vartype_group[name])[:,0])
	vfile.close()
	return data
		
def load_variable(name,vartype,step,vlevel,variant,simname,gridtype,gridvariant):
	vfile = h5py.File(simname + '/' + gridtype + '/' + gridvariant + '/' + str(vlevel) + '-' + variant  + '/vars.hdf5', 'r')
	vartype_group = vfile[vartype]
	data = np.array((vartype_group[name])[step,:])
	vfile.close()
	return data
