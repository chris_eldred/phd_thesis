from constants import *
from mesh import *
from configuration import *
import sys
import time
import grid
import matplotlib.pyplot as plt
from scheme import *
from output import *
from initialize import *
from sympy import *
from sympy.utilities.lambdify import implemented_function

def grad(A):
	return 1/(a*cos(theta)) * diff(A,lambda1), 1/a * diff(A,theta) 

def div(A,B):
	return 1/(a*cos(theta)) * (diff(A,lambda1) + diff(B * cos(theta),theta))
	
def jacob(A,B):
	return 1/(a*a*cos(theta)) * (diff(A,lambda1)*diff(B,theta) - diff(A,theta)*diff(B,lambda1))

def laplac(A):
	#t1 = 1/(a*a*cos(theta)*cos(theta))
	#t2 = 1/(a*a*cos(theta)) * diff(cos(theta) * diff(A,theta),theta)
	gradA = grad(A)
	return div(gradA[0],gradA[1])
	
#get configuration
get_configuration(sys.argv[1])
print 'configured'

settings.vlevel = 6

#load operators and connectivity
set_grid_sizes_geodesic(grid.grid,settings.vlevel)
allocate_connectivity_geodesic(grid.grid)
allocate_operators_geodesic(grid.grid)
allocate_geometry_geodesic(grid.grid)
	
load_geometry(grid.grid)
load_connectivity(grid.grid)
load_operators(grid.grid)
grid.grid.dimensionalize_operators(settings.a)
grid.grid.dimensionalize_geometry(settings.a)

print 'operators and connectivity loaded!'

#create vars
create_scheme_vars()
print 'vars created'

def l2_error(diff):
	return np.sqrt(np.average(np.square(diff)))
	
def linf_error(diff):
	return np.amax(np.abs(diff))

ngrids = settings.vlevel

nvert = grid.grid.nvert[ngrids-1]
nface = grid.grid.nface[ngrids-1]
nedge = grid.grid.nedge[ngrids-1]

I = create_scipy_sparse_matrix(grid.grid.i_stencil[:nface,:,ngrids-1],grid.grid.i_stencil_size[:nface,ngrids-1],grid.grid.i_coeffs[:nface,:,ngrids-1],nface,nface)
H = create_scipy_sparse_matrix(grid.grid.h_stencil[:nedge,:,ngrids-1],grid.grid.h_stencil_size[:nedge,ngrids-1],grid.grid.h_coeffs[:nedge,:,ngrids-1],nedge,nedge)
D1 =  create_scipy_sparse_matrix(grid.grid.ve[:nedge,:,ngrids-1],np.ones(nedge,dtype=np.int32)*2,grid.grid.tev_edge[:nedge,:,ngrids-1],nedge,nvert)
D2 = create_scipy_sparse_matrix(grid.grid.ec[:nface,:,ngrids-1],grid.grid.ecsize[:nface,ngrids-1],grid.grid.nei_cell[:nface,:,ngrids-1],nface,nedge)
D1bar =  create_scipy_sparse_matrix(grid.grid.ce[:nedge,:,ngrids-1],np.ones(nedge,dtype=np.int32)*2,-1*grid.grid.nei_edge[:nedge,:,ngrids-1],nedge,nface)
D2bar = create_scipy_sparse_matrix(grid.grid.ev[:nvert,:,ngrids-1],grid.grid.evsize[:nvert,ngrids-1],grid.grid.tev_vertex[:nvert,:,ngrids-1],nvert,nedge)
R = create_scipy_sparse_matrix(grid.grid.cv[:nvert,:,ngrids-1],grid.grid.evsize[:nvert,ngrids-1],np.ones((nvert,3),dtype=np.float64)*1./3.,nvert,nface)
NJ1 = create_scipy_sparse_matrix(grid.grid.ce[:nedge,0,ngrids-1],np.ones(nedge,dtype=np.int32),grid.grid.nei_edge[:nedge,0,ngrids-1],nedge,nface)
NJ2 = create_scipy_sparse_matrix(grid.grid.ce[:nedge,1,ngrids-1],np.ones(nedge,dtype=np.int32),grid.grid.nei_edge[:nedge,1,ngrids-1],nedge,nface)
J1 = create_scipy_sparse_matrix(grid.grid.ce[:nedge,0,ngrids-1],np.ones(nedge,dtype=np.int32),np.ones(nedge),nedge,nface)
J2 = create_scipy_sparse_matrix(grid.grid.ce[:nedge,1,ngrids-1],np.ones(nedge,dtype=np.int32),np.ones(nedge),nedge,nface)
Phi = create_scipy_sparse_matrix(grid.grid.ce[:nedge,:,ngrids-1],np.ones(nedge,dtype=np.int32)*2,np.ones((nedge,2),dtype=np.float64)*0.5,nedge,nface)
Iinv = create_scipy_sparse_matrix(grid.grid.i_stencil[:nface,:,ngrids-1],grid.grid.i_stencil_size[:nface,ngrids-1],1.0/grid.grid.i_coeffs[:nface,:,ngrids-1],nface,nface)
K = create_scipy_sparse_matrix(grid.grid.ec[:nface,:,ngrids-1],grid.grid.ecsize[:nface,ngrids-1],np.ones((nface,6)),nface,nedge)
L = I * D2 * H * D1bar
ID2 = I * D2
HD1bar = H * D1bar
D1R = D1 * R
IK = I * K
	
theta = symbols('theta') #lat
lambda1 = symbols('lambda1') #lon
a = symbols('a')

chi = cos(theta) * sin(5*lambda1)
psi = cos(3*theta) * sin(8*lambda1) * cos(3*lambda1)
grad_chi = grad(chi)
grad_psi = grad(psi)
h0 = 8000.0
hi = h0 + 200*cos(theta)
hinv = 1/hi
#delta_exact = div(hinv*grad_chi[0],hinv*grad_chi[1]) - jacob(hinv,psi)
#zeta_exact = div(hinv*grad_psi[0],hinv*grad_psi[1]) + jacob(hinv,chi)
#delta_func = lambdify((theta,lambda1,a),delta_exact,modules='numpy')
#zeta_func = lambdify((theta,lambda1,a),zeta_exact,modules='numpy')
chi_func = lambdify((theta,lambda1,a),chi,modules='numpy')
psi_func = lambdify((theta,lambda1,a),psi,modules='numpy')
hi_func = lambdify((theta,lambda1,a),hi,modules='numpy')

lat = grid.grid.clat[:,ngrids-1]
lon = grid.grid.clon[:,ngrids-1]
rad = settings.a
chi_exact = chi_func(lat,lon,rad)
psi_exact = psi_func(lat,lon,rad)
hi = hi_func(lat,lon,rad)
grid.grid.heinv = np.zeros((grid.grid.nedge[-1],grid.grid.ngrids),dtype=np.float64)
grid.grid.hvinv = np.zeros((grid.grid.nvert[-1],grid.grid.ngrids),dtype=np.float64)
grid.grid.heinv2 = np.zeros((grid.grid.nedge[-1],grid.grid.ngrids),dtype=np.float64)
grid.grid.hvinv2 = np.zeros((grid.grid.nvert[-1],grid.grid.ngrids),dtype=np.float64)
grid.grid.diagnose_h_quantities(hi,ngrids)
chipsi = np.concatenate((chi_exact,psi_exact))
deltazeta = grid.grid.chi_psi_apply_operator_highest(chipsi)
delta,zeta = np.split(deltazeta,2)
#delta = delta_func(lat,lon,rad)
#zeta = zeta_func(lat,lon,rad)


#Asparse = spsparselin.LinearOperator((2*grid.grid.nface[-1],2*grid.grid.nface[-1]),afunc,dtype=np.float64) 
niters = 20
tolvals = [1e-12,1e-15,1e-16,1e-17,1e-18]
solvers = [spsparselin.lgmres,]
#, spsparselin.minres] - wrong solution past v3
#spsparselin.gmres] - convergence issues past v3
#spsparselin.cg ,  spsparselin.cgs spsparselin.bicgstab
solvernames = ['lgmres']
#,'minres'] 'cg' 'cgs',
#'gmres']
#bicg- need to implement other operators in Af2py
#qmr- need to implement other operators in Af2py
smoother_dict = {}
smoother_dict[1e-12] = 200
smoother_dict[1e-15] = 400
smoother_dict[1e-16] = 600
smoother_dict[1e-17] = 800
smoother_dict[1e-18] = 1000
multigrid_dict = {}
multigrid_dict[1e-12] = (1,2,10)
multigrid_dict[1e-15] = (1,3,10)
multigrid_dict[1e-16] = (1,4,10)
multigrid_dict[1e-17] = (2,2,10)
multigrid_dict[1e-18] = (2,3,10)

grid.grid.inj_stencil_size = np.zeros((grid.grid.nface[-1],ngrids-1),dtype=np.int32)
grid.grid.inj_weights = np.zeros((grid.grid.nface[-1],7,ngrids-1),dtype=np.float64)
grid.grid.inj_stencil = np.zeros((grid.grid.nface[-1],7,ngrids-1),dtype=np.int32)

#print 'inj create'
#print np.amax(grid.grid.inj_stencil_size),np.amin(grid.grid.inj_stencil_size)
#print np.amax(grid.grid.inj_stencil),np.amin(grid.grid.inj_stencil)
#print np.amax(grid.grid.inj_weights),np.amin(grid.grid.inj_weights)
#print

grid.grid.buildinj()
#check that ifp0's are all filled properly
print 'ifp0 checks'
for i in range(grid.grid.ngrids-1):
	print i,np.amax(grid.grid.inj_stencil[:grid.grid.nface[i],0,i]),np.amin(grid.grid.inj_stencil[:grid.grid.nface[i],0,i]),np.unique(grid.grid.inj_stencil[:grid.grid.nface[i],0,i]).shape
	
#print 'inj build'
#print np.amax(grid.grid.inj_stencil_size),np.amin(grid.grid.inj_stencil_size)
#print np.amax(grid.grid.inj_stencil),np.amin(grid.grid.inj_stencil)
#print np.amax(grid.grid.inj_weights),np.amin(grid.grid.inj_weights)
#print
############### FULL SOLVER ###############
print 
Af2py = spsparselin.LinearOperator((2*grid.grid.nface[-1],2*grid.grid.nface[-1]),grid.grid.chi_psi_apply_operator_highest,dtype=np.float64) 
grid.grid.diagcoeff = np.zeros((2*grid.grid.nface[-1],grid.grid.ngrids),dtype=np.float64)
#print 'diag create'
#print np.amax(grid.grid.inj_stencil_size),np.amin(grid.grid.inj_stencil_size)
#print np.amax(grid.grid.inj_stencil),np.amin(grid.grid.inj_stencil)
#print np.amax(grid.grid.inj_weights),np.amin(grid.grid.inj_weights)
#print
grid.grid.build_multigrid_operator(hi)
grid.grid.set_diagonal_coefficient()

#print 'diag build'
#print np.amax(grid.grid.inj_stencil_size),np.amin(grid.grid.inj_stencil_size)
#print np.amax(grid.grid.inj_stencil),np.amin(grid.grid.inj_stencil)
#print np.amax(grid.grid.inj_weights),np.amin(grid.grid.inj_weights)
#print np.amax(grid.grid.diagcoeff),np.amin(grid.grid.diagcoeff),np.average(grid.grid.diagcoeff)
#print np.amax(grid.grid.heinv),np.amin(grid.grid.heinv),np.average(grid.grid.heinv)
#print np.amax(grid.grid.hvinv),np.amin(grid.grid.hvinv),np.average(grid.grid.hvinv)
#print np.amax(grid.grid.heinv2),np.amin(grid.grid.heinv2),np.average(grid.grid.heinv2)
#print np.amax(grid.grid.hvinv2),np.amin(grid.grid.hvinv2),np.average(grid.grid.hvinv2)
#print

#check diagonal coeffs
for i in range(grid.grid.ngrids):
	nface = grid.grid.nface[i]
	nedge = grid.grid.nedge[i]
	I = create_scipy_sparse_matrix(grid.grid.i_stencil[:nface,:,i],grid.grid.i_stencil_size[:nface,i],grid.grid.i_coeffs[:nface,:,i],nface,nface)
	H = create_scipy_sparse_matrix(grid.grid.h_stencil[:nedge,:,i],grid.grid.h_stencil_size[:nedge,i],grid.grid.h_coeffs[:nedge,:,i],nedge,nedge)
	D2 = create_scipy_sparse_matrix(grid.grid.ec[:nface,:,i],grid.grid.ecsize[:nface,i],grid.grid.nei_cell[:nface,:,i],nface,nedge)
	D1bar =  create_scipy_sparse_matrix(grid.grid.ce[:nedge,:,i],np.ones(nedge,dtype=np.int32)*2,-1*grid.grid.nei_edge[:nedge,:,i],nedge,nface)
	heinv = np.expand_dims(grid.grid.heinv[:nedge,i],axis=0)
	heinvmat = spsparse.dia_matrix((heinv,[0,]),(nedge,nedge))
	Asparse = I * D2 * heinvmat * H * D1bar
	true_diagcoeffs = Asparse.diagonal()
	
	diff = (true_diagcoeffs - grid.grid.diagcoeff[:nface,i])/true_diagcoeffs
	print i,'coeff diffs 1',np.amax(diff),np.amin(diff),np.average(diff)
	diff = (true_diagcoeffs - grid.grid.diagcoeff[nface:2*nface,i])/true_diagcoeffs
	print i,'coeff diffs 2',np.amax(diff),np.amin(diff),np.average(diff)
print


#rhs = np.concatenate((delta,zeta))
##print rhs.shape
#print np.amax(grid.grid.diagcoeff),np.amin(grid.grid.diagcoeff),np.average(grid.grid.diagcoeff)
#print

#chipsi_multigrid1 = np.zeros(2*nface,dtype=np.float64)
#chipsi_multigrid1 = grid.grid.fcycle(rhs,grid.grid.ngrids,5,5,20)

#print np.amax(grid.grid.diagcoeff),np.amin(grid.grid.diagcoeff),np.average(grid.grid.diagcoeff)
#print

#chipsi_multigrid2 = np.zeros(2*nface,dtype=np.float64)
#chipsi_multigrid2 = grid.grid.fcycle(rhs,grid.grid.ngrids,5,5,20)
#chipsi_multigrid2 = grid.grid.fcycle(rhs,grid.grid.ngrids,5,5,20)
#chipsi_multigrid2 = grid.grid.fcycle(rhs,grid.grid.ngrids,5,5,20)
##WHY DOES THIS WORK, BUT BELOW DOES NOT?

#print np.amax(grid.grid.diagcoeff),np.amin(grid.grid.diagcoeff),np.average(grid.grid.diagcoeff)
#print
#diff = chipsi_multigrid1 - chipsi_multigrid2
#print np.amax(diff),np.amin(diff),np.average(diff)

#print np.amax(grid.grid.inj_stencil_size),np.amin(grid.grid.inj_stencil_size)
#print np.amax(grid.grid.inj_stencil),np.amin(grid.grid.inj_stencil)
#print np.amax(grid.grid.inj_weights),np.amin(grid.grid.inj_weights)
#print np.amax(grid.grid.diagcoeff),np.amin(grid.grid.diagcoeff),np.average(grid.grid.diagcoeff)
#print np.amax(grid.grid.heinv),np.amin(grid.grid.heinv),np.average(grid.grid.heinv)
#print np.amax(grid.grid.hvinv),np.amin(grid.grid.hvinv),np.average(grid.grid.hvinv)
#print np.amax(grid.grid.heinv2),np.amin(grid.grid.heinv2),np.average(grid.grid.heinv2)
#print np.amax(grid.grid.hvinv2),np.amin(grid.grid.hvinv2),np.average(grid.grid.hvinv2)
#print

print 
for solver,solvername in zip(solvers,solvernames):
	for tolval in tolvals:
		print solvername,tolval
		start = time.time()
		for i in range(niters):
			rhs = np.concatenate((delta,zeta))
			chipsi_f2py,info = solver(Af2py,rhs,tol=tolval)
			chi_f2py,psi_f2py = np.split(chipsi_f2py,2)
			if info != 0:
				print 'convergence error',info
		end = time.time()
		print 'f2py chi_psi solve time',(end-start)/niters
		print np.amin(chi_f2py),np.amax(chi_f2py),np.average(chi_f2py)
		chi_error = chi_f2py-chi_exact
		psi_error = psi_f2py-psi_exact
		print 'l2 chi',l2_error(chi_error)
		print 'l2 psi',l2_error(psi_error)
		print 'linf chi',linf_error(chi_error)
		print 'linf psi',linf_error(psi_error)
		print

		#nsmooth = smoother_dict[tolval]
		#print 'smoother',nsmooth
		#start = time.time()
		#for i in range(niters):
			#rhs = np.concatenate((delta,zeta))
			#chipsi_smoother = np.zeros(2*grid.grid.nface[-1],dtype=np.float64)
			#grid.grid.smooth(chipsi_smoother,rhs,grid.grid.ngrids,nsmooth)
			#chi_smoother,psi_smoother = np.split(chipsi_smoother,2)
		#end = time.time()
		#print 'smoother chi_psi solve time',(end-start)/niters
		#print np.amin(chi_smoother),np.amax(chi_smoother),np.average(chi_smoother)
		#chi_error = chi_smoother-chi_exact
		#psi_error = psi_smoother-psi_exact
		#print 'l2 chi',l2_error(chi_error)
		#print 'l2 psi',l2_error(psi_error)
		#print 'linf chi',linf_error(chi_error)
		#print 'linf psi',linf_error(psi_error)
		#print		
		
		niterc = multigrid_dict[tolval][2]
		niter = multigrid_dict[tolval][1]
		npass = multigrid_dict[tolval][0]
		print 'multigrid',npass,niter,niterc
		start = time.time()
		for i in range(niters):
			grid.grid.build_multigrid_operator(hi)
			grid.grid.set_diagonal_coefficient()
			rhs = np.concatenate((delta,zeta))
			chipsi_multigrid = grid.grid.fcycle(rhs,grid.grid.ngrids,npass,niter,niterc)
			chi_multigrid,psi_multigrid = np.split(chipsi_multigrid,2)
		end = time.time()
		print 'multigrid chi_psi solve time',(end-start)/niters
		print np.amin(chi_multigrid),np.amax(chi_multigrid),np.average(chi_multigrid)
		chi_error = chi_multigrid-chi_exact
		psi_error = psi_multigrid-psi_exact
		print 'l2 chi',l2_error(chi_error)
		print 'l2 psi',l2_error(psi_error)
		print 'linf chi',linf_error(chi_error)
		print 'linf psi',linf_error(psi_error)
		print

		chi_diff = chi_f2py - chi_multigrid
		print 'l2 diff f2py multigrid',l2_error(chi_diff)
		print 'linf diff f2py multigrid',linf_error(chi_diff)	
		print
#check that hi = const -> reduces to pair of Laplacians
#grid.grid.diagnose_h_quantities(hi,ngrids)
#ADD THIS!!

############## LAPLACIAN ONLY ####################3
#print 
#Af2py = spsparselin.LinearOperator((grid.grid.nface[-1],grid.grid.nface[-1]),grid.grid.apply_laplacian_highest,dtype=np.float64) 
#delta = L * chi_exact
#zeta = L * chi_exact
#grid.grid.laplac_diag_coeffs = np.zeros((grid.grid.nface[-1],grid.grid.ngrids),dtype=np.float64)
#grid.grid.laplac_dual_diag_coeffs = np.zeros((2*grid.grid.nface[-1],grid.grid.ngrids),dtype=np.float64)
#grid.grid.set_laplac_diag_coeffs()

#check diagonal coeffs
#for i in range(grid.grid.ngrids):
	#nface = grid.grid.nface[i]
	#nedge = grid.grid.nedge[i]
	#I = create_scipy_sparse_matrix(grid.grid.i_stencil[:nface,:,i],grid.grid.i_stencil_size[:nface,i],grid.grid.i_coeffs[:nface,:,i],nface,nface)
	#H = create_scipy_sparse_matrix(grid.grid.h_stencil[:nedge,:,i],grid.grid.h_stencil_size[:nedge,i],grid.grid.h_coeffs[:nedge,:,i],nedge,nedge)
	#D2 = create_scipy_sparse_matrix(grid.grid.ec[:nface,:,i],grid.grid.ecsize[:nface,i],grid.grid.nei_cell[:nface,:,i],nface,nedge)
	#D1bar =  create_scipy_sparse_matrix(grid.grid.ce[:nedge,:,i],np.ones(nedge,dtype=np.int32)*2,-1*grid.grid.nei_edge[:nedge,:,i],nedge,nface)
	#Asparse = I * D2 * H * D1bar
	#true_diagcoeffs = Asparse.diagonal()

	#diff = (true_diagcoeffs - 1./grid.grid.laplac_diag_coeffs[:nface,i])/true_diagcoeffs
	#print i,'coeff diffs',np.amax(diff),np.amin(diff),np.average(diff)
	#diff = (true_diagcoeffs - 1./grid.grid.laplac_dual_diag_coeffs[:nface,i])/true_diagcoeffs
	#print i,'coeff diffs 1',np.amax(diff),np.amin(diff),np.average(diff)
	#diff = (true_diagcoeffs - 1./grid.grid.laplac_dual_diag_coeffs[nface:2*nface,i])/true_diagcoeffs
	#print i,'coeff diffs 2',np.amax(diff),np.amin(diff),np.average(diff)
#print

#for solver,solvername in zip(solvers,solvernames):
	#for tolval in tolvals:
		#print solvername,tolval,'f2py'
		#start = time.time()
		#for i in range(niters):
			#chi_f2py,info = solver(Af2py,delta,tol=tolval)
			#if info != 0:
				#print 'convergence error',info
		#end = time.time()
		#print 'f2py chi solve time',(end-start)/niters
		#print np.amin(chi_f2py),np.amax(chi_f2py),np.average(chi_f2py)
		#chi_error = chi_f2py-chi_exact
		#print 'l2 chi',l2_error(chi_error)
		#print 'linf chi',linf_error(chi_error)
		#print

		#print solvername,tolval,'sparse'
		#start = time.time()
		#for i in range(niters):
			#chi_sparse,info = solver(L,delta,tol=tolval)
			#if info != 0:
				#print 'convergence error',info
		#end = time.time()
		#print 'sparse chi solve time',(end-start)/niters
		#print np.amin(chi_sparse),np.amax(chi_sparse),np.average(chi_sparse)
		#chi_error = chi_sparse-chi_exact
		#print 'l2 chi',l2_error(chi_error)
		#print 'linf chi',linf_error(chi_error)
		#print

		#niterc = multigrid_dict[tolval][2]
		#niter = multigrid_dict[tolval][1]
		#npass = multigrid_dict[tolval][0]
		#print 'multigrid',npass,niter,niterc
		#start = time.time()
		#for i in range(niters):
			#chi_multigrid = grid.grid.fcycle_single(delta,grid.grid.ngrids,npass,niter,niterc)
			#rhs = np.concatenate((delta,zeta))
			#chipsi_multigrid = grid.grid.fcycle(rhs,grid.grid.ngrids,npass,niter,niterc)
			#chi_multigrid,psi_multigrid = np.split(chipsi_multigrid,2)
		#end = time.time()
		#print 'multigrid chi solve time',(end-start)/niters
		#print np.amin(chi_multigrid),np.amax(chi_multigrid),np.average(chi_multigrid)
		#chi_error = chi_multigrid-chi_exact
		#print 'l2 chi',l2_error(chi_error)
		#print 'linf chi',linf_error(chi_error)
		#print

		#nsmooth = smoother_dict[tolval]
		#print 'smoother',nsmooth
		#start = time.time()
		#for i in range(niters):
			#chi_smoother = np.zeros(grid.grid.nface[-1],dtype=np.float64)
			#grid.grid.smooth_single(chi_smoother,delta,grid.grid.ngrids,nsmooth)
			#rhs = np.concatenate((delta,zeta))
			#chipsi_smoother = np.zeros(2*grid.grid.nface[-1],dtype=np.float64)
			#grid.grid.smooth(chipsi_smoother,rhs,grid.grid.ngrids,nsmooth)
			#chi_smoother,psi_smoother = np.split(chipsi_smoother,2)
		#end = time.time()
		#print 'smoother chi solve time',(end-start)/niters
		#print np.amin(chi_smoother),np.amax(chi_smoother),np.average(chi_smoother)
		#chi_error = chi_smoother-chi_exact
		#print 'l2 chi',l2_error(chi_error)
		#print 'linf chi',linf_error(chi_error)
		#print		
		
		#chi_diff = chi_f2py - chi_sparse
		#print 'l2 diff f2py sparse',l2_error(chi_diff)
		#print 'linf diff f2py sparse',linf_error(chi_diff)	
		#chi_diff = chi_f2py - chi_multigrid
		#print 'l2 diff f2py multigrid',l2_error(chi_diff)
		#print 'linf diff f2py multigrid',linf_error(chi_diff)	
		#print
		#print
