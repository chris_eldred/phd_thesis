from basemap_plotting_helpers import *
import subprocess
maxvlevel = 8
gridlist = ['geodesic','cubedsphere']
gridvariantdict = {'geodesic': ['tweaked',], 'cubedsphere': ['thuburn',]} #'cvt','spring_beta=1.1','spring_beta=0.8',
a = 6371220.0
operatorslist = ['Ld0','Lp0','Q_TEQrot','R','Le','Wdiv','Wrot','Q_Qrot','Q_Qdiv','Q_TEdiv','Q_TErot','Q_TEQdiv','Phi','ki_rot','ki_div','laplacZ','fluxdivZ','jacobianZ']
operator_loc = ['primal','dual','edge','primal','edge','edge','edge','edge','edge','edge','edge','edge','edge','primal','primal','primal','primal','primal']
for operator,loc in zip(operatorslist,operator_loc):
	for gridtype in gridlist:
		if gridtype == 'cubedsphere':
			if operator == 'laplacZ' or operator == 'fluxdivZ' or operator == 'jacobianZ':
				continue
		for gridvariant in gridvariantdict[gridtype]:
			grid = get_geometry(gridtype,gridvariant,maxvlevel)
			print 'plotting',operator,gridtype,gridvariant,maxvlevel
			data = load_operator_error(operator,gridtype,gridvariant)
			if loc == 'primal':
				data = 1./grid.Ai/a/a * data
			if loc == 'dual':
				data = 1./grid.Av/a/a * data
			if loc == 'edge':
				data = 1./grid.de/a * data
			subprocess.call(['mkdir','-p','orderofaccuracy/' + operator])
			#plot_scalar(grid,data,loc,operator,'orderofaccuracy/' + operator + '/' + operator + '.' + gridtype + '.' + gridvariant,'spherical','grid')
			#plot_scalar(grid,data,loc,operator,'orderofaccuracy/' + operator + '/' + operator + '.' + gridtype + '.' + gridvariant,'spherical','contour')
			plot_scalar(grid,data,loc,operator,'orderofaccuracy/' + operator + '/' + operator + '.' + gridtype + '.' + gridvariant,'latlon','contour')

scheme_list = ['TC2',]
variant_list = ['Q','TEQ','TE']
for scheme in scheme_list:
	subprocess.call(['mkdir','-p','orderofaccuracy/' + scheme])
	for gridtype in gridlist:
		for gridvariant in gridvariantdict[gridtype]:
			grid = get_geometry(gridtype,gridvariant,maxvlevel)
			for variant in variant_list:
				print 'plotting',variant,gridtype,gridvariant
				herr = load_scheme_error('TC2',variant,'h',gridtype,gridvariant)
				uerr = load_scheme_error('TC2',variant,'u',gridtype,gridvariant)
				#plot_scalar(grid,herr/grid.Ai/a/a,'primal',variant + '-h','TC2/' + 'TC2.' + variant + '.h.' + gridtype + '.' + gridvariant,'spherical','grid') 
				#plot_scalar(grid,herr/grid.Ai/a/a,'primal',variant + '-h','TC2/' + 'TC2.' + variant + '.h.' + gridtype + '.' + gridvariant,'spherical','contour') 
				plot_scalar(grid,herr/grid.Ai/a/a,'primal',variant + '-h','orderofaccuracy/' + scheme + '/' + scheme + '.' + variant + '.h.' + gridtype + '.' + gridvariant,'latlon','contour') 
				#plot_scalar(grid,uerr/grid/de/a,'edge',variant + '-u','TC2/' + 'TC2.' + variant + '.u.' + gridtype + '.' + gridvariant,'spherical','grid') 
				#plot_scalar(grid,uerr/grid/de/ar,'edge',variant + '-u','TC2/' + 'TC2.' + variant + '.u.' + gridtype + '.' + gridvariant,'spherical','contour') 
				plot_scalar(grid,uerr/grid.de/a,'edge',variant + '-u','orderofaccuracy/' + scheme + '/' + scheme + '.' + variant + '.u.' + gridtype + '.' + gridvariant,'latlon','contour') 
