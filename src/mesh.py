from __future__ import print_function
from constants import *
import numpy as np
import h5py
import scipy.sparse as spsparse
import scipy.sparse.linalg as spsparselin

def apply_Hinv(v,H):
	x, info = spsparselin.bicgstab(H,v,tol=1e-15)
	return x
	
def set_grid_sizes_cubedsphere(grid,ngrids):
	grid.ngrids = ngrids
	x = np.arange(grid.ngrids)
	n0 = 3
	grid.nface = np.power(n0*np.power(2,x),2) * 6
	grid.nedge = 2*grid.nface
	grid.nvert = grid.nface + 2

def set_grid_sizes_geodesic(grid,ngrids):
	grid.ngrids = ngrids
	x = np.arange(grid.ngrids)
	grid.nface = 10*np.power(np.power(2,x+1),2) + 2
	grid.nedge = 30*np.power(np.power(2,x+1),2)
	grid.nvert = 20*np.power(np.power(2,x+1),2)

def set_grid_sizes_square(grid,nx,ny):
	grid.ngrids = 1
	grid.basename = str(nx) + 'x' + str(ny)
	grid.nface = [nx * ny]
	grid.nedge = [2 * nx * ny]
	grid.nvert = [nx * ny]

def set_grid_sizes_hex(grid,nx,ny):
	grid.ngrids = 1
	grid.basename = str(nx) + 'x' + str(ny)
	grid.nface = [nx * ny]
	grid.nedge = [3 * nx * ny]
	grid.nvert = [2 * nx * ny]

################# HEX ################

def allocate_connectivity_hex(grid):
	grid.ecsize = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.evsize = np.zeros((grid.nvert[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.ecpsize = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.ecpdsize = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')

	grid.vc = np.zeros((grid.nface[-1],6,grid.ngrids),dtype=np.int32,order='F')
	grid.ec = np.zeros((grid.nface[-1],6,grid.ngrids),dtype=np.int32,order='F')
	grid.nc = np.zeros((grid.nface[-1],6,grid.ngrids),dtype=np.int32,order='F')
	grid.cv = np.zeros((grid.nvert[-1],3,grid.ngrids),dtype=np.int32,order='F')
	grid.ev = np.zeros((grid.nvert[-1],3,grid.ngrids),dtype=np.int32,order='F')
	grid.ce = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.int32,order='F')
	grid.ve = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.int32,order='F')
	grid.ecp = np.zeros((grid.nedge[-1],11,grid.ngrids),dtype=np.int32,order='F')
	grid.ecpd = np.zeros((grid.nedge[-1],5,grid.ngrids),dtype=np.int32,order='F')

	grid.nei_cell = np.zeros((grid.nface[-1],6,grid.ngrids),dtype=np.int32,order='F')
	grid.tev_vertex = np.zeros((grid.nvert[-1],3,grid.ngrids),dtype=np.int32,order='F')
	grid.nei_edge = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.int32,order='F')
	grid.tev_edge = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.int32,order='F')

def allocate_operators_hex(grid):
	grid.i_stencil_size = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.j_stencil_size = np.zeros((grid.nvert[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.h_stencil_size = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.rt_stencil_size = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.r_stencil_size = np.zeros((grid.nvert[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.q_stencil_size = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.e_stencil_size = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.l_stencil_size = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')
	
	grid.i_stencil = np.zeros((grid.nface[-1],1,grid.ngrids),dtype=np.int32,order='F')
	grid.j_stencil = np.zeros((grid.nvert[-1],1,grid.ngrids),dtype=np.int32,order='F')
	grid.h_stencil = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.int32,order='F')
	grid.rt_stencil = np.zeros((grid.nface[-1],6,grid.ngrids),dtype=np.int32,order='F')
	grid.r_stencil = np.zeros((grid.nvert[-1],3,grid.ngrids),dtype=np.int32,order='F')
	grid.q_stencil = np.zeros((grid.nface[-1],90,3,grid.ngrids),dtype=np.int32,order='F')
	grid.e_stencil = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.int32,order='F')
	grid.l_stencil = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.int32,order='F')
	
	grid.i_coeffs = np.zeros((grid.nface[-1],1,grid.ngrids),dtype=np.float64,order='F')
	grid.j_coeffs = np.zeros((grid.nvert[-1],1,grid.ngrids),dtype=np.float64,order='F')
	grid.h_coeffs = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.float64,order='F')
	grid.rt_coeffs = np.zeros((grid.nface[-1],6,grid.ngrids),dtype=np.float64,order='F')
	grid.r_coeffs = np.zeros((grid.nvert[-1],3,grid.ngrids),dtype=np.float64,order='F')
	grid.q_coeffs = np.zeros((grid.nface[-1],90,grid.ngrids),dtype=np.float64,order='F')
	grid.w_coeffs = np.zeros((grid.nedge[-1],11,grid.ngrids),dtype=np.float64,order='F')
	grid.wl_coeffs = np.zeros((grid.nedge[-1],11,grid.ngrids),dtype=np.float64,order='F')
	grid.wd_coeffs = np.zeros((grid.nedge[-1],5,grid.ngrids),dtype=np.float64,order='F')
	grid.phi_coeffs = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.float64,order='F')
	grid.phit_coeffs = np.zeros((grid.nface[-1],6,grid.ngrids),dtype=np.float64,order='F')
	grid.e_coeffs = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.float64,order='F')
	grid.l_coeffs = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.float64,order='F')
	grid.phi_coeffs = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.float64,order='F')
	grid.laplac_primal_coeffs = np.zeros((grid.nface[-1],6,grid.ngrids),dtype=np.float64,order='F')

def allocate_geometry_hex(grid):
	grid.ai = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.av = np.zeros((grid.nvert[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.ae = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.le = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.de = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.aie = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.float64,order='F')
	grid.aiv = np.zeros((grid.nface[-1],6,grid.ngrids),dtype=np.float64,order='F')
	grid.clon = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.clat = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.float64,order='F')


################# SQUARE ################
def allocate_connectivity_square(grid):
	grid.ecsize = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.evsize = np.zeros((grid.nvert[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.ecpsize = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.ecpdsize = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')

	grid.vc = np.zeros((grid.nface[-1],4,grid.ngrids),dtype=np.int32,order='F')
	grid.ec = np.zeros((grid.nface[-1],4,grid.ngrids),dtype=np.int32,order='F')
	grid.nc = np.zeros((grid.nface[-1],4,grid.ngrids),dtype=np.int32,order='F')
	grid.cv = np.zeros((grid.nvert[-1],4,grid.ngrids),dtype=np.int32,order='F')
	grid.ev = np.zeros((grid.nvert[-1],4,grid.ngrids),dtype=np.int32,order='F')
	grid.ce = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.int32,order='F')
	grid.ve = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.int32,order='F')
	grid.ecp = np.zeros((grid.nedge[-1],7,grid.ngrids),dtype=np.int32,order='F')
	grid.ecpd = np.zeros((grid.nedge[-1],7,grid.ngrids),dtype=np.int32,order='F')

	grid.nei_cell = np.zeros((grid.nface[-1],4,grid.ngrids),dtype=np.int32,order='F')
	grid.tev_vertex = np.zeros((grid.nvert[-1],4,grid.ngrids),dtype=np.int32,order='F')
	grid.nei_edge = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.int32,order='F')
	grid.tev_edge = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.int32,order='F')

def allocate_operators_square(grid):
	grid.i_stencil_size = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.j_stencil_size = np.zeros((grid.nvert[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.h_stencil_size = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.rt_stencil_size = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.r_stencil_size = np.zeros((grid.nvert[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.q_stencil_size = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.e_stencil_size = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.l_stencil_size = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')

	grid.i_stencil = np.zeros((grid.nface[-1],1,grid.ngrids),dtype=np.int32,order='F')
	grid.j_stencil = np.zeros((grid.nvert[-1],1,grid.ngrids),dtype=np.int32,order='F')
	grid.h_stencil = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.int32,order='F')
	grid.rt_stencil = np.zeros((grid.nface[-1],4,grid.ngrids),dtype=np.int32,order='F')
	grid.r_stencil = np.zeros((grid.nvert[-1],4,grid.ngrids),dtype=np.int32,order='F')
	grid.q_stencil = np.zeros((grid.nface[-1],24,3,grid.ngrids),dtype=np.int32,order='F')
	grid.e_stencil = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.int32,order='F')
	grid.l_stencil = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.int32,order='F')

	grid.i_coeffs = np.zeros((grid.nface[-1],1,grid.ngrids),dtype=np.float64,order='F')
	grid.j_coeffs = np.zeros((grid.nvert[-1],1,grid.ngrids),dtype=np.float64,order='F')
	grid.h_coeffs = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.float64,order='F')
	grid.rt_coeffs = np.zeros((grid.nface[-1],4,grid.ngrids),dtype=np.float64,order='F')
	grid.r_coeffs = np.zeros((grid.nvert[-1],4,grid.ngrids),dtype=np.float64,order='F')
	grid.q_coeffs = np.zeros((grid.nface[-1],24,grid.ngrids),dtype=np.float64,order='F')
	grid.w_coeffs = np.zeros((grid.nedge[-1],7,grid.ngrids),dtype=np.float64,order='F')
	grid.wl_coeffs = np.zeros((grid.nedge[-1],7,grid.ngrids),dtype=np.float64,order='F')
	grid.wd_coeffs = np.zeros((grid.nedge[-1],7,grid.ngrids),dtype=np.float64,order='F')
	grid.phi_coeffs = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.float64,order='F')
	grid.phit_coeffs = np.zeros((grid.nface[-1],4,grid.ngrids),dtype=np.float64,order='F')
	grid.e_coeffs = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.float64,order='F')
	grid.l_coeffs = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.float64,order='F')
	grid.phi_coeffs = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.float64,order='F')
	grid.laplac_primal_coeffs = np.zeros((grid.nface[-1],5,grid.ngrids),dtype=np.float64,order='F')

def allocate_geometry_square(grid):
	grid.ai = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.av = np.zeros((grid.nvert[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.ae = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.le = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.de = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.aie = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.float64,order='F')
	grid.aiv = np.zeros((grid.nface[-1],4,grid.ngrids),dtype=np.float64,order='F')
	grid.clon = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.clat = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.float64,order='F')

################# CUBED SPHERE ################

def allocate_connectivity_cubedsphere(grid):
	grid.ecsize = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.evsize = np.zeros((grid.nvert[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.ecpsize = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.ecpdsize = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')

	grid.vc = np.zeros((grid.nface[-1],4,grid.ngrids),dtype=np.int32,order='F')
	grid.ec = np.zeros((grid.nface[-1],4,grid.ngrids),dtype=np.int32,order='F')
	grid.nc = np.zeros((grid.nface[-1],4,grid.ngrids),dtype=np.int32,order='F')
	grid.cv = np.zeros((grid.nvert[-1],4,grid.ngrids),dtype=np.int32,order='F')
	grid.ev = np.zeros((grid.nvert[-1],4,grid.ngrids),dtype=np.int32,order='F')
	grid.ce = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.int32,order='F')
	grid.ve = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.int32,order='F')
	grid.ecp = np.zeros((grid.nedge[-1],7,grid.ngrids),dtype=np.int32,order='F')
	grid.ecpd = np.zeros((grid.nedge[-1],7,grid.ngrids),dtype=np.int32,order='F')

	grid.nei_cell = np.zeros((grid.nface[-1],4,grid.ngrids),dtype=np.int32,order='F')
	grid.tev_vertex = np.zeros((grid.nvert[-1],4,grid.ngrids),dtype=np.int32,order='F')
	grid.nei_edge = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.int32,order='F')
	grid.tev_edge = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.int32,order='F')
	
def allocate_multigrid_cubedsphere(grid):
	grid.inj_stencil_size = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.inj_stencil = np.zeros((grid.nface[-1],4,grid.ngrids),dtype=np.int32,order='F')
	grid.inj_weights = np.zeros((grid.nvert[-1],4,grid.ngrids),dtype=np.float64,order='F')
	grid.diagcoeff = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.float64,order='F')

def allocate_operators_cubedsphere(grid):
	grid.i_stencil_size = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.j_stencil_size = np.zeros((grid.nvert[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.h_stencil_size = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.r_stencil_size = np.zeros((grid.nvert[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.rt_stencil_size = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.q_stencil_size = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.e_stencil_size = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.l_stencil_size = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')

	grid.i_stencil = np.zeros((grid.nface[-1],1,grid.ngrids),dtype=np.int32,order='F')
	grid.j_stencil = np.zeros((grid.nvert[-1],1,grid.ngrids),dtype=np.int32,order='F')
	grid.h_stencil = np.zeros((grid.nedge[-1],5,grid.ngrids),dtype=np.int32,order='F')
	grid.r_stencil = np.zeros((grid.nvert[-1],4,grid.ngrids),dtype=np.int32,order='F')
	grid.rt_stencil = np.zeros((grid.nface[-1],4,grid.ngrids),dtype=np.int32,order='F')
	grid.q_stencil = np.zeros((grid.nface[-1],24,3,grid.ngrids),dtype=np.int32,order='F')
	grid.e_stencil = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.int32,order='F')
	grid.l_stencil = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.int32,order='F')

	grid.i_coeffs = np.zeros((grid.nface[-1],1,grid.ngrids),dtype=np.float64,order='F')
	grid.j_coeffs = np.zeros((grid.nvert[-1],1,grid.ngrids),dtype=np.float64,order='F')
	grid.h_coeffs = np.zeros((grid.nedge[-1],5,grid.ngrids),dtype=np.float64,order='F')
	grid.r_coeffs = np.zeros((grid.nvert[-1],4,grid.ngrids),dtype=np.float64,order='F')
	grid.rt_coeffs = np.zeros((grid.nface[-1],4,grid.ngrids),dtype=np.float64,order='F')
	grid.q_coeffs = np.zeros((grid.nface[-1],24,grid.ngrids),dtype=np.float64,order='F')
	grid.w_coeffs = np.zeros((grid.nedge[-1],7,grid.ngrids),dtype=np.float64,order='F')
	grid.wl_coeffs = np.zeros((grid.nedge[-1],7,grid.ngrids),dtype=np.float64,order='F')
	grid.wd_coeffs = np.zeros((grid.nedge[-1],7,grid.ngrids),dtype=np.float64,order='F')
	grid.phi_coeffs = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.float64,order='F')
	grid.phit_coeffs = np.zeros((grid.nface[-1],4,grid.ngrids),dtype=np.float64,order='F')
	grid.e_coeffs = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.float64,order='F')
	grid.l_coeffs = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.float64,order='F')
	grid.phi_coeffs = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.float64,order='F')
	grid.laplac_primal_coeffs = np.zeros((grid.nface[-1],9,grid.ngrids),dtype=np.float64,order='F')

def allocate_geometry_cubedsphere(grid):
	grid.ai = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.av = np.zeros((grid.nvert[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.ae = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.le = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.de = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.aie = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.float64,order='F')
	grid.aiv = np.zeros((grid.nface[-1],4,grid.ngrids),dtype=np.float64,order='F')
	grid.clon = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.clat = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.float64,order='F')

################# GEODESIC ################

def allocate_connectivity_geodesic(grid):
	grid.ecsize = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.evsize = np.zeros((grid.nvert[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.ecpsize = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.ecpdsize = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')

	grid.vc = np.zeros((grid.nface[-1],6,grid.ngrids),dtype=np.int32,order='F')
	grid.ec = np.zeros((grid.nface[-1],6,grid.ngrids),dtype=np.int32,order='F')
	grid.nc = np.zeros((grid.nface[-1],6,grid.ngrids),dtype=np.int32,order='F')
	grid.cv = np.zeros((grid.nvert[-1],3,grid.ngrids),dtype=np.int32,order='F')
	grid.ev = np.zeros((grid.nvert[-1],3,grid.ngrids),dtype=np.int32,order='F')
	grid.ce = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.int32,order='F')
	grid.ve = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.int32,order='F')
	grid.ecp = np.zeros((grid.nedge[-1],11,grid.ngrids),dtype=np.int32,order='F')
	grid.ecpd = np.zeros((grid.nedge[-1],5,grid.ngrids),dtype=np.int32,order='F')

	grid.nei_cell = np.zeros((grid.nface[-1],6,grid.ngrids),dtype=np.int32,order='F')
	grid.tev_vertex = np.zeros((grid.nvert[-1],3,grid.ngrids),dtype=np.int32,order='F')
	grid.nei_edge = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.int32,order='F')
	grid.tev_edge = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.int32,order='F')

def allocate_multigrid_geodesic(grid):
	grid.inj_stencil_size = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.inj_stencil = np.zeros((grid.nface[-1],4,grid.ngrids),dtype=np.int32,order='F') #FIX THIS
	grid.inj_weights = np.zeros((grid.nface[-1],4,grid.ngrids),dtype=np.float64,order='F') #FIX THIS
	grid.diagcoeff = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.float64,order='F')

def allocate_operators_geodesic(grid):
	grid.i_stencil_size = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.j_stencil_size = np.zeros((grid.nvert[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.h_stencil_size = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.rt_stencil_size = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.r_stencil_size = np.zeros((grid.nvert[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.q_stencil_size = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.e_stencil_size = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')
	grid.l_stencil_size = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.int32,order='F')
	
	grid.i_stencil = np.zeros((grid.nface[-1],1,grid.ngrids),dtype=np.int32,order='F')
	grid.j_stencil = np.zeros((grid.nvert[-1],1,grid.ngrids),dtype=np.int32,order='F')
	grid.h_stencil = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.int32,order='F')
	grid.rt_stencil = np.zeros((grid.nface[-1],6,grid.ngrids),dtype=np.int32,order='F')
	grid.r_stencil = np.zeros((grid.nvert[-1],3,grid.ngrids),dtype=np.int32,order='F')
	grid.q_stencil = np.zeros((grid.nface[-1],90,3,grid.ngrids),dtype=np.int32,order='F')
	grid.e_stencil = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.int32,order='F')
	grid.l_stencil = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.int32,order='F')
	
	grid.i_coeffs = np.zeros((grid.nface[-1],1,grid.ngrids),dtype=np.float64,order='F')
	grid.j_coeffs = np.zeros((grid.nvert[-1],1,grid.ngrids),dtype=np.float64,order='F')
	grid.h_coeffs = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.float64,order='F')
	grid.rt_coeffs = np.zeros((grid.nface[-1],6,grid.ngrids),dtype=np.float64,order='F')
	grid.r_coeffs = np.zeros((grid.nvert[-1],3,grid.ngrids),dtype=np.float64,order='F')
	grid.q_coeffs = np.zeros((grid.nface[-1],90,grid.ngrids),dtype=np.float64,order='F')
	grid.w_coeffs = np.zeros((grid.nedge[-1],11,grid.ngrids),dtype=np.float64,order='F')
	grid.wl_coeffs = np.zeros((grid.nedge[-1],11,grid.ngrids),dtype=np.float64,order='F')
	grid.wd_coeffs = np.zeros((grid.nedge[-1],5,grid.ngrids),dtype=np.float64,order='F')
	grid.phi_coeffs = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.float64,order='F')
	grid.phit_coeffs = np.zeros((grid.nface[-1],6,grid.ngrids),dtype=np.float64,order='F')
	grid.e_coeffs = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.float64,order='F')
	grid.l_coeffs = np.zeros((grid.nedge[-1],1,grid.ngrids),dtype=np.float64,order='F')
	grid.phi_coeffs = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.float64,order='F')
	grid.laplac_primal_coeffs = np.zeros((grid.nface[-1],6,grid.ngrids),dtype=np.float64,order='F')

def allocate_geometry_geodesic(grid):
	grid.ai = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.av = np.zeros((grid.nvert[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.ae = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.le = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.de = np.zeros((grid.nedge[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.aie = np.zeros((grid.nedge[-1],2,grid.ngrids),dtype=np.float64,order='F')
	grid.aiv = np.zeros((grid.nface[-1],6,grid.ngrids),dtype=np.float64,order='F')
	grid.clon = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.float64,order='F')
	grid.clat = np.zeros((grid.nface[-1],grid.ngrids),dtype=np.float64,order='F')











def load_connectivity(grid):
	for i in range(grid.ngrids):
		ncells = grid.nface[i]
		if settings.grid_type == 'geodesic' or settings.grid_type == 'cubedsphere':
			gridfile = h5py.File(settings.meshdir + settings.grid_type + '/' + settings.grid_variant + '/' + str(ncells) + '.hdf5','r')
		if settings.grid_type == 'square' or settings.grid_type == 'hex':
			gridfile = h5py.File(settings.meshdir + settings.grid_type + '/' + grid.basename + '/' + grid.basename + '.hdf5','r')
		connect = gridfile['connectivity']
		grid.ecsize[:grid.nface[i],i] = np.array(connect['nedgef'],dtype=np.int32,order='F')
		grid.evsize[:grid.nvert[i],i] = np.array(connect['nedgev'],dtype=np.int32,order='F')
		grid.ecpsize[:grid.nedge[i],i] = np.array(connect['necp'],dtype=np.int32,order='F')
		grid.ecpdsize[:grid.nedge[i],i] = np.array(connect['necpd'],dtype=np.int32,order='F')

		grid.vc[:grid.nface[i],:,i] = np.array(connect['VC'],dtype=np.int32,order='F')
		grid.ec[:grid.nface[i],:,i] = np.array(connect['EC'],dtype=np.int32,order='F')
		grid.nc[:grid.nface[i],:,i] = np.array(connect['NC'],dtype=np.int32,order='F')
		grid.cv[:grid.nvert[i],:,i] = np.array(connect['CV'],dtype=np.int32,order='F')
		grid.ev[:grid.nvert[i],:,i] = np.array(connect['EV'],dtype=np.int32,order='F')
		grid.ce[:grid.nedge[i],:,i] = np.array(connect['CE'],dtype=np.int32,order='F')
		grid.ve[:grid.nedge[i],:,i] = np.array(connect['VE'],dtype=np.int32,order='F')
		grid.ecp[:grid.nedge[i],:,i] = np.array(connect['ECP'],dtype=np.int32,order='F')
		grid.ecpd[:grid.nedge[i],:,i] = np.array(connect['ECPd'],dtype=np.int32,order='F')

		grid.nei_edge[:grid.nedge[i],:,i] = np.array(connect['nei_edge'],dtype=np.int32,order='F')
		grid.tev_edge[:grid.nedge[i],:,i] = np.array(connect['tev_edge'],dtype=np.int32,order='F')
		grid.nei_cell[:grid.nface[i],:,i] = np.array(connect['nei_cell'],dtype=np.int32,order='F')
		grid.tev_vertex[:grid.nvert[i],:,i] = np.array(connect['tev_vertex'],dtype=np.int32,order='F')
		gridfile.close()

def load_geometry(grid):
	for i in range(grid.ngrids):
		ncells = grid.nface[i]
		if settings.grid_type == 'geodesic' or settings.grid_type == 'cubedsphere':
			gridfile = h5py.File(settings.meshdir + settings.grid_type + '/' + settings.grid_variant + '/' + str(ncells) + '.hdf5','r')
		if settings.grid_type == 'square' or settings.grid_type == 'hex':
			gridfile = h5py.File(settings.meshdir + settings.grid_type + '/' + grid.basename + '/' + grid.basename + '.hdf5','r')
		geometry = gridfile['geometry']
		coords = gridfile['coords']
		grid.ai[:grid.nface[i],i] = np.array(geometry['Ai'],dtype=np.float64,order='F')
		grid.av[:grid.nvert[i],i] = np.array(geometry['Av'],dtype=np.float64,order='F')
		grid.ae[:grid.nedge[i],i] = np.array(geometry['Ae'],dtype=np.float64,order='F')
		grid.le[:grid.nedge[i],i] = np.array(geometry['le'],dtype=np.float64,order='F')
		grid.de[:grid.nedge[i],i] = np.array(geometry['de'],dtype=np.float64,order='F')
		grid.aie[:grid.nedge[i],:,i] = np.array(geometry['Aie'],dtype=np.float64,order='F')
		grid.aiv[:grid.nface[i],:,i] = np.array(geometry['Aiv'],dtype=np.float64,order='F')

		if settings.grid_type == 'geodesic' or settings.grid_type == 'cubedsphere':
			grid.clon[:grid.nface[i],i] = np.array(coords['clon'],dtype=np.float64,order='F')
			grid.clat[:grid.nface[i],i] = np.array(coords['clat'],dtype=np.float64,order='F')

		gridfile.close()
		
def create_operators(grid,gtype):
	for i in range(grid.ngrids):
		ncells = grid.nface[i]
		#get Q stuff
		if settings.grid_type == 'geodesic' or settings.grid_type == 'cubedsphere':
			Qfile = h5py.File(settings.meshdir + settings.grid_type + '/' + settings.grid_variant + '/Q' + str(ncells) + '.hdf5','r')
		if settings.grid_type == 'square' or settings.grid_type == 'hex':
			Qfile = h5py.File(settings.meshdir + settings.grid_type + '/' + grid.basename + '/Q' + grid.basename + '.hdf5','r')
		grid.q_stencil_size[:grid.nface[i],i] = np.squeeze(np.array(Qfile['num_coeffs'],dtype=np.int32,order='F'))
		grid.q_stencil[:grid.nface[i],:,:,i] = np.array(Qfile['coeff_indices'],dtype=np.int32,order='F') +1
		grid.q_coeffs[:grid.nface[i],:,i] = np.squeeze(np.array(Qfile['coeffs'],dtype=np.float64,order='F'))
		Qfile.close()
		
		print( 'op build for grid',i+1)
		
		print( 'building i')
		grid.build_i(i+1)
		print( 'building j')
		grid.build_j(i+1)
		print( 'building e')
		grid.build_e(i+1)
		print( 'building l')
		grid.build_l(i+1)
		print( 'building r')
		grid.build_r(i+1)
		print( 'building rt')
		grid.build_rt(i+1)
		print( 'building h')
		if gtype == 'cubedsphere':
			grid.build_h_cs(i+1)
		if gtype == 'geodesic' or gtype == 'square' or gtype =='hex':
			grid.build_h_geo(i+1)
		print( 'building phi')
		grid.set_phi_coeffs(i+1)
		print( 'building phiT')
		grid.set_phit_coeffs(i+1)
		print( 'building w')
		grid.set_w_coeffs(i+1)
		print( 'building wl')
		grid.set_wl_coeffs(i+1)	
		print( 'building wd')
		grid.set_wd_coeffs(i+1)
		
		if settings.grid_type == 'geodesic':
			print( 'building laplac')
			grid.set_laplac_primal_coeffs(i+1)
			
		#ADD INJECTION STENCIL STUFF
		
def save_operator_h5py(grid):
	for i in range(grid.ngrids):
		ncells = grid.nface[i]
		if settings.grid_type == 'geodesic' or settings.grid_type == 'cubedsphere':
			opfile = h5py.File(settings.meshdir + settings.grid_type + '/' + settings.grid_variant + '/' + str(ncells) + '.operators.hdf5','w')
		if settings.grid_type == 'square' or settings.grid_type == 'hex':
			opfile = h5py.File(settings.meshdir + settings.grid_type + '/' + grid.basename + '/' + grid.basename + '.operators.hdf5','w')
		operators = opfile.create_group('operators')
		
		#print( i,np.amax(grid.w_coeffs[:,:,i]),np.amin(grid.w_coeffs[:,:,i]),np.average(grid.w_coeffs[:,:,i])
				
		operators.create_dataset('Isize',data=grid.i_stencil_size[:grid.nface[i],i])
		operators.create_dataset('Jsize',data=grid.j_stencil_size[:grid.nvert[i],i])
		operators.create_dataset('Hsize',data=grid.h_stencil_size[:grid.nedge[i],i])
		operators.create_dataset('Rsize',data=grid.r_stencil_size[:grid.nvert[i],i])
		operators.create_dataset('RTsize',data=grid.rt_stencil_size[:grid.nface[i],i])
		operators.create_dataset('Esize',data=grid.e_stencil_size[:grid.nedge[i],i])
		operators.create_dataset('Lsize',data=grid.l_stencil_size[:grid.nedge[i],i])
		operators.create_dataset('Qsize',data=grid.q_stencil_size[:grid.nface[i],i])

		operators.create_dataset('Istencil',data=grid.i_stencil[:grid.nface[i],:,i])
		operators.create_dataset('Jstencil',data=grid.j_stencil[:grid.nvert[i],:,i])
		operators.create_dataset('Hstencil',data=grid.h_stencil[:grid.nedge[i],:,i])
		operators.create_dataset('Rstencil',data=grid.r_stencil[:grid.nvert[i],:,i])
		operators.create_dataset('RTstencil',data=grid.rt_stencil[:grid.nface[i],:,i])
		operators.create_dataset('Estencil',data=grid.e_stencil[:grid.nedge[i],:,i])
		operators.create_dataset('Lstencil',data=grid.l_stencil[:grid.nedge[i],:,i])
		operators.create_dataset('Qstencil',data=grid.q_stencil[:grid.nface[i],:,:,i])

		operators.create_dataset('Icoeffs',data=grid.i_coeffs[:grid.nface[i],:,i])
		operators.create_dataset('Jcoeffs',data=grid.j_coeffs[:grid.nvert[i],:,i])
		operators.create_dataset('Hcoeffs',data=grid.h_coeffs[:grid.nedge[i],:,i])
		operators.create_dataset('Rcoeffs',data=grid.r_coeffs[:grid.nvert[i],:,i])
		operators.create_dataset('RTcoeffs',data=grid.rt_coeffs[:grid.nface[i],:,i])
		operators.create_dataset('Ecoeffs',data=grid.e_coeffs[:grid.nedge[i],:,i])
		operators.create_dataset('Lcoeffs',data=grid.l_coeffs[:grid.nedge[i],:,i])
		operators.create_dataset('Qcoeffs',data=grid.q_coeffs[:grid.nface[i],:,i])
		operators.create_dataset('Wcoeffs',data=grid.w_coeffs[:grid.nedge[i],:,i])
		operators.create_dataset('WLcoeffs',data=grid.wl_coeffs[:grid.nedge[i],:,i])
		operators.create_dataset('WDcoeffs',data=grid.wd_coeffs[:grid.nedge[i],:,i])
		operators.create_dataset('Phicoeffs',data=grid.phi_coeffs[:grid.nedge[i],:,i])
		operators.create_dataset('PhiTcoeffs',data=grid.phit_coeffs[:grid.nface[i],:,i])
		operators.create_dataset('Laplaccoeffs',data=grid.laplac_primal_coeffs[:grid.nface[i],:,i])
		#ADD INJECTION STENCIL STUFF
		opfile.close()
		
def load_operators(grid):

	for i in range(grid.ngrids):
		ncells = grid.nface[i]
		if settings.grid_type == 'geodesic' or settings.grid_type == 'cubedsphere':
			opfile = h5py.File(settings.meshdir + settings.grid_type + '/' + settings.grid_variant + '/' + str(ncells) + '.operators.hdf5','r')
		if settings.grid_type == 'square' or settings.grid_type == 'hex':
			opfile = h5py.File(settings.meshdir + settings.grid_type + '/' + grid.basename + '/' + grid.basename + '.operators.hdf5','r')
		operators = opfile['operators']
	
		#if i < grid.ngrids-1:
			#grid.inj_stencil_size[:grid.nface[i],i] = np.array(operators['injsize'],dtype=np.int32,order='F')
			#grid.inj_stencil[:grid.nface[i],:,i] = np.array(operators['injstencil'],dtype=np.int32,order='F')
			#grid.inj_weights[:grid.nface[i],:,i] = np.array(operators['injcoeffs'],dtype=np.float64,order='F')
		
		grid.i_stencil_size[:grid.nface[i],i] = np.array(operators['Isize'],dtype=np.int32,order='F')
		grid.j_stencil_size[:grid.nvert[i],i] = np.array(operators['Jsize'],dtype=np.int32,order='F')
		grid.h_stencil_size[:grid.nedge[i],i] = np.array(operators['Hsize'],dtype=np.int32,order='F')
		grid.rt_stencil_size[:grid.nface[i],i] = np.array(operators['RTsize'],dtype=np.int32,order='F')
		grid.r_stencil_size[:grid.nvert[i],i] = np.array(operators['Rsize'],dtype=np.int32,order='F')
		grid.e_stencil_size[:grid.nedge[i],i] = np.array(operators['Esize'],dtype=np.int32,order='F')
		grid.l_stencil_size[:grid.nedge[i],i] = np.array(operators['Lsize'],dtype=np.int32,order='F')
		grid.q_stencil_size[:grid.nface[i],i] = np.array(operators['Qsize'],dtype=np.int32,order='F')

		grid.i_stencil[:grid.nface[i],:,i] = np.array(operators['Istencil'],dtype=np.int32,order='F')
		grid.j_stencil[:grid.nvert[i],:,i] = np.array(operators['Jstencil'],dtype=np.int32,order='F')
		grid.h_stencil[:grid.nedge[i],:,i] = np.array(operators['Hstencil'],dtype=np.int32,order='F') 
		grid.rt_stencil[:grid.nface[i],:,i] = np.array(operators['RTstencil'],dtype=np.int32,order='F')
		grid.r_stencil[:grid.nvert[i],:,i] = np.array(operators['Rstencil'],dtype=np.int32,order='F')
		grid.e_stencil[:grid.nedge[i],:,i] = np.array(operators['Estencil'],dtype=np.int32,order='F')
		grid.l_stencil[:grid.nedge[i],:,i] = np.array(operators['Lstencil'],dtype=np.int32,order='F')
		grid.q_stencil[:grid.nface[i],:,:,i] = np.array(operators['Qstencil'],dtype=np.int32,order='F')

		grid.i_coeffs[:grid.nface[i],:,i] = np.array(operators['Icoeffs'],dtype=np.float64,order='F')
		grid.j_coeffs[:grid.nvert[i],:,i] = np.array(operators['Jcoeffs'],dtype=np.float64,order='F')
		grid.h_coeffs[:grid.nedge[i],:,i] = np.array(operators['Hcoeffs'],dtype=np.float64,order='F')
		grid.rt_coeffs[:grid.nface[i],:,i] = np.array(operators['RTcoeffs'],dtype=np.float64,order='F')
		grid.r_coeffs[:grid.nvert[i],:,i] = np.array(operators['Rcoeffs'],dtype=np.float64,order='F')
		grid.q_coeffs[:grid.nface[i],:,i] = np.array(operators['Qcoeffs'],dtype=np.float64,order='F')
		grid.e_coeffs[:grid.nedge[i],:,i] = np.array(operators['Ecoeffs'],dtype=np.float64,order='F')
		grid.l_coeffs[:grid.nedge[i],:,i] = np.array(operators['Lcoeffs'],dtype=np.float64,order='F')
		grid.w_coeffs[:grid.nedge[i],:,i] = np.array(operators['Wcoeffs'],dtype=np.float64,order='F')
		grid.wl_coeffs[:grid.nedge[i],:,i] = np.array(operators['WLcoeffs'],dtype=np.float64,order='F')
		grid.wd_coeffs[:grid.nedge[i],:,i] = np.array(operators['WDcoeffs'],dtype=np.float64,order='F')
		grid.phi_coeffs[:grid.nedge[i],:,i] = np.array(operators['Phicoeffs'],dtype=np.float64,order='F')
		grid.phit_coeffs[:grid.nface[i],:,i] = np.array(operators['PhiTcoeffs'],dtype=np.float64,order='F')
		grid.laplac_primal_coeffs[:grid.nface[i],:,i] = np.array(operators['Laplaccoeffs'],dtype=np.float64,order='F')

		grid.h_stencil_max = np.amax(grid.h_stencil_size)
		
		opfile.close()
		
def check_connectivity(grid):
	for i in range(grid.ngrids):
		
		print( 'checking connectivity for grid',i+1)
		nedge = grid.nedge[i]
		nvert = grid.nvert[i]
		nface = grid.nface[i]
		
		D1 =  create_scipy_sparse_matrix(grid.ve[:nedge,:,i],np.ones(nedge,dtype=np.int32)*2,grid.tev_edge[:nedge,:,i],nedge,nvert)
		D2 = create_scipy_sparse_matrix(grid.ec[:nface,:,i],grid.ecsize[:nface,i],grid.nei_cell[:nface,:,i],nface,nedge)
		D1bar =  create_scipy_sparse_matrix(grid.ce[:nedge,:,i],np.ones(nedge,dtype=np.int32)*2,-1*grid.nei_edge[:nedge,:,i],nedge,nface)
		D2bar = create_scipy_sparse_matrix(grid.ev[:nvert,:,i],grid.evsize[:nvert,i],grid.tev_vertex[:nvert,:,i],nvert,nedge)
		
		test = D2 * D1
		print( 'D2 D1',test.data.sum())
		test = D2bar * D1bar
		print( 'Dbar2 Dbar1',test.data.sum())
		test = D2.T + D1bar
		print( 'D2^T = - Dbar1',test.data.sum())
		test = D2bar.T - D1
		print( 'Dbar2^T = D1',test.data.sum())
		
		#ALSO DOMAIN SUM STUFF SUCH AS SUM(D2) = 0, etc.

def create_scipy_sparse_matrix(stncl,stncl_size,stncl_coeffs,M,N):
	stencil = np.squeeze(np.copy(stncl))
	stencil_size = np.squeeze(np.copy(stncl_size))
	stencil_coeffs = np.squeeze(np.copy(stncl_coeffs))
	zero_stencil = np.logical_not(np.equal(stencil,0))
	indptr = np.cumsum(stencil_size)
	indptr = np.insert(indptr,0,0)
	data = stencil_coeffs[zero_stencil]
	indices = stencil[zero_stencil] - 1
	return spsparse.csr_matrix((data,indices,indptr),shape=(M,N))

def check_operators(grid):
	for i in range(grid.ngrids):
		print( 'checking operators for grid',i+1)
		nedge = grid.nedge[i]
		nvert = grid.nvert[i]
		nface = grid.nface[i]
		
		D1 =  create_scipy_sparse_matrix(grid.ve[:nedge,:,i],np.ones(nedge,dtype=np.int32)*2,grid.tev_edge[:nedge,:,i],nedge,nvert)
		D2 = create_scipy_sparse_matrix(grid.ec[:nface,:,i],grid.ecsize[:nface,i],grid.nei_cell[:nface,:,i],nface,nedge)
		D1bar =  create_scipy_sparse_matrix(grid.ce[:nedge,:,i],np.ones(nedge,dtype=np.int32)*2,-1*grid.nei_edge[:nedge,:,i],nedge,nface)
		D2bar = create_scipy_sparse_matrix(grid.ev[:nvert,:,i],grid.evsize[:nvert,i],grid.tev_vertex[:nvert,:,i],nvert,nedge)
		W = create_scipy_sparse_matrix(grid.ecp[:nedge,:,i],grid.ecpsize[:nedge,i],grid.w_coeffs[:nedge,:,i],nedge,nedge)
		Wd = create_scipy_sparse_matrix(grid.ecpd[:nedge,:,i],grid.ecpdsize[:nedge,i],grid.wd_coeffs[:nedge,:,i],nedge,nedge)
		WL = create_scipy_sparse_matrix(grid.ecp[:nedge,:,i],grid.ecpsize[:nedge,i],grid.wl_coeffs[:nedge,:,i],nedge,nedge)
		RT = create_scipy_sparse_matrix(grid.rt_stencil[:nface,:,i],grid.rt_stencil_size[:nface,i],grid.rt_coeffs[:nface,:,i],nface,nvert)
		R = create_scipy_sparse_matrix(grid.r_stencil[:nvert,:,i],grid.r_stencil_size[:nvert,i],grid.r_coeffs[:nvert,:,i],nvert,nface)
		rivd = grid.aiv[:nface,:,i] / grid.av[grid.vc[:nface,:,i]-1,i]
		Rd = create_scipy_sparse_matrix(grid.vc[:nface,:,i],grid.ecsize[:nface,i],rivd,nface,nvert) 
		I = create_scipy_sparse_matrix(grid.i_stencil[:nface,:,i],grid.i_stencil_size[:nface,i],grid.i_coeffs[:nface,:,i],nface,nface)
		J = create_scipy_sparse_matrix(grid.j_stencil[:nvert,:,i],grid.j_stencil_size[:nvert,i],grid.j_coeffs[:nvert,:,i],nvert,nvert)
		H = create_scipy_sparse_matrix(grid.h_stencil[:nedge,:,i],grid.h_stencil_size[:nedge,i],grid.h_coeffs[:nedge,:,i],nedge,nedge)
		#PhiT = 
		#Wd =
		Iinv = I.copy()
		I.data = 1.0/I.data
		Jinv = J.copy()
		J.data = 1.0/J.data
		if settings.grid_type == 'geodesic' or settings.grid_type == 'square' or settings.grid_type == 'hex':
			Hinv = H.copy()
			Hinv.data = 1.0/Hinv.data
		
		#Hodge star and phi tests
		test = I * Iinv
		test.data = test.data - 1.0 #relies on fact that I and Iinv are diagonal
		print( 'I Iinv = 1.0',np.amin(test.data),np.amax(test.data),np.average(test.data),test.data.sum())
		test = Iinv * I 
		test.data = test.data - 1.0 #relies on fact that I and Iinv are diagonal
		print( 'Iinv I = 1.0',np.amin(test.data),np.amax(test.data),np.average(test.data),test.data.sum())
		test = J * Jinv 
		test.data = test.data - 1.0 #relies on fact that J and Jinv are diagonal
		print( 'J Jinv = 1.0',np.amin(test.data),np.amax(test.data),np.average(test.data),test.data.sum())
		test = Jinv * J 
		test.data = test.data - 1.0 #relies on fact that J and Jinv are diagonal
		print( 'Jinv J = 1.0',np.amin(test.data),np.amax(test.data),np.average(test.data),test.data.sum())
		if settings.grid_type == 'cubedsphere':
			v = np.random.rand(nedge)
			x = apply_Hinv(v,H)
			test = H * x - v
			print( 'H Hinv = 1.0',np.amin(test),np.amax(test),np.average(test),test.sum())
			v = np.random.rand(nedge)
			x = H * v
			y = apply_Hinv(x,H)
			test = y - v
			print( 'Hinv H = 1.0',np.amin(test),np.amax(test),np.average(test),test.sum())
		else:
			test = H * Hinv
			test.data = test.data - 1.0 #relies on fact that H and Hinv are diagonal
			print( 'H Hinv = 1.0',np.amin(test.data),np.amax(test.data),np.average(test.data),test.data.sum())
			test = Hinv * H
			test.data = test.data - 1.0 #relies on fact that H and Hinv are diagonal
			print( 'Hinv H = 1.0',np.amin(test.data),np.amax(test.data),np.average(test.data),test.data.sum())
		test = np.sum(grid.phi_coeffs[:nedge,:,i],axis=1) - 1.0
		print( 'sum Phi = 1',np.amin(test),np.amax(test),np.average(test),np.sum(test))
		#Phi^T = PhiT
		#E and L checks
		
		#TRISK-specific tests
		test = W + W.T
		if test.data.shape[0] != 0:
			print( 'W = -W^T',np.amin(test.data),np.amax(test.data),np.average(test.data),test.data.sum())
		else:
			print( 'W = -W^T',0)
		test = R*D2 - D2bar * W
		if test.data.shape[0] != 0:
			print( 'RD2 = Dbar2W',np.amin(test.data),np.amax(test.data),np.average(test.data),test.data.sum())
		else:
			print( 'RD2 = Dbar2W',0)
		test = D1bar*RT - W*D1
		if test.data.shape[0] != 0:
			print( 'Dbar1RT = WD1',np.amin(test.data),np.amax(test.data),np.average(test.data),test.data.sum())
		else:
			print( 'Dbar1RT = WD1',0)
		test = np.sum(grid.r_coeffs[:nface,:,i],axis=1) - 1.0
		print( 'sum R for each cell = 1',np.amin(test),np.amax(test),np.average(test),np.sum(test))
		test = WL + WL.T
		if test.data.shape[0] != 0:
			print( 'WL = -WL^T',np.amin(test.data),np.amax(test.data),np.average(test.data),test.data.sum())
		else:
			print( 'WL = -WL^T',0)
		test = W - WL
		print( 'QL = W',np.amin(test.data),np.amax(test.data),np.average(test.data),test.data.sum())
		test = Wd + Wd.T
		print( 'Wd = -Wd^T',np.amin(test.data),np.amax(test.data),np.average(test.data),test.data.sum())
		test = Rd*D2bar - D2 * Wd
		print( 'RTdD2bar = D2Wd',np.amin(test.data),np.amax(test.data),np.average(test.data),test.data.sum())
		test = D1*(Rd.T) - Wd*D1bar
		print( 'D1Rd = WdD1bar',np.amin(test.data),np.amax(test.data),np.average(test.data),test.data.sum())
		
		#D2bar * W * D1 = 0.0
		#J * R * Iinv = nverts
		#Q(A,B) = -Q(B,A)
		
		#BVE OPERATOR CHECKS- SUM(JACOBIAN) = 0, SUM(A*JACOBIAN)=0, J = -J^T, etc.
		#EVENTUALLY ADD CHECKS FOR ZGRID OPERATORS- JACOBIANS SUMS, ETC.
	
def deallocate_unused(grid):
	if settings.scheme == 'TRISK':
		grid.laplac_primal_coeffs = None
		#grid.q_stencil_size = None
		#grid.q_stencil = None
		#grid.e_stencil = None
		grid.l_stencil = None
		#grid.e_coeffs = None
		grid.l_coeffs = None
		#grid.e_stencil_size = None
		grid.l_stencil_size = None
		
	if settings.scheme == 'ZGRID':
		grid.av = None
		grid.ae = None
		grid.Aiv = None
		grid.Aie = None
		grid.clon = None
		grid.clat = None
		#LOTS MORE HERE
