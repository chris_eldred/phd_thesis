from constants import *
from mesh import *
from configuration import *
import sys
import grid
from basemap_plotting_helpers import *
from initialize import *
from scipy.stats import binned_statistic

#get configuration
get_configuration(sys.argv[1])
settings.scheme = sys.argv[2]
settings.grid_type = sys.argv[3]
settings.grid_variant = sys.argv[4]
settings.tempname = settings.tempname +'.'+ settings.grid_type +'.'+ settings.grid_variant +'.'+ str(settings.vlevel) + '.' + settings.trisk_variant +'.' + settings.simname
print('configured')

#load operators and connectivity
if settings.grid_type == 'cubedsphere':
	set_grid_sizes_cubedsphere(grid.grid,settings.vlevel)
	allocate_connectivity_cubedsphere(grid.grid)
	if settings.scheme == 'ZGRID':
		allocate_geometry_cubedsphere(grid.grid)
	allocate_operators_cubedsphere(grid.grid)
if settings.grid_type == 'geodesic':
	set_grid_sizes_geodesic(grid.grid,settings.vlevel)
	allocate_connectivity_geodesic(grid.grid)
	allocate_operators_geodesic(grid.grid)
	if settings.scheme == 'ZGRID':
		allocate_geometry_geodesic(grid.grid)
if settings.grid_type == 'square':
	set_grid_sizes_square(grid.grid,settings.nx,settings.ny)
	allocate_connectivity_square(grid.grid)
	allocate_operators_square(grid.grid)
	if settings.scheme == 'ZGRID':
		allocate_geometry_square(grid.grid)
if settings.grid_type == 'hex':
	set_grid_sizes_hex(grid.grid,settings.nx,settings.ny)
	allocate_connectivity_hex(grid.grid)
	allocate_operators_hex(grid.grid)
	if settings.scheme == 'ZGRID':
		allocate_geometry_hex(grid.grid)
		
load_connectivity(grid.grid)
load_operators(grid.grid)
if settings.grid_type == 'square' or settings.grid_type == 'hex':
	settings.a = settings.dx
grid.grid.dimensionalize_operators(settings.a)
if settings.scheme == 'ZGRID':
	load_geometry(grid.grid)
	grid.grid.dimensionalize_geometry(settings.a)
#deallocate_unused(grid.grid)
grid.H = create_scipy_sparse_matrix(grid.grid.h_stencil[:grid.grid.nedge[settings.vlevel-1],:,settings.vlevel-1],grid.grid.h_stencil_size[:grid.grid.nedge[settings.vlevel-1],settings.vlevel-1],
grid.grid.h_coeffs[:grid.grid.nedge[settings.vlevel-1],:,settings.vlevel-1],grid.grid.nedge[settings.vlevel-1],grid.grid.nedge[settings.vlevel-1])
print('operators and connectivity loaded!')

if settings.scheme == 'TRISK':
	grid.mi,grid.ue,grid.fv,grid.bi = initialize_vars()
	if settings.initial_condition == 'heldsuarezdry':
		grid.mi_eqm = np.copy(grid.mi)
		grid.ue_eqm = np.copy(grid.ue)
		grid.tau_h = 100. * 86400.
		grid.tau_u = 100. * 86400.
		#grid.mi = np.zeros(grid.mi_eqm.shape,dtype=np.float64)
		grid.ue = np.zeros(grid.ue_eqm.shape,dtype=np.float64)

if settings.scheme == 'ZGRID':
	grid.hi,grid.zeta,grid.delta,grid.fi,grid.bi = initialize_vars()

	if settings.initial_condition == 'heldsuarezdry':
		grid.hi_eqm = np.copy(grid.hi)
		grid.zeta_eqm = np.copy(grid.zeta)
		grid.delta_eqm = np.copy(grid.delta)
		grid.tau_h = 100. * 86400.
		grid.tau_u = 100. * 86400.
		#grid.hi = np.zeros(grid.hi_eqm.shape,dtype=np.float64)
		grid.zeta = np.zeros(grid.zeta_eqm.shape,dtype=np.float64)
		grid.delta = np.zeros(grid.delta_eqm.shape,dtype=np.float64)

#calculate hyperviscosity
if settings.scheme == 'TRISK':
	grid.gd = grid.grid.graddiv(grid.ue,settings.vlevel,grid.grid.nface[-1])
	grid.cc = grid.grid.curlcurl(grid.ue,settings.vlevel,grid.grid.nvert[-1])
	#cc = apply_Hinv(grid.grid.curlcurl_part(grid.ue,settings.vlevel,grid.grid.nvert[-1]),grid.H)
	ldelta = grid.grid.i(grid.grid.d2(grid.grid.h(grid.gd + grid.cc,settings.vlevel),settings.vlevel,grid.grid.nface[-1]),settings.vlevel)
	lzeta = grid.grid.j(grid.grid.d2bar(grid.cc + grid.gd,settings.vlevel,grid.grid.nvert[-1]),settings.vlevel)

if settings.scheme == 'ZGRID':
	ldelta = grid.grid.laplac_primal(grid.delta,settings.vlevel)
	lzeta = grid.grid.laplac_primal(grid.zeta,settings.vlevel)

#diagnose quantities
if settings.scheme == 'TRISK':
	nface = grid.grid.nface[-1]
	nvert = grid.grid.nvert[-1]
	nedge = grid.grid.nedge[-1]
	zetav = grid.grid.d2bar(grid.ue,settings.vlevel,nvert)
	di = grid.grid.d2(grid.grid.h(grid.ue,settings.vlevel),settings.vlevel,nface)
	mv = grid.grid.r(grid.mi,settings.vlevel,nvert)
	grid.hi = grid.grid.i(grid.mi,settings.vlevel)
	grid.zetav = grid.grid.j(zetav,settings.vlevel)
	grid.etav = grid.grid.j(zetav + grid.fv,settings.vlevel)
	grid.qv = (zetav + grid.fv)/mv
	fv = np.copy(grid.fv)
	grid.fv = grid.grid.j(fv,settings.vlevel)
	grid.di = grid.grid.i(di,settings.vlevel)

if settings.scheme == 'ZGRID':
	grid.eta = grid.zeta + grid.fi
	grid.q = grid.eta / grid.hi

#plot initial conditions, including diagnostic quantities; and hyperviscosity
if settings.scheme == 'TRISK':
	variables_list = [('hi','primal','Height at Cell Centers'),('zetav','dual','Relative Vorticity at Cell Corners'),
('etav','dual','Absolute Vorticity at Cell Corners'),
('qv','dual','Potential Vorticity at Cell Corners'),
('fv','dual','Coriolis at Cell Corners'),
('di','primal','Divergence at Cell Centers'),
('ldelta','primal','Delta Hypervisc at Cell Centers'),
('lzeta','dual','Zeta Hypervisc at Cell Corners'),
('gd','edge','Grad Div at Cell Edges'),
('cc','edge','Curl Curl at Cell Edges'),
('hi_eqm','primal','Height Forcing at Cell Centers'),
('zeta_eqm','dual','Relative Vorticity Forcing at Cell Corners'),
('delta_eqm','primal','Divergence Forcing at Cell Centers'),
('bi','primal','Topography at Cell Centers')]

if settings.scheme == 'ZGRID':
	variables_list = [('hi','primal','Height at Cell Centers'),
('zeta','primal','Relative Vorticity at Cell Centers'),
('q','primal','Potential Vorticity at Cell Centers'),
('eta','primal','Absolute Vorticity at Cell Centers'),
('fi','primal','Coriolis at Cell Centers'),
('delta','primal','Divergence at Cell Centers'),
('ldelta','primal','Delta Hypervisc at Cell Centers'),
('lzeta','primal','Zeta Hypervisc at Cell Centers'),
('hi_eqm','primal','Height Forcing at Cell Centers'),
('zeta_eqm','primal','Relative Vorticity Forcing at Cell Centers'),
('delta_eqm','primal','Divergence Forcing at Cell Centers'),
('bi','primal','Topography at Cell Centers')]

gridtype = settings.grid_type
gridvariant = settings.grid_variant
vlevel = settings.vlevel
meshdata = get_geometry(gridtype,gridvariant,vlevel)
variables = {}
if settings.scheme == 'TRISK':
	variables['hi'] = grid.hi
	variables['etav'] = grid.etav
	variables['zetav'] = grid.zetav
	variables['qv'] = grid.qv
	variables['di'] = grid.di
	variables['fv'] = grid.fv
	variables['bi'] = grid.bi
	variables['gd'] = grid.gd
	variables['cc'] = grid.cc

variables['hi_eqm'] = None
variables['zeta_eqm'] = None
variables['delta_eqm'] = None


if settings.initial_condition == 'heldsuarezdry':
	if settings.scheme == 'ZGRID':
		variables['hi_eqm'] = grid.hi_eqm
		variables['zeta_eqm'] = grid.zeta_eqm
		variables['delta_eqm'] = grid.delta_eqm
		ldelta = grid.grid.laplac_primal(grid.delta_eqm,settings.vlevel)
		lzeta = grid.grid.laplac_primal(grid.zeta_eqm,settings.vlevel)

	if settings.scheme == 'TRISK':
		variables['hi_eqm'] = grid.grid.i(grid.mi_eqm,settings.vlevel)
		variables['zeta_eqm'] = grid.grid.j(grid.grid.d2bar(grid.ue_eqm,settings.vlevel,nvert),settings.vlevel)
		variables['delta_eqm'] = grid.grid.i(grid.grid.d2(grid.grid.h(grid.ue_eqm,settings.vlevel),settings.vlevel,nface),settings.vlevel)
		grid.hi_eqm = grid.grid.i(grid.mi_eqm,settings.vlevel)
		grid.zeta_eqm = grid.grid.j(grid.grid.d2bar(grid.ue_eqm,settings.vlevel,nvert),settings.vlevel)
		grid.delta_eqm = grid.grid.i(grid.grid.d2(grid.grid.h(grid.ue_eqm,settings.vlevel),settings.vlevel,nface),settings.vlevel)
		grid.gd = grid.grid.graddiv(grid.ue_eqm,settings.vlevel,grid.grid.nface[-1])
		grid.cc = grid.grid.curlcurl(grid.ue_eqm,settings.vlevel,grid.grid.nvert[-1])
		#cc = apply_Hinv(grid.grid.curlcurl_part(grid.ue,settings.vlevel,grid.grid.nvert[-1]),grid.H)
		ldelta = grid.grid.i(grid.grid.d2(grid.grid.h(grid.gd + grid.cc,settings.vlevel),settings.vlevel,grid.grid.nface[-1]),settings.vlevel)
		lzeta = grid.grid.j(grid.grid.d2bar(grid.cc + grid.gd,settings.vlevel,grid.grid.nvert[-1]),settings.vlevel)
		variables['gd'] = grid.gd
		variables['cc'] = grid.cc
	print np.amax(grid.zeta_eqm),np.amin(grid.zeta_eqm),np.average(grid.zeta_eqm)
	print np.amax(grid.hi_eqm),np.amin(grid.hi_eqm),np.average(grid.hi_eqm)
	print np.amax(grid.delta_eqm),np.amin(grid.delta_eqm),np.average(grid.delta_eqm)

variables['ldelta'] = ldelta
variables['lzeta'] = lzeta

if settings.scheme == 'ZGRID':
	variables['hi'] = grid.hi
	variables['eta'] = grid.eta
	variables['zeta'] = grid.zeta
	variables['q'] = grid.q
	variables['delta'] = grid.delta
	variables['fi'] = grid.fi
	variables['bi'] = grid.bi

def plot_zonal_mean(data,dataname,lats,fname,name):
	plt.figure()
	plt.plot(data,lats,label=dataname)
	plt.title(name)
	plt.legend()
	plt.ylabel('Latitude')
	plt.savefig('testinit/' + fname + '.' + settings.scheme + '.png')

def get_zonal_means(y):
	sy, _, _ = binned_statistic(x,y,statistic='sum',bins=bins)
	return sy

#compute zonal averages
bins = np.linspace(-80,80,320)
bins_adj = np.cumsum(np.diff(bins))
lats = bins_adj - 80. - bins_adj[0]/2.

data = grid.hi_eqm
areas = meshdata.Ai
areaweighted_data = np.expand_dims(data * areas,axis=1)
x = meshdata.clats
zonal_means = get_zonal_means(areaweighted_data[:,0])
area_means,_,_ = binned_statistic(x,areas,statistic='sum',bins=bins)
zonal_means = zonal_means / area_means	
plot_zonal_mean(zonal_means,'',lats,'hzm','Height Forcing Zonal Mean')

data = grid.delta_eqm
areas = meshdata.Ai
areaweighted_data = np.expand_dims(data * areas,axis=1)
x = meshdata.clats
zonal_means = get_zonal_means(areaweighted_data[:,0])
area_means,_,_ = binned_statistic(x,areas,statistic='sum',bins=bins)
zonal_means = zonal_means / area_means	
plot_zonal_mean(zonal_means,'',lats,'dzm','Divergence Forcing Zonal Mean')

if settings.scheme == 'TRISK':
	areas = meshdata.Av
	x = meshdata.vlats
if settings.scheme == 'ZGRID':
	areas = meshdata.Ai
	x = meshdata.clats
data = grid.zeta_eqm
areaweighted_data = np.expand_dims(data * areas,axis=1)
zonal_means = get_zonal_means(areaweighted_data[:,0])
area_means,_,_ = binned_statistic(x,areas,statistic='sum',bins=bins)
zonal_means = zonal_means / area_means	
plot_zonal_mean(zonal_means,'',lats,'zzm','Vorticity Forcing Zonal Mean')

#if varloc == 'dual':
#areas = meshdata.Av
#areaweighted_data = data * areas
#x = meshdata.vlats
#zonal_means = np.apply_along_axis(get_zonal_means,1,areaweighted_data)
#area_means,_,_ = binned_statistic(x,areas,statistic='sum',bins=bins)

#if varloc == 'primal':


#plot_zonal_mean(zonal_stddev,'',lats,'hstdev','Height Forcing Zonal Standard Deviation')

for varname,varloc,longname in variables_list:
	print 'plotting',varname
	data = variables[varname]
	if np.sum(data) == 0.0:
		print 'zero',varname
		continue
	if data == None:
		print 'none',varname
		continue
	plot_scalar(meshdata,data,varloc,longname, \
		'testinit/'+ varname + '.' + settings.scheme + '.' + gridtype + '.' + gridvariant + '.' + str(vlevel),'latlon','contour',None)
	plot_scalar(meshdata,data,varloc,longname, \
		'testinit/'+ varname + '.' + settings.scheme + '.' + gridtype + '.' + gridvariant + '.' + str(vlevel),'spherical','grid',None)



