import matplotlib.pyplot as plt
import numpy as np
import h5py
import sys
from configuration import *
from math import ceil,sqrt,pow
from basemap_plotting_helpers import *
from constants import *


#get configuration
get_configuration(sys.argv[1])
get_eigenconfig(sys.argv[1])
if settings.use_dissipation == False:
	disspdir = 'none'
if settings.use_dissipation == True:
	disspdir = 'hyper'
basedir = settings.outdir + settings.simname + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(settings.vlevel) + '-' + settings.trisk_variant + '/' + disspdir + '/'
outdir = settings.outdir + settings.simname + '/plots/' + disspdir + '/' + settings.grid_type + '.' + settings.grid_variant + '.' + str(settings.vlevel) +  '.' + settings.trisk_variant + '.' + disspdir + '.'

if settings.grid_type == 'geodesic':
	nfaces = 10*(2**settings.vlevel)**2 + 2
	nverts = 20*(2**settings.vlevel)**2
	nedges = 30*(2**settings.vlevel)**2
	
if settings.grid_type == 'cubedsphere':
	n0 = 3
	nfaces = int(pow(n0*pow(2,settings.vlevel-1),2) * 6)
	nedges = 2*nfaces
	nverts = nfaces + 2

if settings.scheme == 'TRISK':
	nstationary = nverts
	nrossby = nverts
	ninertia = 2*nfaces-2
	ndofs = nedges + nfaces
	operators = ['Div','Grad','Perp','A']

if settings.scheme == 'ZGRID':
	nstationary = nfaces
	nrossby = nfaces
	ninertia = 2*nfaces
	ndofs = 3*nfaces
	operators = ['Laplac','A','J','FD']

#load eigenvalues from file
eigenpostfile = h5py.File(basedir + 'eigen.post.hdf5', 'r')

igw_real_pos = np.array(eigenpostfile['igw_real_pos'])
igw_real_neg = np.array(eigenpostfile['igw_real_neg'])
igw_imag_pos = np.array(eigenpostfile['igw_imag_pos'])
igw_imag_neg = np.array(eigenpostfile['igw_imag_neg'])
ig_index = np.arange(igw_real_pos.shape[0])

if settings.variable_f == False:
	stat_real_pos = np.array(eigenpostfile['stat_real_pos'])
	stat_real_neg = np.array(eigenpostfile['stat_real_neg'])
	stat_imag_pos = np.array(eigenpostfile['stat_imag_pos'])
	stat_imag_neg = np.array(eigenpostfile['stat_imag_neg'])
	stat_index = np.arange(stat_real_pos.shape[0])
if settings.variable_f == True:
	rossby_real_pos = np.array(eigenpostfile['rossby_real_pos'])
	rossby_real_neg = np.array(eigenpostfile['rossby_real_neg'])
	rossby_imag_pos = np.array(eigenpostfile['rossby_imag_pos'])
	rossby_imag_neg = np.array(eigenpostfile['rossby_imag_neg'])
	rossby_index = np.arange(rossby_real_pos.shape[0])

eigenpostfile.close()

def real_dispersion_plot(mode_indices0,modes0,mode_indices1,modes1,theoretical_vals,modetype):
	plt.figure(figsize=(12, 6), dpi=100)
	plt.scatter(mode_indices0,modes0,c='k',marker='+')
	if theoretical_vals != None:
		plt.scatter(mode_indices0,theoretical_vals,facecolors='none', edgecolors='g',marker='o')
	plt.scatter(mode_indices1,modes1,c='r',marker='+')
	plt.xlabel('Mode Index')
	plt.ylabel('Normalized Real Frequency (omega/f)')
	plt.tight_layout()
	plt.savefig(outdir + 'omega.real.' + modetype + '.png',bbox_inches='tight')

def imag_dispersion_plot(mode_indices0,modes0,mode_indices1,modes1,modetype):
	plt.figure(figsize=(12, 6), dpi=100)
	plt.scatter(mode_indices0,modes0,c='k',marker='+')
	plt.scatter(mode_indices1,modes1,c='r',marker='+')
	mode0_max = np.amax(modes0)
	mode0_min = np.amin(modes0)
	mode1_max = np.amax(modes1)
	mode1_min = np.amin(modes1)
	plt.ylim(0.9*min(mode0_min,mode1_min),1.1*max(mode0_max,mode1_max))
	plt.xlabel('Mode Index')
	plt.ylabel('Imaginary Frequency')
	plt.tight_layout()
	plt.savefig(outdir + 'omega.imag.' + modetype + '.png',bbox_inches='tight')

#determine theoretical eigenvalues
gridfile = h5py.File(settings.meshdir + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(nfaces) + '.hdf5','r')
de = np.array(gridfile['geometry']['de'],dtype=np.float64,order='F')
gridfile.close()
dx = np.average(de) * settings.a
Hbar = settings.f*settings.f * settings.C*settings.C * dx * dx / settings.g
lambda2 = settings.g * Hbar/settings.f/settings.f
lambda2scaled = lambda2/settings.a/settings.a
N = int(ceil(sqrt(ninertia/2 - 1)))
N_array = np.zeros(N*N+1,dtype=np.int32)
m_array = np.zeros(N*N+1,dtype=np.int32)
j = 0
for k in range(0,N):
	for l in range(-k,k+1):
		N_array[j] = k
		m_array[j] = l
		j = j+1
theoretical_igw_modes = np.sqrt(1. + N_array * (N_array+1.) * lambda2scaled)


if settings.variable_f == False:
	real_dispersion_plot(ig_index,igw_real_pos,stat_index,stat_real_pos,theoretical_igw_modes[:ninertia/2],'pos')
	real_dispersion_plot(ig_index,-1*igw_real_neg,stat_index,stat_real_neg,theoretical_igw_modes[:ninertia/2],'neg')
	imag_dispersion_plot(ig_index,igw_imag_pos,stat_index,stat_imag_pos,'pos')
	imag_dispersion_plot(ig_index,igw_imag_neg,stat_index,stat_imag_neg,'neg')

if settings.variable_f == True:
	real_dispersion_plot(ig_index,igw_real_pos,rossby_index,rossby_real_pos,None,'pos')
	real_dispersion_plot(ig_index,-1*igw_real_neg,rossby_index,1*rossby_real_neg,None,'neg')
	imag_dispersion_plot(ig_index,igw_imag_pos,rossby_index,rossby_imag_pos,'pos')
	imag_dispersion_plot(ig_index,igw_imag_neg,rossby_index,rossby_imag_neg,'neg')


