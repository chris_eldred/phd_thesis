from constants import *
from mesh import *
from configuration import *
import sys
import time
import grid
import matplotlib.pyplot as plt
from scheme import *
from output import *
from initialize import *

#get configuration
get_configuration(sys.argv[1])
print 'configured'

settings.vlevel = 3

#load operators and connectivity
set_grid_sizes_geodesic(grid.grid,settings.vlevel)
allocate_connectivity_geodesic(grid.grid)
allocate_operators_geodesic(grid.grid)
allocate_geometry_geodesic(grid.grid)
	
load_geometry(grid.grid)
load_connectivity(grid.grid)
load_operators(grid.grid)
grid.grid.dimensionalize_operators(settings.a)
grid.grid.dimensionalize_geometry(settings.a)

print 'operators and connectivity loaded!'

#create vars
create_scheme_vars()
print 'vars created'


ngrids = settings.vlevel
niters = 100

nvert = grid.grid.nvert[ngrids-1]
nface = grid.grid.nface[ngrids-1]
nedge = grid.grid.nedge[ngrids-1]

I = create_scipy_sparse_matrix(grid.grid.i_stencil[:nface,:,ngrids-1],grid.grid.i_stencil_size[:nface,ngrids-1],grid.grid.i_coeffs[:nface,:,ngrids-1],nface,nface)
H = create_scipy_sparse_matrix(grid.grid.h_stencil[:nedge,:,ngrids-1],grid.grid.h_stencil_size[:nedge,ngrids-1],grid.grid.h_coeffs[:nedge,:,ngrids-1],nedge,nedge)
D1 =  create_scipy_sparse_matrix(grid.grid.ve[:nedge,:,ngrids-1],np.ones(nedge,dtype=np.int32)*2,grid.grid.tev_edge[:nedge,:,ngrids-1],nedge,nvert)
D2 = create_scipy_sparse_matrix(grid.grid.ec[:nface,:,ngrids-1],grid.grid.ecsize[:nface,ngrids-1],grid.grid.nei_cell[:nface,:,ngrids-1],nface,nedge)
D1bar =  create_scipy_sparse_matrix(grid.grid.ce[:nedge,:,ngrids-1],np.ones(nedge,dtype=np.int32)*2,-1*grid.grid.nei_edge[:nedge,:,ngrids-1],nedge,nface)
D2bar = create_scipy_sparse_matrix(grid.grid.ev[:nvert,:,ngrids-1],grid.grid.evsize[:nvert,ngrids-1],grid.grid.tev_vertex[:nvert,:,ngrids-1],nvert,nedge)
R = create_scipy_sparse_matrix(grid.grid.cv[:nvert,:,ngrids-1],grid.grid.evsize[:nvert,ngrids-1],np.ones((nvert,3),dtype=np.float64)*1./3.,nvert,nface)
NJ1 = create_scipy_sparse_matrix(grid.grid.ce[:nedge,0,ngrids-1],np.ones(nedge,dtype=np.int32),grid.grid.nei_edge[:nedge,0,ngrids-1],nedge,nface)
NJ2 = create_scipy_sparse_matrix(grid.grid.ce[:nedge,1,ngrids-1],np.ones(nedge,dtype=np.int32),grid.grid.nei_edge[:nedge,1,ngrids-1],nedge,nface)
J1 = create_scipy_sparse_matrix(grid.grid.ce[:nedge,0,ngrids-1],np.ones(nedge,dtype=np.int32),np.ones(nedge),nedge,nface)
J2 = create_scipy_sparse_matrix(grid.grid.ce[:nedge,1,ngrids-1],np.ones(nedge,dtype=np.int32),np.ones(nedge),nedge,nface)
Phi = create_scipy_sparse_matrix(grid.grid.ce[:nedge,:,ngrids-1],np.ones(nedge,dtype=np.int32)*2,np.ones((nedge,2),dtype=np.float64)*0.5,nedge,nface)
Iinv = create_scipy_sparse_matrix(grid.grid.i_stencil[:nface,:,ngrids-1],grid.grid.i_stencil_size[:nface,ngrids-1],1.0/grid.grid.i_coeffs[:nface,:,ngrids-1],nface,nface)
K = create_scipy_sparse_matrix(grid.grid.ec[:nface,:,ngrids-1],grid.grid.ecsize[:nface,ngrids-1],np.ones((nface,6)),nface,nedge)

L = I * D2 * H * D1bar
ID2 = I * D2
HD1bar = H * D1bar
D1R = D1 * R
IK = I * K

testvar = np.random.rand(nface)
testvar2 = np.random.rand(nface)
testvar3 = np.random.rand(nface)
testvar4 = np.random.rand(nface)
testvar5 = np.random.rand(nface)
print 

############  Laplacian  #################
start = time.time()
for i in range(niters):
	test = L * testvar
end = time.time()
print 'Sparse L time',(end-start)/niters

start = time.time()
for i in range(niters):
	testop = grid.grid.laplac_primal(testvar,ngrids)
end = time.time()
print 'f2py L time',(end-start)/niters
diff1 = test - testop
print np.amax(diff1/test),np.amin(diff1/test),np.average(diff1/test)
print 

############  Flux Div  #################
start = time.time()
for i in range(niters):
	test = ID2 * np.multiply(Phi * testvar, HD1bar * testvar2)
end = time.time()
print 'Sparse FD time',(end-start)/niters

start = time.time()
for i in range(niters):
	testop = grid.grid.fluxdiv(testvar,testvar2,ngrids)
end = time.time()
print 'f2py FD time',(end-start)/niters
diff1 = test - testop
print np.amax(diff1/test),np.amin(diff1/test),np.average(diff1/test)
print 

############  Jacobian  #################
start = time.time()
for i in range(niters):
	test = -1./2.*ID2 * np.multiply(D1R * testvar,Phi*testvar2) + 1./2.*ID2 * np.multiply(D1R * testvar2,Phi*testvar) 
end = time.time()
print 'Sparse J time',(end-start)/niters

start = time.time()
for i in range(niters):
	testop = grid.grid.jacobian(testvar,testvar2,ngrids)
end = time.time()
print 'f2py J time',(end-start)/niters
diff1 = test - testop
print np.amax(diff1/test),np.amin(diff1/test),np.average(diff1/test)
print 

############  qi Diagnose  #################
start = time.time()
for i in range(niters):
	test = np.divide(testvar + testvar2,testvar3)
end = time.time()
print 'Sparse qi time',(end-start)/niters

start = time.time()
for i in range(niters):
	testop = grid.grid.diagnose_qi(testvar,testvar2,testvar3,ngrids)
end = time.time()
print 'f2py qi time',(end-start)/niters
diff1 = test - testop
print np.amax(diff1/test),np.amin(diff1/test),np.average(diff1/test)
print 

############  h quantities diagnose  #################
start = time.time()
for i in range(niters):
	test1 = 1./(Phi*testvar)
	test2 = 1./(R*testvar)
	test3 = np.square(test1)
	test4 = np.square(test2)
end = time.time()
print 'Sparse h quantities time',(end-start)/niters

start = time.time()
for i in range(niters):
	grid.grid.diagnose_h_quantities(testvar,ngrids)
end = time.time()
testop1 = grid.grid.heinv
testop2 = grid.grid.hvinv
testop3 = grid.grid.heinv2
testop4 = grid.grid.hvinv2
print 'f2py h quantities time',(end-start)/niters
diff1 = test1 - testop1
diff2 = test2 - testop2
diff3 = test3 - testop3
diff4 = test4 - testop4
print np.amax(diff1/test1),np.amin(diff1/test1),np.average(diff1/test1)
print np.amax(diff2/test2),np.amin(diff2/test2),np.average(diff2/test2)
print np.amax(diff3/test3),np.amin(diff3/test3),np.average(diff3/test3)
print np.amax(diff4/test4),np.amin(diff4/test4),np.average(diff4/test4)
print 

############  phi diagnose  #################
hi = testvar
bi = testvar2
chi = testvar3
psi = testvar4
heinv2 = np.square(1./(Phi*hi))
hvinv2 = np.square(1./(R*hi))
grid.grid.diagnose_h_quantities(hi,ngrids)
C = 1./3.
start = time.time()
for i in range(niters):
	PE = settings.g * (hi + bi)
	KE_FD = 0.25 * IK * (np.multiply(D1bar * chi,HD1bar * chi) * heinv2) + 0.25 * IK * (np.multiply(D1bar * psi,HD1bar * psi) * heinv2)
	KE_J = C/2.0 * IK * np.multiply(D1 * hvinv2, (NJ2 * psi) * (J1 * chi) + (NJ1 * psi) * (J2 * chi))
	test = PE + KE_FD + KE_J
end = time.time()
print 'Sparse phi time',(end-start)/niters

start = time.time()
for i in range(niters):
	testop = grid.grid.diagnose_phi(hi,bi,chi,psi,settings.g,ngrids)
end = time.time()
print 'f2py phi time',(end-start)/niters
diff1 = test - testop
print np.amax(diff1/test),np.amin(diff1/test),np.average(diff1/test)
print 

def afunc(v):
	chi,psi = np.split(v,2)
	FDchi = ID2 * np.multiply(heinv,HD1bar * chi)
	FDpsi = ID2 * np.multiply(heinv,HD1bar * psi)
	Jchi = 0.5*ID2 * np.multiply(D1hvinv,2. * (Phi * chi))
	Jpsi = 0.5*ID2 * np.multiply(D1hvinv,2. * (Phi * psi))
	return np.concatenate((FDchi - Jpsi,FDpsi + Jchi))

############ SET VARIABLES FOR BELOW ##############3
hi,zeta,delta,fi,bi = initialize_vars()
meshfile = h5py.File(settings.meshdir + '/' + settings.grid_type + '/' + settings.grid_variant + '/' + str(grid.grid.nface[-1]) + '.hdf5','r')
clat = np.array(meshfile['coords']['clat'])
clon = np.array(meshfile['coords']['clon'])
meshfile.close()
delta = np.cos(clat) * np.cos(clon)
heinv = 1./(Phi*hi)
hvinv = 1./(R*hi)
D1hvinv = D1 * hvinv #THIS IS BEING COMPUTED AT EVERY STEP IN F2PY, IT SHOULDNT BE!
grid.grid.heinv = heinv
grid.grid.hvinv = hvinv
C = 1./3.
Asparse = spsparselin.LinearOperator((2*grid.grid.nface[-1],2*grid.grid.nface[-1]),afunc,dtype=np.float64) 
Af2py = spsparselin.LinearOperator((2*grid.grid.nface[-1],2*grid.grid.nface[-1]),grid.grid.chi_psi_apply_operator,dtype=np.float64) 

############  chi_psi_apply  #################
start = time.time()
testvarbig = np.random.rand(2*nface) #np.concatenate((delta,zeta))
for i in range(niters):
	test = afunc(testvarbig)
end = time.time()
print 'Sparse chi_psi func time',(end-start)/niters

start = time.time()
for i in range(niters):
	testop = grid.grid.chi_psi_apply_operator(testvarbig)
end = time.time()
print 'f2py chi_psi func time',(end-start)/niters
diff1 = test - testop
print np.amax(diff1/test),np.amin(diff1/test),np.average(diff1/test)
print 

#Test FD independently
start = time.time()
for i in range(niters):
	test = ID2 * np.multiply(heinv,HD1bar*testvar)
end = time.time()
print 'Sparse chi_psi fd func time',(end-start)/niters

start = time.time()
for i in range(niters):
	testop = grid.grid.chi_psi_apply_fd_operator(testvar)
end = time.time()
print 'f2py chi_psi fd func time',(end-start)/niters
diff1 = test - testop
print np.amax(diff1/test),np.amin(diff1/test),np.average(diff1/test)
print 

#Test J independently
start = time.time()
for i in range(niters):
	test = 0.5*ID2 * np.multiply(D1hvinv,2. * (Phi * testvar))
end = time.time()
print 'Sparse chi_psi jacob func time',(end-start)/niters

start = time.time()
for i in range(niters):
	testop = grid.grid.chi_psi_apply_jacob_operator(testvar)
end = time.time()
print 'f2py chi_psi jacob func time',(end-start)/niters
diff1 = test - testop
stencil = np.logical_not(np.equal(test,0.0))
diff1 = diff1[stencil]
test = test[stencil]
print np.nanmax(diff1/test),np.nanmin(diff1/test),np.average(diff1/test)
print 

############ elliptic solver ###########
niters = 10
for i in range(niters):
	rhs = np.concatenate((delta,zeta))
	chipsi,info = spsparselin.gmres(Asparse,rhs,tol=1e-15)
end = time.time()
print 'Sparse chi_psi solve time',(end-start)/niters

start = time.time()
for i in range(niters):
	rhs = np.concatenate((delta,zeta))
	chipsi_f2py,info = spsparselin.gmres(Af2py,rhs,tol=1e-15)
end = time.time()
print 'f2py chi_psi solve time',(end-start)/niters
diff1 = chipsi - chipsi_f2py
print np.amax(diff1/chipsi),np.amin(diff1/chipsi),np.average(diff1/chipsi)
print 


############ full diganose test ##########
heinv = 1./(Phi*hi)
hvinv = 1./(R*hi)
heinv2 = np.square(heinv)
hvinv2 = np.square(hvinv)
D1hvinv = D1 * hvinv
grid.grid.heinv = heinv
grid.grid.hvinv = hvinv
rhs = np.concatenate((delta,zeta))
chipsi,info = spsparselin.gmres(Af2py,rhs,tol=1e-15) #should be using Asparse here, but solvers are not giving the same answers!
chi,psi = np.split(chipsi,2)
qi = np.divide(zeta + fi,hi)
PE = settings.g * (hi + bi)
KE_FD = 0.25 * IK * (np.multiply(D1bar * chi,HD1bar * chi) * heinv2) + 0.25 * IK * (np.multiply(D1bar * psi,HD1bar * psi) * heinv2)
KE_J = C/2.0 * IK * np.multiply(D1 * hvinv2, (NJ2 * psi) * (J1 * chi) + (NJ1 * psi) * (J2 * chi))
phi = PE + KE_FD + KE_J

grid.grid.diagnose_h_quantities(hi,ngrids)
heinvf2py = grid.grid.heinv
hvinvf2py = grid.grid.hvinv
qif2py = grid.grid.diagnose_qi(zeta,fi,hi,ngrids)
rhs = np.concatenate((delta,zeta))
chipsi_f2py,info = spsparselin.gmres(Af2py,rhs,tol=1e-15)
chif2py,psif2py = np.split(chipsi_f2py,2)
phif2py = grid.grid.diagnose_phi(hi,bi,chif2py,psif2py,settings.g,ngrids)
diff1 = qi - qif2py
diff2 = chi - chif2py
diff3 = psi - psif2py
diff4 = phi - phif2py
diff5 = heinv - heinvf2py
diff6 = hvinv - hvinvf2py
print 'diagnose!'
print np.amax(diff1/qi),np.amin(diff1/qi),np.average(diff1/qi)
print np.amax(diff2/chi),np.amin(diff2/chi),np.average(diff2/chi)
print np.amax(diff3/psi),np.amin(diff3/psi),np.average(diff3/psi)
print np.amax(diff4/phi),np.amin(diff4/phi),np.average(diff4/phi)
print np.amax(diff5/heinv),np.amin(diff5/heinv),np.average(diff5/heinv)
print np.amax(diff6/hvinv),np.amin(diff6/hvinv),np.average(diff6/hvinv)
print

############  all rhs's  ################# 
niters = 10
start = time.time()
#fi = np.random.rand(nface)
#bi = np.random.rand(nface)
#qi = np.random.rand(nface)
#chi = np.random.rand(nface)
#psi = np.random.rand(nface)
#phi = np.random.rand(nface)
for i in range(niters):
	#diagnose vars
	heinv = 1./(Phi*hi)
	hvinv = 1./(R*hi)
	heinv2 = np.square(heinv)
	hvinv2 = np.square(hvinv)
	D1hvinv = D1 * hvinv
	rhs = np.concatenate((delta,zeta))
	chipsi,info = spsparselin.gmres(Asparse,rhs,tol=1e-12) #should be using Asparse here, but solvers are not giving the same answers!
	chi,psi = np.split(chipsi,2)
	qi = np.divide(zeta + fi,hi)
	PE = settings.g * (hi + bi)
	KE_FD = 0.25 * IK * (np.multiply(D1bar * chi,HD1bar * chi) * heinv2) + 0.25 * IK * (np.multiply(D1bar * psi,HD1bar * psi) * heinv2)
	KE_J = C/2.0 * IK * np.multiply(D1 * hvinv2, (NJ2 * psi) * (J1 * chi) + (NJ1 * psi) * (J2 * chi))
	phi = PE + KE_FD + KE_J
	
	#calculate rhs's
	#hi
	hi_rhs = -L * chi
	#zeta
	FD = ID2 * np.multiply(Phi*qi,HD1bar * chi)
	J = -1./2.*ID2 * np.multiply(D1R * qi,Phi*psi) + 1./2.*ID2 * np.multiply(D1R * psi,Phi*qi) 
	zeta_rhs = J - FD
	#delta
	FD = ID2 * np.multiply(Phi*qi,HD1bar * psi)
	J = -1./2.*ID2 * np.multiply(D1R * qi,Phi*chi) + 1./2.*ID2 * np.multiply(D1R * chi,Phi*qi) 
	delta_rhs = J + FD - L * phi
	
end = time.time()
print 'Sparse rhs time',(end-start)/niters

start = time.time()
for i in range(niters):
	grid.grid.diagnose_h_quantities(hi,ngrids)
	qi = grid.grid.diagnose_qi(zeta,fi,hi,ngrids)
	rhs = np.concatenate((delta,zeta))
	chipsi,info = spsparselin.gmres(Af2py,rhs,tol=1e-12)
	chi,psi = np.split(chipsi,2)
	phi = grid.grid.diagnose_phi(hi,bi,chi,psi,settings.g,ngrids)

	#calculate rhs's
	hi_rhs_f2py = grid.grid.hi_rhs(chi,ngrids)
	zeta_rhs_f2py = grid.grid.zeta_rhs(qi,chi,psi,ngrids)
	delta_rhs_f2py = grid.grid.delta_rhs(qi,chi,psi,phi,ngrids)
end = time.time()
print 'f2py rhs time',(end-start)/niters


diff1 = hi_rhs - hi_rhs_f2py
diff2 = zeta_rhs - zeta_rhs_f2py
diff3 = delta_rhs - delta_rhs_f2py
print np.amax(diff1/hi_rhs),np.amin(diff1/hi_rhs),np.average(diff1/hi_rhs)
print np.amax(diff2/zeta_rhs),np.amin(diff2/zeta_rhs),np.average(diff2/zeta_rhs)
print np.amax(diff3/delta_rhs),np.amin(diff3/delta_rhs),np.average(diff3/delta_rhs)
print 

############  precompute stats  #################
hi = np.random.rand(nface)
bi = np.random.rand(nface)
start = time.time()
for i in range(niters):
	hibar = np.sum(hi,dtype="float128") / (4.*pi*settings.a*settings.a) * np.ones(nface)
	bibar = np.sum(bi,dtype="float128") / (4.*pi*settings.a*settings.a) * np.ones(nface)
	test2 = np.sum(Iinv * (settings.g * (hibar+bibar) * (hibar+bibar)/2.),dtype="float128")
	test1 = Iinv * bi
end = time.time()
print 'Sparse precompute time',(end-start)/niters

start = time.time()
for i in range(niters):
	grid.grid.precompute_zgrid_statistics(hi,bi,settings.a,settings.g)
end = time.time()
testop2 = grid.grid.flatpe
testop1 = grid.grid.bih
print 'f2py precompute time',(end-start)/niters
diff1 = test1 - testop1
diff2 = test2 - testop2
print np.amax(diff1/test1),np.amin(diff1/test1),np.average(diff1/test1)
print diff2/test2
print 

############  stats  #################
hi = np.random.rand(nface)
bi = np.random.rand(nface)
zeta = np.random.rand(nface)
fi = np.random.rand(nface)
chi = np.random.rand(nface)
psi = np.random.rand(nface)
grid.grid.precompute_zgrid_statistics(hi,bi,settings.a,settings.g)
flatpe = grid.grid.flatpe
start = time.time()
he = Phi * hi
hv = R * hi
grid.grid.heinv = 1./he
grid.grid.hvinv = 1./hv
for i in range(niters):
	ke_fd = H * ((D1bar * chi) * (D1bar * chi)/2./he) + H * ((D1bar * psi) * (D1bar * psi)/2./he)
	ke_j =  0.5 * (D1 * (1./hv)) * ((NJ2 * psi) * (J1 * chi) + (NJ1 * psi) * (J2 * chi))
	ape = Iinv * (settings.g * (hi+bi) * (hi+bi)/2.) - flatpe
	q = ((zeta + fi) * (Iinv * (zeta + fi))/2./hi)
	test1 = np.sum(Iinv*hi,dtype="float128")
	test2 = np.sum(Iinv*zeta,dtype="float128")
	test3 = np.sum(Iinv*(zeta+fi),dtype="float128")
	test4 = np.sum(ke_fd + ke_j,dtype="float128")
	test5 = np.sum(ape,dtype="float128")
	test6 = test4 + test5
	test7 = np.sum(q,dtype="float128")
end = time.time()
print 'Sparse stats time',(end-start)/niters

start = time.time()
for i in range(niters):
	grid.grid.compute_zgrid_statistics(1,hi,bi,zeta,fi,chi,psi,settings.g,grid.grid.nedge[-1])
end = time.time()
testop1 = grid.grid.mc[0]
testop2 = grid.grid.zeta[0]
testop3 = grid.grid.eta[0]
testop4 = grid.grid.kinetic[0]
testop5 = grid.grid.potential[0]
testop6 = grid.grid.total[0]
testop7 = grid.grid.enstrophy[0]

print 'f2py stats time',(end-start)/niters
diff1 = test1 - testop1
diff2 = test2 - testop2
diff3 = test3 - testop3
diff4 = test4 - testop4
diff5 = test5 - testop5
diff6 = test6 - testop6
diff7 = test7 - testop7
print np.amax(diff1/test1),np.amin(diff1/test1),np.average(diff1/test1)
print np.amax(diff2/test2),np.amin(diff2/test2),np.average(diff2/test2)
print np.amax(diff3/test3),np.amin(diff3/test3),np.average(diff3/test3)
print np.amax(diff4/test4),np.amin(diff4/test4),np.average(diff4/test4)
print np.amax(diff5/test5),np.amin(diff5/test5),np.average(diff5/test5)
print np.amax(diff6/test6),np.amin(diff6/test6),np.average(diff6/test6)
print np.amax(diff7/test7),np.amin(diff7/test7),np.average(diff7/test7)
print 
