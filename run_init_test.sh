#!/bin/bash

#export OMP_NUM_THREADS=1
#export OMP_STACKSIZE=8g
#ulimit -s unlimited

python src/test_init.py config.template ZGRID geodesic tweaked
python src/test_init.py config.template TRISK geodesic tweaked
python src/test_init.py config.template TRISK cubedsphere thuburn

