
import os
import sys

###################### GENERAL SETTINGS ##########################

gridtypes = ['geodesic','cubedsphere']
gridvariant_dict = {'geodesic' : ['tweaked','cvt','spring_beta=0.8','spring_beta=1.1'] , 'cubedsphere': ['thuburn',]}
trisk_variants = ['TEQ','TE','Q'] 
geodesic_timestep_dict = {}
geodesic_timestep_dict[2] = 1440
geodesic_timestep_dict[3] = 720
geodesic_timestep_dict[4] = 360
geodesic_timestep_dict[5] = 180
geodesic_timestep_dict[6] = 90
geodesic_timestep_dict[7] = 45
geodesic_timestep_dict[8] = 22.5
cubedsphere_timestep_dict = {}
cubedsphere_timestep_dict[2] = 1200
cubedsphere_timestep_dict[3] = 480
cubedsphere_timestep_dict[4] = 240
cubedsphere_timestep_dict[5] = 120
cubedsphere_timestep_dict[6] = 60
cubedsphere_timestep_dict[7] = 30
cubedsphere_timestep_dict[8] = 15 #GALEWSKY CS-TE-8 IS UNSTABLE? POSSIBLY? WITH THIS- MAYBE DO 10 INSTEAD?
timestep_dict = { 'geodesic' : geodesic_timestep_dict , 'cubedsphere' : cubedsphere_timestep_dict}
stepfactor = 1.0

#GET THESE FROM SYS
tempname = sys.argv[3]
scheme = sys.argv[2]
simulation = sys.argv[1]

###################### SIMULATIONS ##########################
if simulation == 'Galewsky':
	initial_condition = 'Galewsky' 
	output_name = 'Galewsky'
	vlevels = [5,6,7,8]
	total_run_time = 10 #in days
	use_dissipation = 'True'
	perturb_H = 'True'
	outtime = 6 #in hours
	
if simulation == 'GalewskyInit':
	initial_condition = 'Galewsky' 
	output_name = 'GalewskyInit'
	vlevels = [5,6,7,8]
	total_run_time = 1 #in days
	use_dissipation = 'False'
	perturb_H = 'True'
	outtime = 1 #in hours
	stepfactor = 2.
	
if simulation == 'GalewskyNoPerturb':
	initial_condition = 'Galewsky' 
	output_name = 'GalewskyNoPerturb'
	vlevels = [2,3,4,5,6,7,8]
	total_run_time = 10 #in days
	use_dissipation = 'False'
	perturb_H = 'False'
	outtime = 6 #in hours

if simulation == 'TC2':
	initial_condition = 'TC2' 
	output_name = 'TC2'
	vlevels = [2,3,4,5,6,7,8]
	total_run_time = 10 #in days
	use_dissipation = 'False'
	perturb_H = 'False'
	outtime = 6 #in hours

if simulation == 'RHwave':
	initial_condition = 'RHwave' 
	output_name = 'RHwave'
	vlevels = [5,6,7] #5,6,7]
	total_run_time = 50 #in days
	use_dissipation = 'True'
	perturb_H = 'True'
	outtime = 6 #in hours
	#gridtypes = ['cubedsphere',]

if simulation == 'heldsuarez':
	initial_condition = 'heldsuarezdry' 
	output_name = 'heldsuarez'
	vlevels = [6,]
	total_run_time = 1200 #in days
	perturb_H = 'True'
	outtime = 24 #in hours

if simulation == 'geostrophic':
	initial_condition = 'geostrophic_turbulence' 
	output_name = 'geostrophic'
	vlevels = [6,]
	total_run_time = 1200 #in days
	perturb_H = 'True'
	outtime = 24 #in hours

if simulation == 'TC5':
	initial_condition = 'TC5' 
	output_name = 'TC5'
	vlevels = [5,6,7] #5,6,7]
	total_run_time = 50 #in days
	use_dissipation = 'True'
	perturb_H = 'True'
	outtime = 6 #in hours
	#gridtypes = ['cubedsphere',]

#adjust some things for ZGRID
if scheme == 'ZGRID':
	trisk_variants = ['TEQ',]
	gridtypes = ['geodesic',]
	output_name = output_name + 'Z'

###################### ACTUAL RUN CODE ##########################

for vlevel in vlevels:
	for gridtype in gridtypes:
		tstep = timestep_dict[gridtype][vlevel] / stepfactor
		nsteps = int(total_run_time * 86400. / tstep)
		nout = int(outtime * 3600. / tstep)
		for gridvariant in gridvariant_dict[gridtype]:
			if gridvariant == 'spring_beta=0.8' or gridvariant == 'spring_beta=1.1':
				if vlevel > 5:
					continue
			for triskvar in trisk_variants:
				print 'running',vlevel,gridtype,gridvariant,scheme,triskvar,initial_condition
				#output_name,nsteps,tstep,nout,use_dissipation,tempname,perturb_H
				os.spawnv(os.P_WAIT,'./run_testcase.sh',['foo',str(vlevel),gridtype,gridvariant,scheme,triskvar,initial_condition,output_name,str(nsteps),str(tstep),str(nout),use_dissipation,tempname,perturb_H])
