#!/bin/bash

export OMP_NUM_THREADS=$2
export OMP_STACKSIZE=8g
ulimit -s unlimited

/usr/local/bin/python3 src/swe.py $1
# -m cProfile -o swe.$3.dat 
#gprof2dot -f pstats -n 1 swe.dat | dot -Tpdf -o swe.pdf
