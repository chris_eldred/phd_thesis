#!/bin/bash

python src/general_plot.py Galewsky TRISK False hyperviscosity #HAD ISSUES
#python src/general_plot.py Galewsky TRISK True hyperviscosity #HAD ISSUES
python src/general_plot.py Galewsky ZGRID False hyperviscosity
#python src/general_plot.py Galewsky ZGRID True hyperviscosity

python src/general_plot.py TC5 TRISK False hyperviscosity
#python src/general_plot.py TC5 TRISK True hyperviscosity
python src/general_plot.py TC5 ZGRID False hyperviscosity
#python src/general_plot.py TC5 ZGRID True hyperviscosity

python src/general_plot.py RHwave TRISK False hyperviscosity
#python src/general_plot.py RHwave TRISK True hyperviscosity
python src/general_plot.py RHwave ZGRID False hyperviscosity
#python src/general_plot.py RHwave ZGRID True hyperviscosity

python src/general_plot.py GalewskyInit TRISK False hyperviscosity
#python src/general_plot.py GalewskyInit TRISK True hyperviscosity
python src/general_plot.py GalewskyInit ZGRID False hyperviscosity
#python src/general_plot.py GalewskyInit ZGRID True hyperviscosity

python src/general_plot.py GalewskyNoPerturb TRISK False hyperviscosity
python src/general_plot.py GalewskyNoPerturb ZGRID False hyperviscosity

python src/general_plot.py TC2 TRISK False hyperviscosity
python src/general_plot.py TC2 ZGRID False hyperviscosity

python src/heldsuarez_plotting.py TRISK False hyperviscosity
python src/heldsuarez_plotting.py ZGRID False hyperviscosity
