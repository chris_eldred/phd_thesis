
import sys
import subprocess

gridtypes = ['geodesic','cubedsphere']
gridvariant_dict = {'geodesic' : ['tweaked',] , 'cubedsphere': ['thuburn',]} #'cvt','spring_beta=0.8','spring_beta=1.1'
trisk_variants = ['TEQ','TE','Q'] 
schemelist = ['TRISK','ZGRID',]
vlevels = [2,] #,3]

dissipation = sys.argv[3]
tempname = sys.argv[4]
cval = float(sys.argv[2])
simulation = sys.argv[1]

if simulation == 'fsphere':
	outdir = 'fsphere'
	varf = 'False'
	trisk_variants = ['TEQ']
	
if simulation =='fullsphere':
	outdir = 'fullsphere'
	varf = 'True'
	
for vlevel in vlevels:
	for gridtype in gridtypes:
		for gridvariant in gridvariant_dict[gridtype]:
			for scheme in schemelist:
				for triskvar in trisk_variants:
					outname = outdir
					if scheme == 'ZGRID':
						outname = outdir + 'Z'
					if scheme == 'ZGRID' and (triskvar == 'TE' or triskvar == 'Q' or gridtype =='cubedsphere'):
						continue
					print 'running',str(vlevel),gridtype,gridvariant,scheme,triskvar,outdir,dissipation,tempname,str(cval),varf
					subprocess.call(['./run_eigencase.sh',str(vlevel),gridtype,gridvariant,scheme,triskvar,outname,dissipation,tempname,str(cval),varf])
