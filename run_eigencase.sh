#!/bin/bash

sed "s/VLEVEL/$1/g" cfg/eigen.template > cfg/${8}.temp1
sed "s/GRIDTYPE/$2/g" cfg/${8}.temp1 > cfg/${8}.temp2
sed "s/GRIDVARIANT/$3/g" cfg/${8}.temp2 > cfg/${8}.temp3

sed "s/SCHEME/$4/g" cfg/${8}.temp3 > cfg/${8}.temp4
sed "s/TRISKVAR/$5/g" cfg/${8}.temp4 > cfg/${8}.temp5

sed "s/OUTPUTNAME/$6/g" cfg/${8}.temp5 > cfg/${8}.temp6

sed "s/DISSIPATION/$7/g" cfg/${8}.temp6 > cfg/${8}.temp7

sed "s/TNAME/$8/g" cfg/${8}.temp7 > cfg/${8}.temp8
sed "s/CVAL/$9/g" cfg/${8}.temp8 > cfg/${8}.temp9
sed "s/VARF/${10}/g" cfg/${8}.temp9 > cfg/${8}.temp10

if [ ${7} = 'False' ]
then 
FINALDIR='none'
fi
if [ ${7} = 'True' ]
then 
FINALDIR='hyper'
fi

mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}
python src/eigensolver.py ${8}.temp10 > ${8}.out
mv output/temp/eigen.${8}.hdf5 output/${6}/$2/$3/$1-$5/${FINALDIR}/eigen.hdf5
mv output/temp/svd.${8}.hdf5 output/${6}/$2/$3/$1-$5/${FINALDIR}/svd.hdf5
mv ${8}.out output/${6}/$2/$3/$1-$5/${FINALDIR}/${8}.out
python src/eigenpost.py ${8}.temp10 1 > ${8}.post.out
mv ${8}.post.out output/${6}/$2/$3/$1-$5/${FINALDIR}/${8}.post.out
mv cfg/${8}.temp10 output/${6}/$2/$3/$1-$5/${FINALDIR}/${8}.template

#if [ ${10} = "True" ]
#then
#python src/eigenanalysis_fullsphere.py ${8}.temp10 1 > ${8}.analysis.out
#fi
#if [ ${10} = "False" ]
#then
#python src/eigenanalysis_fsphere.py ${8}.temp10 2 > ${8}.analysis.out
#fi
#mv ${8}.analysis.out output/${6}/$2/$3/$1-$5/${8}.analysis.out

rm cfg/${8}.temp1
rm cfg/${8}.temp2
rm cfg/${8}.temp3
rm cfg/${8}.temp4
rm cfg/${8}.temp5
rm cfg/${8}.temp6
rm cfg/${8}.temp7
rm cfg/${8}.temp8
rm cfg/${8}.temp9
