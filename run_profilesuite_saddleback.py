from __future__ import print_function
import subprocess
import sys

###################### GENERAL SETTINGS ##########################

gridtypes = ['geodesic','cubedsphere']
gridvariant_dict = {'geodesic' : ['tweaked',] , 'cubedsphere': ['thuburn',]} #'cvt','spring_beta=0.8','spring_beta=1.1'


dissipationtype = sys.argv[4]
dissipation = sys.argv[3]
vlevel = int(sys.argv[2])
scheme = sys.argv[1]

if scheme == 'TRISK':
	nsteps = 240

if scheme == 'ZGRID':
	nsteps = 240
	gridtypes = ['geodesic',]




###################### ACTUAL RUN CODE ##########################


for gridtype in gridtypes:
	for gridvariant in gridvariant_dict[gridtype]:
		if gridvariant == 'spring_beta=0.8' or gridvariant == 'spring_beta=1.1':
			if vlevel > 5:
				continue
		print('running',vlevel,gridtype,gridvariant,scheme,dissipation,dissipationtype)
		#output_name,nsteps,tstep,nout,use_dissipation,tempname,perturb_H
		arg_list =  str(vlevel) + " " + gridtype + " " + gridvariant + " " + scheme + " " + dissipation + " " + dissipationtype + " " + str(nsteps)  #'\"' +
		#print(arg_list)
		subprocess.call(['qsub','swe.pbs','-F ' + arg_list])
