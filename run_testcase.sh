#!/bin/bash

sed "s/VLEVEL/$1/g" cfg/swe_run.template > cfg/${12}.temp1
sed "s/GRIDTYPE/$2/g" cfg/${12}.temp1 > cfg/${12}.temp2
sed "s/GRIDVARIANT/$3/g" cfg/${12}.temp2 > cfg/${12}.temp3

sed "s/SCHEME/$4/g" cfg/${12}.temp3 > cfg/${12}.temp4
sed "s/TRISKVAR/$5/g" cfg/${12}.temp4 > cfg/${12}.temp5

sed "s/INITCON/$6/g" cfg/${12}.temp5 > cfg/${12}.temp6

sed "s/OUTPUTNAME/$7/g" cfg/${12}.temp6 > cfg/${12}.temp7

sed "s/NSTEPS/$8/g" cfg/${12}.temp7 > cfg/${12}.temp8
sed "s/DT/$9/g" cfg/${12}.temp8 > cfg/${12}.temp9
sed "s/OUTPUTFREQ/${10}/g" cfg/${12}.temp9 > cfg/${12}.temp10

sed "s/DISSIPATION/${11}/g" cfg/${12}.temp10 > cfg/${12}.temp11

sed "s/TNAME/${12}/g" cfg/${12}.temp11 > cfg/${12}.temp12
sed "s/PERTURB/${13}/g" cfg/${12}.temp12 > cfg/${12}.temp13

./run_swe.sh ${12}.temp13 > ${12}.out
mv output/temp/vars.${12}.hdf5 output/${7}/$2/$3/$1-$5/vars.hdf5
mv ${12}.out output/${7}/$2/$3/$1-$5/${12}.out
mv output/${7}/*.png output/${7}/$2/$3/$1-$5/
mv cfg/${12}.temp13 output/${7}/$2/$3/$1-$5/${12}.template

rm cfg/${12}.temp1
rm cfg/${12}.temp2
rm cfg/${12}.temp3
rm cfg/${12}.temp4
rm cfg/${12}.temp5
rm cfg/${12}.temp6
rm cfg/${12}.temp7
rm cfg/${12}.temp8
rm cfg/${12}.temp9
rm cfg/${12}.temp10
rm cfg/${12}.temp11
rm cfg/${12}.temp12
