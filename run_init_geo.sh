#!/bin/bash


if [ $2 -eq 1 ]
then
VARNAME="tweaked"
fi
if [ $2 -eq 2 ]
then
VARNAME="cvt"
fi
if [ $2 -eq 3 ]
then
VARNAME="spring_beta=0.8"
fi
if [ $2 -eq 4 ]
then
VARNAME="spring_beta=1.1"
fi

sed "s/VLEVEL/$1/g" cfg/geodesic_init.template > cfg/geodesic.temp1
sed "s/VARIANT/$VARNAME/g" cfg/geodesic.temp1 > cfg/geodesic.temp2

python src/initializer.py geodesic.temp2 > geodesic.out

mv geodesic.out meshes/geodesic/$VARNAME/init.$1.out

rm cfg/geodesic.temp1
rm cfg/geodesic.temp2
