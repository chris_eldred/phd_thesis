#!/bin/bash

sed "s/VLEVEL/$1/g" cfg/eigen.template > cfg/${8}.temp1
sed "s/GRIDTYPE/$2/g" cfg/${8}.temp1 > cfg/${8}.temp2
sed "s/GRIDVARIANT/$3/g" cfg/${8}.temp2 > cfg/${8}.temp3

sed "s/SCHEME/$4/g" cfg/${8}.temp3 > cfg/${8}.temp4
sed "s/TRISKVAR/$5/g" cfg/${8}.temp4 > cfg/${8}.temp5

sed "s/OUTPUTNAME/$6/g" cfg/${8}.temp5 > cfg/${8}.temp6

sed "s/DISSIPATION/$7/g" cfg/${8}.temp6 > cfg/${8}.temp7

sed "s/TNAME/$8/g" cfg/${8}.temp7 > cfg/${8}.temp8
sed "s/CVAL/$9/g" cfg/${8}.temp8 > cfg/${8}.temp9
sed "s/VARF/${10}/g" cfg/${8}.temp9 > cfg/${8}.temp10

if [ ${7} = 'False' ]
then 
FINALDIR='none'
fi
if [ ${7} = 'True' ]
then 
FINALDIR='hyper'
fi

mkdir -p output/${6}/plots/${FINALDIR}
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/igw/real/zeta
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/igw/real/hi
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/igw/real/delta
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/igw/imag/zeta
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/igw/imag/hi
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/igw/imag/delta
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/svd
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/svd/A

python src/eigen_dispersion_plot.py ${8}.temp10 > ${8}.analysis.out

if [ ${10} = "True" ]
then
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/rossby/real/hi
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/rossby/real/zeta
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/rossby/real/delta
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/rossby/imag/hi
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/rossby/imag/zeta
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/rossby/imag/delta
#python src/propagating_mode_plot.py ${8}.temp10 > ${8}.analysis.out
#python src/svd_plot.py ${8}.temp10 > ${8}.analysis.out
fi
if [ ${10} = "False" ]
then
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/stat/real/hi
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/stat/real/zeta
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/stat/real/delta
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/stat/imag/hi
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/stat/imag/zeta
mkdir -p output/${6}/$2/$3/$1-$5/${FINALDIR}/stat/imag/delta
#python src/propagating_mode_plot.py ${8}.temp10 > ${8}.analysis.out
#python src/svd_plot.py ${8}.temp10 > ${8}.analysis.out
fi
mv ${8}.analysis.out output/${6}/$2/$3/$1-$5/${FINALDIR}/${8}.analysis.out

rm cfg/${8}.temp1
rm cfg/${8}.temp2
rm cfg/${8}.temp3
rm cfg/${8}.temp4
rm cfg/${8}.temp5
rm cfg/${8}.temp6
rm cfg/${8}.temp7
rm cfg/${8}.temp8
rm cfg/${8}.temp9
