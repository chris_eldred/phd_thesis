#!/bin/bash

actualtempname=${12}.$2.$3.$1.$5.$7

sed "s/VLEVEL/$1/g" cfg/swe_run.template > cfg/${actualtempname}.temp1
sed "s/GRIDTYPE/$2/g" cfg/${actualtempname}.temp1 > cfg/${actualtempname}.temp2
sed "s/GRIDVARIANT/$3/g" cfg/${actualtempname}.temp2 > cfg/${actualtempname}.temp3

sed "s/SCHEME/$4/g" cfg/${actualtempname}.temp3 > cfg/${actualtempname}.temp4
sed "s/TRISKVAR/$5/g" cfg/${actualtempname}.temp4 > cfg/${actualtempname}.temp5

sed "s/INITCON/$6/g" cfg/${actualtempname}.temp5 > cfg/${actualtempname}.temp6

sed "s/OUTPUTNAME/$7/g" cfg/${actualtempname}.temp6 > cfg/${actualtempname}.temp7

sed "s/NSTEPS/$8/g" cfg/${actualtempname}.temp7 > cfg/${actualtempname}.temp8
sed "s/DT/$9/g" cfg/${actualtempname}.temp8 > cfg/${actualtempname}.temp9
sed "s/OUTPUTFREQ/${10}/g" cfg/${actualtempname}.temp9 > cfg/${actualtempname}.temp10

sed "s/DISSIPATION/${11}/g" cfg/${actualtempname}.temp10 > cfg/${actualtempname}.temp11

sed "s/TNAME/${12}/g" cfg/${actualtempname}.temp11 > cfg/${actualtempname}.temp12
sed "s/PERTURB/${13}/g" cfg/${actualtempname}.temp12 > cfg/${actualtempname}.temp13
sed "s/DISSIPTYPE/${14}/g" cfg/${actualtempname}.temp13 > cfg/${actualtempname}.temp14

if [ ${11} = 'False' ]
then 
FINALDIR='none'
fi
if [ ${11} = 'True' ]
then 
if [ ${14} = 'hyperviscosity' ]
then
FINALDIR='hyper'
fi
if [ ${14} = 'APVM' ]
then
FINALDIR='apvm'
fi
fi

./run_swe.sh ${actualtempname}.temp14 ${15} > ${actualtempname}.out
mkdir -p /Users/celdred/output/${7}/$2/$3/$1-$5/${FINALDIR}
mv output/temp/vars.${actualtempname}.hdf5 /Users/celdred/output/${7}/$2/$3/$1-$5/${FINALDIR}/vars.hdf5
mv ${actualtempname}.out /Users/celdred/output/${7}/$2/$3/$1-$5/${FINALDIR}/${actualtempname}.out
mv output/temp/*.${actualtempname}.png /Users/celdred/output/${7}/$2/$3/$1-$5/${FINALDIR}
mv cfg/${actualtempname}.temp14 /Users/celdred/output/${7}/$2/$3/$1-$5/${FINALDIR}/${actualtempname}.template
#mv swe.${actualtempname}.dat /Users/celdred/output/${7}/$2/$3/$1-$5/${FINALDIR}/swe.dat

rm cfg/${actualtempname}.temp1
rm cfg/${actualtempname}.temp2
rm cfg/${actualtempname}.temp3
rm cfg/${actualtempname}.temp4
rm cfg/${actualtempname}.temp5
rm cfg/${actualtempname}.temp6
rm cfg/${actualtempname}.temp7
rm cfg/${actualtempname}.temp8
rm cfg/${actualtempname}.temp9
rm cfg/${actualtempname}.temp10
rm cfg/${actualtempname}.temp11
rm cfg/${actualtempname}.temp12
rm cfg/${actualtempname}.temp13
