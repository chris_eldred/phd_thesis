#!/bin/bash

cp run_swe_profile_saddleback.sh /Users/celdred/run_swe_profile.sh
cp run_swe_saddleback.sh /Users/celdred/run_swe.sh
cp run_testcase_saddleback.sh /Users/celdred/run_testcase.sh
cp run_testsuites_saddleback.py /Users/celdred/run_testsuites.py
cp run_profilesuite_saddleback.py /Users/celdred/run_profilesuite.py
cp swe.pbs /Users/celdred/swe.pbs
cp testcase.pbs /Users/celdred/testcase.pbs


cp run_postprocess_swe_saddleback.sh /Users/celdred/run_postprocess_swe.sh
cp run_postprocess_profile_saddleback.sh /Users/celdred/run_postprocess_profile.sh
cp run_postprocess_saddleback_swe_suite.py /Users/celdred/run_postprocess_swe_suite.py
cp run_postprocess_saddleback_profile_suite.py /Users/celdred/run_postprocess_profile_suite.py

cp src/grid.f90 /Users/celdred/src/grid.f90
cp src/mycpu.c /Users/celdred/src/mycpu.c
cp src/create_pymod_saddleback.sh /Users/celdred/src/create_pymod.sh
cp src/f2py3 /Users/celdred/src/f2py3

cp src/swe.py /Users/celdred/src/swe.py
cp src/scheme.py /Users/celdred/src/scheme.py
cp src/output.py /Users/celdred/src/output.py
cp src/mesh.py /Users/celdred/src/mesh.py
cp src/initialize.py /Users/celdred/src/initialize.py
cp src/constants.py /Users/celdred/src/constants.py
cp src/configuration.py /Users/celdred/src/configuration.py

cp cfg/swe_profile.template /Users/celdred/cfg/swe_profile.template
cp cfg/swe_run.template /Users/celdred/cfg/swe_run.template
cp cfg/config.template /Users/celdred/cfg/config.template
