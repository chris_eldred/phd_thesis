#!/bin/bash

#python run_eigensuite.py fsphere 2.0 False temp1 >& fsphere.solve.false.out
#python run_eigensuite.py fsphere 2.0 True temp1 >& fsphere.solve.true.out
#python run_eigensuite.py fullsphere 2.0 False temp1 >& fullsphere.solve.false.out
#python run_eigensuite.py fullsphere 2.0 True temp1 >& fullsphere.solve.true.out

python run_eigenanalysis_suite.py fsphere 2.0 False temp1 >& fsphere.analysis.false.out
python run_eigenanalysis_suite.py fsphere 2.0 True temp1 >& fsphere.analysis.true.out
python run_eigenanalysis_suite.py fullsphere 2.0 True temp1 >& fullsphere.analysis.true.out
python run_eigenanalysis_suite.py fullsphere 2.0 False temp1 >& fullsphere.analysis.false.out

#./run_eigenanalysis.sh 2 geodesic tweaked TRISK TEQ fsphere DISSIP temp1 2.0 VARF

#./run_eigenanalysis.sh 2 geodesic tweaked TRISK TEQ fsphere False temp1 2.0 False
#./run_eigenanalysis.sh 2 geodesic tweaked TRISK TEQ fullsphere False temp1 2.0 True

#./run_eigenanalysis.sh 2 geodesic tweaked ZGRID TEQ fsphereZ False temp1 2.0 False
#./run_eigenanalysis.sh 2 cubedsphere thuburn TRISK TEQ fsphere False temp1 2.0 False
#./run_eigenanalysis.sh 2 geodesic tweaked TRISK TEQ fsphere True temp1 2.0 False
#./run_eigenanalysis.sh 2 cubedsphere thuburn TRISK TEQ fsphere True temp1 2.0 False
#./run_eigenanalysis.sh 2 geodesic tweaked ZGRID TEQ fsphereZ True temp1 2.0 False



#./run_eigenanalysis.sh 2 geodesic tweaked TRISK TEQ fullsphere False temp1 2.0 True
#./run_eigenanalysis.sh 2 geodesic tweaked TRISK TE fullsphere False temp1 2.0 True
#./run_eigenanalysis.sh 2 geodesic tweaked TRISK Q fullsphere False temp1 2.0 True
#./run_eigenanalysis.sh 2 geodesic tweaked ZGRID TEQ fullsphereZ False temp1 2.0 True
#./run_eigenanalysis.sh 2 cubedsphere thuburn TRISK TEQ fullsphere False temp1 2.0 True
#./run_eigenanalysis.sh 2 cubedsphere thuburn TRISK TE fullsphere False temp1 2.0 True
#./run_eigenanalysis.sh 2 cubedsphere thuburn TRISK Q fullsphere False temp1 2.0 True

#./run_eigenanalysis.sh 2 geodesic tweaked TRISK TEQ fullsphere True temp1 2.0 True
#./run_eigenanalysis.sh 2 geodesic tweaked TRISK TE fullsphere True temp1 2.0 True
#./run_eigenanalysis.sh 2 geodesic tweaked TRISK Q fullsphere True temp1 2.0 True
#./run_eigenanalysis.sh 2 geodesic tweaked ZGRID TEQ fullsphereZ True temp1 2.0 True
#./run_eigenanalysis.sh 2 cubedsphere thuburn TRISK TEQ fullsphere True temp1 2.0 True
#./run_eigenanalysis.sh 2 cubedsphere thuburn TRISK TE fullsphere True temp1 2.0 True
#./run_eigenanalysis.sh 2 cubedsphere thuburn TRISK Q fullsphere True temp1 2.0 True
