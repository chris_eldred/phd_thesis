#!/bin/bash


if [ $2 -eq 1 ]
then
VARNAME="thuburn"
fi

sed "s/VLEVEL/$1/g" cfg/cubedsphere_init.template > cfg/cubedsphere.temp1
sed "s/VARIANT/$VARNAME/g" cfg/cubedsphere.temp1 > cfg/cubedsphere.temp2

python src/initializer.py cubedsphere.temp2 > cubedsphere.out

mv cubedsphere.out meshes/cubedsphere/$VARNAME/init.$1.out

rm cfg/cubedsphere.temp1
rm cfg/cubedsphere.temp2
