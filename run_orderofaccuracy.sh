#!/bin/bash

export OMP_NUM_THREADS=1
export OMP_STACKSIZE=8g
ulimit -s unlimited

python src/orderofaccuracy.py config.template
