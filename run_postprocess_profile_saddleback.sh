#!/bin/bash


actualtempname=temp.$2.$3.$1.TEQ.PROFILE

if [ ${5} = 'False' ]
then 
FINALDIR='none'
fi
if [ ${5} = 'True' ]
then 
if [ ${6} = 'hyperviscosity' ]
then
FINALDIR='hyper'
fi
if [ ${6} = 'APVM' ]
then
FINALDIR='apvm'
fi
fi

if [ $4 = 'ZGRID' ]
then
PROFILEDIR='PROFILEZ'
fi 
if [ $4 = 'TRISK' ]
then
PROFILEDIR='PROFILE'
fi

mkdir -p /pool/eldred/output/${PROFILEDIR}/$2/$3/$1-TEQ/${FINALDIR}/$8
mv /Users/celdred/output/${PROFILEDIR}/$2/$3/$1-TEQ/${FINALDIR}/$8/* /pool/eldred/output/${PROFILEDIR}/$2/$3/$1-TEQ/${FINALDIR}/$8
mv /Users/celdred/*.$1.$2.$3.$4.${FINALDIR}.out /pool/eldred/output/${PROFILEDIR}/$2/$3/$1-TEQ/${FINALDIR}/$8
chmod -R 777 /pool/eldred/output/${PROFILEDIR}/
