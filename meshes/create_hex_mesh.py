import numpy as np
import h5py
import sys
from math import sqrt

def SCONV(i,nx):
	return (i+nx)%nx

def ijprimal_to_lprimal(i,j,nx,ny):
	return SCONV(j,ny)*nx + SCONV(i,nx)

def ijdualT1_to_ldual(i,j,nx,ny):
	return 2*(SCONV(j,ny)*nx + SCONV(i,nx))

def ijdualT2_to_ldual(i,j,nx,ny):
	return 2*(SCONV(j,ny)*nx + SCONV(i,nx))+1

def iju1edge_to_ledge(i,j,nx,ny):
	return 3*(SCONV(j,ny)*nx + SCONV(i,nx))

def iju2edge_to_ledge(i,j,nx,ny):
	return 3*(SCONV(j,ny)*nx + SCONV(i,nx))+1

def iju3edge_to_ledge(i,j,nx,ny):
	return 3*(SCONV(j,ny)*nx + SCONV(i,nx))+2
	
nx = int(sys.argv[1])
ny = int(sys.argv[2])
nedges = 3*nx*ny
nfaces = nx*ny
nverts = 2*nx*ny
	
outfile = h5py.File('hex/' + str(nx) + 'x' + str(ny) + '/' +  str(nx) + 'x' + str(ny) + '.hdf5', 'w')
sizes = outfile.create_group('sizes')
coords = outfile.create_group('coords')
connect = outfile.create_group('connectivity')
geometry = outfile.create_group('geometry')
sizes.create_dataset('nface',data=nfaces)
sizes.create_dataset('nedge',data=nedges)
sizes.create_dataset('nvert',data=nverts)

#geometry
Ai = np.ones(nfaces,dtype=np.float64) * sqrt(3.)/2.
Ae = np.ones(nedges,dtype=np.float64) / sqrt(3.)
Av = np.ones(nverts,dtype=np.float64) * sqrt(3.)/4.
Aie = np.ones((nedges,2),dtype=np.float64) / sqrt(3.) / 2.
Aiv = np.ones((nfaces,6),dtype=np.float64) * sqrt(3.)/12.
le = np.ones(nedges,dtype=np.float64) / sqrt(3.)
de = np.ones(nedges,dtype=np.float64)
Wee = np.zeros((nedges,7),dtype=np.float64)
ne = np.zeros((nedges,2),dtype=np.float64)
te = np.zeros((nedges,2),dtype=np.float64)

#connectivity
nedgef = np.ones(nfaces,dtype=np.int32) * 6
nedgev = np.ones(nverts,dtype=np.int32) * 3
necp = np.ones(nedges,dtype=np.int32) * 11
necpd = np.ones(nedges,dtype=np.int32) * 4
NC = np.zeros((nfaces,6),dtype=np.int32)
EC = np.zeros((nfaces,6),dtype=np.int32)
VC = np.zeros((nfaces,6),dtype=np.int32)
CE = np.zeros((nedges,2),dtype=np.int32)
VE = np.zeros((nedges,2),dtype=np.int32)
CV = np.zeros((nverts,3),dtype=np.int32)
EV = np.zeros((nverts,3),dtype=np.int32)
ECP = np.zeros((nedges,11),dtype=np.int32)
ECPd = np.zeros((nedges,5),dtype=np.int32)
nei = np.zeros((nedges,2),dtype=np.int32)
tev = np.zeros((nedges,2),dtype=np.int32)
nei_cell = np.zeros((nfaces,6),dtype=np.int32)
tev_vertex = np.zeros((nverts,3),dtype=np.int32)

#coordinates
ccoords = np.zeros((nfaces,2),dtype=np.float64)
vcoords = np.zeros((nverts,2),dtype=np.float64)
ecoords = np.zeros((nedges,2),dtype=np.float64)
coffsets = np.zeros((nfaces,6,2),dtype=np.float64)
voffsets = np.zeros((nverts,3,2),dtype=np.float64)
eoffsets = np.zeros((nedges,4,2),dtype=np.float64)

#fill in arrays
for i in range(nx):
		for j in range(ny):
			l = ijprimal_to_lprimal(i,j,nx,ny)
			#create primal cells
			
			EC[l,0] = iju1edge_to_ledge(i+1,j  ,nx,ny)
			EC[l,1] = iju3edge_to_ledge(i  ,j  ,nx,ny)
			EC[l,2] = iju2edge_to_ledge(i  ,j  ,nx,ny)
			EC[l,3] = iju1edge_to_ledge(i  ,j  ,nx,ny)
			EC[l,4] = iju3edge_to_ledge(i  ,j-1,nx,ny)
			EC[l,5] = iju2edge_to_ledge(i+1,j-1,nx,ny)
			
			VC[l,0] = ijdualT2_to_ldual(i+1,j-1,nx,ny)
			VC[l,1] = ijdualT1_to_ldual(i+1,j  ,nx,ny)
			VC[l,2] = ijdualT2_to_ldual(i  ,j  ,nx,ny)
			VC[l,3] = ijdualT1_to_ldual(i  ,j  ,nx,ny)
			VC[l,4] = ijdualT2_to_ldual(i  ,j-1,nx,ny)
			VC[l,5] = ijdualT1_to_ldual(i+1,j-1,nx,ny)
			
			nei_cell[l,0] = 1
			nei_cell[l,1] = 1
			nei_cell[l,2] = 1
			nei_cell[l,3] = -1
			nei_cell[l,4] = -1
			nei_cell[l,5] = -1
			
			ccoords[l,:] = np.array([i*1.+j*1./2.,j*1.*sqrt(3.)/2.])
			coffsets[l,:,0] = np.array([ 1./2.,1./2.,0.,-1.*1./2.,-1.*1./2.,0. ])
			coffsets[l,:,1] = np.array([ -1.*1./(2.*sqrt(3.)),1./(2.*sqrt(3.)),1./sqrt(3.),1./(2.*sqrt(3.)),-1.*1./(2.*sqrt(3.)),-1.*1./sqrt(3.) ])
            
			#create dual cells
			lt1 = ijdualT1_to_ldual(i,j,nx,ny)
			lt2 = ijdualT2_to_ldual(i,j,nx,ny)
				
			#Type 1
			EV[lt1,0] = iju1edge_to_ledge(i  ,j,nx,ny)
			EV[lt1,1] = iju2edge_to_ledge(i  ,j,nx,ny)
			EV[lt1,2] = iju3edge_to_ledge(i-1,j,nx,ny)
			
			CV[lt1,0] = ijprimal_to_lprimal(i  ,j    ,nx,ny)
			CV[lt1,1] = ijprimal_to_lprimal(i-1,j+1,nx,ny)
			CV[lt1,2] = ijprimal_to_lprimal(i-1,j  ,nx,ny)
			
			tev_vertex[lt1,0] = 1
			tev_vertex[lt1,1] = 1
			tev_vertex[lt1,2] = -1
			
			vcoords[lt1,:] = np.array([i*1.+j*1./2. - 0.5*1.,j*1.*sqrt(3.)/2. + 1./(2.*sqrt(3.))])
			voffsets[lt1,:,0] = np.array([1./2.,0.,-1.*1./2.])
			voffsets[lt1,:,1] = np.array([-1.*sqrt(3.)*1./6.,1./sqrt(3.),-1.*sqrt(3.)*1./6.])
            
			#Type 2
			EV[lt2,0] = iju1edge_to_ledge(i,j+1,nx,ny)
			EV[lt2,1] = iju2edge_to_ledge(i,j  ,nx,ny)
			EV[lt2,2] = iju3edge_to_ledge(i,j  ,nx,ny)
			
			CV[lt2,0] = ijprimal_to_lprimal(i  ,j+1 ,nx,ny)
			CV[lt2,1] = ijprimal_to_lprimal(i-1,j+1,nx,ny)
			CV[lt2,2] = ijprimal_to_lprimal(i  ,j  ,nx,ny)
			
			tev_vertex[lt2,0] = -1
			tev_vertex[lt2,1] = -1
			tev_vertex[lt2,2] = 1

			vcoords[lt2,:] = np.array([i*1.+j*1./2.,j*1.*sqrt(3.)/2. + 1./sqrt(3.)])
			voffsets[lt2,:,0] = np.array([1./2.,-1.*1./2.,0.])
			voffsets[lt2,:,1] = np.array([ sqrt(3.)*1./6.,sqrt(3.)*1./6.,-1.*1./sqrt(3.)])

			#create edge cells
			lu1 = iju1edge_to_ledge(i,j,nx,ny)
			lu2 = iju2edge_to_ledge(i,j,nx,ny)
			lu3 = iju3edge_to_ledge(i,j,nx,ny)
			
			#u1 edges
			CE[lu1,0] = ijprimal_to_lprimal(i-1,j,nx,ny)
			CE[lu1,1] = ijprimal_to_lprimal(i  ,j,nx,ny)
			
			VE[lu1,0] = ijdualT2_to_ldual(i,j-1,nx,ny)
			VE[lu1,1] = ijdualT1_to_ldual(i,j  ,nx,ny)
			
			ECP[lu1,0] =  iju1edge_to_ledge(i  ,j  ,nx,ny)
			ECP[lu1,1] =  iju2edge_to_ledge(i  ,j  ,nx,ny)
			ECP[lu1,2] =  iju3edge_to_ledge(i  ,j  ,nx,ny)
			ECP[lu1,3] =  iju1edge_to_ledge(i+1,j  ,nx,ny)
			ECP[lu1,4] =  iju2edge_to_ledge(i+1,j-1,nx,ny)
			ECP[lu1,5] =  iju3edge_to_ledge(i  ,j-1,nx,ny)
			ECP[lu1,6] =  iju2edge_to_ledge(i  ,j-1,nx,ny)
			ECP[lu1,7] =  iju3edge_to_ledge(i-1,j-1,nx,ny)
			ECP[lu1,8] =  iju1edge_to_ledge(i-1,j  ,nx,ny)
			ECP[lu1,9] =  iju2edge_to_ledge(i-1,j  ,nx,ny)
			ECP[lu1,10] = iju3edge_to_ledge(i-1,j  ,nx,ny)
			
			tev[lu1,0] = -1
			tev[lu1,1] = 1
			
			nei[lu1,0] = 1
			nei[lu1,1] = -1

			ecoords[lu1,:] = np.array([i*1.+j*1./2. - 1./2.,j*1.*sqrt(3.)/2.])
			ne[lu1,:] = np.array([0.,1.])
			te[lu1,:] = np.array([-1.,0.])
			eoffsets[lu1,:,0] = np.array([ 0., -1.*1./2., 0., 1./2. ])
			eoffsets[lu1,:,1] = np.array([-1.*1./(2.*sqrt(3.)), 0., 1./(2.*sqrt(3.)),  0.])
            
			#u2 edges
			CE[lu2,0] = ijprimal_to_lprimal(i  ,j  ,nx,ny)
			CE[lu2,1] = ijprimal_to_lprimal(i-1,j+1,nx,ny)
			
			VE[lu2,0] = ijdualT1_to_ldual(i,j,nx,ny)
			VE[lu2,1] = ijdualT2_to_ldual(i,j,nx,ny)
			
			ECP[lu2,0] =  iju2edge_to_ledge(i  ,j  ,nx,ny)
			ECP[lu2,1] =  iju3edge_to_ledge(i  ,j  ,nx,ny)
			ECP[lu2,2] =  iju1edge_to_ledge(i+1,j  ,nx,ny)
			ECP[lu2,3] =  iju2edge_to_ledge(i+1,j-1,nx,ny)
			ECP[lu2,4] =  iju3edge_to_ledge(i  ,j-1,nx,ny)
			ECP[lu2,5] =  iju1edge_to_ledge(i  ,j  ,nx,ny)
			ECP[lu2,6] =  iju3edge_to_ledge(i-1,j  ,nx,ny)
			ECP[lu2,7] =  iju1edge_to_ledge(i-1,j+1,nx,ny)
			ECP[lu2,8] =  iju2edge_to_ledge(i-1,j+1,nx,ny)
			ECP[lu2,9] =  iju3edge_to_ledge(i-1,j+1,nx,ny)
			ECP[lu2,10] = iju1edge_to_ledge(i  ,j+1,nx,ny)
			
			tev[lu2,0] = 1
			tev[lu2,1] = -1
			
			nei[lu2,0] = 1
			nei[lu2,1] = -1

			ecoords[lu2,:] = np.array([i*1.+j*1./2. - 1./4.,j*1.*sqrt(3.)/2. +sqrt(3.)/4.*1.])
			ne[lu2,:] = np.array([-0.5,sqrt(3.)/2.])
			te[lu2,:] = np.array([-sqrt(3.)/2.,-0.5])
			eoffsets[lu2,:,0] = np.array([ -1.*1./4., 1./4.,  1./4. , -1.*1./4. ])
			eoffsets[lu2,:,1] = np.array([ -1.*sqrt(3.)/12.*1., -1.*sqrt(3.)/4.*1., sqrt(3.)/12.*1., sqrt(3.)/4.*1.])
			
			#u3 edges
			
			CE[lu3,0] = ijprimal_to_lprimal(i,j  ,nx,ny)
			CE[lu3,1] = ijprimal_to_lprimal(i,j+1,nx,ny)
			
			VE[lu3,0] = ijdualT1_to_ldual(i+1,j,nx,ny)
			VE[lu3,1] = ijdualT2_to_ldual(i ,j,nx,ny)
			
			ECP[lu3,0] =  iju3edge_to_ledge(i  ,j  ,nx,ny)
			ECP[lu3,1] =  iju1edge_to_ledge(i+1,j  ,nx,ny)
			ECP[lu3,2] =  iju2edge_to_ledge(i+1,j-1,nx,ny)
			ECP[lu3,3] =  iju3edge_to_ledge(i  ,j-1,nx,ny)
			ECP[lu3,4] =  iju1edge_to_ledge(i  ,j  ,nx,ny)
			ECP[lu3,5] =  iju2edge_to_ledge(i  ,j  ,nx,ny)
			ECP[lu3,6] =  iju1edge_to_ledge(i  ,j+1,nx,ny)
			ECP[lu3,7] =  iju2edge_to_ledge(i  ,j+1,nx,ny)
			ECP[lu3,8] =  iju3edge_to_ledge(i  ,j+1,nx,ny)
			ECP[lu3,9] =  iju1edge_to_ledge(i+1,j+1,nx,ny)
			ECP[lu3,10] = iju2edge_to_ledge(i+1,j  ,nx,ny)
			
			tev[lu3,0] = -1
			tev[lu3,1] = 1
			
			nei[lu3,0] = 1
			nei[lu3,1] = -1
			
			ecoords[lu3,:] = np.array([i*1.+j*1./2. + 1./4.,j*1.*sqrt(3.)/2. + sqrt(3.)/4.*1.])
			ne[lu3,:] = np.array([0.5,sqrt(3.)/2.])
			te[lu3,:] = np.array([-sqrt(3.)/2.,0.5])
			eoffsets[lu3,:,0] = np.array([ 1./4., -1.*1./4., -1.*1./4., 1./4.])
			eoffsets[lu3,:,1] = np.array([-1.*sqrt(3.)/12.*1., -1.*sqrt(3.)/4.*1., sqrt(3.)/12.*1. , sqrt(3.)/4.*1. ])

#create datasets
connect.create_dataset('NC', data=NC+1)
connect.create_dataset('EC', data=EC+1)
connect.create_dataset('VC', data=VC+1)
connect.create_dataset('nedgef', data=nedgef)
connect.create_dataset('CE', data=CE+1)
connect.create_dataset('VE', data=VE+1)
connect.create_dataset('CV', data=CV+1)
connect.create_dataset('EV', data=EV+1)
connect.create_dataset('nedgev', data=nedgev)
connect.create_dataset('ECP', data=ECP+1)
connect.create_dataset('ECPd', data=ECPd+1)
connect.create_dataset('nei_edge', data=nei)
connect.create_dataset('tev_edge', data=tev)
connect.create_dataset('nei_cell', data=nei_cell)
connect.create_dataset('tev_vertex', data=tev_vertex)

connect.create_dataset('necp', data=necp)
connect.create_dataset('necpd', data=necpd)

coords.create_dataset('ccoords', data=ccoords)
coords.create_dataset('vcoords', data=vcoords)
coords.create_dataset('ecoords', data=ecoords)
coords.create_dataset('eoffsets', data=eoffsets)
coords.create_dataset('voffsets', data=voffsets)
coords.create_dataset('coffsets', data=coffsets)

geometry.create_dataset('Ai', data=Ai)
geometry.create_dataset('le', data=le)
geometry.create_dataset('de', data=de)
geometry.create_dataset('Ae', data=Ae)
geometry.create_dataset('Av', data=Av)
geometry.create_dataset('Aie', data=Aie)
geometry.create_dataset('Aiv', data=Aiv)
geometry.create_dataset('ne', data=ne)
geometry.create_dataset('te', data=te)
	
outfile.close()
