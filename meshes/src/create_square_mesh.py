import numpy as np
import h5py
import sys

def SCONV(i,nx):
	return (i+nx)%nx

def ijprimal_to_lprimal(i,j,nx,ny):
	return SCONV(j,ny)*nx + SCONV(i,nx)
	
def ijdual_to_ldual(i,j,nx,ny):
    return SCONV(j,ny)*nx + SCONV(i,nx)

def ijhorizedge_to_ledge(i,j,nx,ny):
	return (2*SCONV(j,ny)+1)*nx + SCONV(i,nx)

def ijvertedge_to_ledge(i,j,nx,ny):
	return SCONV(j,ny)*nx*2 + SCONV(i,nx)
	
nx = int(sys.argv[1])
ny = int(sys.argv[2])
nedges = 2*nx*ny
nfaces = nx*ny
nverts = nx*ny
	
outfile = h5py.File('square/' + str(nx) + 'x' + str(ny) + '/' +  str(nx) + 'x' + str(ny) + '.hdf5', 'w')
sizes = outfile.create_group('sizes')
coords = outfile.create_group('coords')
connect = outfile.create_group('connectivity')
geometry = outfile.create_group('geometry')
sizes.create_dataset('nface',data=nfaces)
sizes.create_dataset('nedge',data=nedges)
sizes.create_dataset('nvert',data=nverts)

#geometry
Ai = np.ones(nfaces,dtype=np.float64)
Ae = np.ones(nedges,dtype=np.float64)
Av = np.ones(nverts,dtype=np.float64)
Aie = 0.5*np.ones((nedges,2),dtype=np.float64)
Aiv = 0.25*np.ones((nfaces,4),dtype=np.float64)
le = np.ones(nedges,dtype=np.float64)
de = np.ones(nedges,dtype=np.float64)
ne = np.zeros((nedges,2),dtype=np.float64)
te = np.zeros((nedges,2),dtype=np.float64)

#connectivity
nedgef = np.ones(nfaces,dtype=np.int32) * 4
nedgev = np.ones(nverts,dtype=np.int32) * 4
necp = np.ones(nedges,dtype=np.int32) * 7
necpd = np.ones(nedges,dtype=np.int32) * 7
NC = np.zeros((nfaces,4),dtype=np.int32)
EC = np.zeros((nfaces,4),dtype=np.int32)
VC = np.zeros((nfaces,4),dtype=np.int32)
CE = np.zeros((nedges,2),dtype=np.int32)
VE = np.zeros((nedges,2),dtype=np.int32)
CV = np.zeros((nverts,4),dtype=np.int32)
EV = np.zeros((nverts,4),dtype=np.int32)
ECP = np.zeros((nedges,7),dtype=np.int32)
ECPd = np.zeros((nedges,7),dtype=np.int32)
nei = np.zeros((nedges,2),dtype=np.int32)
tev = np.zeros((nedges,2),dtype=np.int32)
nei_cell = np.zeros((nfaces,4),dtype=np.int32)
tev_vertex = np.zeros((nverts,4),dtype=np.int32)

#coordinates
ccoords = np.zeros((nfaces,2),dtype=np.float64)
vcoords = np.zeros((nverts,2),dtype=np.float64)
ecoords = np.zeros((nedges,2),dtype=np.float64)
coffsets = np.zeros((nfaces,4,2),dtype=np.float64)
voffsets = np.zeros((nverts,4,2),dtype=np.float64)
eoffsets = np.zeros((nedges,4,2),dtype=np.float64)

			

			
#fill in arrays
for i in range(nx):
	for j in range(ny):
		#cells
		l = ijprimal_to_lprimal(i,j,nx,ny)
		EC[l,0] = ijvertedge_to_ledge(i,j,nx,ny)
		EC[l,1] = ijhorizedge_to_ledge(i,j,nx,ny)
		EC[l,2] = ijvertedge_to_ledge(i-1,j,nx,ny)
		EC[l,3] = ijhorizedge_to_ledge(i,j-1,nx,ny)
		
		VC[l,0] = ijdual_to_ldual(i,j,nx,ny)
		VC[l,1] = ijdual_to_ldual(i-1,j,nx,ny)
		VC[l,2] = ijdual_to_ldual(i-1,j-1,nx,ny)
		VC[l,3] = ijdual_to_ldual(i,j-1,nx,ny)
		
		nei_cell[l,0] = 1
		nei_cell[l,1] = 1
		nei_cell[l,2] = -1
		nei_cell[l,3] = -1
		
		ccoords[l,:] = np.array([i*1.,j*1.])
		coffsets[l,:,0] = np.array([1./2.,1./2.,-1./2.,-1./2.])
		coffsets[l,:,1] = np.array([1./2.,-1./2.,-1./2.,1./2.])
			
		#vertices
		l = ijdual_to_ldual(i,j,nx,ny)
		
		EV[l,0] = ijhorizedge_to_ledge(i+1,j,nx,ny)
		EV[l,1] = ijvertedge_to_ledge(i,j+1,nx,ny)
		EV[l,2] = ijhorizedge_to_ledge(i,j,nx,ny)
		EV[l,3] = ijvertedge_to_ledge(i,j,nx,ny)
		
		CV[l,0] = ijprimal_to_lprimal(i+1,j+1,nx,ny)
		CV[l,1] = ijprimal_to_lprimal(i,j+1,nx,ny)
		CV[l,2] = ijprimal_to_lprimal(i,j,nx,ny)
		CV[l,3] = ijprimal_to_lprimal(i+1,j,nx,ny)
		
		tev_vertex[l,0] = 1
		tev_vertex[l,1] = -1
		tev_vertex[l,2] = -1
		tev_vertex[l,3] = 1
			
		vcoords[l,:] = np.array([i*1. + 1./2.,j*1. + 1./2.])
		voffsets[l,:,0] = np.array([1./2.,1./2.,-1./2.,-1./2.])
		voffsets[l,:,1] = np.array([1./2.,-1./2.,-1./2.,1./2.])
			
		#edges
		lvert = ijvertedge_to_ledge(i,j,nx,ny)
		lhoriz = ijhorizedge_to_ledge(i,j,nx,ny)
		
		#horiz edges
		CE[lhoriz,0] = ijprimal_to_lprimal(i,j,nx,ny)
		CE[lhoriz,1] = ijprimal_to_lprimal(i,j+1,nx,ny)
		
		VE[lhoriz,0] = ijdual_to_ldual(i-1,j,nx,ny)
		VE[lhoriz,1] = ijdual_to_ldual(i,j,nx,ny)
		
		ECP[lhoriz,0] = ijhorizedge_to_ledge(i,j,nx,ny)
		ECP[lhoriz,1] = ijvertedge_to_ledge(i,j+1,nx,ny)
		ECP[lhoriz,2] = ijhorizedge_to_ledge(i,j+1,nx,ny)
		ECP[lhoriz,3] = ijvertedge_to_ledge(i-1,j+1,nx,ny)
		ECP[lhoriz,4] = ijvertedge_to_ledge(i-1,j,nx,ny)
		ECP[lhoriz,5] = ijhorizedge_to_ledge(i,j-1,nx,ny)
		ECP[lhoriz,6] = ijvertedge_to_ledge(i,j,nx,ny)
		
		tev[lhoriz,0] = 1
		tev[lhoriz,1] = -1
		
		nei[lhoriz,0] = 1
		nei[lhoriz,1] = -1

		ecoords[lhoriz,:] = np.array([i*1.,j*1. + 1./2.0])
		eoffsets[lhoriz,:,0] = np.array([1./2.,1./2.,-1./2.,-1./2.])
		eoffsets[lhoriz,:,1] = np.array([1./2.,-1./2.,-1./2.,1./2.])
		ne[lhoriz,:] = np.array([0.,1.])
		te[lhoriz,:] = np.array([-1.,0.])
			
		#vert edges
		CE[lvert,0] = ijprimal_to_lprimal(i,j,nx,ny)
		CE[lvert,1] = ijprimal_to_lprimal(i+1,j,nx,ny)
		
		VE[lvert,0] = ijdual_to_ldual(i,j-1,nx,ny)
		VE[lvert,1] = ijdual_to_ldual(i,j,nx,ny)
		
		ECP[lvert,0] = ijvertedge_to_ledge(i,j,nx,ny)
		ECP[lvert,1] = ijhorizedge_to_ledge(i+1,j,nx,ny)
		ECP[lvert,2] = ijvertedge_to_ledge(i+1,j,nx,ny)
		ECP[lvert,3] = ijhorizedge_to_ledge(i+1,j-1,nx,ny)
		ECP[lvert,4] = ijhorizedge_to_ledge(i,j-1,nx,ny)
		ECP[lvert,5] = ijvertedge_to_ledge(i-1,j,nx,ny)
		ECP[lvert,6] = ijhorizedge_to_ledge(i,j,nx,ny)
		
		tev[lvert,0] = -1
		tev[lvert,1] = 1
		
		nei[lvert,0] = 1
		nei[lvert,1] = -1

		ecoords[lvert,:] = np.array([i+1/2.,j])
		eoffsets[lvert,:,0] = np.array([1./2.,1./2.,-1./2.,-1./2.])
		eoffsets[lvert,:,1] = np.array([1./2.,-1./2.,-1./2.,1./2.])
		ne[lvert,:] = np.array([1.,0.])
		te[lvert,:] = np.array([0.,1.])

#create ECPd FIX
#ECPd = np.concatenate((EV[VEi[:,0],:],EV[VEi[:,1],:]),axis=1)
#ECPd = np.apply_along_axis(np.unique,1,ECPd)
#necpd = np.sum(np.logical_not(np.equal(ECPd,0)),axis=1)

#create NC FIX

#create datasets
connect.create_dataset('NC', data=NC+1)
connect.create_dataset('EC', data=EC+1)
connect.create_dataset('VC', data=VC+1)
connect.create_dataset('nedgef', data=nedgef)
connect.create_dataset('CE', data=CE+1)
connect.create_dataset('VE', data=VE+1)
connect.create_dataset('CV', data=CV+1)
connect.create_dataset('EV', data=EV+1)
connect.create_dataset('nedgev', data=nedgev)
connect.create_dataset('ECP', data=ECP+1)
connect.create_dataset('ECPd', data=ECPd+1)
connect.create_dataset('nei_edge', data=nei)
connect.create_dataset('tev_edge', data=tev)
connect.create_dataset('nei_cell', data=nei_cell)
connect.create_dataset('tev_vertex', data=tev_vertex)

connect.create_dataset('necp', data=necp)
connect.create_dataset('necpd', data=necpd)

coords.create_dataset('ccoords', data=ccoords)
coords.create_dataset('vcoords', data=vcoords)
coords.create_dataset('ecoords', data=ecoords)
coords.create_dataset('eoffsets', data=eoffsets)
coords.create_dataset('voffsets', data=voffsets)
coords.create_dataset('coffsets', data=coffsets)

geometry.create_dataset('Ai', data=Ai)
geometry.create_dataset('le', data=le)
geometry.create_dataset('de', data=de)
geometry.create_dataset('Ae', data=Ae)
geometry.create_dataset('Av', data=Av)
geometry.create_dataset('Aie', data=Aie)
geometry.create_dataset('Aiv', data=Aiv)
geometry.create_dataset('ne', data=ne)
geometry.create_dataset('te', data=te)

outfile.close()
