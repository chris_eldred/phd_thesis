import numpy as np
import csv
import sys

vlevel = int(sys.argv[1])
ncells = 10*(2**vlevel)**2 + 2
nvertices = 20*(2**vlevel)**2
suffix = "{0:012d}"
suffix = suffix.format(ncells)

meshtype = int(sys.argv[2])
if meshtype == 1:
	meshname = 'tweaked'
	modifier = ''
if meshtype == 2:
	meshname = 'cvt'
	modifier = 'centroidal_'
if meshtype == 3:
	meshname = 'spring_beta=0.8'
	modifier = 'spring_beta0.8_'
if meshtype == 4:
	meshname = 'spring_beta=1.1'
	modifier = 'spring_beta1.1_'

NC_file = open('geodesic_data/' + meshname + '/NC_' + suffix,'rb')
VC_file = open('geodesic_data/' + meshname + '/VC_' + suffix,'rb')
EC_file = open('geodesic_data/' + meshname + '/EC_' + suffix,'wb')
EV_file = open('geodesic_data/' + meshname + '/EV_' + suffix,'wb')
CE_file = open('geodesic_data/' + meshname + '/CE_' + suffix,'wb')
VE_file = open('geodesic_data/' + meshname + '/VE_' + suffix,'wb')

edge_dict = {}

NC_reader = csv.reader(NC_file,delimiter=' ',skipinitialspace=True)
VC_reader = csv.reader(VC_file,delimiter=' ',skipinitialspace=True)
EC_writer = csv.writer(EC_file,delimiter=' ',skipinitialspace=True)
EV_writer = csv.writer(EV_file,delimiter=' ',skipinitialspace=True)
CE_writer = csv.writer(CE_file,delimiter=' ',skipinitialspace=True)
VE_writer = csv.writer(VE_file,delimiter=' ',skipinitialspace=True)

#create NC, VC, EV and EC arrays
NC = np.zeros((ncells,6),dtype=np.int32)
VC = np.zeros((ncells,6),dtype=np.int32)
EC = np.zeros((ncells,6),dtype=np.int32)
EV = np.zeros((nvertices,3),dtype=np.int32)
ECsize = np.zeros(ncells,dtype=np.int32)
EVsize = np.zeros(nvertices,dtype=np.int32)
i = 0
for row in NC_reader:
	NC[i,:] = np.array(row,dtype=np.int32) - 1
	i = i + 1
i = 0
for row in VC_reader:
	VC[i,:] = np.array(row,dtype=np.int32) - 1
	i = i + 1
	
#loop over faces and generate VC + vertex_coords
curr_edge = 1
for i in xrange(ncells):
	cell_NC = NC[i,:]
	cell_VC = VC[i,:]
	cell_len = 6
	if cell_NC[-1] == -1:
		cell_len = 5
	
	for j in xrange(cell_len):
		VE0 = cell_VC[j%cell_len]
		VE1 = cell_VC[(j-1)%cell_len]
		#VC1 = VC[cell_NC[j],:]
		#print np.intersect1d(np.intersect1d(VC1,cell_VC),(VE0,VE1))
		CE0 = i
		CE1 = cell_NC[j]
		VE_double = [VE0+1,VE1+1] #adjust back to 1-based indexing
		CE_double = [CE0+1,CE1+1] #adjust back to 1-based indexing
		VE_double.sort()
		VE_double = tuple(VE_double)
		if VE_double in edge_dict:
			continue
		edge_dict[VE_double] = curr_edge
		EC[CE0,ECsize[CE0]] = curr_edge
		EC[CE1,ECsize[CE1]] = curr_edge
		EV[VE0,EVsize[VE0]] = curr_edge
		EV[VE1,EVsize[VE1]] = curr_edge

		ECsize[CE0] = ECsize[CE0] + 1
		ECsize[CE1] = ECsize[CE1] + 1
		EVsize[VE0] = EVsize[VE0] + 1
		EVsize[VE1] = EVsize[VE1] + 1

		CE_writer.writerow(CE_double)
		VE_writer.writerow(VE_double)
		#print curr_edge,CE0,EC[CE0,:]
		#print curr_edge,CE1,EC[CE1,:]
		curr_edge = curr_edge + 1
for i in xrange(ncells):
	EC_writer.writerow(EC[i,:])
for i in xrange(nvertices):
	EV_writer.writerow(EV[i,:])
	
print meshname, vlevel, curr_edge-1
