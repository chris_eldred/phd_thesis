import scipy.sparse as spsparse
import scipy.sparse.linalg as sparselinalg
import scipy.linalg as linalg
import numpy as np
import sys
import time
import h5py

start = time.clock()

gridtype = sys.argv[1]
nx = int(sys.argv[2])
ny = int(sys.argv[3])
ncells = nx * ny

if gridtype == 'hex':
	meshfilename = 'hex/' + str(nx) + 'x' + str(ny) + '/' + str(nx) + 'x' + str(ny) + '.hdf5'
	output_name = 'hex/' + str(nx) + 'x' + str(ny) + '/Q' + str(nx) + 'x' + str(ny) + '.hdf5'
	neev = 90

if gridtype == 'square':
	meshfilename = 'square/' + str(nx) + 'x' + str(ny) + '/' + str(nx) + 'x' + str(ny) + '.hdf5'
	output_name = 'square/' + str(nx) + 'x' + str(ny) + '/Q' + str(nx) + 'x' + str(ny) + '.hdf5'
	neev = 24
	
mesh = h5py.File(meshfilename,'r')
tev_edge = np.array(mesh['connectivity']['tev_edge'],dtype=np.int32)
nei_cell = np.array(mesh['connectivity']['nei_cell'],dtype=np.int32)
EC_all = np.array(mesh['connectivity']['EC'],dtype=np.int32)
VC_all = np.array(mesh['connectivity']['VC'],dtype=np.int32) 
EV_all = np.array(mesh['connectivity']['EV'],dtype=np.int32)
VE_all = np.array(mesh['connectivity']['VE'],dtype=np.int32) 
nedgef = np.array(mesh['connectivity']['nedgef'],dtype=np.int32)
Ai = np.array(mesh['geometry']['Ai'],dtype=np.float64)
Aiv = np.array(mesh['geometry']['Aiv'],dtype=np.float64)
Riv = Aiv / np.expand_dims(Ai,axis=1)
load_time = time.clock()

outputfile = h5py.File(output_name, 'w')
num_coeffs = outputfile.create_dataset('num_coeffs',(ncells,1),dtype='int32')
coeff_indices = outputfile.create_dataset('coeff_indices',(ncells,neev,3),dtype='int32')
coeffs = outputfile.create_dataset('coeffs',(ncells,neev,1),dtype='float64')
lsqr_out_int = outputfile.create_dataset('lsqr_out_int',(ncells,2),dtype='int32')
lsqr_out_float = outputfile.create_dataset('lsqr_out_float',(ncells,6),dtype='float64')

for cell in xrange(ncells):
	EC = list(EC_all[cell,:] - 1)
	VC = list(VC_all[cell,:] - 1)
	ECsize = nedgef[cell]
	if EC[-1] == -1:
		EC.pop()
		VC.pop()
	#create A and b
	ne = ECsize
	nv = ECsize #cells have same number of edges and vertices
	n = ne * nv * (nv + 1) / 2 #number of equations
	m = nv * ne * (ne - 1) / 2 #number of unknowns
	A = spsparse.dok_matrix((n,m))
	b = np.zeros(n)

	#construct vertex pairs
	vpairs = set()
	for i in xrange(nv):
		for j in xrange(i,nv):
			vpairs.add((VC[i],VC[j]))
			
	#create alphavector
	alphavector = []
	alphaarray = np.zeros((m,3),dtype=np.int32)
	l = 0
	for i in xrange(ne):
		for j in xrange(i+1,ne):
			edge = EC[i]
			eprime = EC[j]
			for k in range(nv):
				vertex = VC[k]
				alpha = 'alpha_' + str(edge) + '_' + str(eprime) + '_' + str(vertex)
				alphavector.append(alpha)
				alphaarray[l] = (edge,eprime,vertex)
				l = l + 1
	
	#fill in A and b
	i = 0
	for e in EC:
		VE = list(VE_all[e,:]-1)
		for vpair in vpairs:
			v = vpair[0]
			vprime = vpair[1]
			EV = list(EV_all[v,:] - 1)
			EVprime = list(EV_all[vprime,:] - 1)
			if EV[-1] == -1:
				EV.pop()
			if EVprime[-1] == -1:
				EVprime.pop()
			EVE = (set(EC).intersection(EV)).difference([e])
			EVprimeE = (set(EC).intersection(EVprime)).difference([e])
			if v == vprime:
				for eprime in EVE: 
					sgn = -1.0
					alpha = 'alpha_' + str(e) + '_' + str(eprime) + '_' + str(v)
					VEprime =  list(VE_all[eprime,:] - 1) 
					tev = tev_edge[eprime,VEprime.index(v)]
					if (EC.index(e) > EC.index(eprime)):
						sgn = 1.0
						alpha = 'alpha_' + str(eprime) + '_' + str(e) + '_' + str(v)			
					val = tev * sgn
					j = alphavector.index(alpha)
					A[i,j] = val
				riv = Riv[cell,VC.index(v)]
				nei = nei_cell[cell,EC.index(e)]
				alphaval = -4/24.
				b[i] = riv/2. * nei
				if v in VE:
					b[i] = (riv/2. + alphaval)*nei

			else:
				for eprime in EVE: 
					sgn = -1.0
					alpha = 'alpha_' + str(e) + '_' + str(eprime) + '_' + str(vprime) 	
					VEprime =  list(VE_all[eprime,:] - 1) 
					tev = tev_edge[eprime,VEprime.index(v)]			
					if (EC.index(e) > EC.index(eprime)):
						sgn = 1.0
						alpha = 'alpha_' + str(eprime) + '_' + str(e) + '_' + str(vprime)	
					val = tev * sgn
					j = alphavector.index(alpha)
					A[i,j] = val
				for eprime in EVprimeE: 
					sgn = -1.0
					alpha = 'alpha_' + str(e) + '_' + str(eprime) + '_' + str(v) 
					VEprime =  list(VE_all[eprime,:] - 1) 
					tev = tev_edge[eprime,VEprime.index(vprime)]
					if (EC.index(e) > EC.index(eprime)):
						sgn = 1.0
						alpha = 'alpha_' + str(eprime) + '_' + str(e) + '_' + str(v)	
					val = tev * sgn
					j = alphavector.index(alpha)
					A[i,j] = val
				b[i] = 0.	
				nei = nei_cell[cell,EC.index(e)]
				alphaval2 = -4/24.
				if (v in VE) and (vprime in VE):
					b[i] = alphaval2*nei
			i = i + 1
	
	#ADD QL- APPEARS TO NOT BE NEEDED, CAN I PROVE THIS?

	#finish up construction of A/b
	A.tocsr()
	
	#Solve A and b for alphas
	x,istop,itn,r1norm,r2norm,anorm,acond,arnorm,xnorm,var = sparselinalg.lsqr(A,b)

	lsqr_out_int[cell,:] = np.array((istop,itn))
	lsqr_out_float[cell,:] = np.array((r1norm,r2norm,anorm,acond,arnorm,xnorm))

	#set solutions
	num_coeffs[cell,0] = len(alphavector)
	coeff_indices[cell,:len(alphavector),:] = alphaarray
	coeffs[cell,:len(alphavector),0] = x
		
outputfile.close()
end_time = time.clock()

print 'total time = ', end_time - start
print 'mesh creation time = ', load_time - start
print 'solution with output time = ', end_time - load_time
print 'per cell sol and output time = ', (end_time - load_time)/ncells
