import numpy as np
import csv
import sys

def voronoi_corner(p1,p2,p3):
	temp1 = np.zeros(3,dtype=np.float64)
	temp2 = np.zeros(3,dtype=np.float64)
	temp1[0] = p2[0] - p1[0]
	temp1[1] = p2[1] - p1[1]
	temp1[2] = p2[2] - p1[2]
	temp2[0] = p3[0] - p1[0]
	temp2[1] = p3[1] - p1[1]
	temp2[2] = p3[2] - p1[2]
	
	temp3 = np.cross(temp1,temp2)
	return unit_vector (temp3)
	
def unit_vector(p):
	return p / np.sqrt(np.dot(p,p))

vlevel = int(sys.argv[1])
ncells = 10*(2**vlevel)**2 + 2
nvertices = 20*(2**vlevel)**2
suffix = "{0:012d}"
suffix = suffix.format(ncells)

meshtype = int(sys.argv[2])
if meshtype == 1:
	meshname = 'tweaked'
	modifier = ''
if meshtype == 2:
	meshname = 'cvt'
	modifier = 'centroidal_'
if meshtype == 3:
	meshname = 'spring_beta=0.8'
	modifier = 'spring_beta0.8_'
if meshtype == 4:
	meshname = 'spring_beta=1.1'
	modifier = 'spring_beta1.1_'

cell_coords_file = open('geodesic_data/' + meshname + '/point_' + modifier + suffix,'rb')
nghbr_file = open('geodesic_data/' + meshname + '/nghbr_' + suffix,'rb')
NC_file = open('geodesic_data/' + meshname + '/NC_' + suffix,'wb')
VC_file = open('geodesic_data/' + meshname + '/VC_' + suffix,'wb')
CV_file = open('geodesic_data/' + meshname + '/CV_' + suffix,'wb')
vertex_coords_file = open('geodesic_data/' + meshname + '/vertex_coords_'+ suffix,'wb')

vertex_dict = {}

cell_coords_reader = csv.reader(cell_coords_file,delimiter=' ',skipinitialspace=True)
cell_NC_reader = csv.reader(nghbr_file,delimiter=' ',skipinitialspace=True)
cell_VC_writer = csv.writer(VC_file,delimiter=' ',skipinitialspace=True)
vertex_CV_writer = csv.writer(CV_file,delimiter=' ',skipinitialspace=True)
vertex_coords_writer = csv.writer(vertex_coords_file,delimiter=' ',skipinitialspace=True)
cell_NC_writer = csv.writer(NC_file,delimiter=' ',skipinitialspace=True)

#create coordinates array
cell_coord_array = np.zeros((ncells,3),dtype=np.float64)
i = 0
for cell_coords in cell_coords_reader:
	cell_coord_array[i] = np.array(cell_coords,dtype=np.float64)
	i = i + 1
	
#loop over faces and generate VC + vertex_coords
curr_cell = 1
curr_vertex = 1
for cell_NC in cell_NC_reader:
	cell_len = 6
	if cell_NC.count('-1') == 1:
		cell_NC.remove('-1')
		cell_NC.insert(len(cell_NC),0)
		cell_len = 5
	cell_VC = []
	#loop over vertices
	for i in range(cell_len):
		#create CV triplet
		CV_triplet = [curr_cell,int(cell_NC[i]),int(cell_NC[(i+1)%cell_len])]
		CV_triplet.sort()
		CV_triplet = tuple(CV_triplet)
		#check if CV triplet is new
		if CV_triplet in vertex_dict:
			#add vertex to cell_VC list
			cell_VC.append(vertex_dict[CV_triplet])
			continue
		#new, add to vertex_dict
		vertex_dict[CV_triplet] = curr_vertex
		#add vertex to cell_VC list
		cell_VC.append(curr_vertex)
		#write vertex coordinates
		p1 = cell_coord_array[curr_cell-1]
		p2 = cell_coord_array[int(cell_NC[i])-1]
		p3 = cell_coord_array[int(cell_NC[(i+1)%cell_len])-1]
		vertex_coords = voronoi_corner(p1,p2,p3)
		vertex_coords_writer.writerow(vertex_coords)
		vertex_CV_writer.writerow(CV_triplet)
		#print curr_vertex,CV_triplet
		#update vertex number
		curr_vertex = curr_vertex + 1
	if cell_len == 5:
		cell_VC.append(0)
	cell_VC_writer.writerow(cell_VC)
	cell_NC_writer.writerow(cell_NC)
	curr_cell = curr_cell + 1
	
print meshname, vlevel, curr_cell-1,curr_vertex-1
