import numpy as np
import h5py
from math import pi
import geometry as geo_helpers

inputfile = 'cubedsphere/thuburn/gridmap_cube_0000884736.dat'
f = open(inputfile,'rb')

#read ngrids
recl = np.fromfile(f, dtype='uint32', count=1)
ngrids = np.fromfile(f,dtype='int32',count=1)
recl = np.fromfile(f, dtype='uint32', count=1)

#read nface/nedge/nvert
recl = np.fromfile(f, dtype='uint32', count=1)
nface = np.fromfile(f,dtype='int32',count=ngrids)
recl = np.fromfile(f, dtype='uint32', count=1)

recl = np.fromfile(f, dtype='uint32', count=1)
nedge = np.fromfile(f,dtype='int32',count=ngrids)
recl = np.fromfile(f, dtype='uint32', count=1)

recl = np.fromfile(f, dtype='uint32', count=1)
nvert = np.fromfile(f,dtype='int32',count=ngrids)
recl = np.fromfile(f, dtype='uint32', count=1)

###############CONNECTIVITY#################

#read nedgef and nedgev
nedgef = [np.zeros((1)) for i in range(ngrids)]
recl = np.fromfile(f, dtype='uint32', count=1)
for i in range(ngrids):
	nedgef[i] = np.fromfile(f,dtype='int32',count=nface[i])
recl = np.fromfile(f, dtype='uint32', count=1)

nedgev = [np.zeros((1)) for i in range(ngrids)]
recl = np.fromfile(f, dtype='uint32', count=1)
for i in range(ngrids):
	nedgev[i] = np.fromfile(f,dtype='int32',count=nvert[i])
recl = np.fromfile(f, dtype='uint32', count=1)

#read NC
NC = [np.zeros((1)) for i in range(ngrids)]
recl = np.fromfile(f, dtype='uint32', count=1)
for i in range(ngrids):
	NC[i] = np.fromfile(f,dtype='int32',count=nface[i]*4)
recl = np.fromfile(f, dtype='uint32', count=1)

#read EC
EC = [np.zeros((1)) for i in range(ngrids)]
recl = np.fromfile(f, dtype='uint32', count=1)
for i in range(ngrids):
	EC[i] = np.fromfile(f,dtype='int32',count=nface[i]*4)
recl = np.fromfile(f, dtype='uint32', count=1)

#read VC
VC = [np.zeros((1)) for i in range(ngrids)]
recl = np.fromfile(f, dtype='uint32', count=1)
for i in range(ngrids):
	VC[i] = np.fromfile(f,dtype='int32',count=nface[i]*4)
recl = np.fromfile(f, dtype='uint32', count=1)

#read CE
CE = [np.zeros((1)) for i in range(ngrids)]
recl = np.fromfile(f, dtype='uint32', count=1)
for i in range(ngrids):
	CE[i] = np.fromfile(f,dtype='int32',count=nedge[i]*2)
recl = np.fromfile(f, dtype='uint32', count=1)

#read VE
VE = [np.zeros((1)) for i in range(ngrids)]
recl = np.fromfile(f, dtype='uint32', count=1)
for i in range(ngrids):
	VE[i] = np.fromfile(f,dtype='int32',count=nedge[i]*2)
recl = np.fromfile(f, dtype='uint32', count=1)

#read CV
CV = [np.zeros((1)) for i in range(ngrids)]
recl = np.fromfile(f, dtype='uint32', count=1)
for i in range(ngrids):
	CV[i] = np.fromfile(f,dtype='int32',count=nvert[i]*4)
recl = np.fromfile(f, dtype='uint32', count=1)

#read EV
EV = [np.zeros((1)) for i in range(ngrids)]
recl = np.fromfile(f, dtype='uint32', count=1)
for i in range(ngrids):
	EV[i] = np.fromfile(f,dtype='int32',count=nvert[i]*4)
recl = np.fromfile(f, dtype='uint32', count=1)

###############GEOMETRY#################
#read clon
clon = [np.zeros((1)) for i in range(ngrids)]
recl = np.fromfile(f, dtype='uint32', count=1)
for i in range(ngrids):
	clon[i] = np.fromfile(f,dtype='float64',count=nface[i])
recl = np.fromfile(f, dtype='uint32', count=1)

#read clat
clat = [np.zeros((1)) for i in range(ngrids)]
recl = np.fromfile(f, dtype='uint32', count=1)
for i in range(ngrids):
	clat[i] = np.fromfile(f,dtype='float64',count=nface[i])
recl = np.fromfile(f, dtype='uint32', count=1)

#read vlon
vlon = [np.zeros((1)) for i in range(ngrids)]
recl = np.fromfile(f, dtype='uint32', count=1)
for i in range(ngrids):
	vlon[i] = np.fromfile(f,dtype='float64',count=nvert[i])
recl = np.fromfile(f, dtype='uint32', count=1)

#read vlat
vlat = [np.zeros((1)) for i in range(ngrids)]
recl = np.fromfile(f, dtype='uint32', count=1)
for i in range(ngrids):
	vlat[i] = np.fromfile(f,dtype='float64',count=nvert[i])
recl = np.fromfile(f, dtype='uint32', count=1)

#read Ai
Ai = [np.zeros((1)) for i in range(ngrids)]
recl = np.fromfile(f, dtype='uint32', count=1)
for i in range(ngrids):
	Ai[i] = np.fromfile(f,dtype='float64',count=nface[i])
recl = np.fromfile(f, dtype='uint32', count=1)

#read le
le = [np.zeros((1)) for i in range(ngrids)]
recl = np.fromfile(f, dtype='uint32', count=1)
for i in range(ngrids):
	le[i] = np.fromfile(f,dtype='float64',count=nedge[i])
recl = np.fromfile(f, dtype='uint32', count=1)

#read de
de = [np.zeros((1)) for i in range(ngrids)]
recl = np.fromfile(f, dtype='uint32', count=1)
for i in range(ngrids):
	de[i] = np.fromfile(f,dtype='float64',count=nedge[i])
recl = np.fromfile(f, dtype='uint32', count=1)

###############OPERATORS#################

##read I stencil size
#Isize = [np.zeros((1)) for i in range(ngrids)]
#recl = np.fromfile(f, dtype='uint32', count=1)
#for i in range(ngrids):
	#Isize[i] = np.fromfile(f,dtype='int32',count=nface[i])
#recl = np.fromfile(f, dtype='uint32', count=1)

##read J stencil size
#Jsize = [np.zeros((1)) for i in range(ngrids)]
#recl = np.fromfile(f, dtype='uint32', count=1)
#for i in range(ngrids):
	#Jsize[i] = np.fromfile(f,dtype='int32',count=nvert[i])
#recl = np.fromfile(f, dtype='uint32', count=1)

##read H stencil size
#Hsize = [np.zeros((1)) for i in range(ngrids)]
#recl = np.fromfile(f, dtype='uint32', count=1)
#for i in range(ngrids):
	#Hsize[i] = np.fromfile(f,dtype='int32',count=nedge[i])
#recl = np.fromfile(f, dtype='uint32', count=1)

##read R stencil size
#Rsize = [np.zeros((1)) for i in range(ngrids)]
#recl = np.fromfile(f, dtype='uint32', count=1)
#for i in range(ngrids):
	#Rsize[i] = np.fromfile(f,dtype='int32',count=nface[i])
#recl = np.fromfile(f, dtype='uint32', count=1)

##read I stencil
#Istencil = [np.zeros((1)) for i in range(ngrids)]
#recl = np.fromfile(f, dtype='uint32', count=1)
#for i in range(ngrids):
	#Istencil[i] = np.fromfile(f,dtype='int32',count=nface[i])
#recl = np.fromfile(f, dtype='uint32', count=1)

##read J stencil
#Jstencil = [np.zeros((1)) for i in range(ngrids)]
#recl = np.fromfile(f, dtype='uint32', count=1)
#for i in range(ngrids):
	#Jstencil[i] = np.fromfile(f,dtype='int32',count=nvert[i])
#recl = np.fromfile(f, dtype='uint32', count=1)

##read H stencil
#Hstencil = [np.zeros((1)) for i in range(ngrids)]
#recl = np.fromfile(f, dtype='uint32', count=1)
#for i in range(ngrids):
	#Hstencil[i] = np.fromfile(f,dtype='int32',count=nedge[i]*5)
#recl = np.fromfile(f, dtype='uint32', count=1)

##read R stencil
#Rstencil = [np.zeros((1)) for i in range(ngrids)]
#recl = np.fromfile(f, dtype='uint32', count=1)
#for i in range(ngrids):
	#Rstencil[i] = np.fromfile(f,dtype='int32',count=nface[i]*4)
#recl = np.fromfile(f, dtype='uint32', count=1)

##read I coeffs
#Icoeffs = [np.zeros((1)) for i in range(ngrids)]
#recl = np.fromfile(f, dtype='uint32', count=1)
#for i in range(ngrids):
	#Icoeffs[i] = np.fromfile(f,dtype='float64',count=nface[i])
#recl = np.fromfile(f, dtype='uint32', count=1)

##read J coeffs
#Jcoeffs = [np.zeros((1)) for i in range(ngrids)]
#recl = np.fromfile(f, dtype='uint32', count=1)
#for i in range(ngrids):
	#Jcoeffs[i] = np.fromfile(f,dtype='float64',count=nvert[i])
#recl = np.fromfile(f, dtype='uint32', count=1)

##read H coeffs
#Hcoeffs = [np.zeros((1)) for i in range(ngrids)]
#recl = np.fromfile(f, dtype='uint32', count=1)
#for i in range(ngrids):
	#Hcoeffs[i] = np.fromfile(f,dtype='float64',count=nedge[i]*5)
#recl = np.fromfile(f, dtype='uint32', count=1)

##read R coeffs
#Rcoeffs = [np.zeros((1)) for i in range(ngrids)]
#recl = np.fromfile(f, dtype='uint32', count=1)
#for i in range(ngrids):
	#Rcoeffs[i] = np.fromfile(f,dtype='float64',count=nface[i]*4)
#recl = np.fromfile(f, dtype='uint32', count=1)

##read inj stencil size
#injsize = [np.zeros((1)) for i in range(ngrids)]
#recl = np.fromfile(f, dtype='uint32', count=1)
#for i in range(ngrids-1):
	#injsize[i] = np.fromfile(f,dtype='int32',count=nface[i])
#recl = np.fromfile(f, dtype='uint32', count=1)

##read inj stencil
#injstencil = [np.zeros((1)) for i in range(ngrids)]
#recl = np.fromfile(f, dtype='uint32', count=1)
#for i in range(ngrids-1):
	#injstencil[i] = np.fromfile(f,dtype='int32',count=nface[i]*4)
#recl = np.fromfile(f, dtype='uint32', count=1)

##read inj coeffs
#injcoeffs = [np.zeros((1)) for i in range(ngrids)]
#recl = np.fromfile(f, dtype='uint32', count=1)
#for i in range(ngrids-1):
	#injcoeffs[i] = np.fromfile(f,dtype='float64',count=nface[i]*4)
#recl = np.fromfile(f, dtype='uint32', count=1)

for i in range(ngrids):
	print 'creating grid',i+1

	#create cell coords
	ccoords = np.zeros((nface[i],3),order='F')
	coslon = np.cos(clon[i])
	coslat = np.cos(clat[i])
	sinlon = np.sin(clon[i])
	sinlat = np.sin(clat[i])
	ccoords[:,0] = coslon * coslat
	ccoords[:,1] = sinlon * coslat
	ccoords[:,2] = sinlat

	#create vertex coords
	vcoords = np.zeros((nvert[i],3),order='F')
	coslon = np.cos(vlon[i])
	coslat = np.cos(vlat[i])
	sinlon = np.sin(vlon[i])
	sinlat = np.sin(vlat[i])
	vcoords[:,0] = coslon * coslat
	vcoords[:,1] = sinlon * coslat
	vcoords[:,2] = sinlat
	
	#get edge connectivity and endpoint coords
	CEi = np.reshape(CE[i],(nedge[i],2),order='F') - 1
	VEi = np.reshape(VE[i],(nedge[i],2),order='F') - 1
	v0 = vcoords[VEi[:,0],:]
	v1 = vcoords[VEi[:,1],:]
	c0 = ccoords[CEi[:,0],:]
	c1 = ccoords[CEi[:,1],:]
	
	#create edge coords
	#normal to plane of primal edge
	nx1 = np.cross(v0,v1)
	
	#normal to plane of dual edge
	nx2 = np.cross(c0,c1)
	
	#perpendicular to both is crossing point = edge coord
	ecoords = np.cross(nx2,nx1)
	ecoords = ecoords / np.expand_dims(np.linalg.norm(ecoords,axis=1),axis=1)
	
	elon = np.arctan2(ecoords[:,1],ecoords[:,0])
	elat = np.arcsin(ecoords[:,2])
	
	#adjust longitude #IS THIS NEEDED????
	clon[i] = clon[i] - pi
	vlon[i] = vlon[i] - pi
	
	#create Aie and Ae
	Aie = np.zeros((nedge[i],2),dtype=np.float64,order='F')
	d12 = 2.0*np.arcsin(0.5*np.sqrt(np.sum(np.square(c0-v0),axis=1)))
	d13 = 2.0*np.arcsin(0.5*np.sqrt(np.sum(np.square(c0-v1),axis=1)))
	d23 = 2.0*np.arcsin(0.5*np.sqrt(np.sum(np.square(v0-v1),axis=1)))
	p = 0.5 * (d12 + d13 + d23)
	T0 = np.tan(0.5 * (p-d12))
	T1 = np.tan(0.5 * (p-d13))
	T2 = np.tan(0.5 * (p-d23))
	T3 = np.tan(0.5 * p)
	Aie[:,0] = 4.0 * np.arctan(np.sqrt(T0 * T1 * T2 * T3))

	d12 = 2.0*np.arcsin(0.5*np.sqrt(np.sum(np.square(c1-v0),axis=1)))
	d13 = 2.0*np.arcsin(0.5*np.sqrt(np.sum(np.square(c1-v1),axis=1)))
	d23 = 2.0*np.arcsin(0.5*np.sqrt(np.sum(np.square(v0-v1),axis=1)))
	p = 0.5 * (d12 + d13 + d23)
	T0 = np.tan(0.5 * (p-d12))
	T1 = np.tan(0.5 * (p-d13))
	T2 = np.tan(0.5 * (p-d23))
	T3 = np.tan(0.5 * p)
	Aie[:,1] = 4.0 * np.arctan(np.sqrt(T0 * T1 * T2 * T3))
	Ae = Aie[:,0] + Aie[:,1]
	
	#create ECP
	ECi = np.reshape(EC[i],(nface[i],4),order='F')
	ECP = np.concatenate((ECi[CEi[:,0],:],ECi[CEi[:,1],:]),axis=1)
	ECP = np.apply_along_axis(np.unique,1,ECP)

	#create ECPd
	EVi = np.reshape(EV[i],(nvert[i],4),order='F')
	ECPd = np.concatenate((EVi[VEi[:,0],:],EVi[VEi[:,1],:]),axis=1)
	ECPd = np.apply_along_axis(np.unique,1,ECPd)
	ECPd = np.roll(ECPd,-1,axis=1) #moves 0s to end of arrays
	necpd = np.sum(np.logical_not(np.equal(ECPd,0)),axis=1)

	#create nei/tev
	nei = np.zeros((nedge[i],2),dtype=np.int32,order='F')
	tev = np.zeros((nedge[i],2),dtype=np.int32,order='F')
	nei[:,0] = 1
	nei[:,1] = -1
	ne = np.expand_dims(nei[:,0],1)*c0 + np.expand_dims(nei[:,1],1)*c1
	te1 = np.cross(ecoords,ne)
	te2 = v1 - v0
	tev[:,0] = 1 * np.sign(np.sum(np.multiply(te1,te2),axis=1))
	tev[:,1] =  -1 *  np.sign(np.sum(np.multiply(te1,te2),axis=1))
	
	EVi = np.reshape(EV[i],(nvert[i],4),order='F')
	nei_cell = geo_helpers.geometry.set_nei_cell(nedgef[i],ECi,CEi+1,nei)
	tev_vertex = geo_helpers.geometry.set_tev_vertex(nedgev[i],EVi,VEi+1,tev)

	#create Av
	Av = geo_helpers.geometry.set_av(nedgev[i],EVi,CEi+1,ccoords,vcoords)

	#create Aiv and Riv
	VCi = np.reshape(VC[i],(nface[i],4),order='F')
	Aiv = geo_helpers.geometry.set_aiv(nedgef[i],ECi,VCi,VEi+1,ccoords,vcoords,ecoords)
	
	#adjust EV,CV so that empty spots are duplicated instead of 0 (fixes some plotting stuff)
	#(EV[i])[np.equal(nedgev[i],3),-1] = (EV[i])[np.equal(nedgev[i],3),-2]
	#(CV[i])[np.equal(nedgev[i],3),-1] = (CV[i])[np.equal(nedgev[i],3),-2]
	#FIX THIS
	
	#checking geometry
	print 'Ai sum',np.sum(Ai[i]) - 4.*pi 
	print 'Av sum',np.sum(Av) - 4.*pi 
	print 'Ae sum',np.sum(Ae) - 4.*pi 
	print 'Aie sum',np.sum(Aie) - 4.*pi 
	print 'Aiv sum',np.sum(Aiv) - 4.*pi 
	test = np.sum(Aiv,axis=1) - Ai[i]
	print 'Aiv sum around cell i',np.amax(test),np.amin(test),np.average(test)
	test = np.sum(Aie,axis=1) - Ae
	print 'Aie sum around edge e',np.amax(test),np.amin(test),np.average(test)

	#outputing grid
	print 'outputing grid',i+1
	outfile = h5py.File('cubedsphere/thuburn/' + str(nface[i]) + '.hdf5', 'w')
	sizes = outfile.create_group('sizes')
	coords = outfile.create_group('coords')
	connect = outfile.create_group('connectivity')
	geometry = outfile.create_group('geometry')
	
	sizes.create_dataset('nface',data=nface[i])
	sizes.create_dataset('nedge',data=nedge[i])
	sizes.create_dataset('nvert',data=nvert[i])

	connect.create_dataset('NC', data=np.reshape(NC[i],(nface[i],4),order='F'))
	connect.create_dataset('EC', data=np.reshape(EC[i],(nface[i],4),order='F'))
	connect.create_dataset('VC', data=np.reshape(VC[i],(nface[i],4),order='F'))
	connect.create_dataset('nedgef', data=nedgef[i])
	connect.create_dataset('CE', data=np.reshape(CE[i],(nedge[i],2),order='F'))
	connect.create_dataset('VE', data=np.reshape(VE[i],(nedge[i],2),order='F'))
	connect.create_dataset('CV', data=np.reshape(CV[i],(nvert[i],4),order='F'))
	connect.create_dataset('EV', data=np.reshape(EV[i],(nvert[i],4),order='F'))
	connect.create_dataset('nedgev', data=nedgev[i])
	connect.create_dataset('ECP', data=np.reshape(ECP,(nedge[i],7),order='F'))
	connect.create_dataset('nei_edge', data=np.reshape(nei,(nedge[i],2),order='F'))
	connect.create_dataset('tev_edge', data=np.reshape(tev,(nedge[i],2),order='F'))
	connect.create_dataset('nei_cell', data=np.reshape(nei_cell,(nface[i],4),order='F'))
	connect.create_dataset('tev_vertex', data=np.reshape(tev_vertex,(nvert[i],4),order='F'))

	connect.create_dataset('necp', data=np.ones((ECP.shape[0]),dtype=np.int32)*7)
	connect.create_dataset('necpd', data=necpd)
	connect.create_dataset('ECPd', data=ECPd)

	coords.create_dataset('clon', data=clon[i])
	coords.create_dataset('clat', data=clat[i])
	coords.create_dataset('vlon', data=vlon[i])
	coords.create_dataset('vlat', data=vlat[i])
	coords.create_dataset('elon', data=elon)
	coords.create_dataset('elat', data=elat)
	coords.create_dataset('ccoords', data=ccoords)
	coords.create_dataset('vcoords', data=vcoords)
	coords.create_dataset('ecoords', data=ecoords)

	geometry.create_dataset('Ai', data=Ai[i])
	geometry.create_dataset('le', data=le[i])
	geometry.create_dataset('de', data=de[i])
	geometry.create_dataset('Ae', data=Ae)
	geometry.create_dataset('Av', data=Av)
	geometry.create_dataset('Aie', data=np.reshape(Aie,(nedge[i],2),order='F'))
	geometry.create_dataset('Aiv', data=np.reshape(Aiv,(nface[i],4),order='F'))
		
	outfile.close()


