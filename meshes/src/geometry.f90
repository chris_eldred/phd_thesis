MODULE geometry
IMPLICIT NONE

CONTAINS

! ================================================================
SUBROUTINE set_nei_cell(ecsize,ec,ce,nei_edge,nei_cell,nf,ne,maxec,maxce)
IMPLICIT NONE
INTEGER, INTENT(IN) :: nf, ne, maxec, maxce
INTEGER, INTENT(IN) :: ecsize(nf), ec(nf,maxec), ce(ne,maxce), nei_edge(ne,maxce) 
INTEGER, INTENT(OUT) :: nei_cell(nf,maxec)
INTEGER :: if1,ix,ie,if2

DO if1=1,nf
	DO ix=1,ecsize(if1) !LOOP OVER EC
		ie = ec(if1,ix)
		if2 = ce(ie,1)
		IF (if1 == if2) then
			nei_cell(if1,ix) = nei_edge(ie,1)
		ELSE
			nei_cell(if1,ix) = nei_edge(ie,2)
		ENDIF
	ENDDO
ENDDO
					
END SUBROUTINE
! ================================================================

! ================================================================
SUBROUTINE set_tev_vertex(evsize,ev,ve,tev_edge,tev_vertex,nv,ne,maxev,maxve)
IMPLICIT NONE
INTEGER, INTENT(IN) :: nv, ne, maxev, maxve
INTEGER, INTENT(IN) :: evsize(nv), ev(nv,maxev), ve(ne,maxve), tev_edge(ne,maxve) 
INTEGER, INTENT(OUT) :: tev_vertex(nv,maxev)
INTEGER :: iv1,ix,ie,iv2

DO iv1=1,nv
	DO ix=1,evsize(iv1) !LOOP OVER EV
		ie = ev(iv1,ix)
		iv2 = ve(ie,1)
		IF (iv1 == iv2) then
			tev_vertex(iv1,ix) = tev_edge(ie,1)
		ELSE
			tev_vertex(iv1,ix) = tev_edge(ie,2)
		ENDIF
	ENDDO
ENDDO
					
END SUBROUTINE
! ================================================================

! ================================================================
SUBROUTINE set_Ai(ecsize,ec,ve,ccoords,vcoords,Ai,nf,nv,ne,maxec)
IMPLICIT NONE
INTEGER, INTENT(IN) :: nv,nf,ne,maxec
INTEGER, INTENT(IN) :: ecsize(nf), ec(nf,maxec), ve(ne,2)
REAL*8, INTENT(IN) :: ccoords(nf,3), vcoords(nv,3)
REAL*8, INTENT(OUT) :: Ai(nf)
INTEGER :: if1,ix,iv1,iv2,ie
REAL*8 :: temp
Ai = 0.0d0

DO if1=1,nf
	DO ix=1,ecsize(if1)
		ie = ec(if1,ix)
		iv1 = ve(ie,1)
		iv2 = ve(ie,2)
		CALL spherical_area(ccoords(if1,:),vcoords(iv1,:),vcoords(iv2,:),temp)
		Ai(if1) = Ai(if1) + temp
	ENDDO
ENDDO
END SUBROUTINE
! ================================================================

! ================================================================
SUBROUTINE set_Av(evsize,ev,ce,ccoords,vcoords,Av,nf,nv,ne,maxev)
IMPLICIT NONE
INTEGER, INTENT(IN) :: nv,nf,ne,maxev
INTEGER, INTENT(IN) :: evsize(nv), ev(nv,maxev), ce(ne,2)
REAL*8, INTENT(IN) :: ccoords(nf,3), vcoords(nv,3)
REAL*8, INTENT(OUT) :: Av(nv)
INTEGER :: iv1,ix,ic1,ic2,ie
REAL*8 :: temp
Av = 0.0d0

DO iv1=1,nv
	DO ix=1,evsize(iv1)
		ie = ev(iv1,ix)
		ic1 = ce(ie,1)
		ic2 = ce(ie,2)
		CALL spherical_area(ccoords(ic1,:),ccoords(ic2,:),vcoords(iv1,:),temp)
		Av(iv1) = Av(iv1) + temp
	ENDDO
ENDDO
END SUBROUTINE
! ================================================================

! ================================================================
SUBROUTINE set_Aiv(ecsize,ec,vc,ve,ccoords,vcoords,ecoords,Aiv,nf,nv,ne,maxec)
IMPLICIT NONE
INTEGER, INTENT(IN) :: nv,nf,ne,maxec
INTEGER, INTENT(IN) :: ecsize(nf), ec(nf,maxec), vc(nf,maxec), ve(ne,2)
REAL*8, INTENT(IN) :: ccoords(nf,3), vcoords(nv,3),ecoords(ne,3)
REAL*8, INTENT(OUT) :: Aiv(nf,maxec)
INTEGER :: if1,ix1,ix2,ix3,iv,ie
REAL*8 :: temp
Aiv = 0.0d0

DO if1=1,nf
	DO ix1=1,ecsize(if1)
		ie = ec(if1,ix1)
		DO ix2=1,2
		iv = ve(ie,ix2)
		CALL find_index(iv,vc(if1,:),ecsize(if1),ix3)
		CALL spherical_area(ccoords(if1,:),vcoords(iv,:),ecoords(ie,:),temp)
		!write(*,*) if1,ie,iv,temp
		Aiv(if1,ix3) = Aiv(if1,ix3) + temp	
		ENDDO
	ENDDO
ENDDO
END SUBROUTINE
! ================================================================

! ================================================================
SUBROUTINE spherical_area(p1,p2,p3,area)
IMPLICIT NONE
REAL*8, INTENT(IN) :: p1(3),p2(3),p3(3)
REAL*8, INTENT(OUT) :: area
REAL*8 :: D0,D1,D2,S,T0,T1,T2,T3
!
!
!     Distances between pairs of points
CALL SPDIST(p1,p2,D2)
CALL SPDIST(p2,p3,D0)
CALL SPDIST(p3,p1,D1)
!
!     Half perimeter
S=0.5D0*(D0+D1+D2)
!
!     Tangents
T0 = TAN(0.5D0*(S-D0))
T1 = TAN(0.5D0*(S-D1))
T2 = TAN(0.5D0*(S-D2))
T3 = TAN(0.5D0*S)
!
!     Area
area = 4.0D0*ATAN(SQRT(T0*T1*T2*T3))
!write(*,*) S,D0,D1,D2

END SUBROUTINE
! ================================================================


! ================================================================
SUBROUTINE SPDIST(p1,p2,s)
!
!     Calculate the spherical distance S between two points with Cartesian
!     coordinates (X1,Y1,Z1), (X2,Y2,Z2) on the unit sphere

IMPLICIT NONE

REAL*8, INTENT(IN) :: p1(3),p2(3)
REAL*8, INTENT(OUT) :: s
REAL*8 ::  DX, DY, DZ, AD


DX = p2(1) - p1(1)
DY = p2(2) - p1(2)
DZ = p2(3) - p1(3)
AD = SQRT(DX*DX + DY*DY + DZ*DZ)
S = 2.0D0*ASIN(0.5D0*AD)

END SUBROUTINE
! ================================================================

! ================================================================
SUBROUTINE find_index(target_value,arr,narr,loc)
IMPLICIT NONE
INTEGER, INTENT(IN) :: target_value, arr(narr)
INTEGER, INTENT(IN) :: narr
INTEGER, INTENT(OUT) :: loc
INTEGER :: i

DO i = 1, narr
    IF (arr(i) == target_value) THEN
        loc = i
        EXIT
    ENDIF
ENDDO
END SUBROUTINE
! ================================================================

END MODULE geometry

