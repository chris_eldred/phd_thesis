import numpy as np
import h5py
from math import pi
import geometry as geo_helpers
import csv
import sys

vlevel = int(sys.argv[1])
ncells = 10*(2**vlevel)**2 + 2
nverts = 20*(2**vlevel)**2
nedges = 30*(2**vlevel)**2
suffix = "{0:012d}"
suffix = suffix.format(ncells)

meshtype = int(sys.argv[2])
if meshtype == 1:
	meshname = 'tweaked'
	modifier = ''
if meshtype == 2:
	meshname = 'cvt'
	modifier = 'centroidal_'
if meshtype == 3:
	meshname = 'spring_beta=0.8'
	modifier = 'spring_beta0.8_'
if meshtype == 4:
	meshname = 'spring_beta=1.1'
	modifier = 'spring_beta1.1_'

print 'reading',meshname,vlevel
###############CONNECTIVITY#################
NC_file = open('geodesic_data/' + meshname + '/NC_' + suffix,'rb')
VC_file = open('geodesic_data/' + meshname + '/VC_' + suffix,'rb')
EC_file = open('geodesic_data/' + meshname + '/EC_' + suffix,'rb')
CV_file = open('geodesic_data/' + meshname + '/CV_' + suffix,'rb')
EV_file = open('geodesic_data/' + meshname + '/EV_' + suffix,'rb')
CE_file = open('geodesic_data/' + meshname + '/CE_' + suffix,'rb')
VE_file = open('geodesic_data/' + meshname + '/VE_' + suffix,'rb')

NC_reader = csv.reader(NC_file,delimiter=' ',skipinitialspace=True)
VC_reader = csv.reader(VC_file,delimiter=' ',skipinitialspace=True)
EC_reader = csv.reader(EC_file,delimiter=' ',skipinitialspace=True)
CV_reader = csv.reader(CV_file,delimiter=' ',skipinitialspace=True)
EV_reader = csv.reader(EV_file,delimiter=' ',skipinitialspace=True)
CE_reader = csv.reader(CE_file,delimiter=' ',skipinitialspace=True)
VE_reader = csv.reader(VE_file,delimiter=' ',skipinitialspace=True)

NC = np.zeros((ncells,6),dtype=np.int32)
VC = np.zeros((ncells,6),dtype=np.int32)
EC = np.zeros((ncells,6),dtype=np.int32)
EV = np.zeros((nverts,3),dtype=np.int32)
CV = np.zeros((nverts,3),dtype=np.int32)
CE = np.zeros((nedges,2),dtype=np.int32)
VE = np.zeros((nedges,2),dtype=np.int32)
nedgef = np.zeros(ncells,dtype=np.int32)
nedgev = np.zeros(nverts,dtype=np.int32)

i = 0
for row in NC_reader:
	NC[i,:] = np.array(row,dtype=np.int32)
	i = i + 1
i = 0
for row in VC_reader:
	VC[i,:] = np.array(row,dtype=np.int32)
	i = i + 1
i = 0
for row in EC_reader:
	EC[i,:] = np.array(row,dtype=np.int32)
	i = i + 1
i = 0
for row in EV_reader:
	EV[i,:] = np.array(row,dtype=np.int32)
	i = i + 1
i = 0
for row in CV_reader:
	CV[i,:] = np.array(row,dtype=np.int32)
	i = i + 1
i = 0
for row in CE_reader:
	CE[i,:] = np.array(row,dtype=np.int32)
	i = i + 1
i = 0
for row in VE_reader:
	VE[i,:] = np.array(row,dtype=np.int32)
	i = i + 1
	
###############GEOMETRY#################
ccoords_file = open('geodesic_data/' + meshname + '/point_' + modifier + suffix,'rb')
vcoords_file = open('geodesic_data/' + meshname + '/vertex_coords_' + suffix,'rb')

ccoords_reader = csv.reader(ccoords_file,delimiter=' ',skipinitialspace=True)
vcoords_reader = csv.reader(vcoords_file,delimiter=' ',skipinitialspace=True)

ccoords = np.zeros((ncells,3),dtype=np.float64)
vcoords = np.zeros((nverts,3),dtype=np.float64)
i = 0
for row in vcoords_reader:
	vcoords[i,:] = np.array(row,dtype=np.float64)
	i = i + 1
i = 0
for row in ccoords_reader:
	ccoords[i,:] = np.array(row,dtype=np.float64)
	i = i + 1

print 'creating',meshname,vlevel

#create cell lat/lon
clon = np.arctan2(ccoords[:,1],ccoords[:,0])
clat = np.arcsin(ccoords[:,2])

#create vertex lat/lon
vlon = np.arctan2(vcoords[:,1],vcoords[:,0])
vlat = np.arcsin(vcoords[:,2])

#get edge connectivity and endpoint coords
CEi = CE - 1
VEi = VE - 1
v0 = vcoords[VEi[:,0],:]
v1 = vcoords[VEi[:,1],:]
c0 = ccoords[CEi[:,0],:]
c1 = ccoords[CEi[:,1],:]

#create edge coords- uses Voronoi property that midpoint of de is where edge crossing is!
midpoint =  (c0 + c1)/2
midpoint = midpoint / np.expand_dims(np.linalg.norm(midpoint,axis=1),axis=1)
ecoords = midpoint

elon = np.arctan2(ecoords[:,1],ecoords[:,0])
elat = np.arcsin(ecoords[:,2])

#create le,de
de_vector = c0 - c1
ad = np.sqrt(np.sum(np.square(de_vector),axis=1))
de = 2.0 * np.arcsin(0.5 * ad)
le_vector = v0 - v1
ad = np.sqrt(np.sum(np.square(le_vector),axis=1))
le = 2.0 * np.arcsin(0.5 * ad)

#create Aie and Ae
Aie = np.zeros((nedges,2),dtype=np.float64,order='F')
d12 = 2.0*np.arcsin(0.5*np.sqrt(np.sum(np.square(c0-v0),axis=1)))
d13 = 2.0*np.arcsin(0.5*np.sqrt(np.sum(np.square(c0-v1),axis=1)))
d23 = 2.0*np.arcsin(0.5*np.sqrt(np.sum(np.square(v0-v1),axis=1)))
p = 0.5 * (d12 + d13 + d23)
T0 = np.tan(0.5 * (p-d12))
T1 = np.tan(0.5 * (p-d13))
T2 = np.tan(0.5 * (p-d23))
T3 = np.tan(0.5 * p)
Aie[:,0] = 4.0 * np.arctan(np.sqrt(T0 * T1 * T2 * T3))

d12 = 2.0*np.arcsin(0.5*np.sqrt(np.sum(np.square(c1-v0),axis=1)))
d13 = 2.0*np.arcsin(0.5*np.sqrt(np.sum(np.square(c1-v1),axis=1)))
d23 = 2.0*np.arcsin(0.5*np.sqrt(np.sum(np.square(v0-v1),axis=1)))
p = 0.5 * (d12 + d13 + d23)
T0 = np.tan(0.5 * (p-d12))
T1 = np.tan(0.5 * (p-d13))
T2 = np.tan(0.5 * (p-d23))
T3 = np.tan(0.5 * p)
Aie[:,1] = 4.0 * np.arctan(np.sqrt(T0 * T1 * T2 * T3))
Ae = Aie[:,0] + Aie[:,1]

#create ECP
ECP = np.concatenate((EC[CEi[:,0],:],EC[CEi[:,1],:]),axis=1)
ECP = np.apply_along_axis(np.unique,1,ECP)
ECP = np.roll(ECP,-1,axis=1) #moves 0s to end of arrays

#create ECPd
ECPd = np.concatenate((EV[VEi[:,0],:],EV[VEi[:,1],:]),axis=1)
ECPd = np.apply_along_axis(np.unique,1,ECPd)
necpd = np.sum(np.logical_not(np.equal(ECPd,0)),axis=1)

#set nedgef and nedgev and necp
nedgef = np.sum(np.logical_not(np.equal(EC,0)),axis=1)
nedgev = np.sum(np.logical_not(np.equal(EV,0)),axis=1)
necp = np.sum(np.logical_not(np.equal(ECP,0)),axis=1)

#create nei/tev
nei = np.zeros((nedges,2),dtype=np.int32)
tev = np.zeros((nedges,2),dtype=np.int32)
nei[:,0] = 1
nei[:,1] = -1
ne = np.expand_dims(nei[:,0],1)*c0 + np.expand_dims(nei[:,1],1)*c1
te1 = np.cross(ecoords,ne)
te2 = v1 - v0
tev[:,0] = 1 * np.sign(np.sum(np.multiply(te1,te2),axis=1))
tev[:,1] =  -1 *  np.sign(np.sum(np.multiply(te1,te2),axis=1))

nei_cell = geo_helpers.geometry.set_nei_cell(nedgef,EC,CE,nei)
tev_vertex = geo_helpers.geometry.set_tev_vertex(nedgev,EV,VE,tev)

#create Av
Av = geo_helpers.geometry.set_av(nedgev,EV,CE,ccoords,vcoords)

#create Ai
Ai = geo_helpers.geometry.set_ai(nedgef,EC,VE,ccoords,vcoords)

#create Aiv and Riv
Aiv = geo_helpers.geometry.set_aiv(nedgef,EC,VC,VE,ccoords,vcoords,ecoords)

#checking geometry
print 'Ai sum',np.sum(Ai) - 4.*pi 
print 'Av sum',np.sum(Av) - 4.*pi 
print 'Ae sum',np.sum(Ae) - 4.*pi 
print 'Aie sum',np.sum(Aie) - 4.*pi 
print 'Aiv sum',np.sum(Aiv) - 4.*pi 
test = np.sum(Aiv,axis=1) - Ai
print 'Aiv sum around cell i',np.amax(test),np.amin(test),np.average(test)
test = np.sum(Aie,axis=1) - Ae
print 'Aie sum around edge e',np.amax(test),np.amin(test),np.average(test)

#outputing grid
print 'outputing',meshname,vlevel
outfile = h5py.File('geodesic/' + meshname + '/' + str(ncells) + '.hdf5', 'w')
sizes = outfile.create_group('sizes')
coords = outfile.create_group('coords')
connect = outfile.create_group('connectivity')
geometry = outfile.create_group('geometry')

sizes.create_dataset('nface',data=ncells)
sizes.create_dataset('nedge',data=nedges)
sizes.create_dataset('nvert',data=nverts)

connect.create_dataset('NC', data=NC)
connect.create_dataset('EC', data=EC)
connect.create_dataset('VC', data=VC)
connect.create_dataset('nedgef', data=nedgef)
connect.create_dataset('CE', data=CE)
connect.create_dataset('VE', data=VE)
connect.create_dataset('CV', data=CV)
connect.create_dataset('EV', data=EV)
connect.create_dataset('nedgev', data=nedgev)
connect.create_dataset('ECP', data=ECP)
connect.create_dataset('ECPd', data=ECPd)
connect.create_dataset('nei_edge', data=nei)
connect.create_dataset('tev_edge', data=tev)
connect.create_dataset('nei_cell', data=nei_cell)
connect.create_dataset('tev_vertex', data=tev_vertex)

connect.create_dataset('necp', data=necp)
connect.create_dataset('necpd', data=necpd)

coords.create_dataset('clon', data=clon)
coords.create_dataset('clat', data=clat)
coords.create_dataset('vlon', data=vlon)
coords.create_dataset('vlat', data=vlat)
coords.create_dataset('elon', data=elon)
coords.create_dataset('elat', data=elat)
coords.create_dataset('ccoords', data=ccoords)
coords.create_dataset('vcoords', data=vcoords)
coords.create_dataset('ecoords', data=ecoords)

geometry.create_dataset('Ai', data=Ai)
geometry.create_dataset('le', data=le)
geometry.create_dataset('de', data=de)
geometry.create_dataset('Ae', data=Ae)
geometry.create_dataset('Av', data=Av)
geometry.create_dataset('Aie', data=Aie)
geometry.create_dataset('Aiv', data=Aiv)
	
outfile.close()


